#!/bin/bash
# Run Spelunk, rebuilding first if necessary (intended for development mode)
#
# Requires redo: https://github.com/apenwarr/redo
set -eu -o pipefail

cd -- "$(dirname -- "${BASH_SOURCE[0]}")"

MVN_ARGS=-DskipTests=true redo-ifchange spelunk.fat.jar
java -jar spelunk.fat.jar "$@"
