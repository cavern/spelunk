# Spelunk: Ideas

Brainstorming, notes, and partial decisions.

## Rewrite in Compose

JavaFX is pretty unpleasant to work with. How about Compose?

- https://github.com/JetBrains/compose-multiplatform
- https://github.com/arkivanov/Decompose
- https://github.com/arkivanov/MVIKotlin

Also look into using
https://github.com/Syer10/Kotlin-Multiplatform-AppDirs for some of the
OS-dependent things.

## Config versioning and migrations

Apps must be able to deal with config files and file
layouts that are produced by other versions of the software. Common
scenarios:

- A normal upgrade, and the new version allows new keys in the config
  maps and places new files in the repository
- A downgrade, when the user was dissatisfied with the upgrade
- Restoring a repo from backup or moving it to a new computer, in
  which case the data may have been produced by either a newer or
  older version of the software that is installed

Upgrades are more or less a solved problem; every version understands
how to read the files of all versions before it, and can make any
necessary adjustments to the files, migrating them to new formats. The
problem, of course, is the *downgrades*.

Problems that arise:

- Old version can't understand a config or data file at all
- Old version thinks it understands a file, but misinterprets it,
  because keys are missing or have changed semantics
- Old version can read the files, but in making changes to them, wipes
  out config keys that it doesn't understand. (Read file into model
  object in memory, ignoring some keys; modify model; write back to
  disk. Ignored keys are now missing.)
- Old version can correctly write a file back, but fails to make a
  coordinated update to another file that it was supposed to keep in
  sync

Old versions, in the *general case*, cannot cope with config and data
files produced by new versions. A common approach is a version number
in the root config file, serving as a warning to old versions and
telling newer versions if they need to migrate the data.

### Proposed approach (config semver)

- Root config file in repo has a version with a major and minor
  version number.
- This is approximately a "semver" (semantic version)
    - Major version changes are *breaking*. They cannot
      necessarily be reversed, even by software that understands
      the new version, and it may be dangerous for an old version
      of the software to attempt to use a newer version's configs.
    - Minor version changes are *additive*. A minor version
      mismatch only adds new keys to maps, or adds additional
      files, and it is safe for an older version of the software
      to read a config file created by a newer minor version of
      the software with the same major version. E.g. v5.1 can read
      files managed by v5.3 without causing a dangerous situation.
- If the major version is higher than what the software
  understands, exit with a failure. E.g. v5.1 software can use
  configs for v1 through v5, but should abort if presented with a
  v6+ config.
- If only the *minor* version is too high, enter a read-only mode
  unless the user forces an override. This ensures that keys that
  are added in the new minor version are not lost when the older
  software writes to config files.
    - Alternatively, if the software is capable of merging changes
      back without losing unknown keys, it may enter in read-write
      mode.
- Optionally, newer versions may offer a downgrade option, or older
  versions may be rereleased with the ability to downgrade from future
  versions, when possible.

## Emoji packs

For people using a markup language supporting emoji as inline icons,
it's easy enough to have an attachment for every emoji. Many posts
would reference the same attachments.

Some considerations for app-authoring:

- For ease of use, these should be managed as emoji packs which can be
  imported and exported, each emoji already preconfigured with
  description text (for accessibility) and perhaps a shortcode.
  Users would be able to add their own individually.
- If an icon is *replaced*, all existing posts should continue
  referencing the older version, unless the user opts to have those
  inclusions retroactively replaced.

## Full text search

...of every post you've ever *seen*, even when browsing a journal you
don't subscribe to. Because it sucks to think "where did I just see
that" and not be able to find it.

Maybe have it be time-limited -- only things you've seen in the past
two weeks.
