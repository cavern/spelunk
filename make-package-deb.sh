# Build Debian package

# Fresh build, no concern of cached compilation etc.
mvn clean
redo spelunk.fat.jar

version=$(
    unzip -p spelunk.fat.jar META-INF/maven/org.timmc/spelunk/pom.properties \
        | grep '^version=' | sed 's/.*=//' | tr -d '\r'
)
main_class=$(
    unzip -p spelunk.fat.jar META-INF/MANIFEST.MF \
        | grep -P '^Main-Class: ' | cut -d' ' -f2 | tr -d '\r'
)

build_dir=target/package-root/

mkdir -p "$build_dir"
rm -f "$build_dir"/spelunk*.deb
cp spelunk.fat.jar "$build_dir"/spelunk.jar

# Options: https://docs.oracle.com/en/java/javase/14/docs/specs/man/jpackage.html
# Menu groups: https://specifications.freedesktop.org/menu-spec/menu-spec-1.0.html#category-registry
# App category: https://www.debian.org/doc/debian-policy/ch-archive.html#s-subsections
jpackage --type deb --name "spelunk" --app-version "$version" \
         --description "Cavern app for social journaling" \
         --input "$build_dir" --main-jar spelunk.jar --main-class "$main_class" \
         --icon src/main/resources/org/timmc/spelunk/gui/app-icon-256.png \
         --vendor 'Tim McCormack' \
         --linux-package-name spelunk \
         --linux-deb-maintainer "cortex"'@brainonfire.net' \
         --linux-menu-group 'Network' --linux-app-category 'net'
