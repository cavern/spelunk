# Spelunk

Spelunk is a testbed app for the Cavern underground social network.

Cavern is still being defined, and Spelunk is as well. This is **alpha quality**
software; it is incomplete and data formats are in flux. There is a high chance
of metadata corruption during upgrades.

Find out more about Cavern: https://www.brainonfire.net/cavern/

## Building and running

You must have Java 11 or higher installed to build and run
Spelunk. Tested with OpenJDK 11.

If you have `redo` installed, you can run `run.sh`, and it will run
the jar file, building first if needed. (It will also automatically
rebuild if anything in the source tree has changed.)

If you do not have `redo` installed, you can use `mvn package` and
then `java -jar target/spelunk-*-jar-with-dependencies.jar`.

A Debian package can be built by bumping `version` in the pom.xml and
using `./make-package-deb.sh`. Use semver with respect to file and DB
format changes, following the changes to `State.latestVersion`.

## Filesystem layout

Layout for your Spelunk journal directory.

- `settings.sqlite3`: Configuration for Spelunk, including passwords
  and keys. Keep this file secure, and don't share it! Especially
  don't share the file `private-key-pass`, which helps secure the
  configuration file.
- `journal/`: Your posts
    - (Numbered directories)
        - `post.json`: Body and metadata of post, including manifest
        - `attach/`: Flat directory containing arbitrary files
          (all should be listed in manifest, otherwise no guarantee
          what will happen to them)
        - `DELETED`: Empty file that marks a deleted post and keeps
          the post number from being reused. When this is present, the
          directory should be otherwise empty.
- `cache/`: Various files that are downloaded from elsewhere
  (contacts' journals), or generated from local files (encrypted file
  working directory)

## Caveats

- This is a proof of concept, not even "alpha" quality yet
    - Currently the simplest possible implementation
- I have skipped some error checking in places
- File formats and directory layouts subject to change

## License

To be determined; possibly AGPL v3 or other strong copyleft.
