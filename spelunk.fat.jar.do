# Build uberjar

redo-ifchange pom.xml .src-stamp

mvn $MVN_ARGS package >&2
cp target/spelunk-*-jar-with-dependencies.jar $3
