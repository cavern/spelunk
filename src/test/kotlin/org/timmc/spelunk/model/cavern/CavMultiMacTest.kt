package org.timmc.spelunk.model.cavern

import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.SodiumBoxKeyPair
import org.timmc.spelunk.SodiumBoxPrivateKey
import org.timmc.spelunk.SodiumBoxPublicKey
import org.timmc.spelunk.TEST_ALICE_BOX
import org.timmc.spelunk.TEST_BOB_BOX
import org.timmc.spelunk.Utils
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotEquals

class CavMultiMacTest {
    @Test
    fun `shared secret equality`() {
        val topic = "unit-test"
        val senderKeys = Crypto.createBoxKeys()
        val readerKeys = Crypto.createBoxKeys()

        val senderSecret = CavMultiMac.sharedSecretForSender(
            senderKeys, readerKeys.publicKey, topic,
        )
        val readerSecret = CavMultiMac.sharedSecretForReader(
            senderKeys.publicKey, readerKeys, topic,
        )
        assertEquals(readerSecret, senderSecret)

        val readerTopicDifferent = CavMultiMac.sharedSecretForReader(
            senderKeys.publicKey, readerKeys, "different-topic",
        )
        assertNotEquals(readerSecret, readerTopicDifferent)

        val readerKeysDifferent = CavMultiMac.sharedSecretForReader(
            senderKeys.publicKey, Crypto.createBoxKeys(), topic,
        )
        assertNotEquals(readerSecret, readerKeysDifferent)
    }

    @Test
    fun `shared secret - can send to self`() {
        val topic = "unit-test"
        val senderKeys = Crypto.createBoxKeys()

        val asSender = CavMultiMac.sharedSecretForSender(
            senderKeys, senderKeys.publicKey, topic,
        )
        val asReader = CavMultiMac.sharedSecretForReader(
            senderKeys.publicKey, senderKeys, topic,
        )
        assertEquals(asSender, asReader)
    }

    @Test
    fun `mac values - pinning test`() {
        val mac = CavMultiMac.computeMac(
            PayloadDigest.of("this is just a test".toByteArray()),
            IntegritySecret(Utils.hexDecode("d3c4aaabf7f89f15dad12701bb8b05d7")),
        )
        assertEquals("324D957EC99E3B889F28C32667681CD4", Utils.hexEncode(mac.sharedBytes))
    }

    @Test
    fun `mac-finding`() {
        val macs = List(5) { i -> ByteArray(16) { i.toByte() }}
            .reduce(ByteArray::plus)
        val all = CavMultiMac(List::class.java, "stuff".toByteArray(), macs)

        // We only search at 16 byte boundaries
        assertTrue(all.macPresent(CavMac(macs.sliceArray(16 until 32))))
        assertFalse(all.macPresent(CavMac(macs.sliceArray(15 until 31))))

        // macs must be multiple of MAC length
        val unevenShort = CavMultiMac(
            List::class.java, "stuff".toByteArray(), macs.sliceArray(0 until 39)
        )
        val eShort = assertFailsWith<EAuthenticatedDataMacsUneven> {
            unevenShort.macPresent(CavMac(ByteArray(16)))
        }
        assertEquals(39, eShort.allMacsSize)

        val unevenLong = CavMultiMac(
            List::class.java, "stuff".toByteArray(), macs.sliceArray(0 until 41)
        )
        val eLong = assertFailsWith<EAuthenticatedDataMacsUneven> {
            unevenLong.macPresent(CavMac(ByteArray(16)))
        }
        assertEquals(41, eLong.allMacsSize)
    }

    @Test
    fun `mac-finding - zero length tolerated`() {
        val emptyMacs = CavMultiMac(
            List::class.java, "stuff".toByteArray(), ByteArray(0),
        )
        // No error, and correct answer, despite weird input
        assertEquals(false, emptyMacs.macPresent(CavMac(ByteArray(16))))
    }

    @Test
    fun `authentication and key order`() {
        val topic = "unit-test"
        val senderKeys = TEST_ALICE_BOX

        // We set up an artificial scenario where the sender is using multiple
        // valid keys for the reader. This allows us to probe the order in which
        // reader keys are used, and to verify that an early key that lacks
        // MAC matches does not cause an early exit.
        val readerKey1 = SodiumBoxKeyPair(
            SodiumBoxPublicKey(Utils.hexDecode("5796BAA65BD713A6BE766019363C63C7300110E86E1AC7DC9917C396FBB0985F")),
            SodiumBoxPrivateKey(Utils.hexDecode("A9818A837C369A0C8E21F5185720E729DBD28C0DA7A54BC70CA9F68D3BD997D9")),
        )
        val readerKey2 = SodiumBoxKeyPair(
            SodiumBoxPublicKey(Utils.hexDecode("EBABD5627B2F028DEA1D62BE63F3F4EEB63669061B4B626FAD5F36CA4F37C448")),
            SodiumBoxPrivateKey(Utils.hexDecode("B0E4DEA5412D364A98A01D62F16F0C57DB6C80A9060C16D0A819434A44B7885D")),
        )
        val readerKey3 = SodiumBoxKeyPair(
            SodiumBoxPublicKey(Utils.hexDecode("47DBFC67D3939B2845CA401A98EAA3B823072BFC4C4097545E963A14FAE2BA0D")),
            SodiumBoxPrivateKey(Utils.hexDecode("81A13169DFB8A891A0CB4F8188E70C699FBB255894E95D517AD87C48AE93C425")),
        )
        val readerKeys = listOf(readerKey1, readerKey2, readerKey3)

        // Some other key
        val otherKey = TEST_BOB_BOX

        // Packs using given recipient keys, unpacks using reader's keys.
        fun roundTripForRecipients(vararg recipientKeys: SodiumBoxKeyPair): SodiumBoxPublicKey {
            val multimac = CavMultiMac.makeAuthenticatedData(
                String::class.java, "hello".toByteArray(), senderKeys,
                recipientKeys.map { it.publicKey }, topic,
            )
            return multimac.authenticate(
                senderKeys.publicKey, readerKeys, topic, ::String,
            ).second
        }

        // Usual cases
        assertEquals(readerKey1.publicKey, roundTripForRecipients(readerKey1))
        assertEquals(readerKey3.publicKey, roundTripForRecipients(readerKey3))

        // First matching key is used
        assertEquals(readerKey1.publicKey, roundTripForRecipients(readerKey1, otherKey, readerKey3))

        // No failure when one of the intervening keys produces no matches but a
        // later one does.
        assertEquals(readerKey2.publicKey, roundTripForRecipients(otherKey, readerKey2))

        // Only have some other, unrelated key
        assertFailsWith<EAuthenticatedDataNoMatchingMacs> {
            roundTripForRecipients(otherKey)
        }
    }
}
