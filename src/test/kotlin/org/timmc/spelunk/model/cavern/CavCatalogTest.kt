package org.timmc.spelunk.model.cavern

import org.junit.Test
import org.timmc.spelunk.Constants
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.SodiumBoxKeyPair
import org.timmc.spelunk.SodiumBoxPublicKey
import org.timmc.spelunk.Utils
import org.timmc.spelunk.TEST_ALICE_BOX
import org.timmc.spelunk.TEST_BOB_BOX
import org.timmc.spelunk.model.JournalPost
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.time.measureTimedValue

class CavCatalogTest {
    private fun createCatalog(postNum: Int): CavCatalog {
        val key = Crypto.sharedSecret()
        val hash = CiphertextDigest(Utils.hexDecode(
            "01234567890000000000000000000000"
        ))
        return CavCatalog(
            publishTimestamp = 1694145113,
            posts = (0 until postNum).map { id ->
                CavCatalog.PostOffer(
                    id = JournalPost.encryptPostId(id.toLong(), key, 0),
                    pointer = CavDataPointer(
                        hiddenId = Crypto.hiddenId(),
                        symmetricKey = Crypto.symmetricStreamKey(),
                        cipherHash = hash,
                    )
                )
            },
        )
    }

    @Test
    fun `catalog msgpack round trip`() {
        val orig = createCatalog(3)
        assertEquals(orig, CavCatalog.fromMsgpack(orig.toMsgPack()))
    }

    @Test
    fun `post offer marginal size msgpack`() {
        fun sizeOf(postNum: Int): Int {
            return createCatalog(postNum).toMsgPack().size
        }
        assertEquals(94, sizeOf(101) - sizeOf(100))
    }

    @Test
    fun `shared secret (pinning test -- algorithm and constant)`() {
        val expectedSecret = "76C71F369E8CCCF7EE361E72237B733E"
        val computedSecret = CavMultiMac.sharedSecretForSender(
            TEST_ALICE_BOX, TEST_BOB_BOX.publicKey, "cavern-catalog-integrity",
        )
        assertEquals(expectedSecret, computedSecret.asHex())
    }

    @Test
    fun `catalog wrapper - round trip`() {
        val catalog = createCatalog(3)
        // Wrap using Alice's private key
        val wrapperBytes = CavCatalogWrapper.wrapCatalog(
            catalog, TEST_ALICE_BOX, listOf(TEST_BOB_BOX.publicKey),
        ).toMsgPack()
        val decodedWrapper = CavCatalogWrapper.fromMsgpack(wrapperBytes)
        assertEquals(Constants.CAVERN_SPEC, decodedWrapper.spec)
        // Unwrap using Bob's private key
        val unpackedCatalog = decodedWrapper.unwrapCatalog(
            TEST_ALICE_BOX.publicKey, listOf(TEST_BOB_BOX),
        )
        assertEquals(catalog, unpackedCatalog)
    }

    private fun wrapperSize(
        catalog: CavCatalog, senderKeyPair: SodiumBoxKeyPair,
        readerKeys: List<SodiumBoxPublicKey>,
    ): Int {
        return CavCatalogWrapper.wrapCatalog(catalog, senderKeyPair, readerKeys)
            .toMsgPack().size
    }

    @Test
    fun `catalog wrapper - recipient marginal size`() {
        val sender = TEST_ALICE_BOX
        val readers = List(1000) { Crypto.createBoxKeys().publicKey }
        val readersMore = readers.plus(Crypto.createBoxKeys().publicKey)

        val catalog = createCatalog(3)
        val catalogLarger = createCatalog(30)

        val baseSize = wrapperSize(catalog, sender, readers)
        val moreReadersSize = wrapperSize(catalog, sender, readersMore)
        // Each new recipient adds 16 MAC bytes
        assertEquals(16, moreReadersSize - baseSize)

        val largerCatalogSize = wrapperSize(catalogLarger, sender, readers)
        val largerCatalogMoreReadersSize = wrapperSize(catalogLarger, sender, readersMore)
        // This doesn't change with larger catalog files
        assertEquals(16, largerCatalogMoreReadersSize - largerCatalogSize)
    }

    @Test
    fun `marginal speed test - pack and unpack`() {
        val recipCount = 10_000
        val keyGeneration = measureTimedValue {
            List(recipCount) { Crypto.createBoxKeys() }
        }
        val recipKeys = keyGeneration.value
        // Sample cold measurement: ~2 s total
        println("Generated keys in ${keyGeneration.duration}")
        val recipPublicKeys = recipKeys.map { it.publicKey }
        val catalog = createCatalog(10_000) // size doesn't really affect speed

        val wrapped = measureTimedValue {
            CavCatalogWrapper.wrapCatalog(catalog, TEST_ALICE_BOX, recipPublicKeys)
        }

        val wrapMicrosPerRecip = wrapped.duration.inWholeMicroseconds * 1.0 / recipCount
        // Sample cold measurement: ~160 µs/recipient and ~1.6 s total
        // Time is dominated by X25519 point multiplication, about 100 µs per call.
        println("Wrapped at $wrapMicrosPerRecip µs/recipient (${wrapped.duration} total)")
        assertTrue(wrapMicrosPerRecip < 1000)

        val verified = measureTimedValue {
            wrapped.value.unwrapCatalog(TEST_ALICE_BOX.publicKey, recipKeys.takeLast(1))
        }

        val verifiedMicrosPerRecip = verified.duration.inWholeMicroseconds * 1.0 / recipCount
        // Sample cold measurement: ~25 µs/recipient and ~250 ms total
        println("Verified at $verifiedMicrosPerRecip µs/recipient (${verified.duration} total)")
        assertTrue(verifiedMicrosPerRecip < 100)
    }
}
