package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.Crypto
import org.timmc.spelunk.EVerifySignature
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class DataSigningTest {
    // Test fixtures
    val sigKeysStd = Crypto.createSigningKeys()
    val dataStd = CavIdCurrent(
        boxKey = Crypto.createBoxKeys().publicKey,
        mainUrl = "https://example.com/me",
    )
    val topicStd = "unit-test"
    val signedStd = signData(dataStd, topicStd, sigKeysStd.privateKey)

    /** Check signature with correct keys and topic */
    @Test
    fun happyPath() {
        val payload = signedStd.verify(
            topicStd, sigKeysStd.publicKey,
            CavIdCurrent.Companion::fromMsgpack
        )
        assertEquals(dataStd, payload)
    }

    @Test
    fun wrongTopic() {
        assertFailsWith<EVerifySignature> {
            signedStd.verify(
                "wrong-topic", sigKeysStd.publicKey,
                CavIdCurrent.Companion::fromMsgpack
            )
        }
    }

    @Test
    fun wrongKey() {
        val differentVerificationKey = Crypto.createSigningKeys().publicKey
        assertFailsWith<EVerifySignature> {
            signedStd.verify(
                topicStd, differentVerificationKey,
                CavIdCurrent.Companion::fromMsgpack
            )
        }
    }
}
