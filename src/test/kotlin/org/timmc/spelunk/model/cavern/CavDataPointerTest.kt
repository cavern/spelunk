package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.Utils
import kotlin.test.Test
import kotlin.test.assertEquals

class CiphertextDigestTest {
    private val testHashHex = "7F5979FB78F082E8B1C676635DB8795C"
    private val testHashBytes = Utils.hexDecode(testHashHex)

    @Test
    fun json() {
        val json = Utils.json.adapter(CiphertextDigest::class.java)
        assertEquals("\"$testHashHex\"", json.toJson(CiphertextDigest(testHashBytes)))
        assertEquals(CiphertextDigest(testHashBytes), json.fromJson("\"$testHashHex\""))
    }

    @Test
    fun `readable string format`() {
        assertEquals(
            "CiphertextDigest{hex=7F5979FB78F082E8B1C676635DB8795C}",
            "${CiphertextDigest(testHashBytes)}"
        )
    }
}
