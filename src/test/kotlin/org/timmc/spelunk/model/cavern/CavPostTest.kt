package org.timmc.spelunk.model.cavern

import org.junit.Test
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.SodiumSecretStreamKey
import org.timmc.spelunk.Utils
import kotlin.test.assertEquals

class CavPostTest {
    fun createCavContent(withNulls: Boolean): CavAttachment {
        val hash = CiphertextDigest(Utils.hexDecode(
            "01234567890000000000000000000000"
        ))
        val ret = CavAttachment(
            clearFilename = "cat.jpg",
            allegedSize = 196359,
            allegedMimeType = "image/jpeg",
            pointer = CavDataPointer(
                hiddenId = Crypto.hiddenId(),
                symmetricKey = Crypto.symmetricStreamKey(),
                cipherHash = hash,
            )
        )
        return if (withNulls) {
            ret.copy(allegedMimeType = null)
        } else {
            ret
        }
    }

    fun createCavPost(): CavPost {
        return CavPost(
            id = "123",
            type = "journal",
            subject = "Hello, world!",
            body = "Test post, please ignore",
            bodyFormat = "text/plain",
            tags = listOf("test", "diary"),
            publishDate = System.currentTimeMillis() / 1000,
            updatedDate = System.currentTimeMillis() / 1000 + 300,
            visibilityType = CavVisibility.PUBLIC,
            attachments = listOf(
                createCavContent(true),
                createCavContent(false)
            )
        )
    }

    @Test
    fun messagePackRoundTrip() {
        val orig = createCavPost()
        assertEquals(orig, CavPost.fromMsgpack(orig.toMsgPack()))
    }

    @Test
    fun `existing messagepack can still be decoded`() {
        val pinnedExample =
            "8aa26964a3313233a474797065a76a6f75726e616ca77375626a656374ad" +
            "48656c6c6f2c20776f726c6421a4626f6479b85465737420706f73742c20" +
            "706c656173652069676e6f7265aa626f6479466f726d6174aa746578742f" +
            "706c61696ea47461677392a474657374a56469617279ab7075626c697368" +
            "44617465ce64dc1c68ab7570646174656444617465ce64dc1c70ae766973" +
            "6962696c69747954797065a67075626c6963ab6174746163686d656e7473" +
            "9185ad636c65617246696c656e616d65a76361742e6a7067ab64697370" +
            "6f736974696f6ea56f74686572ab616c6c6567656453697a65ce0002ff07" +
            "af616c6c656765644d696d6554797065aa696d6167652f6a706567a7706f" +
            "696e74657283a3686964ad61626330313233343031323334a36b6579c420" +
            "abcdef0000000000011111111112222222222333333333344444444444fe" +
            "dcbaa3696e74c41001234567890000000000009876543210"

        val expectedPost = CavPost(
            id = "123",
            type = "journal",
            subject = "Hello, world!",
            body = "Test post, please ignore",
            bodyFormat = "text/plain",
            tags = listOf("test", "diary"),
            publishDate = 1692146792,
            updatedDate = 1692146800,
            visibilityType = CavVisibility.PUBLIC,
            attachments = listOf(CavAttachment(
                clearFilename = "cat.jpg",
                allegedSize = 196359,
                allegedMimeType = "image/jpeg",
                pointer = CavDataPointer(
                    hiddenId = "abc0123401234",
                    symmetricKey = SodiumSecretStreamKey(Utils.hexDecode(
                        "ABCDEF0000000000011111111112222222222333333333344444444444FEDCBA"
                    )),
                    cipherHash = CiphertextDigest(Utils.hexDecode(
                        "01234567890000000000009876543210"
                    )),
                )
            )),
        )

        val actualDecoded = CavPost.fromMsgpack(Utils.hexDecode(pinnedExample))
        assertEquals(expectedPost, actualDecoded)
    }
}
