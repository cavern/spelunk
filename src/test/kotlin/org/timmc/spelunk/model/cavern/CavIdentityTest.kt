package org.timmc.spelunk.model.cavern

import org.junit.Test
import org.timmc.spelunk.Constants
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.EVerifySignature
import org.timmc.spelunk.Publishing
import org.timmc.spelunk.Utils
import org.timmc.spelunk.model.CatalogFinder
import org.timmc.spelunk.model.Keypair
import org.timmc.spelunk.model.Keyring
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class CavIdentityTest {
    fun createIdentity(postNum: Int, withNulls: Boolean): CavIdentity {
        val senderSigKeys = Crypto.createSigningKeys()
        val senderBoxKeys = Crypto.createBoxKeys()

        return CavIdentity(
            spec = Constants.CAVERN_SPEC,
            sigKey = senderSigKeys.publicKey,
            idSigned = signData(
                CavIdCurrent(
                    boxKey = senderBoxKeys.publicKey,
                    mainUrl = if (withNulls) null else "https://example.com",
                ),
                topic = CavIdCurrent.TOPIC_OWN_CURRENT_IDENTITY,
                signingKey = senderSigKeys.privateKey,
            ),
            name = if (withNulls) null else "Test Person",
            catalogFinder = CatalogFinder.encrypt(
                recipients = (0 until postNum).map {
                    Publishing.Recipient(
                        catalogPointer = CavCatalogPointer.new(),
                        boxPublicKey = Crypto.createBoxKeys().publicKey,
                    )
                },
                myKeys = Keyring(
                    current = Keypair(
                        selfCatalogPointer = CavCatalogPointer.new(),
                        description = "",
                        createdTsMs = 0,
                        isActive = true,
                        sigKeys = senderSigKeys,
                        boxKeys = senderBoxKeys,
                    ),
                    inactiveKeys = emptyList()
                )
            ),
        )
    }

    @Test
    fun messagePackRoundTrip() {
        val full = createIdentity(3, withNulls = false)
        assertEquals(full, CavIdentity.fromMsgpack(full.toMsgPack()))

        val slim = createIdentity(3, withNulls = true)
        assertEquals(slim, CavIdentity.fromMsgpack(slim.toMsgPack()))
    }

    @Test
    fun catalogFinderMarginalSizeMsgPack() {
        fun sizeOf(postNum: Int): Int {
            return createIdentity(postNum, true).toMsgPack().size
        }
        assertEquals(99, sizeOf(101) - sizeOf(100))
    }

    /** Pinned example file used for testing backwards compatibility. */
    val exampleFile = "84a473706563a963617665726e302e33a77369675f6b6579c4202d" +
            "687577617ebdf7f0d290963fd32585ad93540e759213b0d41fc8c42e3f89d8a969" +
            "645f7369676e656482a3736967c440a6ec7c5185b40dca421c3d59b4ca0464d780" +
            "5902886c628e48a899b1abf191c244717bc8f64e82aafa98aab027ae1e8d22553c" +
            "6d75d2ed08a0c1e076bd68bd0da3706179c44882a7656e635f6b6579c420ea84" +
            "f3b2bdc61f547a47e5f57e37fd31ad4f4f6dbffce02dbfbef8a11ec64157a86d61" +
            "696e5f75726cb368747470733a2f2f6578616d706c652e636f6dae636174616c6f" +
            "675f66696e64657290"

    @Test
    fun `existing messagepack can still be decoded`() {
        val decoded = CavIdentity.fromMsgpack(Utils.hexDecode(exampleFile))

        assertEquals("cavern0.3", decoded.spec)
        assertEquals(
            "2D687577617EBDF7F0D290963FD32585AD93540E759213B0D41FC8C42E3F89D8",
            decoded.sigKey.asHex()
        )

        val curId = decoded.idSigned.verify(
            "cavern-own-current-identity", decoded.sigKey, CavIdCurrent::fromMsgpack
        )
        assertEquals(
            "EA84F3B2BDC61F547A47E5F57E37FD31AD4F4F6DBFFCE02DBFBEF8A11EC64157",
            curId.boxKey.asHex()
        )
        assertEquals("https://example.com", curId.mainUrl)

        // Confirm that topic is actually checked
        assertFailsWith<EVerifySignature> {
            decoded.idSigned.verify(
                "cavern-xxx", decoded.sigKey, CavIdCurrent::fromMsgpack
            )
        }
    }
}
