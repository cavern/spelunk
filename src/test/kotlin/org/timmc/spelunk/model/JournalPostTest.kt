package org.timmc.spelunk.model

import org.junit.Test
import org.timmc.spelunk.Utils
import kotlin.test.assertEquals

class JournalPostTest {
    @Test
    fun isPublishable() {
        val base = JournalPost.makeBlankPost(Visibility.QUAINTS)
        assertEquals(false, base.isPublishable())
        assertEquals(false, base.copy(status = PostStatus.DRAFT, persistentId = "").isPublishable())
        assertEquals(false, base.copy(status = PostStatus.DRAFT, persistentId = "s123").isPublishable())
        assertEquals(false, base.copy(status = PostStatus.PUBLISHED, persistentId = "").isPublishable())
        assertEquals(true, base.copy(status = PostStatus.PUBLISHED, persistentId = "s123").isPublishable())
    }

    @Test
    fun postIdEncryptionPinning() {
        val key = Utils.hexDecode("337a5888c985367e51c39feb027685d6")
        val version = 5
        val postId = 17356L

        val encrypted = JournalPost.encryptPostId(postId = postId, key = key, version = version)
        // Ensure that output value and format do not change inadvertently.
        // If they do change, add an Upgrader that calls postIdShuffleKeyRotate.
        assertEquals("5:BXW6", encrypted)
    }
}
