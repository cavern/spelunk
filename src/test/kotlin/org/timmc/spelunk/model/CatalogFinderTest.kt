package org.timmc.spelunk.model

import org.junit.Test
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.Publishing
import org.timmc.spelunk.model.cavern.CavCatalogPointer
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.time.measureTimedValue

class CatalogFinderTest {
    @Test
    fun performance() {
        val senderBoxKeys = Crypto.createBoxKeys()

        // Build a large catalog_finder and see how long it takes per-recipient

        val recipientCount = 50_000
        val recipientKeys = List(recipientCount) { Crypto.createBoxKeys() }
        val recipientPointers = List(recipientCount) { CavCatalogPointer.new() }

        val (catalogFinder, encryptTime) = measureTimedValue {
            CatalogFinder.encrypt(
                recipients = recipientKeys.zip(recipientPointers).map { (recipKeyPair, recipPointer) ->
                    Publishing.Recipient(
                        catalogPointer = recipPointer,
                        boxPublicKey = recipKeyPair.publicKey,
                    )
                },
                myKeys = Keyring(
                    current = Keypair(
                        selfCatalogPointer = CavCatalogPointer.new(),
                        description = "",
                        createdTsMs = 0,
                        isActive = true,
                        sigKeys = Crypto.createSigningKeys(),
                        boxKeys = senderBoxKeys,
                    ),
                    inactiveKeys = emptyList(),
                )
            )
        }

        val microsecondsPerEncryption = encryptTime.inWholeMicroseconds / recipientCount
        println("Encryption required $microsecondsPerEncryption µs/encryption")
        assertTrue(microsecondsPerEncryption < 2_000)

        // Then, try to decrypt the pointer for the last recipient

        val lastRecipKeyring = Keyring(
            current = Keypair(
                selfCatalogPointer = CavCatalogPointer.new(),
                description = "",
                createdTsMs = 0,
                isActive = true,
                sigKeys = Crypto.createSigningKeys(),
                boxKeys = recipientKeys.last(),
            ),
            inactiveKeys = emptyList(),
        )
        val (pointer, decryptTime) = measureTimedValue {
            CatalogFinder.decrypt(catalogFinder, senderBoxKeys.publicKey, lastRecipKeyring)
        }

        assertEquals(recipientPointers.last(), pointer)

        val microsecondsPerDecryption = decryptTime.inWholeMicroseconds / recipientCount
        println("Decryption required $microsecondsPerDecryption µs/decryption")
        assertTrue(microsecondsPerDecryption < 2_000)
    }
}
