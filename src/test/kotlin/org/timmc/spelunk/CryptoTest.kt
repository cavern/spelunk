package org.timmc.spelunk

import com.goterl.lazysodium.interfaces.PwHash
import com.goterl.lazysodium.interfaces.SecretStream
import org.junit.Test
import org.timmc.spelunk.Utils.Companion.utf8
import org.timmc.spelunk.model.cavern.CiphertextDigest
import java.io.ByteArrayOutputStream
import java.io.FilterInputStream
import java.io.InputStream
import kotlin.experimental.xor
import kotlin.math.ceil
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class CryptoTest {
    @Test
    fun randomnessFormats() {
        assertTrue {
            // Reasonable range -- 2^50 to 2^100 entropy, roughly
            Crypto.hiddenId().matches(Regex("""^[a-z0-9]{10,20}$"""))
        }
    }

    /**
     * Explain choice of parameters, via tests.
     */
    @Test
    fun sharedSecrets() {
        // The highest reasonable entropy, and no higher.
        //
        // 128 bits is a common enough effective entropy for cryptographic
        // keys in symmetric systems, so there's no reason to go above that
        // with key-encrypting keys.
        assertEquals(128 / 8, Crypto.sharedSecret().size)
    }

    @Test
    fun `asymmetric round-trip`() {
        val senderKey = Crypto.createBoxKeys()
        val recipKey = Crypto.createBoxKeys()
        val message = "Hello, world!"

        val ciphered = Crypto.asymmetricEncrypt(message.toByteArray(utf8),
            senderPrivKey = senderKey.privateKey, recipPubKey = recipKey.publicKey)

        fun decryptWith(recipKeys: List<SodiumBoxPrivateKey>): String {
            val d = Crypto.asymmetricDecrypt(ciphered, recipKeys, senderKey.publicKey)
            return d.toString(utf8)
        }

        assertEquals(message, decryptWith(listOf(recipKey.privateKey)))

        // Check key fallback and failure

        val otherKey = Crypto.createBoxKeys()

        // Failure when only key is incorrect
        assertFailsWith<EAsymmetricDecryptNoMatchingKey> {
            decryptWith(listOf(otherKey.privateKey))
        }
        // Failure when no keys given
        assertFailsWith<EAsymmetricDecryptNoKeysProvided> {
            decryptWith(emptyList())
        }
        // Success when correct and incorrect keys given in either order
        assertEquals(message, decryptWith(listOf(recipKey.privateKey, otherKey.privateKey)))
        assertEquals(message, decryptWith(listOf(otherKey.privateKey, recipKey.privateKey)))
    }

    @Test
    fun `key agreement`() {
        val ab = Crypto.x25519SharedSecret(TEST_ALICE_BOX.privateKey, TEST_BOB_BOX.publicKey)
        val ba = Crypto.x25519SharedSecret(TEST_BOB_BOX.privateKey, TEST_ALICE_BOX.publicKey)
        // Same as each other
        assertEquals(Utils.hexEncode(ab), Utils.hexEncode(ba))
        // Matches known test vector
        assertEquals(Utils.hexEncode(TEST_ALICE_BOB_SHARED_SECRET), Utils.hexEncode(ba))
    }

    @Test
    fun `symmetric stream round-trip`() {
        // Makes something that will take at least 2 chunks
        val messageBase = "all work and no play makes Jack a dull boy "
        val repeats = (Crypto.STREAM_CHUNK_BYTES * 1.5) / messageBase.length
        val message = messageBase.repeat(repeats.roundToInt())

        val ciphered = ByteArrayOutputStream()
        val key = Crypto.symmetricStreamKey()
        val integrity = ciphered.use {
            Crypto.symmetricEncryptStream(message.byteInputStream(utf8), it, key)
        }
        val decoded = ByteArrayOutputStream()
        decoded.use {
            Crypto.symmetricDecryptStream(ciphered.toByteArray().inputStream(), it, key, integrity)
        }

        assertEquals(message, decoded.toByteArray().toString(utf8))

        // Also confirm the non-streaming encryption is compatible
        assertEquals(message, Crypto.symmetricDecrypt(ciphered.toByteArray(), key, integrity).toString(utf8))
    }

    @Test
    fun `symmetric decryption errors`() {
        // Make a stream where the content is exactly a multiple of chunk size
        // so that we can play with adding and removing chunks from the end
        // without causing chunk decryption failures.
        val messageBase = "all work and no play makes Jack a dull boy "
        val chunkCount = 3
        val messageRepeats = ceil(
            (Crypto.STREAM_CHUNK_BYTES * chunkCount * 1.0) / messageBase.length
        ).roundToInt()
        val message = messageBase.repeat(messageRepeats)
            .slice(0 until (Crypto.STREAM_CHUNK_BYTES * chunkCount))
        val key = Crypto.symmetricStreamKey()

        // Normal case, for comparison. Forgoing integrity checks for simplicity.
        val (cipheredNormal, _) = Crypto.symmetricEncrypt(message.toByteArray(utf8), key)
        assertEquals(message, Crypto.symmetricDecrypt(cipheredNormal, key, null).decodeToString())

        // Not even a real header, just two null bytes.
        assertFailsWith<ESymmetricDecryptHeaderShort> {
            Crypto.symmetricDecrypt(ByteArray(2), key, null)
        }

        // Add stuff just after a chunk boundary
        val cipheredOverlong = cipheredNormal + "more stuff".encodeToByteArray()
        assertFailsWith<ESymmetricDecryptStreamExcess> {
            Crypto.symmetricDecrypt(cipheredOverlong, key, null)
        }

        // Remove final (and final-tagged) chunk
        val chunkOverhead = (Crypto.STREAM_CHUNK_BYTES + SecretStream.ABYTES)
        val cipheredTruncated = cipheredNormal.sliceArray(
            0 until (cipheredNormal.size - chunkOverhead)
        )
        assertFailsWith<ESymmetricDecryptStreamShort> {
            Crypto.symmetricDecrypt(cipheredTruncated, key, null)
        }
    }

    @Test
    fun `symmetric non-streaming round trip, and hash checking`() {
        val message = "Do you see yonder cloud that’s almost in the shape of a camel?"
        val key = Crypto.symmetricStreamKey()

        val (ciphered, cipherHash) = Crypto.symmetricEncrypt(message.encodeToByteArray(), key)
        val randomHash = CiphertextDigest(Utils.hexDecode(
            "45095da39766d6a09b703d9989d97999"
        ))

        assertEquals(
            message,
            Crypto.symmetricDecrypt(ciphered, key, cipherHash).decodeToString()
        )
        // Check that the hash is verified...
        assertFailsWith<ECiphertextIntegrity> {
            Crypto.symmetricDecrypt(ciphered, key, randomHash)
        }
        // ...but not required
        assertEquals(
            message,
            Crypto.symmetricDecrypt(ciphered, key, null).decodeToString()
        )

        // Also confirm the streaming encryption is compatible
        run {
            val decrypted = ByteArrayOutputStream()
            Crypto.symmetricDecryptStream(ciphered.inputStream(), decrypted, key, cipherHash)
            assertEquals(message, decrypted.toByteArray().decodeToString())
        }
        // And again that the hash is verified...
        assertFailsWith<ECiphertextIntegrity> {
            Crypto.symmetricDecryptStream(ciphered.inputStream(), ByteArrayOutputStream(), key, randomHash)
        }
        // ...but not required
        run {
            val decrypted = ByteArrayOutputStream()
            Crypto.symmetricDecryptStream(ciphered.inputStream(), decrypted, key, null)
            assertEquals(message, decrypted.toByteArray().decodeToString())
        }
    }

    /**
     * At least test that the settings are what was initially documented.
     */
    @Test
    fun pbkdfParams() {
        assertEquals(PwHash.ARGON2ID_OPSLIMIT_SENSITIVE, Crypto.MASTER_DERIVATION_OPS_LIMIT)
        assertEquals(PwHash.ARGON2ID_MEMLIMIT_MODERATE, Crypto.MASTER_DERIVATION_MEM_LIMIT.toInt())
    }

    /**
     * Test that we can decrypt anything we encrypt, with fresh random data.
     *
     * (And that decryption fails with the wrong password.)
     */
    @Test
    fun passwordEncryptionRoundTrip() {
        val masterPass = Crypto.sharedSecret()
        val data = "OVALTINE"

        val encrypted = Crypto.encryptWithPassword(plainData = data.toByteArray(), password = masterPass)
        val decrypted = Crypto.decryptWithPassword(cryptData = encrypted, password = masterPass)
        assertEquals(data, String(decrypted))

        assertFails { // bad password
            Crypto.decryptWithPassword(cryptData = encrypted, password = byteArrayOf(42, 17, 0, 1, 2, 3))
        }
    }

    /**
     * Pin password encryption to a specific input/output pair to avoid
     * regressions.
     */
    @Test
    fun passwordEncryptionHardcoded() {
        val masterPass = "8zu+g&I2mDQ~R_*-UOoK".toByteArray()
        val someSecret = "3A3940EB58B183AA7A1D033EF9332EA07AEAD08902212DFCBB457D73D2CE07B3"

        // Normally would be generated fresh during encryption, but pin here for
        // determinism.
        val argonSalt = Utils.hexDecode("94E9E0CD99957EE35EA2FB217AA6FF57")
        val boxNonce = Utils.hexDecode("F108F298EFA50A50CCB838DD81BF83679E404E069CA14E13")
        val encrypted = Crypto.encryptWithPassword(
            plainData = Utils.hexDecode(someSecret), password = masterPass,
            forceArgonSalt = argonSalt, forceBoxNonce = boxNonce)
        assertEquals(
            """
            94E9E0CD99957EE35EA2FB217AA6FF57F108F298EFA50A50CCB838DD81BF83679E4
            04E069CA14E13C0566E6E40F9868E50D299D837E854C70121C29A6F12AE5838FC0D
            B7337051DD9CD46E90DE3963A4F797FBF1A3A9850E925C6DFE006A8F992D9B02278
            C00F6F0F984548855FC5D9899F1BAC9B53BD55B
            """.replace(Regex("""\s+"""), ""),
            Utils.hexEncode(encrypted).uppercase())
        val decrypted = Crypto.decryptWithPassword(cryptData = encrypted, password = masterPass)
        assertEquals(someSecret, Utils.hexEncode(decrypted))
    }

    @Test
    fun `signing roundtrip`() {
        val sigKeys = Crypto.createSigningKeys()
        val message = "the eagle has landed"
        val messageBytes = message.encodeToByteArray()
        val signature = Crypto.signData(messageBytes, sigKeys.privateKey)

        assertFailsWith<EVerifySignature> {
            val otherSigKeys = Crypto.createSigningKeys()
            Crypto.verifySignature(signature, messageBytes, otherSigKeys.publicKey)
        }

        assertFailsWith<EVerifySignature> {
            val badMessage = ByteArray(messageBytes.size)
            Crypto.verifySignature(signature, badMessage, sigKeys.publicKey)
        }

        assertFailsWith<EVerifySignature> {
            // Mutate one byte in a valid signature
            val badSignature = signature.sharedBytes.clone()
            val someIndex = Random.nextInt(badSignature.size)
            badSignature[someIndex] = badSignature[someIndex] xor 1

            Crypto.verifySignature(Ed25519Signature(badSignature), messageBytes, sigKeys.publicKey)
        }

        Crypto.verifySignature(signature, messageBytes, sigKeys.publicKey)
    }

    @Test
    fun `mac round trip`() {
        val data = "Sharks are smooth in every direction.".toByteArray()
        val key = "-0ina4^Aj5W/hwU:".toByteArray()

        val output = Crypto.macCreate(data = data, key = key, outSize = 10)
        assertEquals(10, output.size)

        // Pinning test, to ensure that BLAKE2b continues to be used for MACs.
        val expectedHex = "41AE588B7E9DFAD22F38"
        assertEquals(expectedHex, Utils.hexEncode(output))

        assertTrue(Crypto.macVerify(expected = Utils.hexDecode(expectedHex), actual = output))

        assertFailsWith<IllegalArgumentException> {
            Crypto.macCreate(data, ByteArray(100), 10) // key too long
        }
        assertFailsWith<IllegalArgumentException> {
            Crypto.macCreate(data, ByteArray(10), 100) // output too long
        }
    }
}

class Blake2bHasherTest {
    @Test
    fun `blake2b hashing (pinning test)`() {
        // Test vector from https://github.com/BLAKE2/BLAKE2/blob/5cbb39c9ef8007f0b63723e3aea06cd0887e36ad/testvectors/blake2-kat.json
        val data = Utils.hexDecode(
            "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f" +
                    "202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f" +
                    "404142434445464748494a4b"
        )
        val key = Utils.hexDecode(
            "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f" +
                    "202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f"
        )
        val expectedOutHex = "B893F2FC41F7B0DD6E2F6AA2E0370C0CFF7DF09E3ACFCC0E920B6E6FAD0EF747" +
                "C40668417D342B80D2351E8C175F20897A062E9765E6C67B539B6BA8B9170545"
        val actual = Blake2bHasher.keyed(key, 64).digest(data)
        assertEquals(expectedOutHex, Utils.hexEncode(actual))
    }

    @Test
    fun `can digest without update`() {
        // Test vector from https://github.com/BLAKE2/BLAKE2/blob/5cbb39c9ef8007f0b63723e3aea06cd0887e36ad/testvectors/blake2-kat.json
        val expectedOutHex = "786A02F742015903C6C6FD852552D272912F4740E15847618A86E217F71F54" +
                "19D25E1031AFEE585313896444934EB04B903A685B1448B755D56F701AFE9BE2CE"
        val actual = Blake2bHasher.unkeyed(64).digest()
        assertEquals(expectedOutHex, Utils.hexEncode(actual))
    }

    @Test
    fun `illegal states`() {
        val hasher = Blake2bHasher.unkeyed(32)
        hasher.update(byteArrayOf(1, 2, 3)).digest()
        assertFailsWith<IllegalStateException> { hasher.update(byteArrayOf(1, 2, 3)) }
        assertFailsWith<IllegalStateException> { hasher.digest() }
    }
}

class StreamChunkingTest {

    /**
     * Used for StreamChunker regression test.
     *
     * This could be more robust by overriding more methods, but this is at
     * least good enough for repro.
     */
    class ReluctantInputStream(source: InputStream) : FilterInputStream(source) {
        override fun read(b: ByteArray): Int {
            val accept = ByteArray(1)
            val count = super.read(accept)
            if (count >= 0)
                System.arraycopy(accept, 0, b, 0, count)
            return count
        }

        override fun read(b: ByteArray, off: Int, len: Int): Int {
            val accept = ByteArray(1)
            val count = super.read(accept, 0, min(1, len))
            if (count >= 0)
                System.arraycopy(accept, 0, b, off, count)
            return count
        }
    }

    /**
     * Regression test for case where decrypting a network download was failing,
     * which was because StreamChunker was not reading a complete chunk each
     * time (since it assumed that the byte array read() on InputStream would
     * fill the array.)
     */
    @Test
    fun `stream chunker regression`() {
        // Make a stream that reluctantly gives out one byte at a time, no
        // matter how many were "asked for".
        val source = "0123456789abcdefghijklmnopqrstuvwxyz".toByteArray(utf8)

        // Ragged end
        assertEquals(listOf("01234", "56789", "abcde", "fghij", "klmno", "pqrst", "uvwxy", "z"),
            StreamChunker(ReluctantInputStream(source.inputStream()), 5)
                .asSequence().map { it.toString(utf8) }.toList())

        // Flush end
        assertEquals(listOf("012345", "6789ab", "cdefgh", "ijklmn", "opqrst", "uvwxyz"),
            StreamChunker(ReluctantInputStream(source.inputStream()), 6)
                .asSequence().map { it.toString(utf8) }.toList())
    }
}
