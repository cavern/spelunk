package org.timmc.spelunk

import org.junit.Assume
import org.timmc.spelunk.model.ConfigPatch
import org.timmc.spelunk.model.Contact
import org.timmc.spelunk.model.JournalPost
import org.timmc.spelunk.model.PostStatus
import org.timmc.spelunk.model.PublishLocationConfig
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintStream
import java.lang.IllegalArgumentException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.copyToRecursively
import kotlin.io.path.deleteRecursively
import kotlin.io.path.writeText
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

object IntegrationHelper {
    /**
     * Create a temporary directory and pass it to [tests]; only delete it
     * afterwards if tests throw.
     */
    @OptIn(ExperimentalPathApi::class)
    fun <R> withTempDir(tests: (Path) -> R): R {
        val tmpDir = Files.createTempDirectory("spelunk-integration-tests_")
        try {
            val ret = tests(tmpDir)
            // Only delete on success, otherwise leave up for debugging
            tmpDir.deleteRecursively()
            return ret
        } catch (t: Throwable) {
            println("Tests threw when using temp dir <$tmpDir>: $t")
            throw t
        }
    }

    /**
     * Fetch the value of the environment variable, and skip the test if it's
     * not set.
     */
    fun fromEnv(envName: String): String {
        val value = System.getenv(envName)
        Assume.assumeNotNull(value)
        return value
    }

    /**
     * Copy a test resources directory to some other location.
     */
    fun loadDirectoryFromTestResources(resourceDirPath: String, destDir: Path) {
        val src = File(StateTest::class.java.classLoader.getResource(resourceDirPath)!!.toURI())
        src.copyRecursively(destDir.toFile())
    }

    data class SmokeTestState(
        val aliceState: State,
        val aliceCatPostPubId: String,
        val aliceContactBob: Contact,
        val bobState: State,
        val bobContactAlice: Contact,
    )

    private const val SMOKE_TEST_CAT_SUBJECT = "Cat pics"

    /**
     * Round-trip that initializes two repositories, and has one send a message
     * to the other.
     */
    fun smokeTestWithAliceAndBob(
        alicePublishing: PublishLocationConfig,
        aliceJournalUri: String,
        tests: (SmokeTestState) -> Unit,
    ) {
        val aliceLocal = aliceJournal.makeCopy()
        val bobLocal = bobJournal.makeCopy()

        // Run tests inside repo locks
        val (aliceLock, aliceState) = State.getWriteableWithLock(aliceLocal)
        aliceLock.use {
            val (bobLock, bobState) = State.getWriteableWithLock(bobLocal)
            bobLock.use {

                // Alice will publish to the specified config
                aliceState.configPatch(
                    ConfigPatch(publishing = MaybePatch.Present(alicePublishing))
                )

                // Alice is quaints with Bob (and knows his sig key)
                val bobKeys = bobState.keyringRead().current
                val aliceContactBob = aliceState.addressBookAdd(Contact(
                    id = Contact.PLACEHOLDER_ID,
                    fetchUri = null, // doesn't read from Bob
                    sigKey = bobKeys.sigKeys.publicKey,
                    boxKey = bobKeys.boxKeys.publicKey,
                    separation = 0,
                    handleReceived = null,
                    handleOverride = null
                )).contacts.first { it.sigKey == bobKeys.sigKeys.publicKey }

                // Bob is quaints with Alice (and knows her URL)
                val bobContactAlice = bobState.addressBookAdd(Contact(
                    id = Contact.PLACEHOLDER_ID,
                    fetchUri = aliceJournalUri,
                    sigKey = null,
                    boxKey = null,
                    separation = 0,
                    handleReceived = null,
                    handleOverride = null
                )).contacts.first { it.fetchUri == aliceJournalUri }

                // Alice has one post and Bob is allowed to see it
                val alicePostDir = aliceState.repo.journal.newPost()
                val alicePostMeta = JournalPost.makeBlankPost(aliceState.configRead()).copy(
                    persistentId = aliceState.postIdEncrypted(alicePostDir.id.toLong()),
                    subject = SMOKE_TEST_CAT_SUBJECT,
                    body = "Hey friends, I'll have some cat pictures up shortly.",
                    status = PostStatus.PUBLISHED
                )
                alicePostDir.post.writeJson(alicePostMeta)

                val smokeTestState = SmokeTestState(
                    aliceState = aliceState,
                    aliceCatPostPubId = alicePostMeta.persistentId,
                    aliceContactBob = aliceContactBob,
                    bobState = bobState,
                    bobContactAlice = bobContactAlice,
                )

                tests(smokeTestState)
            }
        }
    }

    /**
     * Run standard battery of smoke tests against Alice and Bob state
     */
    fun standardSmokeTests(env: SmokeTestState) {
        val aliceState = env.aliceState
        val bobState = env.bobState

        // Publish and fetch
        aliceState.publishing.runInitialSync()
        bobState.fetching.blockingFetchAll()

        // Basic read test
        val catPost = bobState.fetching.getAllPosts()
            .firstOrNull { it.postId == env.aliceCatPostPubId }
        assertNotNull(catPost, "Should have found a post from Alice")
        assertEquals(env.bobContactAlice.id, catPost.contact.id)
        assertEquals(SMOKE_TEST_CAT_SUBJECT, catPost.post.subject)
        assertTrue(catPost.attachments.isEmpty())
        assertEquals("Hey friends", catPost.post.body.substring(0, 11))


        // OK, now for some tests of the catalog file.

        val pusher = aliceState.publishing.getConfiguredPusher()!!
        val indexPath = Paths.get("index.html")
        val customHTML = "some existing file"
        val overwriteableHTML = "older ${Constants.INDEX_FILE_MARKER}"

        fun writeIndexFile(contents: String) {
            pusher.writeRemoteFile(indexPath, contents.byteInputStream())
        }

        fun readIndexFile(): String? {
            return pusher.readRemoteFile(indexPath) {
                it?.readBytes()?.decodeToString()
            }
        }

        var publishCounter = 1
        fun forcePublish() {
            // Make a change to a post so publishing will happen
            val catPostAlice = aliceState.repo.journal.posts.list().first().post
            val catPostAliceMeta = catPostAlice.readJson()
            catPostAlice.writeJson(catPostAliceMeta.copy(
                updatedDate = catPostAliceMeta.updatedDate + publishCounter
            ))
            aliceState.publishing.runInitialSync()
            publishCounter++
        }

        // Start with a custom file that we shouldn't overwrite; it
        // should be left alone.
        writeIndexFile(customHTML)
        forcePublish()
        assertEquals(customHTML, readIndexFile())

        // Switch to one with the "rewrites OK" marker. It should be
        // overwritten.
        writeIndexFile(overwriteableHTML)
        forcePublish()
        var remoteIndex = readIndexFile()
        assertNotNull(remoteIndex)
        // It does have the marker...
        assertContains(remoteIndex, Constants.INDEX_FILE_MARKER)
        // ...but it's not the same as the file we put there just now.
        assertNotEquals(remoteIndex, overwriteableHTML)

        // And now delete the remote file (check that deletion works
        // too!) and then check that a new index file is written.
        pusher.deleteRemoteFile(indexPath)
        // Confirm it doesn't complain about already-deleted file
        pusher.deleteRemoteFile(indexPath)
        assertNull(readIndexFile())
        forcePublish()
        remoteIndex = readIndexFile()
        assertNotNull(remoteIndex)
        assertContains(remoteIndex, Constants.INDEX_FILE_MARKER)
    }

    /**
     * Run the given function while temporarily redirecting stdout and return
     * the captured output.
     *
     * This is not thread-safe.
     */
    fun withCaptureStdoutRacy(funk: () -> Unit): String {
        val captOut = ByteArrayOutputStream()
        val oldOut = System.out
        try {
            System.setOut(PrintStream(captOut))
            funk()
        } finally {
            System.setOut(oldOut)
        }
        return captOut.toByteArray().decodeToString()
    }

    /**
     * A Spelunk journal that can be copied for tests.
     *
     * Multiple of these are needed because each journal will have its own keys.
     */
    @OptIn(ExperimentalPathApi::class)
    class JournalFixture(private val sourceDir: Path) {
        fun makeCopy(): Path {
            val newCopyDir = Files.createTempDirectory("spelunk-tests-journal-copy_")
            sourceDir.copyToRecursively(newCopyDir, followLinks = false)
            // Only add to cleanup list after success, for debugging purposes.
            copies.add(newCopyDir)

            return newCopyDir
        }

        companion object {
            private val copies = mutableListOf<Path>()

            init {
                Runtime.getRuntime().addShutdownHook(Thread {
                    copies.forEach { dir ->
                        try {
                            dir.deleteRecursively()
                        } catch (t: Throwable) {
                            println("Failed to clean up journal copy: $dir")
                        }
                    }
                })

            }
        }
    }

    /**
     * Make a unique journal for use as a fixture.
     */
    @OptIn(ExperimentalPathApi::class)
    private fun makeSourceJournal(nameHint: String): JournalFixture {
        // For filesystem safety
        if (!nameHint.matches(Regex("""[a-zA-Z0-9]+""")))
            throw IllegalArgumentException("Name hint must be alphanumeric")

        val dir = Files.createTempDirectory("spelunk-tests-journal-${nameHint}_")
        Runtime.getRuntime().addShutdownHook(Thread(dir::deleteRecursively))

        mainInternal("init", "--no-add-to-profiles", "--dir", dir.toString())
        return JournalFixture(dir)
    }

    val aliceJournal by lazy { makeSourceJournal("alice") }
    val bobJournal by lazy { makeSourceJournal("bob") }

    /**
     * Add a post to a test repo (as post 0) so that tests have a bit more to
     * work with.
     */
    fun addPostToTestRepo(repo: Path) {
        // Add a post so we have something to list
        val postDir = repo.resolve("journal/0")
        Files.createDirectories(postDir)
        postDir.resolve("post.json").writeText("""
            {
              "persistentId": "0:213kg",
              "subject": "First post!",
              "tags": ["meta"],
              "status": "PUBLISHED",
              "publishDate": 1574963649,
              "updatedDate": 1576891918,
              "visibility": "QOAQ2",
              "body": "This is my first Cavern post.",
              "attachments": []
            }
        """.trimIndent())
    }
}
