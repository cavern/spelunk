package org.timmc.spelunk.cmd

import org.junit.Test
import org.timmc.spelunk.model.JournalPost
import org.timmc.spelunk.model.PostStatus
import org.timmc.spelunk.model.Visibility
import kotlin.test.assertEquals

class ListCmdTest {
    @Test
    fun testPreviewRagged() {
        val basePost = JournalPost(
            persistentId = "0:7vgw",
            subject = "Hello, world!",
            body = "This is a test of the social broadcasting system.",
            tags = listOf("first", "post"),
            status = PostStatus.DRAFT,
            publishDate = 1623527649L,
            updatedDate = 1623527649L,
            visibility = Visibility.QOAQ1,
            attachments = emptyList()
        )

        assertEquals(
            "Hello, world!: This is a test of the social broadcasting system.",
            ListCmd.previewRagged(basePost)
        )
    }
}
