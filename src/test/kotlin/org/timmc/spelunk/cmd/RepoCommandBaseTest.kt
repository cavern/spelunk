package org.timmc.spelunk.cmd

import org.junit.Test
import org.timmc.spelunk.IntegrationHelper
import org.timmc.spelunk.mainInternal
import kotlin.test.assertEquals

class RepoCommandBaseTest {
    @Test
    fun `base dir not sticky - regression test`() {
        fun listNewRepo() {
            val repo = IntegrationHelper.aliceJournal.makeCopy()
            IntegrationHelper.addPostToTestRepo(repo)

            val listOutput = IntegrationHelper.withCaptureStdoutRacy {
                mainInternal("list", "--dir", repo.toString(), "--upgrade")
            }
            assertEquals(
                " 0: First post!: This is my first Cavern post.\n",
                listOutput
            )
        }

        // Calling the same command twice using mainInternal from different
        // temp-dir contexts was causing the following error:
        //
        //     Tests threw when using temp dir </tmp/spelunk-integration-tests_16448291041266068334>:
        //     java.nio.file.NoSuchFileException: /tmp/spelunk-integration-tests_5739594364237620544/.lock
        //
        // Notice the differing dir names. This seems to be because we were
        // creating and keeping instances of each of the commands, e.g.
        // `val cli = Spelunk().subcommands(..., ListCmd(), ...)`, but then
        // RepoCommandBase had a `val baseDir: Path by lazy { ... }` that would
        // capture the first path that was in use.
        listNewRepo()
        listNewRepo()
    }
}
