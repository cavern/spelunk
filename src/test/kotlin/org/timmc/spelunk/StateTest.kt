package org.timmc.spelunk

import org.junit.Test
import org.sqlite.SQLiteDataSource
import java.math.BigInteger
import java.nio.file.Path
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class StateTest {
    @Test
    fun postIdEncryption() {
        val tmpDir = IntegrationHelper.aliceJournal.makeCopy()

        // declare out here so it can be shared across locks
        val first: String // will be "34" encrypted with first key

        run {
            val (lock, state) = State.getWriteableWithLock(tmpDir)
            lock.use {
                first = state.postIdEncrypted(34)
                // expected pattern -- 0:xxxx in base64
                assertTrue(
                    first.matches(Regex("""0:[0-9a-zA-Z+/]{4}""")),
                    "$first did not match pattern 0:XXXX"
                )
                // deterministic
                assertEquals(first, state.postIdEncrypted(34))
                // input-dependent
                assertNotEquals(first, state.postIdEncrypted(100))
            }
        }

        // Rotate key using CLI
        mainInternal("maint", "rotate-post-id-secret", "--dir", tmpDir.toString())

        run {
            val (lock, state) = State.getWriteableWithLock(tmpDir)
            lock.use {
                // and now encrypted with second key
                val second = state.postIdEncrypted(34)
                // incremented key version
                assertTrue(
                    second.matches(Regex("""1:[0-9a-zA-Z+/]{4}""")),
                    "$second did not match pattern 0:XXXX"
                )
                // deterministic
                assertEquals(second, state.postIdEncrypted(34))
                // key has changed, so output changes (even ignoring version)
                assertNotEquals(first.substring(1), second.substring(1))
            }
        }

        // Some other tests, just looking at boundaries
        run {
            val (lock, state) = State.getWriteableWithLock(tmpDir)
            lock.use {
                // Block size changes at 2^(3 * 8) boundary
                val threeSixBoundary = BigInteger.TWO.pow(3 * 8).toLong()
                val justBelow = state.postIdEncrypted(threeSixBoundary - 1)
                val justAbove = state.postIdEncrypted(threeSixBoundary)
                assertTrue(
                    justBelow.matches(Regex("""1:[0-9a-zA-Z+/]{4}""")),
                    "$justBelow did not match pattern 1:XXXX"
                )
                assertTrue(
                    justAbove.matches(Regex("""1:[0-9a-zA-Z+/]{8}""")),
                    "$justAbove did not match pattern 1:XXXXXXXX"
                )

                // Demo: Can address up to 2^48 posts at most
                val maxSpace = BigInteger.TWO.pow(48).toLong()
                state.postIdEncrypted(maxSpace - 1)
                assertFails { state.postIdEncrypted(maxSpace) }
            }
        }
    }

    /**
     * Read table schemas from DB.
     *
     * This is pretty hacky but it should only be used from tests anyhow.
     */
    fun readTableSchemas(path: Path): Map<String, String> {
        val db = SQLiteDataSource().apply { url = "jdbc:sqlite:$path" }
        val rs = db.connection
            .prepareStatement("""select name, sql from sqlite_master where type = "table";""")
            .executeQuery()
        val ret = mutableMapOf<String, String>()
        while (rs.next()) {
            ret[rs.getString("name")] = rs.getString("sql")
        }
        return ret
    }

    data class Schemas(
        val state: Map<String, String>,
        val publishing: Map<String, String>,
        val fetching: Map<String, String>,
    )

    /**
     * Normalize a create-table schema to allow text comparison. The output
     * is not safe to use in a DB, but should be OK for most testing
     * purposes.
     */
    private fun normalizeSchemaForComparison(sql: String): String {
        val ws = """[\s\r\n]"""
        return sql
            // Unquote the table name, if quoted
            .replace(
                Regex("""^CREATE TABLE "([^"]+)" """),
                """CREATE TABLE $1 """
            )
            // Strip extra whitespace around parens and commas. Better hope
            // there aren't any strings containing those characters!
            .replace(Regex("""\($ws*"""), "(")
            .replace(Regex("""$ws*\)"""), ")")
            .replace(Regex("""$ws*,$ws*"""), ", ")
    }

    fun assertSchemasEqual(expected: Map<String, String>, actual: Map<String, String>) {
        // Compare keys first, then contents, in order to make debugging easier
        assertEquals(expected.keys, actual.keys)
        for (k in expected.keys) {
            assertEquals(
                normalizeSchemaForComparison(expected[k]!!),
                normalizeSchemaForComparison(actual[k]!!)
            )
        }

    }

    /**
     * Several checks on whether a journal created by an older version of
     * Spelunk can be successfully upgraded to the latest version.
     *
     * This includes a schema equality check to ensure that upgrading an old
     * DB produces a schema equivalent to a newly created DB.
     */
    @Test
    fun upgradeFrom13_0ToCurrent() {
        val upgradedSchemas = IntegrationHelper.withTempDir { restoredRepo ->
            IntegrationHelper.loadDirectoryFromTestResources("upgrading/spelunk-1.0.0-c1202117c1-state13-fetch5-pub4/repo", restoredRepo)

            // Upgrade the repo. Don't capture the output here -- it interferes
            // with debugging if there's a failure, and a stack trace printed.
            mainInternal("list", "--dir", restoredRepo.toString(), "--upgrade")
            // Now do that listing and see if the output looks right
            val listOutput = IntegrationHelper.withCaptureStdoutRacy {
                mainInternal("list", "--dir", restoredRepo.toString())
            }
            assertEquals(
                listOf(
                    " 0: Public post with image: This is a public post. It has an image attached, b…",
                    "*1: Locked draft: Draft post, not yet published.",
                    "*3: Draft public: Another public post, but a draft.",
                ).joinToString("") { it + "\n" },
                listOutput
            )

            // Ensure the other DBs get created
            mainInternal("push", "--dir", restoredRepo.toString())
            runCatching { mainInternal("fetch", "--dir", restoredRepo.toString()) }

            Schemas(
                state = readTableSchemas(restoredRepo.resolve("settings.sqlite3")),
                publishing = readTableSchemas(restoredRepo.resolve("cache/publishing.sqlite3")),
                fetching = readTableSchemas(restoredRepo.resolve("cache/download.sqlite3"))
            )
        }

        val cleanRepo = IntegrationHelper.aliceJournal.makeCopy()
        mainInternal("list", "--dir", cleanRepo.toString(), "--no-upgrade")
        mainInternal("push", "--dir", cleanRepo.toString())
        runCatching { mainInternal("fetch", "--dir", cleanRepo.toString()) }
        val cleanState = readTableSchemas(cleanRepo.resolve("settings.sqlite3"))
        val cleanPublish = readTableSchemas(cleanRepo.resolve("cache/publishing.sqlite3"))
        val cleanFetch = readTableSchemas(cleanRepo.resolve("cache/download.sqlite3"))

        assertSchemasEqual(cleanState, upgradedSchemas.state)
        assertSchemasEqual(cleanPublish, upgradedSchemas.publishing)
        assertSchemasEqual(cleanFetch, upgradedSchemas.fetching)
    }
}
