package org.timmc.spelunk

import org.junit.Test
import org.timmc.spelunk.model.ProfilesFinder
import java.nio.file.Files
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

// TODO Make into proper integration test that calls a built executable
class InitCmdTest {
    @Test
    fun emptyDir() {
        IntegrationHelper.withTempDir { tmpDir ->
            // Existing, empty directory
            val journalDir = tmpDir.resolve("journal")
            Files.createDirectory(journalDir)

            mainInternal("init", "--no-add-to-profiles", "--dir", journalDir.toString())

            val state = State.getReadOnly(journalDir)
            assertTrue(state.repo.isInitComplete())
        }
    }

    @Test
    fun missingCreatableDir() {
        IntegrationHelper.withTempDir { tmpDir ->
            // Missing directory, but parent exists and is writeable
            val journalDir = tmpDir.resolve("journal")

            mainInternal("init", "--no-add-to-profiles", "--dir", journalDir.toString())

            val state = State.getReadOnly(journalDir)
            assertTrue(state.repo.isInitComplete())
        }
    }

    @Test
    fun withProfilesDir() {
        IntegrationHelper.withTempDir { parentDir ->
            val profilesDir = parentDir.resolve("nested/several/levels")
            val repoRootA = parentDir.resolve("journal_a")
            val repoRootB = parentDir.resolve("journal_b")

            val finder = ProfilesFinder(profilesDir)

            assertNull(finder.readProfiles())

            mainInternal("init", "--profiles-dir", profilesDir.toString(), "--dir", repoRootA.toString())
            run { // just for locals isolation
                val listing = finder.readProfiles()
                assertNotNull(listing)
                assertEquals(1, listing.profiles.size)
                assertTrue(listing.hasDefault())
                val default = listing.getDefaultRepoPath()
                assertNotNull(default)
                assertEquals(repoRootA, default)
            }

            mainInternal("init", "--profiles-dir", profilesDir.toString(), "--dir", repoRootB.toString())
            run {
                val listing = finder.readProfiles()
                assertNotNull(listing)
                assertEquals(2, listing.profiles.size) // another added
                assertTrue(listing.hasDefault())
                val default = listing.getDefaultRepoPath()
                assertNotNull(default)
                assertEquals(repoRootA, default) // default hasn't changed

                val other = listing.profiles[1]
                assertEquals(repoRootB.toString(), other.path)
            }
        }
    }
}
