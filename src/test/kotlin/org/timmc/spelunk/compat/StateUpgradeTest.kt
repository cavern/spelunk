package org.timmc.spelunk.compat

import org.junit.Test
import org.sqlite.SQLiteDataSource
import org.timmc.spelunk.IntegrationHelper
import org.timmc.spelunk.State
import java.nio.file.Path
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class StateUpgradeTest {
    @Test
    fun upgradersInvariants() {
        StateUpgrade.formatUpgraders.forEach {
            assertTrue(it.version < it.targetVersion)
        }

        StateUpgrade.formatUpgraders.windowed(size = 2, step = 1, partialWindows = false).forEach { (a, b) ->
            assertTrue(a.version < b.version)
            assertTrue(a.targetVersion < b.targetVersion)
            assertEquals(a.targetVersion, b.version)
        }

        StateUpgrade.formatUpgraders.lastOrNull()?.let { lastUpgrader ->
            assertEquals(State.latestVersion, lastUpgrader.targetVersion)
        }
    }

    data class ColumnShape(val name: String, val type: String, val notnull: Boolean)

    fun readColumnProperties(dbFile: Path, tableName: String): List<ColumnShape> {
        val db = SQLiteDataSource().apply { url = "jdbc:sqlite:$dbFile" }
        val rs = db.connection
            .prepareStatement("""pragma table_info("$tableName");""")
            .executeQuery()

        val foundColumns = mutableListOf<ColumnShape>()
        while (rs.next()) {
            foundColumns.add(ColumnShape(
                name = rs.getString("name"),
                type = rs.getString("type"),
                notnull = rs.getBoolean("notnull")
            ))
        }
        foundColumns.sortBy(ColumnShape::name)
        return foundColumns.toList()
    }

    /**
     * Ensure that the versioning table retains backwards compatibility
     * indefinitely for each of the tables that participates.
     */
    @Test
    fun `versioning table shape is fossilized`() {
        val journalDir = IntegrationHelper.aliceJournal.makeCopy()
        val (lock, state) = State.getWriteableWithLock(journalDir)
        lock.use {
            // Ensure that lazily created members are created, and initialize
            // their DB files.
            state.fetching
            state.publishing
        }

        // If this check ever fails, due to some change in DB path, table name,
        // or table contents, you'll need to ensure that there is always some
        // way for older versions of the app to read exactly this table at
        // exactly this path, even if the real data has been migrated elsewhere.
        // This will allow the upgraders to be invoked.
        assertEquals(
            listOf(
                ColumnShape("is_upgrading", "BOOLEAN", true),
                ColumnShape("version_major", "BIGINT", true),
                ColumnShape("version_minor", "BIGINT", true),
            ).sortedBy(ColumnShape::name),
            // Exact expected path and table name
            readColumnProperties(journalDir.resolve("settings.sqlite3"), "repository"),
        )

        // If this check fails, there may be some unpleasant stacktraces in the
        // log output in different versions of the app, but the DB will be
        // recreated properly anyhow. Try to avoid this, but it's not critical.
        assertEquals(
            listOf(
                ColumnShape("schema_version", "INT", true),
            ),
            readColumnProperties(journalDir.resolve("cache/publishing.sqlite3"), "versioning"),
        )
    }

    @Test
    fun upgrader14Conversion() {
        val v13 = """
{
  "id": "0:byZ2",
  "type": "journal",
  "subject": "My latest cat photos",
  "body": "Attached.",
  "bodyFormat": "text/plain",
  "tags": [
    "cat",
    "photos"
  ],
  "publishDate": 1576301653,
  "updatedDate": 1696102610,
  "visibilityType": "KNOWN",
  "contentFiles": [
    {
      "clearFilename": "cat_loaf.jpg",
      "disposition": "attachment",
      "allegedSize": 5612345,
      "allegedMimeType": "image/jpeg",
      "pointer": {
        "hiddenId": "0961kb3296khl",
        "symmetricKey": {
          "keyBytes": {
            "bytes": [
              129,
              220,
              59,
              135,
              153,
              200,
              155,
              114,
              237,
              164,
              36,
              132,
              114,
              141,
              209,
              23,
              50,
              211,
              7,
              219,
              28,
              244,
              241,
              218,
              124,
              10,
              91,
              97,
              219,
              34,
              8,
              147
            ]
          }
        }
      }
    }
  ]
}
        """.trimIndent()
        val v14 = """
{
  "id": "0:byZ2",
  "type": "journal",
  "subject": "My latest cat photos",
  "body": "Attached.",
  "bodyFormat": "text/plain",
  "tags": [
    "cat",
    "photos"
  ],
  "publishDate": 1576301653,
  "updatedDate": 1696102610,
  "visibilityType": "KNOWN",
  "contentFiles": [
    {
      "clearFilename": "cat_loaf.jpg",
      "disposition": "attachment",
      "allegedSize": 5612345,
      "allegedMimeType": "image/jpeg",
      "pointer": {
        "hiddenId": "0961kb3296khl",
        "symmetricKey": "81DC3B8799C89B72EDA42484728DD11732D307DB1CF4F1DA7C0A5B61DB220893",
        "objectHash": "00000000000000000000000000000000"
      }
    }
  ]
}
        """.trimIndent()

        assertEquals(v14, Upgrade_13_0_to_14_0.convertPostJson(v13))
    }
}

/**
 * Run [block] with a temp dir containing a restored copy of the named upgrader
 * test data repo.
 */
fun <R> withUpgraderTestData(version: String, block: (Path) -> R?): R? {
    return IntegrationHelper.withTempDir { restoredRepo ->
        IntegrationHelper.loadDirectoryFromTestResources("upgrading/$version/repo", restoredRepo)
        block(restoredRepo)
    }
}
