package org.timmc.spelunk.compat

import org.junit.Test
import org.timmc.spelunk.IntegrationHelper
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class StepsTest {
    @Test
    fun makeDir() {
        IntegrationHelper.withTempDir { root ->
            assertContains(
                assertFailsWith<PreflightFailedE> {
                    MakeDir(root).preflight()
                }.message!!,
                "there's already a file or directory there"
            )

            assertContains(
                assertFailsWith<PreflightFailedE> {
                    MakeDir(root.resolve("one/two")).preflight()
                }.message!!,
                "that parent directory does not exist"
            )

            assertContains(
                assertFailsWith<PreflightFailedE> {
                    MakeDir(Paths.get("/foo")).preflight()
                }.message!!,
                "that parent directory is not writeable"
            )

            val step = MakeDir(root.resolve("inner"))
            step.preflight()
            step.execute()
            assertTrue(Files.isDirectory(root.resolve("inner")))
        }
    }

    @Test
    fun moveFiles() {
        IntegrationHelper.withTempDir { root ->
            Files.createDirectories(root.resolve("src/one/two"))
            Files.writeString(root.resolve("src/base.txt"), "base")
            Files.writeString(root.resolve("src/one/middle.txt"), "middle")
            Files.writeString(root.resolve("src/one/two/leaf.txt"), "leaf")

            Files.createDirectories(root.resolve("dest/"))

            val step = MoveFile(root.resolve("src"), root.resolve("dest/deeper/in"))

            step.preflight()
            step.execute()

            assertEquals(
                "base",
                Files.readString(root.resolve("dest/deeper/in/base.txt"))
            )
            assertEquals(
                "middle",
                Files.readString(root.resolve("dest/deeper/in/one/middle.txt"))
            )
            assertEquals(
                "leaf",
                Files.readString(root.resolve("dest/deeper/in/one/two/leaf.txt"))
            )
        }
    }
}
