package org.timmc.spelunk

import org.junit.Test
import java.nio.file.Files
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertTrue

class FsLayoutTest {
    @Test
    fun `recursive deletion doesn't follow symlinks`() {
        val base = Files.createTempDirectory("spelunk-recursive-delete_")
        Files.createDirectories(base.resolve("destroy/foo/bar"))
        Files.createFile(base.resolve("destroy/foo/bar/deleteme.txt"))
        Files.createSymbolicLink(base.resolve("destroy/foo/bar/across"),
            base.resolve("keep/quux"))
        Files.createDirectories(base.resolve("keep/quux/quack"))
        val canary = base.resolve("keep/quux/quack/wanted.txt")
        Files.createFile(canary)

        assertTrue("Setup: Symlink goes where it is expected to") {
            base.resolve("destroy/foo/bar/across/quack/wanted.txt").toFile().isFile
        }
        assertTrue("Setup: Canary exists before delete") {
            canary.toFile().exists()
        }

        // Disallow delete if allowWrite = false
        val thrown = assertFails {
            Directory(base.resolve("destroy"), false).deleteRecursively()
        }
        assertTrue(thrown.message?.contains(Regex("""Not allowed to write at""")) ?: false)
        assertTrue("Canary file still exists when delete is blocked") {
            canary.toFile().exists()
        }

        // Proceed with delete
        Directory(base.resolve("destroy"), true).deleteRecursively()
        assertEquals(false, base.resolve("destroy").toFile().exists(),
            "Target directory destroyed")
        assertEquals(true, canary.toFile().exists(),
            "Canary preserved in other directory")

        // cleanup, at least on success
        Directory(base, true).deleteRecursively()
    }
}
