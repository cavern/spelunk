package org.timmc.spelunk

import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.junit.Test
import org.timmc.spelunk.model.ConfigPatch
import org.timmc.spelunk.model.LocalFilesConfig
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class PublishingTest {
    @Test
    fun `publish before posts`() {
        val journalDir = IntegrationHelper.aliceJournal.makeCopy()
        IntegrationHelper.withTempDir { publishDir ->
            val (lock, state) = State.getWriteableWithLock(journalDir)
            lock.use {
                // Configure publishing
                state.configPatch(
                    ConfigPatch(publishing = MaybePatch.Present(LocalFilesConfig(
                        journalUrl = null,
                        rootDir = publishDir.toString(),
                    )))
                )

                // Check that the first publish always happens even if there
                // are no posts yet, but that subsequent publishes are
                // skipped unless something relevant has changed.
                assertEquals(
                    LastPublish(null, null),
                    state.publishing.lastPublish(),
                )

                // First sync should succeed
                val firstLogs = captureLogs {
                    state.publishing.runInitialSync()
                }
                assertContains(firstLogs, "No record of last publish")

                val lastPublish = state.publishing.lastPublish()
                assertNotNull(lastPublish.timestamp)
                assertEquals(Constants.CAVERN_SPEC, lastPublish.spec)

                // Second sync should be skipped
                val secondLogs = captureLogs {
                    state.publishing.runInitialSync()
                }
                assertContains(secondLogs, "Skipping publish")
                assertEquals(lastPublish, state.publishing.lastPublish())
            }
        }
    }

    @Test
    fun `re-publish due to old spec`() {
        IntegrationHelper.withTempDir { alicePublishPath ->
            IntegrationHelper.smokeTestWithAliceAndBob(
                LocalFilesConfig(
                    journalUrl = null,
                    rootDir = alicePublishPath.toString(),
                ),
                alicePublishPath.toUri().toASCIIString(),
                tests = { env ->
                    env.aliceState.publishing.runInitialSync()

                    // After the initial publish, meddle with the DB and make it
                    // look like the Cavern spec version has since changed.

                    val publishDatabasePath = env.aliceState.repo.cache.publishDB.path
                    val alicePubDB = SQLite3.database(publishDatabasePath, true)
                    transaction(alicePubDB) {
                        Publishing.Status.update { it[lastSpecVersionPublished] = "cavern0" }
                    }

                    val secondPublishLogs = captureLogs {
                        env.aliceState.publishing.runInitialSync()
                    }
                    assertContains(
                        secondPublishLogs,
                        "Publishing because: Last Cavern format used for publishing is now outdated",
                    )
                }
            )
        }
    }
}
