package org.timmc.spelunk

import org.junit.Test
import org.msgpack.core.MessagePackException
import org.timmc.johnny.Urls
import org.timmc.spelunk.model.Config
import org.timmc.spelunk.model.ConfigPatch
import org.timmc.spelunk.model.Contact
import org.timmc.spelunk.model.LocalFilesConfig
import org.timmc.spelunk.model.Visibility
import org.timmc.spelunk.model.cavern.CavIdentity
import org.timmc.spelunk.model.cavern.CavCatalogWrapper
import org.timmc.spelunk.model.cavern.CavPost
import kotlin.io.path.readBytes
import kotlin.io.path.writeBytes
import kotlin.reflect.KClass
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertIs
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

class FetchingTest {
    @Test
    fun `computing safe storage filenames`() {
        // Basic names left unmolested
        assertEquals("hello.txt", storageFilename("hello.txt"))
        // No casefolding
        assertEquals("Hello.txt", storageFilename("Hello.txt"))
        // Leaves alphanumerics alone, in various languages
        assertEquals("قاموس_dictionary_ᕿᒥᕐᕈᐊᑦ_ᐅᓐᓂᖅᑐᖅ_ᒥᑦᓯ_ᑐᑭᐊ",
            storageFilename("قاموس dictionary ᕿᒥᕐᕈᐊᑦ ᐅᓐᓂᖅᑐᖅ ᒥᑦᓯ ᑐᑭᐊ"))

        // Cleans names that are trouble for shells and filesystems
        assertEquals("option_-flags", storageFilename("--option --flags"))
        assertEquals("directory_traversal", storageFilename("../directory/traversal"))
        assertEquals("line_breaks_and_whatnot", storageFilename("line\nbreaks\tand\u0000whatnot"))

        // Doesn't allow long runs of nonalphanumerics that could hide parts of
        // filenames in vulnerable interfaces
        assertEquals("index.html_.exe",
            storageFilename("index.html                                       .exe"))
        assertEquals("index.html.exe",
            storageFilename("index.html........................................exe"))

        // Cuts down huge filenames by center truncation (possibly OS-dependent)
        assertEquals("${"a".repeat(100)}_TRUNCATED_${"z".repeat(100)}",
            storageFilename("a".repeat(200) + "m".repeat(500) + "z".repeat(200)))

        // Composes diacritics before stripping, if possible, since combining
        // forms may otherwise be stripped
        assertEquals("a\u00f1os", storageFilename("an\u0303os"))
    }

    @Test
    fun `base URL inference`() {
        // Happy path
        val expectedBase = Urls.parse("https://example.com/alice/")
        listOf(
            "https://example.com/alice/",
            "https://example.com/alice",
            "https://example.com/alice/_cavern",
            "https://example.com/alice/_cavern/",
            "https://example.com/alice/_cavern/identity.mpk",
            "https://example.com/alice/index.html",
            "https://example.com/alice/index.htm",
            "https://example.com/alice/cavern.html",
            "https://example.com/alice/cavern.htm",
            "https://example.com/alice/WHATEVER.HTML",
        ).forEach {
            assertEquals(expectedBase, inferJournalBaseUrl(Urls.parse(it)))
        }

        // Otherwise return unchanged, but with trailing slash (and doesn't blow
        // up on weird inputs)
        listOf(
            "https://example.com",
            "https://example.com/identity.mpk",
        ).forEach { input ->
            val withSlash = "$input/"
            assertEquals(withSlash, inferJournalBaseUrl(Urls.parse(input)).format())
            assertEquals(withSlash, inferJournalBaseUrl(Urls.parse(withSlash)).format())
        }
    }

    @Test
    fun `interpret fetch URI`() {
        // Default config, with no Tor SOCKS proxy
        val cnf = Config(
            ownName = null,
            defaultPostVisibility = Visibility.QOAQ1,
            publishing = null,
            torSocksProxy = null // the relevant field
        )
        // Don't allow HTTP URLs with parts we don't understand
        assertNull(interpretFetchUri("https://no:userinfo@example.net/foo/bar?user=janet", cnf))
        assertNull(interpretFetchUri("https://example.net/foo/bar?user=janet#no-fragment", cnf))

        // Understand HTTPS and HTTP URLs
        assertTrue(interpretFetchUri("https://example.net/foo/bar?user=janet", cnf) is HttpFetcher)

        // Doesn't throw when URL is malformed
        assertNull(interpretFetchUri("https://%%%%%%/", cnf))

        // Doesn't throw when scheme is not understood
        assertNull(interpretFetchUri("ipfs:QmTkzDwWqPbnAh5YiV5VwcTLnGdwSNsNTn2aDxdXBFca7D/example", cnf))

        // Understand file URLs
        assertTrue(interpretFetchUri("file:///home/timmc/foo/bar", cnf) is FileFetcher)
        // Don't allow hosted file URLs, at least for now
        assertNull(interpretFetchUri("file://otherhost/home/timmc/foo/bar", cnf))
        // And don't allow file URLs with other parts
        assertNull(interpretFetchUri("file:///home/timmc/foo/bar?no-query", cnf))
        assertNull(interpretFetchUri("file:///home/timmc/foo/bar#no-fragment", cnf))

        // Apply Tor socks proxy when enabled and relevant
        val torEnabledConfig = cnf.copy(torSocksProxy = "localhost:9051")
        val mangledTorConfig = cnf.copy(torSocksProxy = "_______:!!!")
        val torUrl = "http://allsortsofstuff.onion/whatever"

        // Tor URL and Tor-enabled config is the only time the socks proxy
        // should be in effect
        interpretFetchUri(torUrl, torEnabledConfig).let {
            assertTrue(it is HttpFetcher)
            val proxy = it.socksProxy
            assertNotNull(proxy)
            assertEquals("localhost", proxy.hostString)
            assertEquals(9051, proxy.port)
        }
        // In all other cases, including a malformed proxy address, just null
        listOf(
            "https://example.com" to torEnabledConfig,
            "https://example.com" to mangledTorConfig,
            torUrl to mangledTorConfig,
            torUrl to cnf,
        ).forEach { (url, config) ->
            interpretFetchUri(url, config).let {
                assertTrue(it is HttpFetcher)
                assertNull(it.socksProxy)
            }
        }
    }

    @Test
    fun socksAddressParsing() {
        listOf(
            "" to "Socket address missing : delimiter",
            ":" to "Socket address missing host",
            "example.com:" to "Socket address missing port",
            ":1234" to "Socket address missing host",
            "a:b:c" to "Socket address port is non-numeric",
            "example.com:1234:" to "Socket address missing port",
            "example.com:0" to "Socket address port out of range [1, 65535]",
            "example.com:65536" to "Socket address port out of range [1, 65535]",
            "1:2::3:4:5" to "Socket address must enclose IPv6 address in square brackets.",
        ).forEach { (addr, error) ->
            val e = assertFails { parseSocketAddress(addr) }
            assertEquals(error, e.message)
        }

        // Round-trip tests of hosts and ports
        listOf(
            // hostnames
            "localhost" to 9050,
            "example.com" to 1,
            "example.com" to 65535,
            // IPv4
            "127.0.0.1" to 80,
            "1.2.3.4" to 443,
            // IPv6 (expanded)
            "0:0:0:0:0:0:0:1" to 25,
            "2620:0:861:ed1a:0:0:0:1" to 8080,
        ).forEach { (host, port) ->
            val formatted = if (':' in host)
                "[$host]:$port"
            else
                "$host:$port"
            parseSocketAddress(formatted).let {
                assertNotNull(it)
                // not hostName, which does reverse lookup on IP addresses!
                assertEquals(host, it.hostString)
                assertEquals(port, it.port)
            }
        }

        // IPv6 shorthands get expanded, which is why they're not handled in the code above (they don't round-trip)
        assertEquals("0:0:0:0:0:0:0:1", parseSocketAddress("[::1]:25").address.hostAddress)
        assertEquals("2620:0:861:ed1a:0:0:0:1", parseSocketAddress("[2620:0:861:ed1a::1]:25").address.hostAddress)
    }

    @Test
    fun torDetection() {
        assertTrue(isOnionSite(Urls.parse("http://allsortsofstuff.onion/whatever")))
        assertTrue(isOnionSite(Urls.parse("http://allsortsofstuff.onion./whatever")))
        assertTrue(isOnionSite(Urls.parse("http://allsortsofstuff.ONION/whatever")))

        assertFalse(isOnionSite(Urls.parse("http://allsortsofstuff.onion.ws/whatever")))
    }

    /**
     * Regression test: Even if the other person isn't publishing a catalog for
     * you, still read their keys.
     *
     * Before this was fixed, we failed to store the contact's keys when there
     * wasn't a catalog pointer. This meant that we would then not be able to
     * publish a catalog for the *other* person, who would have the same problem.
     * The two people would therefore never be able to communicate.
     */
    @Test
    fun regressionFetchWithoutCatalogStillUpdatesKeys() {
        val aliceLocal = IntegrationHelper.aliceJournal.makeCopy()
        val bobLocal = IntegrationHelper.bobJournal.makeCopy()
        IntegrationHelper.withTempDir { tmpDir ->
            val alicePublishPath = tmpDir.resolve("alice-publish")
            val aliceJournalUri = alicePublishPath.toUri().toASCIIString()

            // Run tests inside repo locks
            val (aliceLock, aliceState) = State.getWriteableWithLock(aliceLocal)
            aliceLock.use {
                val (bobLock, bobState) = State.getWriteableWithLock(bobLocal)
                bobLock.use {

                    // Alice will publish to the specified config
                    aliceState.configPatch(
                        ConfigPatch(publishing = MaybePatch.Present(LocalFilesConfig(
                            journalUrl = null,
                            rootDir = alicePublishPath.toString(),
                        )))
                    )

                    // Alice publishes, although she doesn't actually have any posts
                    aliceState.publishing.runInitialSync()

                    // Bob is quaints with Alice (but only knows her URL)
                    bobState.addressBookAdd(
                        Contact(
                            id = Contact.PLACEHOLDER_ID,
                            fetchUri = aliceJournalUri,
                            sigKey = null,
                            boxKey = null,
                            separation = 0,
                            handleReceived = null,
                            handleOverride = null
                        )
                    )

                    // Bob tries to fetch, and fails...
                    assertOriginallyFailsWith(EFetchNothingInCatalogFinder::class) {
                        bobState.fetching.blockingFetchAll()
                    }

                    // ...but now he at least knows Alice's keys
                    val bobsAlice = bobState.addressBookRead().contacts
                        .first { it.fetchUri == aliceJournalUri }

                    assertEquals(aliceState.keyringRead().current.boxKeys.publicKey, bobsAlice.boxKey)
                    assertEquals(aliceState.keyringRead().current.sigKeys.publicKey, bobsAlice.sigKey)
                }
            }
        }
    }

    @Test
    fun `fetch with bad data pointer hash`() {
        IntegrationHelper.withTempDir { alicePublishPath ->
            IntegrationHelper.smokeTestWithAliceAndBob(
                LocalFilesConfig(
                    journalUrl = null,
                    rootDir = alicePublishPath.toString(),
                ),
                alicePublishPath.toUri().toASCIIString(),
                tests = { env ->
                    // Publish, but then alter Alice's post so that it no
                    // longer matches.
                    env.aliceState.publishing.runInitialSync()

                    // Tamper with one of the posts.
                    //
                    // An attacker who is a valid recipient of a post but who
                    // can also tamper with files on the server (or a sneakernet
                    // journal) could replace a post with a different version
                    // encrypted with the same key.
                    val catPost = env.aliceState.repo.journal.posts.list().first {
                        it.post.readJson().persistentId == env.aliceCatPostPubId
                    }
                    val postPub = env.aliceState.publishing.lookupPostCacheEntry(catPost.id)!!
                    val postLoc = alicePublishPath.resolve("_cavern/posts/p_${postPub.hiddenId}.bin")
                    val realPost = CavPost.fromMsgpack(
                        Crypto.symmetricDecrypt(postLoc.readBytes(), postPub.symmetricKey, null)
                    )
                    postLoc.writeBytes(
                        Crypto.symmetricEncrypt(
                            realPost.copy(subject = "Cats suck!").toMsgPack(), postPub.symmetricKey,
                        ).first
                    )

                    // We should expect the decryption to fail specifically with
                    // a hashing error.
                    assertOriginallyFailsWith(ECiphertextIntegrity::class) {
                        env.bobState.fetching.blockingFetchAll()
                    }
                }
            )
        }
    }

    @Test
    fun `spec version is checked`() {
        IntegrationHelper.withTempDir { alicePublishPath ->
            IntegrationHelper.smokeTestWithAliceAndBob(
                LocalFilesConfig(
                    journalUrl = null,
                    rootDir = alicePublishPath.toString(),
                ),
                alicePublishPath.toUri().toASCIIString(),
                tests = { env ->
                    env.aliceState.publishing.runInitialSync()

                    data class Case(val spec: String, val expected: KClass<out EFetchSpec>)
                    val cases = listOf(
                        Case("cavern0", EFetchSpecUnsupported::class),
                        Case("cavern123", EFetchSpecUnknown::class)
                    )

                    // Run tests on identity file

                    val identityPath = alicePublishPath.resolve("_cavern/identity.mpk")
                    val identityOrigBytes = identityPath.readBytes()
                    val identityData = CavIdentity.fromMsgpack(identityOrigBytes)

                    for (case in cases) {
                        identityPath.writeBytes(identityData.copy(spec = case.spec).toMsgPack())
                        val t = assertOriginallyFailsWith(case.expected) {
                            env.bobState.fetching.blockingFetchAll()
                        } as EFetchSpecBadValue
                        assertEquals(case.spec, t.foundSpec)
                        assertEquals("identity", t.componentHint)
                    }
                    identityPath.writeBytes(identityOrigBytes) // restore

                    // Run tests on catalog file

                    val catalogPointer = env.aliceState.publishing
                        .lookupContactPublishing(env.aliceContactBob.id)!!.catalogPointer
                    val catalogPath = alicePublishPath.resolve("_cavern/catalogs/c_${catalogPointer.hiddenId}.bin")
                    val catalogOrigBytes = catalogPath.readBytes()
                    val catalogData = CavCatalogWrapper.fromMsgpack(
                        Crypto.symmetricDecrypt(catalogOrigBytes, catalogPointer.symmetricKey, null)
                    )

                    for (case in cases) {
                        catalogPath.writeBytes(Crypto.symmetricEncrypt(
                            catalogData.copy(spec = case.spec).toMsgPack(),
                            catalogPointer.symmetricKey
                        ).first)
                        val t = assertOriginallyFailsWith(case.expected) {
                            env.bobState.fetching.blockingFetchAll()
                        } as EFetchSpecBadValue
                        assertEquals(case.spec, t.foundSpec)
                        assertEquals("catalog", t.componentHint)
                    }
                    catalogPath.writeBytes(catalogOrigBytes) // restore

                    // Check that arbitrary bad data has its own exception

                    val unavail = assertFailsWith<EFetchSpecUnavailable> {
                        env.aliceState.fetching.assertSpec(ByteArray(5), "catalog")
                    }
                    assertEquals("Unable to determine spec version when reading catalog", unavail.message)
                    assertIs<MessagePackException>(unavail.cause)
                }
            )
        }
    }
}
