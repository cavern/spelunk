package org.timmc.spelunk

import junit.framework.TestCase.assertNull
import org.timmc.spelunk.model.Config
import org.timmc.spelunk.model.ConfigPatch
import org.timmc.spelunk.model.RsyncConfig
import org.timmc.spelunk.model.Visibility
import org.timmc.spelunk.model.cavern.CiphertextDigest
import java.math.BigInteger
import java.util.regex.Pattern
import kotlin.io.path.Path
import kotlin.io.path.deleteExisting
import kotlin.io.path.deleteIfExists
import kotlin.io.path.readText
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class UtilsTest {
    @Test
    fun base32garbage() {
        // No failure with anything in Base32 alphabet
        Utils.base32Decode("ABCDEFGHIJKLmnopqrstuvwxyz234567")

        // But, replace a letter with junk...
        val valid = Utils.base32Encode("foobar")
        val invalid = valid.substring(1) + '_'
        assertFails {
            Utils.base32Decode(invalid)
        }
    }

    @Test
    fun base32alphabet() {
        assertEquals("foobar", Utils.base32Decode("mzxw6ytboi"))
        // Case-insensitive
        assertEquals("foobar", Utils.base32Decode("MZXW6YTBOI"))
    }

    @Test
    fun base32NoPadding() {
        // Normally would have "======" at end, if using padding
        assertEquals("mzxw6ytboi", Utils.base32Encode("foobar"))

        // Can decode with or without padding (though we don't produce it)
        assertEquals("foobar", Utils.base32Decode("mzxw6ytboi"))
        assertEquals("foobar", Utils.base32Decode("mzxw6ytboi======"))
    }

    @Test
    fun base32NoLineBreaks() {
        assertEquals("a".repeat(160), Utils.base32Encode(ByteArray(100)))
    }

    @Test
    fun base32RoundTrip() {
        assertEquals("foobar", Utils.base32Decode(Utils.base32Encode("foobar")))
        assertEquals("", Utils.base32Decode(Utils.base32Encode("")))
    }

    @Test
    fun hexEncoding() {
        assertEquals("", Utils.hexEncode(byteArrayOf()))
        // This also pins encoding as uppercase
        assertEquals("0D", Utils.hexEncode(byteArrayOf(13)))
        assertEquals("0A08", Utils.hexEncode(byteArrayOf(10, 8)))
        assertEquals(listOf(), Utils.hexDecode("").toList())
        assertEquals(listOf(10.toByte(), 8.toByte()), Utils.hexDecode("0A08").toList())
        // Case-insensitive decode
        assertEquals(Utils.hexDecode("abcd").toList(), Utils.hexDecode("ABCD").toList())

        assertErrorMessage(Regex("string length not even: [0-9]+")) { Utils.hexDecode("1") }
        assertErrorMessage(Regex("string length not even: [0-9]+")) { Utils.hexDecode("123") }
        assertErrorMessage(Regex("not a hexadecimal digit: .*")) { Utils.hexDecode("0123__") }
        assertErrorMessage(Regex("not a hexadecimal digit: .*")) { Utils.hexDecode("-1") }
    }

    @Test
    fun formatDataSize() {
        // base case
        assertEquals("0 B", Utils.formatDataSize(0))
        // using 1000, not 1024
        assertEquals("999 B", Utils.formatDataSize(999))
        assertEquals("1 kB", Utils.formatDataSize(1000))
        // rounding down
        assertEquals("37 MB", Utils.formatDataSize(37001001))
        assertEquals("37 MB", Utils.formatDataSize(37990990))
        // max unit size
        assertEquals("37001 GB", Utils.formatDataSize(37001001001001))
    }

    @Test
    fun shimReplaceAll() {
        // no match
        assertEquals("abcdef", Utils.replaceAll(Pattern.compile("[0-9]"), "abcdef") { "X" })
        // whole thing
        assertEquals("X", Utils.replaceAll(Pattern.compile("[0-9]+"), "012") { "X" })
        // beginning
        assertEquals("X__", Utils.replaceAll(Pattern.compile("[0-9]+"), "012__") { "X" })
        // non-beginning, adjacency, end, group access
        assertEquals("__AZ__BBZCCCZ_DZ",
            Utils.replaceAll(Pattern.compile("[a-y]+z"), "__az__bbzcccz_dz") {
                it.group().uppercase()
            }
        )
    }

    @Test
    fun testWriteFileAtomic() {
        val dest = Path("target/spelunk_testing.txt")
        dest.deleteIfExists()

        Utils.writeFileAtomic(dest, "some text")
        assertEquals("some text", dest.readText())

        dest.deleteExisting()
    }

    @Test
    fun cutPrefix() {
        fun cutString(str: String, prefixLen: Int): Pair<String, String> {
            val (a, b) = str.toByteArray(Utils.utf8).divideAt(prefixLen)
            return a.toString(Utils.utf8) to b.toString(Utils.utf8)
        }

        assertEquals("he" to "llo", cutString("hello", 2))
        assertEquals("" to "hello", cutString("hello", 0))
        assertEquals("hello" to "", cutString("hello", 5))
        assertFailsWith(IndexOutOfBoundsException::class) { cutString("hello", 6) }
        assertFailsWith(IndexOutOfBoundsException::class) { cutString("hello", -1) }
    }

    @Test
    fun dataPatchHasChanges() {
        assertFalse(ConfigPatch().hasChanges())
        assertFalse(ConfigPatch(defaultPostVisibility = MaybePatch.Absent()).hasChanges())
        assertTrue(ConfigPatch(defaultPostVisibility = MaybePatch.Present(Visibility.QOAQ1)).hasChanges())
    }

    @Test
    fun dataPatchMinifyAgainst() {
        val emptyPatch = ConfigPatch()
        val baseline = Config(
            ownName = "me",
            defaultPostVisibility = Visibility.PUBLIC,
            publishing = RsyncConfig(
                journalUrl = "https://example.net/",
                server = "example.net",
                remoteDir = "/home/me/cavern",
            ),
            torSocksProxy = "127.0.0.1:12345",
        )
        val patchLikeBaseline = ConfigPatch(
            ownName = MaybePatch.Present("me"),
            defaultPostVisibility = MaybePatch.Present(Visibility.PUBLIC),
            publishing = MaybePatch.Present(RsyncConfig(
                journalUrl = "https://example.net/",
                server = "example.net",
                remoteDir = "/home/me/cavern",
            )),
            torSocksProxy = MaybePatch.Present("127.0.0.1:12345"),
        )

        // Empty patch stays empty
        assertEquals(emptyPatch, emptyPatch.minifyAgainst(baseline))
        // Totally equal patch becomes empty
        assertEquals(emptyPatch, patchLikeBaseline.minifyAgainst(baseline))
        // When patch differs, no minification
        assertEquals(patchLikeBaseline, patchLikeBaseline.minifyAgainst(Config(
            ownName = null,
            defaultPostVisibility = Visibility.QOAQ1,
            publishing = null,
            torSocksProxy = null,
        )))
        // When patch partially matches, strip matching parts
        assertEquals(ConfigPatch(defaultPostVisibility = MaybePatch.Present(Visibility.PUBLIC)),
            patchLikeBaseline.minifyAgainst(Config(
                ownName = baseline.ownName,
                defaultPostVisibility = Visibility.QOAQ1,
                publishing = baseline.publishing,
                torSocksProxy = baseline.torSocksProxy,
            )
        ))
    }

    @Test
    fun dataPatchApplyTo() {
        val baseline = Config(
            ownName = "me",
            defaultPostVisibility = Visibility.PUBLIC,
            publishing = RsyncConfig(
                journalUrl = "https://example.net/",
                server = "example.net",
                remoteDir = "/home/me/cavern",
            ),
            torSocksProxy = "127.0.0.1:12345",
        )
        val emptyPatch = ConfigPatch()

        val patchName = ConfigPatch(ownName = MaybePatch.Present("you"))
        val withPatchedName = baseline.copy(ownName = "you")

        assertEquals(baseline, emptyPatch.applyTo(baseline))
        assertEquals(withPatchedName, patchName.applyTo(baseline))
    }

    @Test
    fun `msgpack uint 64`() {
        // Can the msgpack library successfully pack something just out of range
        // of Long? (as a uint64)
        val longOutOfRange = BigInteger.valueOf(Long.MAX_VALUE).plus(BigInteger.ONE)
        val packed = MsgPackable.packValue(MsgPackable.deepWrap(longOutOfRange))
        assertEquals("CF8000000000000000", Utils.hexEncode(packed))

        // Can it unpack it again?
        val unpacked = MsgUnpackable.deepUnwrap(MsgUnpackable.unpackValue(packed))
        assertEquals(longOutOfRange, unpacked)

        // Just for good measure, can it unpack the largest uint64?
        val largestUint64 = BigInteger.valueOf(2).pow(64).minus(BigInteger.ONE)
        val largestUint64Bytes = Utils.hexDecode("CFFFFFFFFFFFFFFFFF")
        assertEquals(largestUint64, MsgUnpackable.deepUnwrap(MsgUnpackable.unpackValue(largestUint64Bytes)))
    }

    /**
     * If duplicate keys are allowed in maps, then msgpack data could become
     * ambiguous. If it's signed data, this could result in two people seeing
     * different values for the same authenticated bytes, leading to various
     * kinds of attacks.
     */
    @Test
    fun `msgpack repeated map keys`() {
        val twoKeysHex = "82A3666F6F05A362617206"
        assertEquals(
            mapOf("foo" to 5L, "bar" to 6L),
            MsgUnpackable.deepUnwrap(
                MsgUnpackable.unpackValue(Utils.hexDecode(twoKeysHex))
            ) as Map<*, *>
        )

        val fooHex = Utils.hexEncode("foo".toByteArray())
        val barHex = Utils.hexEncode("bar".toByteArray())
        val repeatedKeysHex = twoKeysHex.replace(fooHex, barHex)
        assertFailsWith<EMessagePackMapDuplicateKeys> {
            MsgUnpackable.deepUnwrap(
                MsgUnpackable.unpackValue(Utils.hexDecode(repeatedKeysHex))
            )
        }
    }

    @Test
    fun memoizeTest() {
        val p = "unused"
        var state = 0
        // Only implemented for unary functions, so add unused param
        val stateful = { _: String ->
            state++
        }
        val memoed = stateful.memoize()

        // [stateful] is a counter
        assertEquals(0, stateful(p))
        assertEquals(1, stateful(p))
        assertEquals(2, stateful(p))

        // [memoed] is a memoized wrapper of it
        assertEquals(3, memoed(p)) // only one that causes increment
        assertEquals(3, memoed(p))
        assertEquals(3, memoed(p))

        // [stateful] still works
        assertEquals(4, stateful(p))
        assertEquals(5, stateful(p))

        // and [memoed] still memoized
        assertEquals(3, memoed(p))
    }

    @Test
    fun `friendly class names`() {
        assertEquals("Utils", friendlyClassName(Utils::class.java))
        assertEquals("int", friendlyClassName(5.javaClass))
        assertEquals("byte[]", friendlyClassName(byteArrayOf().javaClass))
        // Not everything has a simple name, so fall back to the full version.
        assertEquals(
            "org.timmc.spelunk.UtilsTest\$friendly class names\$1",
            friendlyClassName(object : Cloneable{}.javaClass)
        )
    }

    @Test
    fun `explain branch`() {
        assertNull(Utils.explainAction(emptyList()))
        assertEquals("first true", Utils.explainAction(listOf(
            "not this" to { false },
            "first true" to { true },
            "second true" to { true },
        )))
        assertNull(Utils.explainAction(listOf(
            "nope" to { false },
            "neither" to { false },
        )))
    }
}

class BytesEqTest {
    val sampleHex = "7F5979FB78F082E8B1C676635DB8795C4AC6FABA03525FB708CB5FD68FD40C5E"
    val sampleBytes = Utils.hexDecode(sampleHex)

    @Test
    fun `container format with hashing and equality`() {
        // ensure different underlying objects, just in case
        val first = BytesEq(sampleBytes.clone())
        val second = BytesEq(sampleBytes.clone())

        assertEquals(first, second)
        assertEquals(first.hashCode(), second.hashCode())
        assertEquals(first.toString(), second.toString())

        assertEquals(sampleHex, first.asHex())

        assertTrue(sampleBytes.contentEquals(first.sharedBytes))
    }

    class SampleBytes(bytes: ByteArray): BytesEq(bytes)

    @Test
    fun `readable string format`() {
        val subclassInstance = SampleBytes(sampleBytes)
        assertEquals("SampleBytes{hex=$sampleHex}", subclassInstance.toString())
    }
}

class BytesSizedTest {
    @Test
    fun validation() {
        assertFailsWith(IllegalArgumentException::class) {
            CiphertextDigest(ByteArray(CiphertextDigest.BYTES_LEN - 1))
        }

        CiphertextDigest(ByteArray(CiphertextDigest.BYTES_LEN))

        assertFailsWith(IllegalArgumentException::class) {
            CiphertextDigest(ByteArray(CiphertextDigest.BYTES_LEN + 1))
        }
    }
}
