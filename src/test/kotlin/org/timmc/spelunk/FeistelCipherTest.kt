package org.timmc.spelunk

import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class FeistelCipherTest {

    //== Companion methods ==//

    @Test
    fun masking() {
        assertEquals(7, DodgyFeistel.bottomNSet(3))
    }

    @Test
    fun bytesAndLongs() {
        val pairs = listOf(
            0L to "0000000000000000",
            1L to "0000000000000001",
            Long.MAX_VALUE to "7FFFFFFFFFFFFFFF",
            Long.MIN_VALUE to "8000000000000000",
            -1L to "FFFFFFFFFFFFFFFF",
            8571615098323652447L to "76F47E847EF4CF5F",
        )

        for ((n, hex) in pairs) {
            assertEquals(n, DodgyFeistel.bytesToLong(Utils.hexDecode(hex)))
            assertEquals(hex, Utils.hexEncode(DodgyFeistel.longToBytes(n)))
            assertEquals(hex.takeLast(4), Utils.hexEncode(DodgyFeistel.longToBytes(n, 2)))
        }
    }

//    /**
//     * A utility for one-time use in setting up the key generation test. It will
//     * iterate through random keys until it finds one that produces a set of
//     * round keys where at least one starts with more than one zero byte.
//     */
//    fun utilFindZeroPrefixRoundKeys() {
//        val r = java.util.Random()
//        val bs = ByteArray(8)
//        val zero = 0.toByte()
//        while (true) {
//            r.nextBytes(bs)
//            val rks = DodgyFeistel.keyToRoundKeys(bs)
//            if (rks.any { it[0] == zero && it[1] == zero }) {
//                print(Utils.hexEncode(bs))
//                print(rks.map { Utils.hexEncode(it) })
//                return
//            }
//        }
//    }

    @Test
    fun keyGeneration() {
        // Chosen to have at least one round key with two leading zero bytes to
        // test for proper sizing of outputs.
        val cipherKey = Utils.hexDecode("33762E0499392BA2")
        val roundKeys = DodgyFeistel.keyToRoundKeys(cipherKey)

        assertEquals(DodgyFeistel.ROUNDS, roundKeys.size)
        // Pinned values
        assertEquals(
            listOf(
                "224BA2862666CD23", "FC1D2C3717858F47", "A77A92C870219A50",
                "E6479F2C7BD7837C", "4BEF5E95D8866211", "5F000D2A81B343F0",
                "96D75DEBBD2472F2", "D8DCEA7A60C9774A", "5E949DB8D213DC64",
                "D640F2387745F4F6",
            ),
            roundKeys.map(Utils::hexEncode)
        )
    }

    //== Instance methods ==//

    @Test
    fun permutation() {
        val cipher = DodgyFeistel(
            key = "some arbitrary seed".encodeToByteArray(),
            totalBits = 6
        )

        val plain = 0L until 64L

        val encrypted = plain.map { cipher.encrypt(it) }
        assertEquals(plain.toList(), encrypted.sorted())
        assertNotEquals(plain.toList(), encrypted) // for good choice of seed

        val decrypted = encrypted.map { cipher.decrypt(it) }
        assertEquals(plain.toList(), decrypted)
    }

    /**
     * Tests corner cases of encryption and decryption around sign bits, using
     * 64 bits. Also serves to pin values to prevent regressions.
     */
    @Test
    fun encryptionRoundTripFull() {
        val f = DodgyFeistel(
            key = "a secret key".encodeToByteArray(),
            totalBits = 64
        )
        val pairs: List<Pair<Long, Long>> = listOf(
            42L to 5870314848404651469L,
            0L to -1330891009618918194L,
            -1L to 4276603277404544167L,
            Long.MAX_VALUE to 2440378535247882309L,
            Long.MIN_VALUE to 7396349017577991696L,
        )
        for ((plain, cipher) in pairs) {
            val encrypted = f.encrypt(plain)
            val decrypted = f.decrypt(encrypted)
            assertEquals(cipher, encrypted)
            assertEquals(plain, decrypted)
        }
    }

    /**
     * Pins values in a smaller bit size
     */
    @Test
    fun encryptionRoundTripSmall() {
        val f = DodgyFeistel(
            key = "another secret key".encodeToByteArray(),
            totalBits = 24
        )
        // Pinned values:
        val pairs: List<Pair<Long, Long>> = listOf(
            42L to 14284396L,
            0L to 10355312L,
            // 16777215 == 2^24 - 1
            16777215L to 2219142L,
        )
        for ((plain, cipher) in pairs) {
            val encrypted = f.encrypt(plain)
            val decrypted = f.decrypt(encrypted)
            assertEquals(cipher, encrypted)
            assertEquals(plain, decrypted)
        }
    }
}
