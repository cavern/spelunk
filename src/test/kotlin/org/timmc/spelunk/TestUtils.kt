package org.timmc.spelunk

import com.google.common.base.Throwables
import org.apache.log4j.LogManager
import org.apache.log4j.PatternLayout
import org.apache.log4j.WriterAppender
import java.io.StringWriter
import kotlin.reflect.KClass
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull
import kotlin.test.assertTrue
import kotlin.time.measureTimedValue

/**
 * Assert that the block throws an exception with this class and message.
 *
 * (Like assertFails, but actually checks the error message.)
 */
fun <T : Throwable> assertErrorMessage(expectedClass: KClass<T>, expectedMessage: String, block: () -> Unit) {
    val t = assertFailsWith(expectedClass, block)
    assertEquals(expectedMessage, t.message)
}

/**
 * Assert that the block throws an exception with this message.
 *
 * (Like assertFails, but actually checks the error message.)
 */
fun assertErrorMessage(expectedMessage: String, block: () -> Unit) {
    val t = assertFails(block)
    val message = t.message
    assertEquals(
        expected = expectedMessage, actual = message,
        message = "Message was not \"$expectedMessage\": $t"
    )
}

/**
 * Assert that the block throws an exception with a message fully matching the
 * pattern.
 */
fun assertErrorMessage(expectedMessage: Regex, block: () -> Unit) {
    val t = assertFails(block)
    val message = t.message
    assertNotNull(message, "Message was null: $t")
    assertTrue(
        expectedMessage.matches(message),
        "Message did not match expected pattern \"$expectedMessage\" in error: $t"
    )
}

/**
 * Assert that the given exception class is in the traceback, and return it.
 */
fun <T: Throwable> assertOriginallyFailsWith(expectedClass: KClass<T>, block: () -> Unit): Throwable {
    val t = assertFails(block)
    val chain = Throwables.getCausalChain(t)
    val found = chain.filterIsInstance(expectedClass.java).firstOrNull()
    assertNotNull(
        found,
        "Expected to find $expectedClass in traceback: ${chain.map { it.javaClass.name }}"
    )
    return found
}

/**
 * Capture log output in some unspecified format.
 */
fun captureLogs(block: () -> Unit): String {
    val writer = StringWriter()
    val appender = WriterAppender(PatternLayout(), writer)
    LogManager.getRootLogger().addAppender(appender)
    try {
        block()
    } finally {
        LogManager.getRootLogger().removeAppender(appender)
    }
    return writer.toString()
}

/**
 * Print message about duration of [block] to stdout. The [description] should
 * use the -ing form of verbs, e.g. "reticulating splines".
 */
fun <R> withPrintDuration(description: String, block: () -> R?): R? {
    val result = measureTimedValue(block)
    println("$description took ${result.duration}")
    return result.value
}

// Test vector from https://datatracker.ietf.org/doc/html/rfc7748.html#section-6.1
val TEST_ALICE_BOX = SodiumBoxKeyPair(
    SodiumBoxPublicKey(Utils.hexDecode("8520f0098930a754748b7ddcb43ef75a0dbf3a0d26381af4eba4a98eaa9b4e6a")),
    SodiumBoxPrivateKey(Utils.hexDecode("77076d0a7318a57d3c16c17251b26645df4c2f87ebc0992ab177fba51db92c2a")),
)
val TEST_BOB_BOX = SodiumBoxKeyPair(
    SodiumBoxPublicKey(Utils.hexDecode("de9edb7d7b7dc1b4d35b61c2ece435373f8343c85b78674dadfc7e146f882b4f")),
    SodiumBoxPrivateKey(Utils.hexDecode("5dab087e624a8a4b79e17f8b83800ee66f3bb1292618b6fd1c2f8b27ff88e0eb")),
)
val TEST_ALICE_BOB_SHARED_SECRET = Utils.hexDecode("4a5d9d5ba4ce2de1728e3bf480350f25e07e21c947d19e3376f09b3c1e161742")
