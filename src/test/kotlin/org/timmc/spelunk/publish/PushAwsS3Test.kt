package org.timmc.spelunk.publish

import org.junit.Test
import org.timmc.spelunk.model.AwsS3Config
import kotlin.test.assertEquals

class PushAwsS3Test {
    @Test
    fun `makeFilesKeyPrefix regularizes slashes`() {
        fun makeConfig(prefix: String): AwsS3Config {
            return AwsS3Config(
                journalUrl = "https://test-bucket.example.net/",
                bucket = "test-bucket",
                keyPrefix = prefix,
                iamKeyId = "key-id",
                iamKeySecret = "key-secret",
                region = "us-east-1"
            )
        }

        assertEquals("", PushAwsS3.makeFilesKeyPrefix(makeConfig("")))
        assertEquals("", PushAwsS3.makeFilesKeyPrefix(makeConfig("/")))
        assertEquals("foo/bar/", PushAwsS3.makeFilesKeyPrefix(makeConfig("foo/bar")))
        assertEquals("foo/bar/", PushAwsS3.makeFilesKeyPrefix(makeConfig("foo/bar/")))
        assertEquals("foo/bar/", PushAwsS3.makeFilesKeyPrefix(makeConfig("/foo/bar")))
        assertEquals("foo/bar/", PushAwsS3.makeFilesKeyPrefix(makeConfig("/foo/bar/")))
    }
}
