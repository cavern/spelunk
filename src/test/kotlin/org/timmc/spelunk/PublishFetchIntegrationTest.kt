package org.timmc.spelunk

import org.junit.Test
import org.timmc.spelunk.IntegrationHelper.fromEnv
import org.timmc.spelunk.IntegrationHelper.smokeTestWithAliceAndBob
import org.timmc.spelunk.IntegrationHelper.standardSmokeTests
import org.timmc.spelunk.IntegrationHelper.withTempDir
import org.timmc.spelunk.model.AwsS3Config
import org.timmc.spelunk.model.ContactPatch
import org.timmc.spelunk.model.LocalFilesConfig
import org.timmc.spelunk.model.RsyncConfig
import org.timmc.spelunk.model.WebDavConfig
import org.timmc.spelunk.model.cavern.CavCatalogPointer

/**
 * Integration tests for publishing and fetching that require network access and
 * credentials configured in the environment.
 */
class PublishFetchIntegrationTest {
    @Test
    fun roundtripAwsS3() {
        smokeTestWithAliceAndBob(
            AwsS3Config(
                journalUrl = fromEnv("SPELUNK_TEST_S3_JOURNAL"),
                bucket = fromEnv("SPELUNK_TEST_S3_PUB_BUCKET"),
                keyPrefix = fromEnv("SPELUNK_TEST_S3_PUB_PREFIX"),
                region = fromEnv("SPELUNK_TEST_S3_PUB_REGION"),
                iamKeyId = fromEnv("SPELUNK_TEST_S3_PUB_KEY_ID"),
                iamKeySecret = fromEnv("SPELUNK_TEST_S3_PUB_KEY_SECRET")
            ),
            fromEnv("SPELUNK_TEST_S3_JOURNAL"),
            tests = ::standardSmokeTests,
        )
    }

    @Test
    fun roundtripLocalFiles() {
        withTempDir { alicePublishPath ->
            smokeTestWithAliceAndBob(
                LocalFilesConfig(
                    journalUrl = null,
                    rootDir = alicePublishPath.toString(),
                ),
                alicePublishPath.toUri().toASCIIString(),
                tests = ::standardSmokeTests,
            )
        }
    }

    @Test
    fun roundtripRsync() {
        smokeTestWithAliceAndBob(
            RsyncConfig(
                journalUrl = fromEnv("SPELUNK_TEST_RSYNC_JOURNAL"),
                server = fromEnv("SPELUNK_TEST_RSYNC_PUB_SERVER"),
                remoteDir = fromEnv("SPELUNK_TEST_RSYNC_PUB_DIR"),
                user = fromEnv("SPELUNK_TEST_RSYNC_PUB_USER")
            ),
            fromEnv("SPELUNK_TEST_RSYNC_JOURNAL"),
            tests = ::standardSmokeTests,
        )
    }

    @Test
    fun roundtripWebDAV() {
        smokeTestWithAliceAndBob(
            WebDavConfig(
                journalUrl = fromEnv("SPELUNK_TEST_WEBDAV_JOURNAL"),
                url = fromEnv("SPELUNK_TEST_WEBDAV_PUB_URL"),
                username = fromEnv("SPELUNK_TEST_WEBDAV_PUB_USER"),
                password = fromEnv("SPELUNK_TEST_WEBDAV_PUB_PASS")
            ),
            fromEnv("SPELUNK_TEST_WEBDAV_JOURNAL"),
            tests = ::standardSmokeTests,
        )
    }

    // Tests retry on missing/wrong catalog
    @Test
    fun fetchWithWrongHiddenId() {
        withTempDir { alicePublishPath ->
            smokeTestWithAliceAndBob(
                LocalFilesConfig(
                    journalUrl = null,
                    rootDir = alicePublishPath.toString(),
                ),
                alicePublishPath.toUri().toASCIIString(),
                tests = { env ->
                    // Publish and fetch as usual
                    env.aliceState.publishing.runInitialSync()
                    env.bobState.fetching.blockingFetchAll()

                    // Set the hiddenID to something incorrect...
                    val wrong = CavCatalogPointer.new()
                    env.bobState.fetching.setCachedContactsCatalogPointerForMe(env.bobContactAlice, wrong)
                    // ...and try fetching again.
                    env.bobState.fetching.blockingFetchAll()
                }
            )
        }
    }

    // Tests retry on outdated public key
    @Test
    fun fetchWithOldPublicKey() {
        withTempDir { alicePublishPath ->
            smokeTestWithAliceAndBob(
                LocalFilesConfig(
                    journalUrl = null,
                    rootDir = alicePublishPath.toString(),
                ),
                alicePublishPath.toUri().toASCIIString(),
                tests = { env ->
                    // Publish and fetch as usual
                    env.aliceState.publishing.runInitialSync()
                    env.bobState.fetching.blockingFetchAll()

                    // Pretend we had an old encryption key for the contact...
                    env.bobState.addressBookPatchContact(
                        env.bobContactAlice.id,
                        ContactPatch(
                            boxKey = MaybePatch.Present(Crypto.createBoxKeys().publicKey)
                        )
                    )
                    // ...and try fetching again.
                    env.bobState.fetching.blockingFetchAll()
                }
            )
        }
    }
}
