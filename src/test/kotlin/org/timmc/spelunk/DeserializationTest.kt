package org.timmc.spelunk

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

val sampleData = mapOf(
    "color" to "red",
    "count" to 5,
    "tags" to listOf("foo", "bar"),
    "props" to mapOf("width" to 1, "height" to 2),
    "none" to null,
)

val sampleContext = AnyValueContext(sampleData, ".in.any")
val sampleMapContext = MapContext(sampleData, ".in.map")

class DeserializationTest {

    @Test
    fun basicCasting() {
        assertEquals(5L, AnyValueContext(5L, "").cast<Long>())
        assertEquals<Map<*, *>>(sampleData, sampleContext.cast<Map<*, *>>())
    }

    @Test
    fun castingErrors() {
        // Basic mismatch, root
        assertFailsWith<EDeserializeTypeMismatch> {
            AnyValueContext(5, "").cast<String>()
        }.let {
            assertEquals("root of data structure", it.where)
            assertEquals<Class<*>>(String::class.java, it.expectedClass)
            assertEquals<Class<*>?>(java.lang.Integer::class.java, it.foundClass)
            assertEquals(
                "Value had wrong type at root of data structure; " +
                    "expected java.lang.String but found java.lang.Integer",
                it.message,
            )
        }

        // Null mismatch, deeper
        assertFailsWith<EDeserializeTypeMismatch> {
            AnyValueContext(null, ".top.inner").cast<String>()
        }.let {
            assertEquals("path `.top.inner`", it.where)
            assertEquals(null, it.foundClass)
            assertEquals(
                "Value had wrong type at path `.top.inner`; " +
                    "expected java.lang.String but found null",
                it.message,
            )
        }

        // Convenience functions
        assertFailsWith<EDeserializeTypeMismatch> {
            AnyValueContext("hi", ".top.inner").asList()
        }.let {
            assertEquals("path `.top.inner`", it.where)
            assertEquals<Class<*>?>(String::class.java, it.foundClass)
        }
        assertFailsWith<EDeserializeTypeMismatch> {
            AnyValueContext("hi", ".top.inner").asMap()
        }.let {
            assertEquals("path `.top.inner`", it.where)
            assertEquals<Class<*>?>(String::class.java, it.foundClass)
        }

        // Not a mismatch to cast null if type is nullable
        assertNull(
            AnyValueContext(null, ".top.inner").cast<String?>()
        )

        // Not a mismatch to have wrong type parameters (due to type erasure)
        assertEquals<List<*>>(
            listOf(5),
            AnyValueContext(listOf(5), ".top.inner").cast<List<String>>()
        )
    }

    @Test
    fun mapRetrievalGeneric() {
        assertEquals("red", sampleMapContext.get("color").cast<String>())
        assertEquals(null, sampleMapContext.get("none").cast<String?>())

        assertFailsWith<EDeserializeMissingElement> {
            sampleMapContext.get("missing")
        }.let {
            assertEquals(".in.map.missing", it.where)
            assertEquals("Missing expected value at `.in.map.missing`", it.message)
        }
    }

    @Test
    fun mapRetrievalOptional() {
        assertEquals("red", sampleMapContext.getOptional("color")?.cast<String>())
        assertEquals(null, sampleMapContext.getOptional("none"))
        assertEquals(null, sampleMapContext.getOptional("missing"))
    }

    /** getList, getMap, getAs */
    @Test
    fun mapConvenienceOptions() {
        assertEquals(
            listOf("foo", "bar"),
            sampleMapContext.getList("tags").items().map { it.cast<String>() }
        )
        assertEquals(
            2,
            sampleMapContext.getMap("props").getAs<Int>("height")
        )
    }

    @Test
    fun contextPropagation() {
        // Casting context propagates path
        assertEquals("", AnyValueContext(listOf("hi"), "").asList().path)
        assertEquals(".outer", AnyValueContext(mapOf(1 to 2), ".outer").asMap().path)

        // Map key append
        assertEquals(".in.map.color", sampleMapContext.get("color").path)
        assertEquals(".in.map.color", sampleMapContext.getOptional("color")?.path)
        assertEquals(".in.map.props", sampleMapContext.getMap("props").path)
        assertEquals(".in.map.tags", sampleMapContext.getList("tags").path)

        // List index append
        assertEquals(
            ".in.map.tags[0]",
            sampleMapContext.getList("tags").items().first().path
        )
    }
}
