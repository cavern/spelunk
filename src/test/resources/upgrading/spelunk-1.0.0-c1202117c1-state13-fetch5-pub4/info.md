Spelunk 1.0.0 (commit c1202117c1) for migrations testing.

Database versions:

- Settings: 13
- Fetching: 5
- Publishing: 4

- Started with `src/test/resources/migrations/v2.0-1ca251c0/repo/`, since deleted
- Ran `spelunk-1.0.0` on this repo to allow it to upgrade from v2.0 to v13.0
- Configured to publish to `/tmp/spelunk-migrations-test-publish/`
- Added contact "Myself", reading from
  `file:///tmp/spelunk-migrations-test-publish/`
- Published and fetched
