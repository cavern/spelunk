Spelunk v13.0 repo (commit c1202117c1) for migrations testing.

Database versions:

- Settings: 13
- Fetching: 5
- Publishing: 5

- Started with `src/test/resources/migrations/spelunk-1.0.0-c1202117c1-state13-fetch5-pub4/`
- Ran `spelunk-1.0.1` on this repo to allow it to upgrade from Publishing v4 to v5
- Republished, in order to repopulate publishing database
