/**
 * Error reporting support for deserialization of generic data structures.
 *
 * - Nodes are wrapped in context values that track and append path descent info.
 * - Missing values or values of an unexpected type produce exceptions that
 *   report the location and type information.
 */

package org.timmc.spelunk

/**
 * Deserialization context for an arbitrary nullable value.
 */
class AnyValueContext(
    val node: Any?,
    val path: String,
) {
    /**
     * Cast the current node to the specified type.
     *
     * Prefer [asMap] if appropriate, as that will propagate
     * context information.
     *
     * Caution: Do not rely on type parameters in T. For example, passing
     * `List<Int>` will only guarantee the return type is a List. The elements
     * could be something else, due to type erasure.
     */
    inline fun <reified T: Any?> cast(): T {
        if (node is T) {
            return node
        } else {
            val where = if (path.isEmpty()) {
                "root of data structure"
            } else {
                "path `$path`"
            }
            throw EDeserializeTypeMismatch(where, T::class.java, node?.javaClass)
        }
    }

    /**
     * Cast to a `Map<*, *>`, wrapped in a context.
     */
    fun asMap(): MapContext {
        return MapContext(map = cast<Map<*, *>>(), path = path)
    }

    /**
     * Cast to a `List<*>`, wrapped in a context.
     */
    fun asList(): ListContext {
        return ListContext(nodes = cast<List<*>>(), path = path)
    }
}

/**
 * Deserialization context for a map value.
 */
class MapContext(
    val map: Map<*, *>,
    val path: String,
) {
    /**
     * Get a nullable value from the map by key, wrapped in a context.
     *
     * This may not be as useful as [getAs] or [getOptional].
     */
    fun get(key: String): AnyValueContext {
        val v = map.getOrDefault(key, SENTINEL_MISSING)
        if (v === SENTINEL_MISSING) {
            throw EDeserializeMissingElement("$path.$key")
        } else {
            return AnyValueContext(node = v, path = "$path.$key")
        }
    }

    /**
     * Get a non-null value from the map by key, casting it to type T or
     * throwing an [EDeserialize] if it is missing or of the wrong (concrete)
     * type.
     *
     * Prefer [getMap] and [getList] if appropriate, as those will propagate
     * context information.
     *
     * Caution: Do not rely on type parameters in T. For example, passing
     * `List<Int>` will only guarantee the return type is a List. The elements
     * could be something else, due to type erasure.
     */
    inline fun <reified T: Any> getAs(key: String): T {
        return get(key).cast<T>()
    }

    /**
     * Get a value (with context) by key, or null if key was missing or value
     * was null.
     */
    fun getOptional(key: String): AnyValueContext? {
        val v = map[key] // null if missing
        return if (v == null) {
            null
        } else {
            AnyValueContext(v, "$path.$key")
        }
    }

    /**
     * Convenience wrapper for [getAs] that gets a value from the map by key,
     * casting it to a Map and wrapping it in another context.
     */
    fun getMap(key: String): MapContext {
        return get(key).asMap()
    }

    /**
     * Convenience wrapper for [getAs] that gets a value from the map by key,
     * casting it to a List and wrapping it in another context.
     */
    fun getList(key: String): ListContext {
        return get(key).asList()
    }

    companion object {
        val SENTINEL_MISSING = Object()
    }
}

/**
 * Deserialization context for a list value.
 */
class ListContext(
    val nodes: List<*>,
    val path: String,
) {
    /**
     * Wrap items of list in context values.
     */
    fun items(): List<AnyValueContext> {
        return nodes.mapIndexed { index, el ->
            AnyValueContext(node = el, path = "$path[$index]")
        }
    }
}

/** Generic deserialization error. */
abstract class EDeserialize(msg: String): RuntimeException(msg)

class EDeserializeTypeMismatch(val where: String, val expectedClass: Class<*>, val foundClass: Class<*>?):
    EDeserialize(
        "Value had wrong type at $where; expected ${expectedClass.canonicalName} " +
        "but found ${foundClass?.canonicalName}"
    )

class EDeserializeMissingElement(val where: String):
    EDeserialize("Missing expected value at `$where`")
