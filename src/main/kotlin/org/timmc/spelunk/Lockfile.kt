package org.timmc.spelunk

import java.io.Closeable
import java.io.IOException
import java.nio.file.FileAlreadyExistsException
import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.Path
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Utility for creating lockfiles.
 */
object Lockfile {
    // TODO: Check on other approaches, such as writing a temp file
    // elsewhere, then creating the lock file as a symlink to it.
    // e.g. https://github.com/ahoward/lockfile

    /**
     * Create a lock file based on the process ID to prevent concurrent access.
     *
     * Caller is responsible for reliably calling [LockfileResource.close] on
     * the returned object; it is recommended to do this via
     * `Lockfile.createLockFile(path).use { ... }`
     *
     * If an invalid lockfile is found, a [LockfileExistsErr] exception is
     * thrown. Other exceptions may be thrown as well, especially
     * [IOException].
     */
    fun createLockFile(lockPath: Path): LockfileResource {
        val lockData = os.processId().toByteArray(Charsets.UTF_8)

        // Atomically create file, or throw if failed
        try {
            Files.createFile(lockPath)
        } catch (e: FileAlreadyExistsException) {
            creationFailureThrow(lockPath)
        }
        // ^ may throw various other IOException as well

        val lock = LockfileResource(lockPath)

        // Write PID to file, or throw (but attempt to delete it first)
        lock.closeIfThrows { Files.write(lockPath, lockData) }

        return lock
    }

    /**
     * Given a lockfile path that we couldn't create ourselves, attempt the
     * most informative possible explanation so the user can remedy it
     * themselves.
     */
    private fun creationFailureThrow(lockPath: Path): Nothing {
        // OK, it's already locked, but is the lock valid?
        try {
            val lockData = Files.readAllBytes(lockPath).toString(Charsets.UTF_8)
            val modified = Files.getLastModifiedTime(lockPath, LinkOption.NOFOLLOW_LINKS)
                .toInstant().formatDefault()
            if (lockData.matches(Regex("^[0-9]+$"))) {
                // It might be a PID. Does the process *exist*?
                if (os.processExists(lockData)) {
                    throw LockfileExistsErr("""
                        Lockfile exists and is locked by process $lockData, which is
                        currently running. The lockfile was created or last modified
                        $modified. Path: $lockPath
                        """.trimIndent().wrap())
                } else {
                    // We *could* try deleting the file, since the process
                    // is dead, but that risks a race condition if another
                    // instance comes to the same conclusion at the same
                    // time, at which point one process will delete the
                    // other's lockfile. Just fail with a good message.
                    throw LockfileExistsErr(
                        """
                        Lockfile exists, but process $lockData appears not to be running.
                        The lockfile was created or last modified $modified.
                        You might try deleting it if you're sure no other instances are
                        running and might modify the repository concurrently. Path: $lockPath
                        """.trimIndent().wrap()
                    )
                }
            } else {
                // Doesn't even look like a PID
                throw LockfileExistsErr("Lockfile exists, but is malformed. Path: $lockPath")
            }
        } catch (pass: LockfileExistsErr) {
            throw pass
        } catch (e: Exception) {
            throw LockfileExistsErr("Lockfile exists, but could not be read. Path: $lockPath", e)
        }
    }
}

/** Indicates that a lock was already present. */
class LockfileExistsErr(msg: String, cause: Throwable? = null): RuntimeException(msg, cause)

/**
 * A representation of the lockfile. The creator of the resource must
 * ensure that [close] is eventually called. It is safe to call [close]
 * multiple times.
 */
class LockfileResource(private val lockPath: Path): Closeable {
    /**
     * Whether the lockfile has been "closed" (deleted)
     */
    private val closed = AtomicBoolean(false)

    override fun close() {
        // This check is necessary to ensure I don't try to delete the
        // lockfile more than once. Once I delete it, someone else could
        // recreate it, and it would be bad to delete *their* lockfile!
        if (!closed.getAndSet(true)) {
            Files.deleteIfExists(lockPath)
        }
    }
}
