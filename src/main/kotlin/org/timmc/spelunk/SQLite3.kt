package org.timmc.spelunk

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transactionManager
import org.sqlite.SQLiteDataSource
import java.nio.file.Path
import java.sql.Connection

/**
 * Shared database utilities for SQLite 3.
 */
object SQLite3 {
    /**
     * Open an Exposed database connection to a SQLite DB, possibly enforced
     * to be read-only.
     */
    fun database(path: Path, allowWrite: Boolean): Database {
        val ds = SQLiteDataSource().apply {
            // This is from before Exposed had a way to set connections as
            // read-only, but probably no harm to keep it.
            setReadOnly(!allowWrite)
            url = "jdbc:sqlite:$path"
        }

        fun configureConnection(db: Database) {
            db.transactionManager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE
            db.transactionManager.defaultReadOnly = !allowWrite
        }

        val realDB = Database.connect(ds).also { configureConnection(it) }

        // Exposed has a nasty pitfall of connecting to the last created
        // Database if a Database is not explicitly passed to its APIs. There's
        // no way to tell it not to do that, and if I accidentally made a SELECT
        // outside of a transaction that explicitly names the db, there could be
        // disastrous results (e.g. if there are two DBs with the same table,
        // and I insert into the most recently connected-to one, rather than the
        // correct one.) The workaround here is to create a fake in-memory DB
        // right after creating any real DB, so that this one gets used by any
        // API call where I've left off the db arg, pretty much guaranteeing an
        // exception.
        //
        // Configuring the fake connection with the same isolation level and
        // writable/read-only settings is not necessary, but keeps things
        // consistent just in case.
        Database.connect("jdbc:sqlite::memory:", "org.sqlite.JDBC")
            .also { configureConnection(it) }

        return realDB
    }
}

/**
 * Upsert into the table: Insert if the conflictKeys do not conflict, update
 * otherwise.
 *
 * Adapted from https://github.com/JetBrains/Exposed/issues/167#issuecomment-480199613
 * (author Maxr1998: Max Rumpf)
 */
fun <T: Table> T.upsert(vararg conflictKeys: Column<*>, body: T.(InsertStatement<Number>) -> Unit) =
    UpsertStatement<Number>(this, *conflictKeys).apply {
        body(this)
        execute(TransactionManager.current())
    }

// Adapted from https://github.com/JetBrains/Exposed/issues/167#issuecomment-480199613
class UpsertStatement<K: Any>(table: Table, private vararg val conflictKeys: Column<*>)
    : InsertStatement<K>(table)
{
    override fun prepareSQL(transaction: Transaction): String {
        val tm = TransactionManager.current()
        // The special "excluded" table complies with sqlite's SQL, but not
        // postgres's. https://www.sqlite.org/lang_UPSERT.html
        val updateSetter = table.columns.joinToString(", ") {
            val quotedColumn = tm.identity(it)
            "$quotedColumn = EXCLUDED.$quotedColumn"
        }
        val conflictKeysList = conflictKeys.joinToString(", ") { tm.identity(it) }
        return "${super.prepareSQL(transaction)} ON CONFLICT ($conflictKeysList) DO UPDATE SET $updateSetter"
    }
}
