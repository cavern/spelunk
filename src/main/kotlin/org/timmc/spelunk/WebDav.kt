package org.timmc.spelunk

import com.github.sardine.DavResource
import com.github.sardine.Sardine
import com.github.sardine.SardineFactory
import com.github.sardine.impl.SardineException
import mu.KotlinLogging
import org.timmc.johnny.Urls
import org.timmc.spelunk.model.WebDavConfig
import java.io.IOException
import java.io.InputStream
import java.net.URL

private val logger = KotlinLogging.logger {}

/**
 * Encapsulate calls to a WebDAV server.
 *
 * Paths are relative to the configured base path.
 *
 * Use `WebDav.make()` to instantiate this class.
 */
class WebDav(
    /** WebDAV client instance */
    val sardine: Sardine,
    /** URL of server with just scheme, hostname, and optional port */
    val server: String,
    /**
     * Server base path, starts and ends with slash.
     */
    val basePath: String
) {
    companion object {
        /**
         * Build a WebDav instance from the current configuration, or return
         * null if webdav publishing is not configured.
         */
        fun make(wdcfg: WebDavConfig): WebDav {
            val webdav = SardineFactory.begin(wdcfg.username, wdcfg.password)

            // Pull apart URL and ignore any userinfo, query, or fragment
            val url = Urls.parse(wdcfg.url)
            val server = Urls.from(url.scheme, url.host).withPort(url.port).format()
            val slashyPath = url.path.withTrailingSlash(true).format()

            // Required for streaming PUT, since otherwise the client will have to
            // retry its request when it gets a 401 demanding auth, and that
            // doesn't work with a PUT.
            //
            // ...except there's a bug in how Sardine sets up its auth cache in
            // the presence of default ports:
            // https://github.com/lookfirst/sardine/issues/285
            //
            // This is only a problem for the first call in the session, as a
            // GET or MKCOL or whatever would be replayable, and after that
            // point the calls always have an Authorization header attached
            // because the Apache HTTP client's auth cache has been populated.
            //
            // I'm not sure why, but the only overload of `enablePreemptiveAuthentication`
            // that works is the one that takes a URL, at least when I have a
            // URL that doesn't specify a port.
            // TODO: Check if this works when a port *is* explicitly specified.
            webdav.enablePreemptiveAuthentication(URL(server))
            return WebDav(webdav, server = server, basePath = slashyPath)
        }
    }

    fun urlOf(relPath: String) = "$server$basePath$relPath"

    /**
     * Create a directory, or do nothing if path already exists. (No
     * guarantee that the existing path is a *directory*, of course.)
     */
    fun ensureDirectory(relPath: String) {
        try {
            val url = urlOf(relPath)
            logger.info("WebDAV create-dir $url")
            sardine.createDirectory(url)
        } catch (e: SardineException) {
            // 405: Method Not Allowed (resource already exists)
            if (e.statusCode != 405)
                throw e
        }
    }

    /**
     * Stream data to a path.
     */
    fun put(relPath: String, dataStream: InputStream) {
        val url = urlOf(relPath)
        logger.info("WebDAV put $url")
        sardine.put(url, dataStream)
    }

    /**
     * List files in a directory. Returns just the list of child resources in
     * the directory, not the directory itself.
     */
    fun listFilesInDir(relDirPath: String): List<DavResource> {
        val expectedPathPrefix = "$basePath$relDirPath/"
        val url = "$server$expectedPathPrefix"
        logger.info("WebDAV mkdir $url")
        return sardine.list(url, 1).mapNotNull {
            val resourcePath = it.path
            if (!resourcePath.startsWith(expectedPathPrefix)) {
                throw IOException("Unexpected path prefix on resource when listing files via webdav")
            }
            // Exclude the directory itself from the listing
            if (resourcePath == expectedPathPrefix) {
                null
            } else {
                it
            }
        }
    }

    /**
     * Provide an input stream to a remote file (caller must close) or throw
     * IOException if cannot. Returns null if file is not present.
     */
    fun readFile(relPath: String): InputStream? {
        val url = urlOf(relPath)
        logger.info("WebDAV read $url")
        return try {
            sardine.get(url)
        } catch (e: SardineException) {
            if (e.statusCode == 404) {
                null
            } else {
                throw e
            }
        }
    }

    /**
     * Delete a file on the server, if it exists.
     */
    fun delete(relPath: String) {
        val url = urlOf(relPath)
        logger.info("WebDAV delete $url")
        try {
            sardine.delete(url)
        } catch (e: SardineException) {
            if (e.statusCode != 404)
                throw e
        }
    }
}
