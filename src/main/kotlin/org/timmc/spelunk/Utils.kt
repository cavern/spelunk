package org.timmc.spelunk

import com.google.common.base.Throwables
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.apache.commons.codec.CodecPolicy
import org.apache.commons.codec.binary.Base32
import org.msgpack.core.MessageFormat
import org.msgpack.core.MessagePack
import org.msgpack.core.MessagePack.DEFAULT_UNPACKER_CONFIG
import org.msgpack.core.MessagePack.UnpackerConfig
import org.msgpack.value.Value
import org.msgpack.value.ValueFactory
import org.msgpack.value.ValueType
import org.timmc.spelunk.model.PublishLocationConfig
import org.timmc.spelunk.model.cavern.CiphertextDigest
import java.io.Closeable
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.math.BigInteger
import java.nio.charset.Charset
import java.nio.charset.CodingErrorAction
import java.nio.file.Files
import java.nio.file.Path
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.HexFormat
import java.util.concurrent.ConcurrentHashMap
import java.util.regex.MatchResult
import java.util.regex.Pattern
import kotlin.math.min
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.instanceParameter
import kotlin.reflect.jvm.javaGetter
import kotlin.system.exitProcess
import kotlin.time.TimeSource

class Utils {
    companion object {
        /** UTF-8 character set */
        val utf8: Charset = Charset.forName("UTF-8")

        /** Moshi instance configured for parsing our model classes. */
        val json: Moshi = Moshi.Builder()
            .add(PublishLocationConfig.moshiAdapter)
            .add(CiphertextDigest.hexJsonAdapter)
            .add(SodiumSecretStreamKey.hexJsonAdapter)
            // This adapter goes last, and covers generic data classes:
            .add(KotlinJsonAdapterFactory())
            .build()

        // All defaults except for CodecPolicy.STRICT (which actually isn't all
        // that strict, since it allows garbage at the end as long as it isn't
        // in-alphabet!)
        private val b32 = Base32(0, null, false, '='.code.toByte(), CodecPolicy.STRICT)

        // Permitted characters in Base32
        private val b32chars = Regex("[a-zA-Z2-7]*")

        /**
         * Encode to RFC 4648 Base32 assuming UTF-8, but output is lowercase
         * and does not use padding. No line breaks.
         */
        fun base32Encode(plaintext: String): String {
            return base32Encode(plaintext.toByteArray(utf8))
        }

        /**
         * Encode to RFC 4648 Base32, but output is lowercase
         * and does not use padding. No line breaks.
         */
        fun base32Encode(plainBytes: ByteArray): String {
            val standard = b32.encodeAsString(plainBytes)
            return standard.replace(Regex("=+"), "").lowercase()
        }

        /**
         * Read from Base32; case-insensitive and padding optional.
         */
        fun base32Decode(encoded: String): String {
            // We don't produce or check padding chars, but should ignore if provided.
            val toValidate = encoded.trimEnd('=')
            // Now that padding is removed, *only* allow base32 chars.
            if (!b32chars.matches(toValidate)) {
                throw IllegalArgumentException("Could not decode Base32: Unexpected characters")
            }
            return b32.decode(toValidate).toString(utf8)
        }

        private val hex = HexFormat.of().withUpperCase()

        /**
         * Encode bytes as a hexadecimal string.
         */
        fun hexEncode(plainBytes: ByteArray): String {
            return hex.formatHex(plainBytes)
        }

        /**
         * Decode hexadecimal string to bytes.
         */
        fun hexDecode(hexStr: String): ByteArray {
            return hex.parseHex(hexStr)
        }

        /**
         * Given a program-ending exception, print a stack trace, or a nicer
         * error message if possible (e.g. if SpelunkExit is in the chain),
         * then exit the process.
         *
         * The program does not continue past this point.
         */
        fun die(t: Throwable): Nothing {
            try {
                // Unwrap a message-bearing SpelunkExit if possible
                val cleanMessageE = Throwables.getCausalChain(t).firstOrNull {
                    it is SpelunkExit
                }
                // Type check in guard is redundant, but Kotlin compiler doesn't know
                if (cleanMessageE == null || cleanMessageE !is SpelunkExit) {
                    t.printStackTrace()
                } else {
                    System.err.println(cleanMessageE.message)
                }
            } finally {
                exitProcess(1)
            }
        }

        /**
         * Format the size of a file in bytes, e.g. "37 MB".
         */
        fun formatDataSize(bytes: Long): String {
            val r = 1000 // ratio
            return when (bytes) {
                in 0 until r -> "$bytes B"
                in r until r*r -> "${bytes / r} kB"
                in r*r until r*r*r -> "${bytes / (r*r)} MB"
                else -> "${bytes / (r*r*r)} GB"
            }
        }

        /**
         * Read N bytes from the input stream, or until the end is reached.
         *
         * Shim for InputStream.readNBytes which is present starting JDK 9.
         */
        fun inputStreamReadNBytes(inputStream: InputStream, n: Int): ByteArray {
            val buffer = ByteArray(n)
            var haveRead = 0
            while (haveRead < n) {
                val cntRead = inputStream.read(buffer, haveRead, n - haveRead)
                if (cntRead < 0) {
                    break
                } else {
                    haveRead += cntRead
                }
            }
            return buffer.sliceArray(IntRange(0, min(haveRead, n) - 1))
        }

        /**
         * Run the replacer on all the matched locations in the string.
         *
         * Shim for Matcher.replaceAll which is present starting JDK 9.
         */
        fun replaceAll(regex: Pattern, input: String, replacer: (MatchResult) -> String): String {
            val out = StringBuilder()

            val matcher = regex.matcher(input)
            var lastEnd = 0
            while (true) {
                if (matcher.find()) {
                    out.append(input.substring(lastEnd, matcher.start()))
                    out.append(replacer(matcher.toMatchResult()))
                    lastEnd = matcher.end()
                } else {
                    // append tail
                    out.append(input.substring(lastEnd))
                    break
                }
            }

            return out.toString()
        }

        /**
         * Call writer on an output stream to a temporary file. Before returning
         * the writer's return value, move the temporary file to the desired
         * destination path.
         *
         * Requires that [dest] does not yet exist.
         */
        fun <R> withTempFile(dest: Path, writer: (OutputStream) -> R): R {
            // Creating the temp file in the destination's containing directory
            // means it will get the same file attributes as if it were written
            // straight into place instead. This is important because the default
            // dir is /tmp, where the file would have mode=600, which may cause
            // problems when publishing to certain web servers. (Observed with
            // NearlyFreeSpeech.net, where the "web" user needs group read
            // permission.)
            //
            // Additionally, the java.io.File method is used rather than the newer
            // java.nio.file.Files, since the latter requires permissions to be
            // explicitly added in the createTempFile call in order to get this
            // effect, and I'd rather just inherit them.
            val dir = dest.parent.toFile()
            val tmp = File.createTempFile("spelunk_writing_", ".tmp", dir).toPath()
            val ret: R = try {
                Files.newOutputStream(tmp).use { writer(it) }
            } catch(t: Throwable) {
                Files.deleteIfExists(tmp)
                throw t
            }
            Files.move(tmp, dest)
            return ret
        }

        /**
         * Write string [contents] to the destination file [dest] while ensuring
         * that the file at [dest] is never in a partially written state.
         *
         * Requires that [dest] does not yet exist.
         */
        fun writeFileAtomic(dest: Path, contents: String) {
            withTempFile(dest) {
                it.bufferedWriter().apply {
                    write(contents)
                    flush()
                }
            }
        }

        /**
         * Find the deepest ancestor of this path that actually exists.
         */
        fun findExistingAncestor(path: Path): Path {
            var current = path.toAbsolutePath()
            while (true) {
                if (Files.exists(current)) {
                    return current
                }
                val parent: Path? = current.parent
                // Shouldn't ever happen, given that this is an absolute path,
                // but getParent() *can* return null.
                if (parent == null) {
                    return path.root
                } else {
                    current = parent
                }
            }
        }

        /**
         * Return the string for the first conditional that returns true, or
         * return null if none did. Conditionals are lazily executed.
         *
         * This can be used to log the reason an action is taken when there
         * might be any number of checks that could cause it.
         */
        fun explainAction(conditionals: List<Pair<String, () -> Boolean>>): String? {
            for ((reason, conditional) in conditionals) {
                if (conditional())
                    return reason
            }
            return null
        }
    }
}

/**
 * Return the string with the ending char appended if it isn't already there.
 */
fun String.ensureLastChar(ending: Char): String {
    return if (this.endsWith(ending))
        this
    else
        this + ending
}

/**
 * Return the string with the start char prepended if it isn't already there.
 */
fun String.ensureFirstChar(start: Char): String {
    return if (this.startsWith(start))
        this
    else
        start + this
}

// TODO: Make this work with code points, rather than bytes
fun String.ellipsize(byteBudget: Int, ellipsis: String = "…"): String {
    // Case: Text is short enough
    if (this.length <= byteBudget)
        return this

    // Case: Budget is tiny or ellipsis is huge; just truncate text
    if (ellipsis.length >= byteBudget)
        return substring(0, byteBudget)

    // Case: Truncate with ellipsis
    return substring(0, byteBudget - ellipsis.length) + ellipsis
}

/**
 * Wrap a multiline string by replacing all newline sequences with a single space.
 */
fun String.wrap(): String {
    return replace(Regex("[\r\n]+"), " ")
}

/**
 * Like Closeable.use(), but only closes if the thunk throws.
 */
fun <R> Closeable.closeIfThrows(thunk: () -> R): R {
    try {
        return thunk()
    } catch (t: Throwable) {
        try {
            close()
        } catch (closeE: Throwable) {
            t.addSuppressed(closeE)
        }
        throw t
    }
}

/**
 * Format a string according to system default locale and time.
 */
fun Instant.formatDefault(): String {
    val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
        .withZone(ZoneId.systemDefault())
    return formatter.format(this)
}

/**
 * Call thunk and return its return value; if it throws, suppress that and just
 * return null.
 */
fun <R> suppressThrowAsNull(thunk: () -> R): R? {
    return try {
        thunk()
    } catch (_: Throwable) {
        null
    }
}

/**
 * Make an iterable that will return chunks of the input stream as byte arrays
 * until the input stream is empty.
 */
fun InputStream.chunkIterator(): Iterable<ByteArray> {
    return object : Iterable<ByteArray> {
        override fun iterator(): Iterator<ByteArray> {
            return object : Iterator<ByteArray> {
                val buffer = ByteArray(4096)
                var eof = false

                override fun hasNext(): Boolean {
                    return !eof
                }

                override fun next(): ByteArray {
                    val readResult = this@chunkIterator.read(buffer)
                    val byteCount = if (readResult < 0) {
                        eof = true
                        0
                    } else {
                        readResult
                    }
                    return buffer.copyOfRange(0, byteCount)
                }
            }
        }
    }
}

/**
 * Divide a byte array at the specified index. Returns a pair of bytes arrays,
 * the first being the prefix of the given length, and the second being the
 * remaining bytes.
 */
fun ByteArray.divideAt(prefixLen: Int): Pair<ByteArray, ByteArray> {
    return sliceArray(0 until prefixLen) to sliceArray(prefixLen until size)
}

/**
 * Standard Maybe/None/Just classes, but for fields on [DataPatch] classes.
 */
sealed class MaybePatch<T> {
    /**
     * Run the given function [f] on the value, if present.
     */
    abstract fun doIfPresent(f: (T) -> Unit)

    /**
     * The field is absent; no value is set.
     */
    class Absent<T> : MaybePatch<T>() {
        override fun doIfPresent(f: (T) -> Unit) { }

        override fun equals(other: Any?): Boolean {
            return other is Absent<*>
        }

        override fun hashCode(): Int {
            return 1385671906
        }

        override fun toString(): String {
            return "<Absent>"
        }
    }

    /**
     * The field is set to [value].
     */
    data class Present<T>(val value: T) : MaybePatch<T>() {
        override fun doIfPresent(f: (T) -> Unit) {
            f(value)
        }
    }
}

/**
 * Marker interface indicating that a class represents a patch to a data entity,
 * where each property is a MaybePatch indicating whether that property should be
 * patched.
 *
 * A DataPatch class must have fields matching (by name and type) some subset of
 * the fields of any class it will be minified or patched against.
 */
interface DataPatch

/**
 * Return true if this patch object has any [MaybePatch.Present] fields.
 */
fun <T: DataPatch> T.hasChanges(): Boolean {
    val me = this
    return me::class.declaredMemberProperties
        .filter { it.returnType.classifier == MaybePatch::class }
        .any { prop -> prop.javaGetter!!.invoke(me) is MaybePatch.Present<*> }
}

/**
 * Remove any Present values that match the provided baseline.
 */
fun <T: DataPatch> T.minifyAgainst(baseline: Any): T {
    var ret = this

    val baselineProps = baseline::class.declaredMemberProperties.associateBy { it.name }

    val copy = this::class.declaredMemberFunctions.first { it.name == "copy" }

    this::class.declaredMemberProperties
        .filter { it.returnType.classifier == MaybePatch::class }
        .forEach { prop ->
            val maybeValue = prop.javaGetter!!.invoke(this@minifyAgainst)
            val baselineProp = baselineProps[prop.name]
            if (maybeValue is MaybePatch.Present<*> && baselineProp != null) {
                val baselineValue = baselineProp.javaGetter!!.invoke(baseline)
                if (maybeValue.value == baselineValue) {
                    val copied = copy.callBy(mapOf(
                        copy.instanceParameter!! to ret,
                        copy.parameters.first { it.name == prop.name } to MaybePatch.Absent<T>()
                    ))
                    @Suppress("UNCHECKED_CAST")
                    ret = copied as T
                }
            }
        }
    return ret
}

/**
 * Apply a patch against a baseline, returning the patched version.
 */
fun <T: DataPatch, B: Any> T.applyTo(baseline: B): B {
    var ret = baseline

    val baselineProps = baseline::class.declaredMemberProperties.associateBy { it.name }

    val copy = baseline::class.declaredMemberFunctions.first { it.name == "copy" }

    this::class.declaredMemberProperties
        .filter { it.returnType.classifier == MaybePatch::class }
        .forEach { prop ->
            val maybeValue = prop.javaGetter!!.invoke(this@applyTo)
            val baselineProp = baselineProps[prop.name]
            if (maybeValue is MaybePatch.Present<*> && baselineProp != null) {
                // Could check if this change is a no-op (baseline and patch
                // values are the same) but there's no real reason to do this
                // optimization.
                val copied = copy.callBy(mapOf(
                    copy.instanceParameter!! to ret,
                    copy.parameters.first { it.name == prop.name } to maybeValue.value
                ))
                @Suppress("UNCHECKED_CAST")
                ret = copied as B
            }
        }
    return ret
}

interface MsgPackable {
    /**
     * Serialize to generic data that is acceptable to [deepWrap].
     */
    fun serialize(): Any?

    /**
     * Return the msgpack bytes for this object.
     */
    fun toMsgPack(): ByteArray {
        return packValue(deepWrap(serialize()))
    }

    companion object {
        /**
         * Wrap any standard data structure in MessagePack Value instances, recursively.
         *
         * TODO: Make a PR on msgpack-java to include this.
         */
        fun deepWrap(x: Any?): Value {
            return when (x) {
                null -> ValueFactory.newNil()
                is String -> ValueFactory.newString(x)
                is Boolean -> ValueFactory.newBoolean(x)
                is List<*> -> ValueFactory.newArray(
                    x.map(::deepWrap).toList()
                )
                is Map<*, *> -> ValueFactory.newMap(x.entries.associate { (k, v) ->
                    deepWrap(k) to deepWrap(v)
                })
                is ByteArray -> ValueFactory.newBinary(x)
                is Byte -> ValueFactory.newInteger(x)
                is Short -> ValueFactory.newInteger(x)
                is Int -> ValueFactory.newInteger(x)
                is Long -> ValueFactory.newInteger(x)
                is BigInteger -> ValueFactory.newInteger(x)
                is Float -> ValueFactory.newFloat(x)
                is Double -> ValueFactory.newFloat(x)
                else -> throw IllegalArgumentException(
                    "Don't know how to wrap value of class ${x.javaClass} for MessagePack"
                )
            }
        }

        private val packer = MessagePack.DEFAULT_PACKER_CONFIG

        fun packValue(v: Value): ByteArray {
            return packer.newBufferPacker().also {
                it.packValue(v)
            }.toByteArray()
        }
    }
}

interface MsgUnpackable<T> {
    /**
     * Construct an instance of T from a generic map of data produced by
     * [deepUnwrap].
     */
    fun deserialize(ctx: MapContext): T

    /**
     * Construct an instance of T from msgpack bytes.
     */
    fun fromMsgpack(bytes: ByteArray): T {
        val genericData = deepUnwrap(unpackValue(bytes))
        val ctx = AnyValueContext(node = genericData, path = "")
        return deserialize(ctx.asMap())
    }

    companion object {
        /**
         * Unwraps MessagePack Values recursively to standard data structures.
         *
         * Numbers are unpacked to Long or Double (or BigInteger if larger than
         * a Long.) Duplicate keys in maps are forbidden. Extension values are
         * unchanged.
         */
        fun deepUnwrap(v: Value): Any? {
            return when(v.valueType) {
                ValueType.NIL -> null
                ValueType.STRING -> v.asStringValue().asString()
                ValueType.BOOLEAN -> v.asBooleanValue().boolean
                ValueType.ARRAY -> v.asArrayValue().list().map(::deepUnwrap)
                ValueType.MAP -> {
                    val mapValue = v.asMapValue()
                    val unwrapped = mapValue.map().map { (k, v) ->
                        deepUnwrap(k) to deepUnwrap(v)
                    }.toMap()
                    if (mapValue.size() != unwrapped.size)
                        throw EMessagePackMapDuplicateKeys()
                    return unwrapped
                }
                ValueType.BINARY -> v.asBinaryValue().asByteArray()
                ValueType.INTEGER -> run {
                    val i = v.asIntegerValue()
                    if (i.mostSuccinctMessageFormat() == MessageFormat.UINT64)
                        i.asBigInteger()
                    else
                        i.asLong()
                }
                ValueType.FLOAT -> v.asFloatValue().toDouble()
                ValueType.EXTENSION -> v // pass through extension types
                else -> throw java.lang.IllegalArgumentException(
                    "Don't know how to unwrap MessagePack value of type ${v.valueType}"
                )
            }
        }

        private val unpacker: UnpackerConfig = DEFAULT_UNPACKER_CONFIG
            .withActionOnMalformedString(CodingErrorAction.REPORT)
            .withActionOnUnmappableString(CodingErrorAction.REPORT)
            .withAllowReadingBinaryAsString(false)
            .withAllowReadingStringAsBinary(false)

        fun unpackValue(bytes: ByteArray): Value {
            return unpacker.newUnpacker(bytes).unpackValue()
        }
    }
}

class EMessagePackMapDuplicateKeys: Exception("MessagePack map had duplicate keys")

/**
 * Wrapper of ByteArray, but with equals and hashCode implemented for content
 * equality.
 *
 * It is critical that [sharedBytes] not be mutated.
 */
open class BytesEq(
    /**
     * The backing byte array. This must not be mutated, either by code that
     * retains a handle to the original input to the constructor, or by code
     * that later retrieves this value by accessor.
     */
    val sharedBytes: ByteArray,
) {
    /**
     * Hexadecimal representation of this byte array.
     */
    fun asHex() = Utils.hexEncode(sharedBytes)

    /**
     * Return true iff the given array matches this array.
     *
     * This is just a convenience method, and does not perform comparison in a
     * way that protects against timing attacks.
     */
    fun matches(data: ByteArray): Boolean {
        return sharedBytes.contentEquals(data)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BytesEq) return false

        return sharedBytes.contentEquals(other.sharedBytes)
    }

    override fun hashCode(): Int {
        return sharedBytes.contentHashCode()
    }

    override fun toString(): String {
        return "${friendlyClassName(javaClass)}{hex=${asHex()}}"
    }
}

/**
 * Array of bytes known to be of a certain length, with data-class properties.
 *
 * Subclassing this is a good way to get basic type safety for binary data.
 */
open class BytesSized(
    expectedSize: Int,
    bytes: ByteArray
): BytesEq(bytes) {
    init {
        if (bytes.size != expectedSize) {
            throw IllegalArgumentException(
                "${friendlyClassName(javaClass)} requires $expectedSize bytes, " +
                        "but was given ${bytes.size}"
            )
        }
    }
}

/**
 * Memoize unary functions of non-null parameter and return value.
 * Concurrency-safe but does not guarantee only-once execution.
 */
fun <P1, R> ((P1) -> R).memoize(): (P1) -> R {
    return object : (P1) -> R {
        // ConcurrentHashMap requires non-null keys and values, which is why
        // this function imposes that restriction.
        private val cache = ConcurrentHashMap<P1, R>()
        override fun invoke(p1: P1): R {
            return cache[p1] ?: run {
                this@memoize(p1).also { ret ->
                    cache[p1] = ret
                }
            }
        }
    }
}

/**
 * Measure time elapsed by a block, in fractional seconds.
 */
fun timeSeconds(block: () -> Any?): Double {
    val mark = TimeSource.Monotonic.markNow()
    block()
    return mark.elapsedNow().inWholeNanoseconds / 1_000_000_000.0
}

/**
 * Make a short version of the name of the class, suitable for human
 * consumption. Falls back to full name if there is no short name.
 */
fun friendlyClassName(javaClass: Class<*>): String {
    return javaClass.simpleName.ifBlank { javaClass.name }
}
