package org.timmc.spelunk

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonEncodingException
import org.timmc.spelunk.Utils.Companion.json
import org.timmc.spelunk.Utils.Companion.utf8
import java.io.FileNotFoundException
import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.deleteRecursively
import kotlin.reflect.KProperty

/**
 * Parent type for various filesystem node classes: Regular file ("leaf"),
 * directory, or symlink.
 */
abstract class Node(
    /** The filesystem path this Node represents. */
    val path: Path,
    /** Whether or not to permit writing to this node and any descendents. */
    val allowWrite: Boolean
) {
    /**
     * Require that this node has been opened with writeable = true, else throw
     * and exception that mentions the [intendedActionMsg] that was going to be
     * attempted.
     */
    fun requireWriteable(intendedActionMsg: String) {
        if(!allowWrite) {
            throw RuntimeException(
                "Not allowed to write at $path, which is opened read-only. Attempted: $intendedActionMsg"
            )
        }
    }

    /**
     * The filename of this node (last segment in path).
     * This is a convenience value.
     */
    val name: String = path.fileName.toString()
}

/**
 * A "regular" file (not a directory or link).
 */
open class Leaf(path: Path, allowWrite: Boolean): Node(path, allowWrite) {
    /** Slurp file as string. */
    fun read(): String {
        return String(readBytes(), utf8)
    }

    /** Slurp file as undecoded bytes. */
    fun readBytes(): ByteArray {
        return Files.readAllBytes(path)
    }

    /** Write string to file. */
    fun write(contents: String) {
        requireWriteable("write string to file")
        writeBytes(contents.toByteArray(utf8))
    }

    /** Write raw bytes to file. */
    fun writeBytes(contents: ByteArray) {
        requireWriteable("write bytes to file")
        Files.write(path, contents)
    }
}

/**
 * Marker interface for model files: Indicates that when reading from JSON,
 * unknown keys can be ignored. This would generally include files produced
 * by other apps, not our own files.
 */
interface JsonAllowUnknownKeys

/**
 * A JSON file with a known structure (parseable as class `M`).
 */
class ModelLeaf<M>(
    path: Path,
    allowWrite: Boolean,
    val type: Class<M>,
    val adapter: JsonAdapter<M> = adapterForType(type)
): Leaf(path, allowWrite) {
    /** Slurp JSON file and parse as model object. */
    fun readJson(): M {
        return fromJson(read())
    }

    /** Parse JSON string as model object. */
    fun fromJson(json: String): M {
        try {
            return adapter.fromJson(json)!!
        } catch (e: JsonDataException) {
            throw RuntimeException("Failed to read JSON file $path as a ${type.simpleName}: ${e.message}", e)
        } catch (e: JsonEncodingException) {
            throw RuntimeException("Malformed JSON file at $path; could not read as a ${type.name}: ${e.message}", e)
        }
    }

    /** Write data to file as JSON. */
    fun writeJson(data: M) {
        requireWriteable("write JSON file")
        write(adapter.toJson(data))
    }

    companion object {
        fun <M> adapterForType(type: Class<M>): JsonAdapter<M> {
            val base = json.adapter(type).indent("  ")
            if (type.isAssignableFrom(JsonAllowUnknownKeys::class.java)) {
                // Allowing unknown keys when reading *other* people's data
                // makes sense, because they may have different apps. I
                // don't need to round-trip that data, though!
                return base
            } else {
                // If I were to permit unknown keys when reading my own repo's
                // files, I would risk manipulating them and writing them back
                // to disk without those keys, losing information. This could
                // happen if I ran an older version of Spelunk on files
                // written by a newer version.

                // FIXME Moshi's PolymorphicJsonAdaptorFactory breaks with .failOnUnknown()
                // https://github.com/square/moshi/issues/858
                //return base.failOnUnknown()
                return base
            }
        }
    }
}

/**
 * A directory node.
 *
 * Recommended: Subclass this to attach child Nodes as fields using delegates.
 */
open class Directory(path: Path, allowWrite: Boolean): Node(path, allowWrite) {
    /**
     * Create the path as a directory, if necessary, and return it.
     */
    fun ensureDir() {
        if (Files.exists(path) && Files.isDirectory(path)) return
        requireWriteable("create directory")
        Files.createDirectories(path)
    }

    /**
     * Recursively delete directory contents, not following symlinks.
     *
     * Return true if deletion was successful or directory didn't exist in the
     * first place.
     */
    @OptIn(ExperimentalPathApi::class)
    fun deleteRecursively() {
        requireWriteable("delete directory and all contents")
        // Kotlin's File.deleteRecursively is dangerous because it follows
        // symlinks when walking the tree (https://youtrack.jetbrains.com/issue/KT-22324)
        // but the newer Path.deleteRecursively apparently deletes the symlinks
        // instead of following them.
        return path.deleteRecursively()
    }
}

/** Not a Node, but a way to look up children of a Node. */
abstract class PatternedLookup<T: Node>(
    val path: Path,
    val allowWrite: Boolean,
    val namePattern: Regex
) {
    /**
     * Get a child by name, and just assume it follows the naming pattern.
     */
    abstract fun getUnchecked(name: String): T

    /** Filter the directory listing in some way, e.g. by file type. */
    protected abstract fun filterListing(path: Path): Boolean

    /** Description of child type (layout name, etc.) for error messages. */
    protected abstract val childDesc: String

    /**
     * Get one child, and throw if it doesn't follow the naming pattern.
     */
    fun lookup(name: String): T {
        if (!namePattern.matches(name))
            throw IllegalArgumentException("Invalid name for $childDesc: $name")
        return getUnchecked(name)
    }

    /**
     * List all matched children.
     *
     * Throws if parent does not exist.
     */
    fun list(): List<T> {
        if (!Files.exists(path))
            throw FileNotFoundException("Cannot list $childDesc files in non-existent directory: $path")
        return Files.list(path)
            .filter { filterListing(it) }
            .map { it.fileName.toString() }
            .filter { namePattern.matches(it) }
            .map(::getUnchecked)
            .collect(Collectors.toList())
    }
}

/** PatternedLayout of subdirectories that are also laid out. */
open class SubDirs<L: Directory>(path: Path, allowWrite: Boolean, namePattern: Regex, val layout: Class<L>):
    PatternedLookup<L>(path, allowWrite, namePattern) {

    override fun getUnchecked(name: String): L {
        return layout.getConstructor(Path::class.java, Boolean::class.java)
            .newInstance(path.resolve(name), allowWrite)
    }

    override fun filterListing(path: Path): Boolean = Files.isDirectory(path)

    override val childDesc: String = "${layout.name} directory"
}

open class SubLeaves(path: Path, allowWrite: Boolean, namePattern: Regex, desc: String):
    PatternedLookup<Leaf>(path, allowWrite, namePattern) {

    override fun getUnchecked(name: String): Leaf {
        return Leaf(path.resolve(name), allowWrite)
    }

    override fun filterListing(path: Path): Boolean = Files.isRegularFile(path)

    override val childDesc: String = "$desc file"
}

open class SubModelLeaves<M>(path: Path, allowWrite: Boolean, namePattern: Regex, val type: Class<M>):
    PatternedLookup<ModelLeaf<M>>(path, allowWrite, namePattern) {

    override fun getUnchecked(name: String): ModelLeaf<M> {
        return ModelLeaf(path.resolve(name), allowWrite, type)
    }

    override fun filterListing(path: Path): Boolean = Files.isRegularFile(path)

    override val childDesc: String = "${type.name} JSON file"
}

//////////////////////
// Note: The point of all these delegates is to grab the `path` val from
// the containing Node.
//////////////////////

/** Define a Leaf property by filename. */
class LeafDelegate(private val name: String) {
    operator fun getValue(thisRef: Directory, property: KProperty<*>): Leaf {
        return Leaf(thisRef.path.resolve(name), thisRef.allowWrite)
    }
}

/** Define a ModelLeaf property by filename and model class. */
class ModelLeafDelegate<M>(private val name: String, private val type: Class<M>) {
    operator fun getValue(thisRef: Directory, property: KProperty<*>): ModelLeaf<M> {
        return ModelLeaf(thisRef.path.resolve(name), thisRef.allowWrite, type)
    }
}

/** Define a Directory property by filename. */
class DirDelegate(private val name: String) {
    operator fun getValue(thisRef: Directory, property: KProperty<*>): Directory {
        return Directory(thisRef.path.resolve(name), thisRef.allowWrite)
    }
}

/** Define a custom Directory subclass property by filename. */
class DirLayoutDelegate<L: Directory>(
    private val name: String,
    private val layout: Class<L>
) {
    operator fun getValue(thisRef: Directory, property: KProperty<*>): L {
        return layout.getConstructor(Path::class.java, Boolean::class.java)
            .newInstance(thisRef.path.resolve(name), thisRef.allowWrite)
    }
}

/** Define a custom SubDirs property by pattern and layout. */
class SubDirsDelegate<L: Directory>(
    private val namePattern: Regex,
    private val layout: Class<L>
) {
    operator fun getValue(thisRef: Directory, property: KProperty<*>): SubDirs<L> {
        return SubDirs(thisRef.path, thisRef.allowWrite, namePattern, layout)
    }
}

/** Define a custom SubLeaves property by pattern and layout. */
class SubLeavesDelegate(
    private val namePattern: Regex,
    private val desc: String
) {
    operator fun getValue(thisRef: Directory, property: KProperty<*>): SubLeaves {
        return SubLeaves(thisRef.path, thisRef.allowWrite, namePattern, desc)
    }
}

/** Define a custom SubModelLeaves property by pattern and layout. */
class SubModelLeavesDelegate<M>(
    private val namePattern: Regex,
    private val type: Class<M>
) {
    operator fun getValue(thisRef: Directory, property: KProperty<*>): SubModelLeaves<M> {
        return SubModelLeaves(thisRef.path, thisRef.allowWrite, namePattern, type)
    }
}

// Delegate constructor shorthands

fun leaf(name: String) = LeafDelegate(name)
fun <M> leaf(name: String, type: Class<M>) = ModelLeafDelegate(name, type)
fun <L: Directory> subdirs(namePattern: Regex, layout: Class<L>) = SubDirsDelegate(namePattern, layout)
fun leaves(namePattern: Regex, desc: String) = SubLeavesDelegate(namePattern, desc)
fun <M> modelLeaves(namePattern: Regex, type: Class<M>) = SubModelLeavesDelegate(namePattern, type)
fun dir(name: String) = DirDelegate(name)
fun <L: Directory> dir(name: String, layout: Class<L>) = DirLayoutDelegate(name, layout)
