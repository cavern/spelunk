package org.timmc.spelunk.gui

import javafx.application.Application
import javafx.application.Platform
import javafx.scene.control.Alert
import javafx.scene.control.ButtonBar
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import javafx.scene.image.Image
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import javafx.stage.Stage
import org.timmc.spelunk.LockfileExistsErr
import org.timmc.spelunk.LockfileResource
import org.timmc.spelunk.SpelunkExit
import org.timmc.spelunk.State
import org.timmc.spelunk.State.Companion.RepoUsability
import org.timmc.spelunk.Utils
import org.timmc.spelunk.closeIfThrows
import org.timmc.spelunk.cmd.GuiOptions
import org.timmc.spelunk.model.ProfilesFinder
import org.timmc.spelunk.wrap
import java.io.InputStream
import java.nio.file.Path
import java.nio.file.Paths

/**
 * GUI application (using JavaFX)
 */
class SpelunkApp: Application() {
    lateinit var launchOptions: GuiOptions
    lateinit var state: State
    /**
     * Lockfile on the repo, must be closed before program exits. May be null
     * if startup did not finish correctly.
     */
    private var repoLock: LockfileResource? = null

    override fun init() {
        super.init()

        // Rehydrate pre-parsed CLI options
        val optionsJson: String = parameters.raw[0]
        launchOptions = Utils.json.adapter(GuiOptions::class.java).fromJson(optionsJson)!!
    }

    override fun start(stage: Stage) {
        // Get a path to an initialized repo, or ask me if none are available.

        val requestedDir = launchOptions.requestedRepoDir?.let { Paths.get(it) }
        // Only read profiles if no specific dir requested
        val repoPath: Path = if (requestedDir == null) {
            // Choose a repo, or decline to and exit
            chooseRepoPath() ?: return Platform.exit()
            // Assume it's initialized, according to chooseRepoPath's contract
        } else {
            // If given a path directly, check first before trying to use it
            if (!State.getReadOnly(requestedDir).repo.isInitComplete()) {
                // CLI option, so don't bother with GUI error display
                Utils.die(SpelunkExit("The requested Spelunk repository has not been initialized: $requestedDir"))
            }
            requestedDir
        }.toAbsolutePath()

        // ===============================================================//
        // Entering dangerous section: Lockfile must be carefully managed //
        // so that it does not get deleted before the program ends.       //
        // ===============================================================//
        val (lock, initialState) = try {
            State.getWriteableWithLock(repoPath)
        } catch (e: LockfileExistsErr) {
            // TODO: Provide a way to prompt for deleting the lockfile
            crashWithMessage("""
                Another process has already locked this Spelunk repository
                for its exclusive use. ${e.message}
            """.trimIndent().wrap())
        }
        // Release the lock if there's an exception during setup
        lock.closeIfThrows {
            // Make the lock available to Application.stop(), which is the
            // expected place for it to be released. Not very local, but good
            // enough.
            repoLock = lock
            // Heck, try to release the lock on shutdown too; it's idempotent
            Runtime.getRuntime().addShutdownHook(object : Thread("lockfile-reaper-on-shutdown") {
                override fun run() {
                    lock.close()
                }
            })
            // Only save out of local scope once lock closing is registered
            this.state = when (val usability = initialState.checkUsability()) {
                is RepoUsability.Uninitialized -> {
                    crashWithMessage("There doesn't appear to be a valid Spelunk repository at $repoPath")
                }
                is RepoUsability.Unknown -> {
                    crashWithMessage("The Spelunk repository at $repoPath is of an unknown version: ${usability.version}")
                }
                is RepoUsability.UpgradeRequired -> {
                    if (askPermissionToUpgrade(repoPath, usability)) {
                        // Produces new state value to use
                        State.repositoryFullUpgrade(initialState)
                    } else {
                        crashWithMessage("Spelunk repository can't be used without upgrading it.")
                    }
                }
                is RepoUsability.Usable -> {
                    initialState // good to go
                }
            }

            // OK, on to the rest of startup

            // TODO Catch startup exception and allow choosing a different profile
            MainTabbed(state, stage).launch()
        }
    }

    /**
     * Attempt to choose a repo. This may involve the welcome screen or
     * (TODO) a profile chooser, but generally will involve picking the
     * default profile from the profiles list.
     *
     * If a Path is returned, I expect that it has already been fully
     * initialized. If null is returned, I want to exit the app.
     */
    fun chooseRepoPath(): Path? {
        val profiles = ProfilesFinder().readProfiles()
        return if (profiles == null || profiles.profiles.isEmpty()) {
            FirstRunWelcome().showAndWait()
        } else {
            val defaultProfile = profiles.profiles.getOrNull(profiles.defaultIndex)
            if (defaultProfile == null) {
                // TODO Show a Profile selector, with existing profiles to
                // choose from. But for now, just pick the first profile if no
                // default is returned.
                Paths.get(profiles.profiles.first().path)
            } else {
                Paths.get(defaultProfile.path)
            }
        }
    }

    fun askPermissionToUpgrade(repoPath: Path, usability: RepoUsability.UpgradeRequired): Boolean {
        val upgradeButton = ButtonType("Upgrade", ButtonBar.ButtonData.YES)
        val cancelButton = ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE)
        val choice = Dialog<ButtonType>().apply {
            headerText = "Upgrade repository?"
            dialogPane.content = TextFlow(Text("""
                Spelunk can't use the repository at $repoPath without first
                upgrading the data from version ${usability.upgrader.version}
                to ${State.latestVersion}. This happens when you
                upgrade your copy of Spelunk. While it's a routine process, it
                does mean you won't be able to use an older copy of Spelunk
                afterwards.
                
                For most people, upgrading is the correct choice. If you have
                doubt, please make a backup copy of your repository first.
            """.trimIndent().wrap()))
            dialogPane.buttonTypes.setAll(upgradeButton, cancelButton)
            dialogPane.maxWidth = 600.0
        }.showAndWait()
        return choice.orElse(null) == upgradeButton
    }

    fun crashWithMessage(message: String): Nothing {
        Alert(Alert.AlertType.ERROR).apply {
            dialogPane.content = TextFlow(Text(message))
            dialogPane.maxWidth = 600.0
        }.showAndWait()
        stop()
        throw SpelunkExit(message)
    }

    override fun stop() {
        super.stop()
        // Get rid of the lockfile
        repoLock?.close()
    }

    companion object {
        val appIcon = Image(resourceStream("app-icon-256.png"))
    }
}

/** Load a GUI resource by path as a stream. */
fun resourceStream(resourcePath: String): InputStream {
    return SpelunkApp::class.java.getResourceAsStream(resourcePath)
}
