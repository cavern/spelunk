package org.timmc.spelunk.gui

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.TextArea
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * When a `tab` keystroke occurs without any modifiers (barring `Shift`),
 * replace it with a `Ctrl tab` to navigate to other fields. This suppresses tab
 * insertion; future versions may allow a swap of tab/C-tab, but this might
 * involve solving an infinite loop problem.
 *
 * JavaFX normally binds Ctrl-tab for traversal out of a TextArea, but that is
 * 1) inconsistent with the traversal keybinding for everything else, and 2)
 * frequently used for tabbing between documents in a multiple-document
 * interface.
 *
 * This is incompatible with KEY_TYPED since it relies on event.code.
 */
class TabTraversalEventHandler : EventHandler<KeyEvent> {
    // Based in part on https://stackoverflow.com/questions/12860478/tab-key-navigation-in-javafx-textarea
    override fun handle(event: KeyEvent) {
        val code = event.code
        // Don't bail on shift, which is used for backwards traversal
        if (code != KeyCode.TAB || event.isAltDown || event.isMetaDown) {
            return // irrelevant, or don't know how to handle
        }

        if (event.isControlDown) {
            return // Allow to pass through, since this method may have generated it
        }

        event.consume()
        val node = event.source as Node
        val newEvent = KeyEvent(event.source, event.target, event.eventType,
            event.character, event.text, event.code,
            // Set CTRL modifier to true
            event.isShiftDown, true, event.isAltDown, event.isMetaDown)
        node.fireEvent(newEvent)
    }
}

/**
 * Change the meaning of a tab-press in a TextArea to mean focus traversal,
 * matching every other control. This precludes entering literal tab characters.
 */
fun TextArea.tabMeansTraversal() {
    // Must be KEY_PRESSED, not KEY_TYPED, since the latter does not have
    // a defined `event.code`.
    addEventFilter(KeyEvent.KEY_PRESSED, TabTraversalEventHandler())
}

/**
 * Make a list of ancestors of this node in the scene graph. If a cycle is
 * detected, the function will terminate and may return an incomplete list
 * (possibly not containing the full cycle).
 */
fun Node.ancestors(): List<Node> {
    val ancestors = mutableListOf(this)
    // Walks upwards through the scene graph, but will bail if cycle detected.
    // This uses Floyd's cycle detection algorithm.
    var tortoise: Node? = this
    var hare: Node? = tortoise?.parent
    // Look, maybe the scene graph changes in the meantime? I just don't like
    // the possibility of an infinite loop. So no while(true) here.
    for (unused in 1..1000000) {
        val keepGoing = when (tortoise) {
            null -> {
                // reached top of scene graph
                false
            }
            hare -> {
                // cycle detected: hare caught up with tortoise; return early
                false
            }
            else -> {
                ancestors.add(tortoise)
                tortoise = tortoise.parent
                hare = hare?.parent?.parent
                true
            }
        }
        if (!keepGoing) break
    }
    return ancestors.toList()
}

/** A format for displaying localized date/times. */
val displayTimestampFormat = DateTimeFormatter.ofPattern("""MMM dd, yyyy 'at' H:mm""")!!

/** Produce a displayable date/time string, given seconds since Unix epoch. */
fun displayTime(secondsSinceEpoch: Long): String {
    return ZonedDateTime
        .ofInstant(Instant.ofEpochSecond(secondsSinceEpoch), ZoneId.systemDefault())
        .format(displayTimestampFormat)
}
