package org.timmc.spelunk.gui

import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.BorderPane
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import javafx.stage.Modality
import javafx.stage.Stage
import org.timmc.spelunk.MaybePatch
import org.timmc.spelunk.SodiumBoxPublicKey
import org.timmc.spelunk.SodiumSigningPublicKey
import org.timmc.spelunk.State
import org.timmc.spelunk.Utils
import org.timmc.spelunk.minifyAgainst
import org.timmc.spelunk.model.AddressBook
import org.timmc.spelunk.model.Contact
import org.timmc.spelunk.model.ContactPatch
import org.timmc.spelunk.wrap
import tornadofx.Stylesheet
import tornadofx.action
import tornadofx.addClass
import tornadofx.box
import tornadofx.button
import tornadofx.constraintsForColumn
import tornadofx.cssclass
import tornadofx.disableWhen
import tornadofx.em
import tornadofx.gridpane
import tornadofx.hbox
import tornadofx.insets
import tornadofx.label
import tornadofx.onDoubleClick
import tornadofx.row
import tornadofx.text
import tornadofx.textfield
import tornadofx.textflow
import tornadofx.titledpane
import tornadofx.tooltip
import tornadofx.vbox
import tornadofx.vboxConstraints

class AddressBookEditor(val state: State): ModalView() {
    override val node: Node by lazy { initialize() }

    private lateinit var quaintListMan: ListingManager<Contact, VBox>

    private fun initialize(): Node {
        val root = BorderPane().apply {
            top = TextFlow(Text("""
            These are your quaints—acquaintances, including both friends
            and more casual contacts.
            """.trimIndent().wrap())).apply {
                padding = insets(5.0)
            }

            center = VBox().apply {
                padding = insets(5.0)
                spacing = 5.0

                hbox {
                    alignment = Pos.BOTTOM_LEFT
                    padding = insets(5.0)
                    spacing = 5.0
                    label("Quaints").addClass(Styles.qGroupHeading)
                    button("Add") {
                        action { onAskAddQuaint() }
                    }
                }
                val quaintListing = vbox {
                    spacing = 5.0
                    padding = insets(5.0)
                }
                quaintListMan = ListingManager(
                    container = quaintListing,
                    placeholder = Label("(none)"),
                    rowFactory = ::rowForQuaint
                )
            }
        }
        root.stylesheets.add(Styles().externalForm)

        onAddressBookUpdated(state.addressBookRead())
        state.bus.addressBookChanged.addListener(::onAddressBookUpdated)

        return root
    }

    fun rowForQuaint(quaint: Contact): Node {
        return BorderPane().apply {
            addClass(Styles.quaint)
            left = button("✎") {
                tooltip("Edit quaint")
                addClass(Styles.editButton)
                BorderPane.setMargin(this, insets(5.0))

                action { onAskEditQuaint(quaint) }
            }
            center = vbox {
                textflow { text(quaint.localHandle ?: "[unnamed]").addClass(Styles.handle) }
                textflow { text("[link] ${quaint.fetchUri}").addClass(Styles.url) }

                onDoubleClick { onAskEditQuaint(quaint) }
            }
        }
    }

    class Styles : Stylesheet() {
        companion object {
            val qGroupHeading by cssclass()
            val quaint by cssclass()
            val editButton by cssclass()
            val handle by cssclass()
            val url by cssclass()
        }

        init {
            qGroupHeading {
                fontSize = 1.3.em
            }

            quaint {
                editButton {
                    fontSize = 1.5.em
                    prefWidth = 2.em
                    padding = box(0.1.em)
                }

                handle {
                    fontSize = 1.2.em
                }

                url {
                    padding = box(0.em, 0.em, 0.em, 0.2.em)
                }
            }
        }
    }

    /** Called when the user asks to add a new quaint. */
    fun onAskAddQuaint() {
        when(val outcome = QuaintEditor.add()) {
            is AddOutcome.Cancel -> return
            is AddOutcome.Add -> state.addressBookAdd(outcome.provisional)
        }
    }

    /** Called when the user asks to edit an existing quaint. */
    fun onAskEditQuaint(toEdit: Contact) {
        when (val outcome = QuaintEditor.edit(toEdit)) {
            is EditOutcome.Cancel -> return
            is EditOutcome.Delete -> state.addressBookRemoveContact(toEdit)
            is EditOutcome.Patch -> state.addressBookPatchContact(toEdit.id, outcome.patch)
        }
    }

    val quaintListComparator = Comparator
        .comparing({ c: Contact -> c.localHandle }, nullsFirst(naturalOrder()))
        .thenComparing({ c: Contact -> c.fetchUri }, nullsFirst(naturalOrder()))
        .thenComparing(Contact::id, naturalOrder())!!

    /** Called when the address book has changed and needs redisplay. */
    fun onAddressBookUpdated(newAbook: AddressBook) {
        val sortedQuaints = newAbook.contacts
            .filter { it.separation == 0 }
            .sortedWith(quaintListComparator)
        quaintListMan.setItems(sortedQuaints)
    }
}

/**
 * A one-time-use Quaint editor, which can be used to add, edit, or delete.
 */
class QuaintEditor private constructor() {
    private lateinit var quaintUrl: TextField
    private lateinit var quaintHandle: TextField
    private lateinit var quaintSigKey: TextField
    private lateinit var quaintBoxKey: TextField

    private lateinit var stage: Stage

    private var wantCancel: Boolean = false
    private var wantDelete: Boolean = false

    fun runForAdd(): AddOutcome {
        setup(null).showAndWait()
        return when {
            wantCancel -> AddOutcome.Cancel
            else -> AddOutcome.Add(Contact(
                id = Contact.PLACEHOLDER_ID,
                fetchUri = enteredFetchUri(),
                sigKey = enteredSigningKey(),
                boxKey = enteredBoxKey(),
                separation = 0,
                handleReceived = null,
                handleOverride = enteredHandle(),
            ))
        }
    }

    fun runForEdit(initial: Contact): EditOutcome {
        setup(initial).showAndWait()
        return when {
            wantCancel -> EditOutcome.Cancel
            wantDelete -> EditOutcome.Delete
            else -> EditOutcome.Patch(ContactPatch(
                fetchUri = MaybePatch.Present(enteredFetchUri()),
                sigKey = MaybePatch.Present(enteredSigningKey()),
                boxKey = MaybePatch.Present(enteredBoxKey()),
                handleOverride = MaybePatch.Present(enteredHandle())
            ).minifyAgainst(initial))
        }
    }

    private fun setup(initial: Contact?): Stage {
        val root = VBox().apply {
            spacing = 5.0
            padding = insets(5.0)
            gridpane {
                padding = insets(5.0)
                vgap = 5.0
                hgap = 5.0
                constraintsForColumn(0).hgrow = Priority.NEVER
                constraintsForColumn(1).hgrow = Priority.ALWAYS
                row {
                    label("URL:") // TODO mark as required
                    quaintUrl = textfield(initial?.fetchUri) {
                        promptText = "https://www.spidersge.org/timmc"
                    }
                }
                row {
                    label("Name:")
                    quaintHandle = textfield(initial?.localHandle) {
                        promptText = "[optional] Tim McCormack"
                    }
                }
            }
            titledpane("Advanced", collapsible = true, node = GridPane().apply {
                padding = insets(5.0)
                vgap = 5.0
                hgap = 5.0
                constraintsForColumn(0).hgrow = Priority.NEVER
                constraintsForColumn(1).hgrow = Priority.ALWAYS
                // TODO better names, maybe explanatory text
                row {
                    label("Signature key:")
                    quaintSigKey = textfield(initial?.sigKey?.asHex()) {
                        promptText = "[optional] F4DDBEAF7381B6C9AA..."
                    }
                }
                row {
                    label("Encryption key:")
                    quaintBoxKey = textfield(initial?.boxKey?.asHex()) {
                        promptText = "[optional] 9BC8ACFAF5A5892D80..."
                    }
                }
            }) {
                isExpanded = false
            }

            val acceptText = if (initial == null) {
                "Add quaint"
            } else {
                "Save"
            }
            // Just a vertical flex-spacer
            hbox {vboxConstraints { vGrow = Priority.ALWAYS } }
            hbox {
                alignment = Pos.CENTER_LEFT
                spacing = 5.0
                padding = insets(5.0)
                button(acceptText) {
                    // TODO some indicator of when there are in fact any changes to be saved?
                    disableWhen(quaintUrl.textProperty().isEmpty.and(quaintBoxKey.textProperty().isEmpty))
                    action { onAccept() }
                }
                if (initial != null) {
                    button("Delete") {
                        action { onAskDelete() }
                    }
                }
            }
        }

        val scene = Scene(root, 500.0, 250.0)
        stage = Stage().apply {
            initModality(Modality.APPLICATION_MODAL)
            setScene(scene)
            title = if (initial == null) {
                "Add quaint"
            } else {
                "Edit quaint"
            }
            setOnCloseRequest {
                wantCancel = true
                wantDelete = false
            }
        }
        return stage
    }

    private fun enteredFetchUri(): String {
        return quaintUrl.text
    }

    private fun enteredHandle(): String? {
        // TODO Don't sniff for empty; instead, have a checkbox to opt out of
        // having a handle (enabled by default, though)
        return quaintHandle.text.takeUnless { it.isBlank() }
    }

    // TODO: Accept keys in friendlier user-facing format
    private fun enteredSigningKey(): SodiumSigningPublicKey? {
        val pubKeyText = quaintSigKey.text
        return if (pubKeyText == null || pubKeyText.isBlank()) {
            null
        } else {
            SodiumSigningPublicKey(Utils.hexDecode(pubKeyText)) // TODO error handling
        }
    }

    private fun enteredBoxKey(): SodiumBoxPublicKey? {
        val pubKeyText = quaintBoxKey.text
        return if (pubKeyText == null || pubKeyText.isBlank()) {
            null
        } else {
            SodiumBoxPublicKey(Utils.hexDecode(pubKeyText)) // TODO error handling
        }
    }

    private fun onAccept() {
        wantCancel = false
        wantDelete = false
        stage.close()
    }

    private fun onAskDelete() {
        wantCancel = false
        wantDelete = true
        stage.close()
    }

    companion object {
        fun add(): AddOutcome {
            return QuaintEditor().runForAdd()
        }

        fun edit(initial: Contact): EditOutcome {
            return QuaintEditor().runForEdit(initial)
        }
    }
}

sealed class AddOutcome {
    /** Contact to be added -- no ID yet. */
    data class Add(val provisional: Contact): AddOutcome()
    data object Cancel : AddOutcome()
}

sealed class EditOutcome {
    data class Patch(val patch: ContactPatch): EditOutcome()
    data object Delete : EditOutcome()
    data object Cancel : EditOutcome()
}
