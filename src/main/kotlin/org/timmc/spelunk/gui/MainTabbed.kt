package org.timmc.spelunk.gui

import javafx.application.Platform
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.ScrollPane
import javafx.scene.control.Tooltip
import javafx.scene.input.KeyEvent
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.scene.text.Text
import javafx.stage.Stage
import org.timmc.spelunk.Publishing
import org.timmc.spelunk.State
import org.timmc.spelunk.os
import tornadofx.Stylesheet
import tornadofx.action
import tornadofx.add
import tornadofx.addClass
import tornadofx.box
import tornadofx.button
import tornadofx.c
import tornadofx.cssclass
import tornadofx.csspseudoclass
import tornadofx.em
import tornadofx.multi
import tornadofx.px
import tornadofx.removeClass
import tornadofx.scrollpane
import tornadofx.separator
import tornadofx.text
import tornadofx.textflow
import tornadofx.vbox
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Main window, a left-tabbed model view that can switch between writing,
 * reading, settings, etc.
 */
class MainTabbed(val state: State, private val stage: Stage) {
    // Tabs of the main view
    val writeMode = Mode(name = "Write",
        desc = "Write a new post; edit or publish earlier drafts; manage published posts",
        default = true,
        view = JournalManage(state)
    )
    val readMode = Mode(name = "Read",
        desc = "Read or search people's journals",
        view = ReadingPane(state)
    )
    val peopleMode = Mode(name = "People",
        desc = "Manage who you're connected to",
        view = AddressBookEditor(state)
    )
    val settingsMode = Mode(name = "Settings",
        desc = "Configure Spelunk",
        view = ConfigEditor(state, stage)
    )
    private val modes: List<Mode> = listOf(writeMode, readMode, peopleMode, settingsMode)

    private lateinit var scene: Scene
    private lateinit var modeTabs: VBox
    private lateinit var activeContainer: ScrollPane
    private lateinit var currentMode: Mode
    private var publishStatus = Text()
    private var fetchStatus = Text()

    fun launch() {
        stage.title = "Spelunk"
        stage.icons.add(SpelunkApp.appIcon)
        scene = makeScene()
        stage.scene = scene

        state.bus.fetchStatusChange.addListener(::onFetchStatusChange)
        state.fetching.queueFetchAll()

        state.bus.publishStatusChange.addListener(::onPublishStatusChange)
        onPublishStatusChange(Publishing.PublishStatus.STARTUP)
        state.publishing.queueInitialSync()
        state.bus.postChange.addListener { postId: String ->
            state.publishing.queueScanOne(postId)
        }

        stage.sizeToScene()
        stage.show()
    }

    private fun makeScene(): Scene {
        val root = BorderPane().apply {
            left = vbox {
                addClass(Styles.sidebar)
                vbox {
                    modeTabs = vbox {
                        addClass(Styles.modetabs)
                        spacing = 1.0
                        children.addAll(modes.map { it.button })
                    }
                    separator {  }
                }
                textflow { add(publishStatus) }
                textflow { add(fetchStatus) }
                button("Re-fetch all") { action {
                    state.fetching.queueFetchAll()
                }}
            }
            activeContainer = scrollpane {
                fitToWidthProperty().set(true)
                fitToHeightProperty().set(true)
                textflow { text("Loading...") }
                // Can't scroll by keyboard unless it's reachable
                isFocusTraversable = true
            }
            center = activeContainer
        }
        val initialMode = modes.firstOrNull { it.default } ?: modes.first()
        currentMode = initialMode
        switchMode(initialMode)
        val scene = Scene(root).apply {
            stylesheets.add(Styles().externalForm)
            focusOwnerProperty().addListener { _, _, newFocus ->
                onFocusChange(newFocus)
            }
        }
        scene.addEventFilter(KeyEvent.KEY_PRESSED) { event ->
            if (os.keypressIsTabSwitchForward(event)) {
                event.consume()
                onKeyboardRequestSwitchTab(true)
                return@addEventFilter
            }
            if (os.keypressIsTabSwitchBackward(event)) {
                event.consume()
                onKeyboardRequestSwitchTab(false)
                return@addEventFilter
            }
        }
        return scene
    }

    class Styles: Stylesheet() {
        companion object {
            val sidebar by cssclass()
            val modetabs by cssclass()
            val activeTab by csspseudoclass()
        }

        init {
            sidebar {
                minWidth = 10.em
            }

            modetabs {
                backgroundColor = multi(Color.BLACK)

                button {
                    // This ensures all the buttons are the same width
                    maxWidth = Double.MAX_VALUE.px

                    padding = box(5.px)
                    backgroundInsets = multi(box(0.px))
                    backgroundRadius = multi(box(0.px))
                    fontSize = 18.px

                    // inactive tab (default)
                    backgroundColor = multi(c("#aaa"))

                    and(activeTab) {
                        backgroundColor = multi(Color.WHITE)
                    }
                }
            }
        }
    }

    inner class Mode(
        val name: String,
        val desc: String,
        val default: Boolean = false,
        val view: ModalView
    ) {
        val button: Button = Button(name).apply {
            tooltip = Tooltip(desc)
            if (default) addClass(Styles.activeTab)
            action { switchMode(this@Mode) }
            isFocusTraversable = false
        }
    }

    /** Perform the actual mode switch. */
    fun switchMode(requestedMode: Mode) {
        activeContainer.content = requestedMode.view.node
        requestedMode.view.onShow()

        currentMode.button.removeClass(Styles.activeTab)
        requestedMode.button.addClass(Styles.activeTab)
        currentMode = requestedMode
    }

    /**
     * When focus changes, possibly save off old focus location for use in
     * restoring focus during tab switching.
     */
    fun onFocusChange(newFocus: Node?) {
        if (newFocus != null && newFocus.ancestors().contains(currentMode.view.node)) {
            currentMode.view.lastFocused = newFocus
        }
    }

    /** Switch mode according to keyboard event (and save focus first.) */
    fun onKeyboardRequestSwitchTab(isForward: Boolean) {
        // Save focus before switching
        val curFocus = scene.focusOwner
        if (curFocus != null && curFocus.ancestors().contains(currentMode.view.node)) {
            currentMode.view.lastFocused = curFocus
        }
        // -1 if not found, but unlikely and shouldn't break things
        val curIndex = modes.indexOf(currentMode)
        val nextIndex = curIndex + if (isForward) 1 else -1
        val nextMode = modes[Math.floorMod(nextIndex, modes.size)]
        switchMode(nextMode)
    }

    fun onPublishStatusChange(status: Publishing.PublishStatus) {
        publishStatus.text = "Publish: " + when (status) {
            Publishing.PublishStatus.STARTUP -> "(startup)"
            Publishing.PublishStatus.RUNNING -> "running"
            Publishing.PublishStatus.FINISHED_SUCCESS -> "done"
            Publishing.PublishStatus.FINISHED_ERROR -> "error"
        }
    }

    fun onFetchStatusChange() {
        // TODO Record and display error status
        val inFlight = state.fetching.getStatus().journalsInFlight
        fetchStatus.text = when (inFlight) {
            0 -> "Fetch: done"
            1 -> "Fetching 1 journal"
            else -> "Fetching $inFlight journals"
        }
    }
}

abstract class ModalView {
    /** Root node of the view. */
    abstract val node: Node

    /** Node inside this view that had focus last time it was open. */
    var lastFocused: Node? = null
    /** True if this view has ever been shown. */
    val everShown = AtomicBoolean(false)

    fun onShow() {
        val toFocus = if (everShown.getAndSet(true)) {
            lastFocused ?: getInitialFocus()
        } else {
            getInitialFocus()
        }
        toFocus?.let { Platform.runLater { it.requestFocus() } }
    }

    /**
     * Get the Node that should have the focus when the view is first shown.
     * May also be called when view is shown subsequently.
     */
    open fun getInitialFocus(): Node? = null
}
