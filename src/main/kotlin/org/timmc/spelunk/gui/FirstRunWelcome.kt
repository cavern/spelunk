package org.timmc.spelunk.gui

import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.scene.text.Text
import javafx.stage.Stage
import org.timmc.spelunk.State
import org.timmc.spelunk.State.Companion.RepoUsability
import org.timmc.spelunk.Utils
import org.timmc.spelunk.cmd.InitCmd
import org.timmc.spelunk.model.ProfilesFinder
import org.timmc.spelunk.os
import org.timmc.spelunk.wrap
import tornadofx.Stylesheet
import tornadofx.action
import tornadofx.add
import tornadofx.addClass
import tornadofx.box
import tornadofx.button
import tornadofx.chooseDirectory
import tornadofx.cssclass
import tornadofx.em
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.insets
import tornadofx.px
import tornadofx.separator
import tornadofx.text
import tornadofx.textfield
import tornadofx.textflow
import tornadofx.vbox
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

/**
 * First-run welcome screen. Prompts for repo location and does one of:
 *
 * - Choose existing repo
 * - Initialize a new repo
 * - Return without picking anything
 */
class FirstRunWelcome {
    private val initialGuessPath: Path = os.guessDesirableJournalLocation()
    private var isChosen = false
    private var chosenPath: Path = initialGuessPath

    private lateinit var stage: Stage
    private lateinit var rootContainer: VBox
    private lateinit var pickContainer: VBox
    private lateinit var pathFreeEdit: TextField
    private lateinit var adviceIcon: Text
    private lateinit var adviceLabel: Text
    private lateinit var goButton: Button

    fun showAndWait(): Path? {
        stage = Stage()
        rootContainer = VBox().apply {
            spacing = 5.0
            padding = insets(5)
            pickContainer = vbox {
                spacing = 5.0
                padding = insets(5)
                minWidth = 600.0
                hbox {
                    alignment = Pos.TOP_CENTER
                    textflow { text("Welcome to Spelunk!") { addClass(Styles.header) } }
                }
                textflow {
                    text("Please choose a directory for your journal.")
                }
                separator(Orientation.HORIZONTAL)
                hbox {
                    alignment = Pos.CENTER_LEFT
                    spacing = 5.0
                    button("Browse...") {
                        hgrow = Priority.NEVER
                        action { chooseFile() }
                    }
                    pathFreeEdit = textfield(chosenPath.toString()) {
                        hgrow = Priority.ALWAYS
                        textProperty().addListener { _, _, newValue ->
                            chosenPath = Paths.get(newValue)
                            onChangedPath()
                        }
                    }
                }
                textflow {
                    adviceIcon = text {
                        addClass(Styles.adviceIconCls)
                    }
                    adviceLabel = text()
                }
                separator(Orientation.HORIZONTAL)
                goButton = button("Looks good, let's go") {
                    action { onLetsGo() }
                }
            }
        }

        onChangedPath() // initialize

        val scene = Scene(rootContainer)
        scene.stylesheets.add(Styles().externalForm)
        stage.scene = scene

        stage.setOnCloseRequest { isChosen = false }
        stage.icons.add(SpelunkApp.appIcon)
        stage.sizeToScene()
        stage.showAndWait()

        return if (isChosen) chosenPath else null
    }

    private fun chooseFile() {
        // Checking absolute path is necessary here, otherwise DirectoryChooser
        // will throw if given an empty path.
        val initialDir = if (chosenPath.isAbsolute && Files.isDirectory(chosenPath)) {
            chosenPath.toFile()
        } else {
            null
        }
        chooseDirectory(
            title = "Create or choose empty directory for journal",
            initialDirectory = initialDir
        )?.let {
            chosenPath = it.toPath()
            pathFreeEdit.text = chosenPath.toString() // update other controls
            onChangedPath()
        }
    }

    /** Validate path as usable; return error message, or null if usable. */
    fun validatePath(path: Path): PathAssessment {
        if (!path.isAbsolute) {
            return PathAssessment(PathAssessmentType.NOT_ABSOLUTE,
                "Please specify an absolute path, e.g. ${os.exampleAbsolutePath()}")
        }
        if (Files.exists(path)) {
            if (Files.isDirectory(path)) {
                if (Files.isWritable(path)) {
                    val isEmpty = Files.list(path).toList().isEmpty()
                    if (isEmpty) {
                        return PathAssessment(PathAssessmentType.EMPTY,
                            "Perfect, that's an empty directory.")
                    }

                    // There are already files there. Would they collide with a
                    // repo? Is the repo valid? Does it require upgrade?
                    val readOnlyState = State.getReadOnly(path)
                    return try {
                        when (val usability = readOnlyState.checkUsability()) {
                            is RepoUsability.Usable -> {
                                PathAssessment(PathAssessmentType.VALID_REPO,
                                    "Perfect, that directory is already set up for Spelunk.")
                            }
                            is RepoUsability.UpgradeRequired -> {
                                PathAssessment(PathAssessmentType.UPGRADEABLE_REPO,"""
                                    Good, that directory is already set up for Spelunk
                                    (although it will need to be upgraded.)
                                """.trimIndent().wrap())
                            }
                            is RepoUsability.Unknown -> {
                                PathAssessment(PathAssessmentType.INVALID_REPO, """
                                    That's an existing Spelunk directory, but I don't know
                                    how to read it. (It uses schema version ${usability.version};
                                    for reference, my own schema version is
                                    ${State.latestVersion}.)
                                    Perhaps it was written by a newer or forked version of Spelunk?
                                """.trimIndent().wrap())
                            }
                            is RepoUsability.Uninitialized -> {
                                // No repo there. Check if existing files would collide with
                                // having a repo.
                                if (readOnlyState.repo.isInitPartial()) {
                                    PathAssessment(PathAssessmentType.COLLIDE_REPO,
                                        "There are some files there that would be overwritten by Spelunk.") // TODO show which ones
                                } else {
                                    PathAssessment(PathAssessmentType.EXTRANEOUS,
                                        "There are already some files there; make sure this is a good place.") // TODO show which ones
                                }
                            }
                        }
                    } catch (t: Throwable) {
                        PathAssessment(PathAssessmentType.INVALID_REPO,
                            "That looks like a Spelunk directory, but something is wrong with it. ($t)")
                    }
                } else {
                    return PathAssessment(PathAssessmentType.NOT_WRITEABLE,
                        "That directory isn't writeable.")
                }
            } else {
                return PathAssessment(PathAssessmentType.NOT_DIRECTORY,
                    "The selected path exists, but isn't a directory.")
            }
        } else {
            // If it doesn't exist, is it at least possible to create the parent
            // dir?
            val someParent = Utils.findExistingAncestor(path)
            return if (Files.isWritable(someParent)) {
                // OK, I can probably create the directory if needed
                PathAssessment(PathAssessmentType.CREATABLE,
                    "OK, I'll create that directory for you.")
            } else {
                PathAssessment(PathAssessmentType.NO_EXIST_CANT_CREATE,
                    "That directory doesn't exist, and it's not in a place I can create new subdirectories.")
            }
        }
    }

    private fun onChangedPath() {
        val chosenPath = chosenPath

        val results = try {
            validatePath(chosenPath)
        } catch (t: Throwable) {
            PathAssessment(PathAssessmentType.ERROR,
                "Something went wrong while checking that directory: $t")
        }

        adviceIcon.text = results.type.usability.iconChar
        adviceLabel.text = results.message
        goButton.disableProperty().value = results.type.usability == PathUsability.BAD
        stage.sizeToScene()
    }

    /**
     * I've made my decision: Disable the picker portion, initialize (and show
     * progress), then allow to close and report the chosen path.
     */
    fun onLetsGo() {
        val progressLabel = Text("Initializing...")
        // Show initialization steps
        val progressContainer = VBox().apply {
            spacing = 5.0
            padding = insets(5)
            separator {  }
            textflow { add(progressLabel) }
        }
        pickContainer.isDisable = true
        rootContainer.add(progressContainer)
        stage.sizeToScene()

        try {
            if (validatePath(chosenPath).type.usability != PathUsability.EXISTING) {
                InitCmd.doInit(chosenPath) { msg -> progressLabel.text = msg }
            }
            progressLabel.text = "Creating or updating profile list file"
            ProfilesFinder().addProfile(chosenPath)
            progressLabel.text = "Spelunk is ready to use."
            isChosen = true
            stage.close()
            return
        } catch (e: java.lang.Exception) {
            isChosen = false
            progressLabel.text = "${e.javaClass.simpleName}: ${e.message}"
            e.printStackTrace()
            // Don't close window; leave the message up for me to see
        }
    }

    class Styles: Stylesheet() {
        companion object {
            val header by cssclass()
            val adviceIconCls by cssclass()
        }

        init {
            header {
                fontSize = 28.px
            }

            adviceIconCls {
                fontSize = 20.px
                padding = box(0.em, 0.3.em)
            }
        }
    }
}

data class PathAssessment(val type: PathAssessmentType, val message: String)

enum class PathAssessmentType(val usability: PathUsability) {
    ERROR(PathUsability.BAD),
    NOT_ABSOLUTE(PathUsability.BAD),
    NO_EXIST_CANT_CREATE(PathUsability.BAD),
    NOT_DIRECTORY(PathUsability.BAD),
    NOT_WRITEABLE(PathUsability.BAD),
    CREATABLE(PathUsability.GOOD),
    EMPTY(PathUsability.GOOD),
    VALID_REPO(PathUsability.EXISTING),
    UPGRADEABLE_REPO(PathUsability.EXISTING),
    INVALID_REPO(PathUsability.BAD),
    COLLIDE_REPO(PathUsability.BAD),
    EXTRANEOUS(PathUsability.WARNING),
}

enum class PathUsability(val iconChar: String) { // TODO color as well
    EXISTING("✅"),
    GOOD("✅"),
    WARNING("⚠️"),
    BAD("❌"),
}
