package org.timmc.spelunk.gui

import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.input.Clipboard
import javafx.scene.input.ClipboardContent
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import javafx.stage.DirectoryChooser
import javafx.stage.Stage
import javafx.util.StringConverter
import org.timmc.spelunk.MaybePatch.Present
import org.timmc.spelunk.State
import org.timmc.spelunk.Utils
import org.timmc.spelunk.hasChanges
import org.timmc.spelunk.minifyAgainst
import org.timmc.spelunk.model.AwsS3Config
import org.timmc.spelunk.model.Config
import org.timmc.spelunk.model.ConfigPatch
import org.timmc.spelunk.model.EncryptedJournal
import org.timmc.spelunk.model.LocalFilesConfig
import org.timmc.spelunk.model.PublishLocationConfig
import org.timmc.spelunk.model.RsyncConfig
import org.timmc.spelunk.model.Visibility
import org.timmc.spelunk.model.WebDavConfig
import org.timmc.spelunk.wrap
import tornadofx.action
import tornadofx.button
import tornadofx.choicebox
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.insets
import tornadofx.label
import tornadofx.textfield
import tornadofx.titledpane
import tornadofx.vbox
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class ConfigEditor(val state: State, ownerWindow: Stage): ModalView() {
    val repo = state.repo

    override val node: Node by lazy { initialize() }

    /**
     * Cached version of what's on disk.
     */
    private var configSaved: Config = state.configRead()

    /**
     * Changes to the config model that haven't yet been saved to disk.
     */
    private var pendingEdits: ConfigPatch = ConfigPatch()

    val localFilesEditor = PublishTypeEditor.LocalFilesEditor(ownerWindow)
    private val publishTypes = listOf(
        PublishTypeEditor.NoneEditor,
        PublishTypeEditor.WebDAVEditor,
        PublishTypeEditor.RsyncEditor,
        PublishTypeEditor.AwsS3Editor,
        localFilesEditor
    )

    private lateinit var publishingType: ChoiceBox<PublishTypeEditor>
    private lateinit var publishingDetailsContainer: VBox
    private lateinit var saveButton: Button

    private fun initialize(): Node {
        val initialConfig = configSaved

        val defaultVisibility = makeVisibilityChoiceBox().also {
            it.value = initialConfig.defaultPostVisibility
            it.valueProperty().addListener { _, _, newValue ->
                onChangeDefaultVisibility(newValue)
            }
        }

        return VBox().apply {
            padding = insets(5.0)
            spacing = 15.0

            // TODO: I'm using TitledPane here because it was easier than
            // styling a fieldset, but that's kind of silly.

            titledpane("Identity", collapsible = false, node = VBox().apply {
                padding = insets(5.0)
                spacing = 5.0

                hbox {
                    spacing = 5.0
                    alignment = Pos.CENTER_LEFT

                    label("Public name:")
                    textfield(initialConfig.ownName ?: "") {
                        textProperty().addListener  { _, _, newValue ->
                            onChangeOwnName(newValue)
                        }
                    }
                }

                button("Copy public key to clipboard") {
                    action {
                        // Just use base32 for now; may later want some more
                        // structured key format with error correction,
                        // searchable prefix, etc.
                        val key32 = Utils.base32Encode(state.keyringRead().current.boxKeys.publicKey.sharedBytes)
                        val content = ClipboardContent().apply { putString(key32) }
                        Clipboard.getSystemClipboard().setContent(content)
                    }
                }
            }) { isFocusTraversable = false }

            titledpane("Journal post defaults", collapsible = false, node = VBox().apply {
                padding = insets(5.0)
                spacing = 5.0

                hbox {
                    spacing = 5.0
                    alignment = Pos.CENTER_LEFT

                    label("Visibility:")
                    children.add(defaultVisibility)
                }
            }) {
                isFocusTraversable = false
            }

            titledpane("Publishing method", collapsible = false, node = VBox().apply {
                padding = insets(5.0)
                spacing = 5.0

                publishingType = choicebox(values = publishTypes) {
                    converter = object : StringConverter<PublishTypeEditor>() {
                        // These two don't implement a round-trip; one deals in
                        // enum names and the other in human-readable names.
                        // Probably fine?
                        override fun fromString(s: String): PublishTypeEditor? = null
                        override fun toString(v: PublishTypeEditor): String = v.displayName
                    }
                    value = when(initialConfig.publishing) {
                        null -> PublishTypeEditor.NoneEditor
                        is WebDavConfig -> PublishTypeEditor.WebDAVEditor
                        is AwsS3Config -> PublishTypeEditor.AwsS3Editor
                        is RsyncConfig -> PublishTypeEditor.RsyncEditor
                        is LocalFilesConfig -> localFilesEditor
                    }
                    publishTypes.forEach {
                        it.init(initialConfig.publishing, ::onEditPublishDetails)
                    }
                    valueProperty().addListener { _, _, newValue ->
                        onChangePublishingType(newValue)
                    }
                }
                publishingDetailsContainer = vbox {
                    // Only a single child, so don't add padding or spacing here
                    children.add(publishingType.value.ui)
                }
            }) {
                isFocusTraversable = false
            }

            titledpane("Network settings", collapsible = false, node = VBox().apply {
                padding = insets(5.0)
                spacing = 5.0

                hbox {
                    spacing = 5.0
                    alignment = Pos.CENTER_LEFT

                    label("Tor SOCKS proxy:")
                    textfield(initialConfig.torSocksProxy ?: "") {
                        textProperty().addListener { _, _, newValue ->
                            onChangeTorSocksProxy(newValue)
                        }
                    }
                }

            }) {
                isFocusTraversable = false
            }

            saveButton = button("Save") {
                action { onSave() }
                disableProperty().set(true)
            }
        }
    }

    fun onEdit() {
        // First, strip out any changes the user has made that they have since
        // reverted -- or that have come into alignment with the saved state
        // via changes being made to the state from elsewhere.
        pendingEdits = pendingEdits.minifyAgainst(configSaved)
        // And then check if there's still a delta.
        val edited = pendingEdits.hasChanges()
        saveButton.disableProperty().set(!edited)
    }

    fun onSave() {
        configSaved = state.configPatch(pendingEdits)
        // TODO Re-publish when ownName has changed
        pendingEdits = ConfigPatch()
        onEdit()
    }

    private fun onChangeOwnName(newValue: String) {
        pendingEdits = pendingEdits.copy(ownName = Present(newValue.takeUnless { it.isBlank() }))
        onEdit()
    }

    private fun onChangeDefaultVisibility(newValue: Visibility) {
        pendingEdits = pendingEdits.copy(defaultPostVisibility = Present(newValue))
        onEdit()
    }

    private fun onChangePublishingType(newValue: PublishTypeEditor) {
        publishingDetailsContainer.children.setAll(newValue.ui)
        onEditPublishDetails()
    }

    private fun onEditPublishDetails() {
        pendingEdits = pendingEdits.copy(publishing = Present(publishingType.value.derivePubConfig()))
        onEdit()
    }

    private fun onChangeTorSocksProxy(newValue: String) {
        pendingEdits = pendingEdits.copy(torSocksProxy = Present(newValue.takeUnless { it.isBlank() }))
        onEdit()
    }
}

sealed class PublishTypeEditor {
    companion object {
        fun makeJournalUrlLabel(): Label {
            return Label("Journal URL:")
        }

        fun makeJournalUrlField(): TextField {
            return TextField().apply {
                promptText = "https://myserver.example.com/myusername/cavern/"
            }
        }
    }

    abstract val displayName: String
    abstract val ui: Node
    abstract fun init(existingPubConfig: PublishLocationConfig?, editListener: () -> Unit)
    abstract fun derivePubConfig(): PublishLocationConfig? // TODO add validation?

    data object NoneEditor : PublishTypeEditor() {
        override val displayName: String = "<none>"

        override val ui: Node = TextFlow(Text(
            """
            No one will be able to read your journal unless you configure
            publishing.
            """.trimIndent().wrap()
        ))

        override fun init(existingPubConfig: PublishLocationConfig?, editListener: () -> Unit) { }

        override fun derivePubConfig(): PublishLocationConfig? {
            return null
        }
    }

    data object WebDAVEditor : PublishTypeEditor() {
        override val displayName: String = "WebDAV"

        val urlField = TextField().apply { promptText = "https://myserver.example.com/myusername/cavern/" }
        val usernameField = TextField().apply { promptText = "myusername" }
        val passwordField = TextField().apply { promptText = "hunter2" }
        val journalUrlField = makeJournalUrlField()

        override val ui: Node = run {
            val root = GridPane().apply {
                hgap = 5.0
                vgap = 5.0
            }
            root.add(Label("Server:"), 0, 0)
            root.add(urlField, 1, 0)
            root.add(Label("Username:"), 0, 1)
            root.add(usernameField, 1, 1)
            root.add(Label("Password:"), 0, 2)
            root.add(passwordField, 1, 2)
            root.add(makeJournalUrlLabel(), 0, 3)
            root.add(journalUrlField, 1, 3)
            root

        }

        override fun init(existingPubConfig: PublishLocationConfig?, editListener: () -> Unit) {
            if (existingPubConfig is WebDavConfig) {
                journalUrlField.text = existingPubConfig.journalUrl
                urlField.text = existingPubConfig.url
                usernameField.text = existingPubConfig.username
                passwordField.text = existingPubConfig.password
            }
            listOf(journalUrlField, urlField, usernameField, passwordField).map {
                it.textProperty().addListener { _, _, _ -> editListener() }
            }
        }

        override fun derivePubConfig(): WebDavConfig {
            return WebDavConfig(
                journalUrl = journalUrlField.text,
                url = urlField.text,
                username = usernameField.text,
                password = passwordField.text
            )
        }
    }

    data object AwsS3Editor : PublishTypeEditor() {
        override val displayName: String = "AWS S3"

        val bucketField = TextField()
        val keyPrefixField = TextField()
        val regionField = TextField()
        val iamKeyIdField = TextField()
        val iamKeySecretField = TextField()
        val journalUrlField = makeJournalUrlField()

        override val ui: Node = run {
            val root = GridPane().apply {
                hgap = 5.0
                vgap = 5.0
            }
            root.add(Label("Bucket name:"), 0, 0)
            root.add(bucketField, 1, 0)
            root.add(Label("Key prefix, if any:"), 0, 1)
            root.add(keyPrefixField, 1, 1)
            root.add(Label("Region, e.g. us-east-1:"), 0, 2)
            root.add(regionField, 1, 2)
            root.add(Label("IAM key ID:"), 0, 3)
            root.add(iamKeyIdField, 1, 3)
            root.add(Label("IAM key secret:"), 0, 4)
            root.add(iamKeySecretField, 1, 4)
            root.add(makeJournalUrlLabel(), 0, 5)
            root.add(journalUrlField, 1, 5)
            root

        }

        override fun init(existingPubConfig: PublishLocationConfig?, editListener: () -> Unit) {
            if (existingPubConfig is AwsS3Config) {
                journalUrlField.text = existingPubConfig.journalUrl
                bucketField.text = existingPubConfig.bucket
                keyPrefixField.text = existingPubConfig.keyPrefix
                regionField.text = existingPubConfig.region
                iamKeyIdField.text = existingPubConfig.iamKeyId
                iamKeySecretField.text = existingPubConfig.iamKeySecret
            }
            listOf(journalUrlField, bucketField, keyPrefixField, regionField, iamKeyIdField, iamKeySecretField).map {
                it.textProperty().addListener { _, _, _ -> editListener() }
            }
        }

        override fun derivePubConfig(): AwsS3Config {
            return AwsS3Config(
                journalUrl = journalUrlField.text,
                bucket = bucketField.text,
                keyPrefix = keyPrefixField.text,
                region = regionField.text,
                iamKeyId = iamKeyIdField.text,
                iamKeySecret = iamKeySecretField.text
            )
        }
    }

    data object RsyncEditor : PublishTypeEditor() {
        override val displayName: String = "rsync"

        val serverField = TextField().apply { promptText = "myserver.example.com" }
        val remoteDirField = TextField().apply { promptText = "/home/myusername/cavern/" }
        val userField = TextField().apply { promptText = "myusername" }
        val rshField = TextField().apply { promptText = "ssh -p 22" }
        val journalUrlField = makeJournalUrlField()

        override val ui: Node = run {
            val root = GridPane().apply {
                hgap = 5.0
                vgap = 5.0
            }
            root.add(Label("Hostname:"), 0, 0)
            root.add(serverField, 1, 0)
            root.add(Label("Remote directory:"), 0, 1)
            root.add(remoteDirField, 1, 1)
            root.add(Label("Username:"), 0, 2)
            root.add(userField, 1, 2)
            root.add(Label("--rsh option (if needed):"), 0, 3)
            root.add(rshField, 1, 3)
            root.add(makeJournalUrlLabel(), 0, 4)
            root.add(journalUrlField, 1, 4)
            root
        }

        override fun init(existingPubConfig: PublishLocationConfig?, editListener: () -> Unit) {
            if (existingPubConfig is RsyncConfig) {
                journalUrlField.text = existingPubConfig.journalUrl
                serverField.text = existingPubConfig.server
                remoteDirField.text = existingPubConfig.remoteDir
                userField.text = existingPubConfig.user
                rshField.text = existingPubConfig.rsh
            }
            listOf(journalUrlField, serverField, remoteDirField, userField, rshField).map {
                it.textProperty().addListener { _, _, _ -> editListener() }
            }
        }

        override fun derivePubConfig(): RsyncConfig {
            return RsyncConfig(
                journalUrl = journalUrlField.text,
                server = serverField.text,
                remoteDir = remoteDirField.text,
                user = userField.text?.takeUnless { it.isEmpty() },
                rsh = rshField.text?.takeUnless { it.isEmpty() }
            )
        }
    }

    class LocalFilesEditor(private val ownerWindow: Stage) : PublishTypeEditor() {
        override val displayName: String = "local directory"

        lateinit var rootDirField: TextField
        val journalUrlField = makeJournalUrlField()

        override val ui: Node = run {
            VBox().apply {
                spacing = 5.0

                hbox {
                    spacing = 5.0
                    alignment = Pos.CENTER_LEFT

                    label("Path:")
                    rootDirField = textfield { hgrow = Priority.ALWAYS }
                    button("Choose...") { action { pickDirectory() } }
                }
                hbox {
                    spacing = 5.0
                    alignment = Pos.CENTER_LEFT

                    children.add(makeJournalUrlLabel())
                    children.add(journalUrlField)
                }
            }
        }

        fun pickDirectory() {
            val existing = rootDirField.text
            val initial = if (existing.isNotBlank() && Files.isDirectory(Paths.get(existing))) {
                File(existing)
            } else { null }

            val chosen = DirectoryChooser().apply {
                title = "Select directory to publish to"
                if (initial != null)
                    initialDirectory = initial
            }.showDialog(ownerWindow)

            if (chosen != null) {
                val unknown = findUnknownDirContents(chosen)
                if (unknown.isEmpty()) {
                    rootDirField.text = chosen.path
                } else {
                    Alert(Alert.AlertType.ERROR).apply {
                        title = "Invalid selection"
                        headerText = "Rejecting possibly dangerous choice."
                        dialogPane.content = Text("""
                            Rejecting choice of local publish directory due to presence
                            of unrecognized files: ${unknown.joinToString(limit = 3)}
                        """.trimIndent().wrap()).apply {
                            wrappingWidth = 600.0
                        }
                    }.showAndWait()
                }
            }
        }

        fun findUnknownDirContents(requestedRootDir: File): List<String> {
            return Files.list(requestedRootDir.toPath())
                .map { it.fileName.toString() }.toList()
                .minus(EncryptedJournal.allFileNames)
        }

        override fun init(existingPubConfig: PublishLocationConfig?, editListener: () -> Unit) {
            if (existingPubConfig is LocalFilesConfig) {
                journalUrlField.text = existingPubConfig.journalUrl
                rootDirField.text = existingPubConfig.rootDir
            }
            listOf(journalUrlField, rootDirField).map {
                it.textProperty().addListener { _, _, _ -> editListener() }
            }
        }

        override fun derivePubConfig(): LocalFilesConfig {
            return LocalFilesConfig(
                journalUrl = journalUrlField.text,
                rootDir = rootDirField.text
            )
        }
    }
}
