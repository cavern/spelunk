package org.timmc.spelunk.gui

import javafx.collections.ObservableList
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TableCell
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.input.KeyCode
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.scene.text.FontPosture
import javafx.util.Callback
import org.timmc.spelunk.State
import org.timmc.spelunk.model.JournalPost
import org.timmc.spelunk.model.OnePost
import org.timmc.spelunk.model.PostStatus
import tornadofx.Stylesheet
import tornadofx.action
import tornadofx.addClass
import tornadofx.box
import tornadofx.button
import tornadofx.cssclass
import tornadofx.em
import tornadofx.insets
import tornadofx.label
import tornadofx.observableListOf
import tornadofx.onDoubleClick
import tornadofx.removeClass
import tornadofx.selectedItem
import tornadofx.tableview
import tornadofx.tooltip
import kotlin.concurrent.read

/**
 * Tab view for managing own journal.
 */
class JournalManage(val state: State): ModalView() {
    val repo = state.repo

    override val node: Node by lazy { initialize() }

    private lateinit var newPostButton: Button
    private lateinit var postsList: TableView<PostAndDir>

    private val postsFeed: ObservableList<PostAndDir> = observableListOf()

    private fun initialize(): Node {
        val root = VBox().apply {
            spacing = 5.0
            padding = insets(5.0)
            newPostButton = button("New post") {
                action { PostEditor.showNewDraftEditor(state) }
                addClass(Styles.newPostButton)
            }
            postsList = tableview(postsFeed) {
                placeholder = HBox().apply {
                    alignment = Pos.CENTER
                    spacing = 10.0
                    label("No posts so far.")
                    button("Write something!") {
                        action { PostEditor.showNewDraftEditor(state) }
                    }
                }
                val idColumn = TableColumn<PostAndDir, Void>("ID").apply {
                    cellFactory = Callback { object : NodeTableCell<PostAndDir>() {
                        init { alignment = Pos.TOP_RIGHT }
                        override val node = Label()
                        override fun update(item: PostAndDir) {
                            node.text = item.dir.id
                        }
                    }}
                }
                val editColumn = TableColumn<PostAndDir, Void>("Edit").apply {
                    cellFactory = Callback { object : NodeTableCell<PostAndDir>() {
                        init { alignment = Pos.TOP_CENTER }
                        override val node = Button("✎").apply {
                            tooltip("Edit post")
                            addClass(Styles.editButton)
                        }
                        override fun update(item: PostAndDir) {
                            node.setOnAction { PostEditor.showEditorFor(state, item.dir.id) }
                        }
                    }}
                }
                val subjectColumn = TableColumn<PostAndDir, Void>("Subject").apply {
                    minWidth = 400.0
                    cellFactory = Callback { object : NodeTableCell<PostAndDir>() {
                        override val node: Label = Label()
                        override fun update(item: PostAndDir) {
                            node.text = item.post.subject
                            if (item.post.status == PostStatus.DRAFT) {
                                node.addClass(Styles.postRowDraft)
                            } else {
                                node.removeClass(Styles.postRowDraft)
                            }
                        }
                    }}
                }
                columns.setAll(idColumn, editColumn, subjectColumn)
                onDoubleClick { selectedItem?.let { onWantPostEdit(it) } }
                setOnKeyPressed { event ->
                    if (event.code == KeyCode.ENTER) {
                        selectedItem?.let { onWantPostEdit(it) }
                    }
                }
            }
        }
        root.stylesheets.add(Styles().externalForm)
        initData()
        state.bus.postChange.addListener { initData() }
        return root
    }

    private fun initData() {
        val posts = repo.journal.posts.list().mapNotNull {
            try {
                state.postLocker(it.id).read {
                    if (it.isDeleted()) return@mapNotNull null
                    PostAndDir(it, it.post.readJson())
                }
            } catch (e: Exception) {
                // TODO Display information about broken posts somewhere in UI
                println("JournalManage: Failed to load post ${it.id}: ${e.javaClass.simpleName} ${e.message}")
                null
            }
        }
        postsFeed.setAll(posts)
    }

    private class Styles: Stylesheet() {
        companion object {
            val newPostButton by cssclass()
            val postRowDraft by cssclass()
            val editButton by cssclass()
        }

        init {
            newPostButton {
                fontSize = 1.2.em
            }

            postRowDraft {
                fontStyle = FontPosture.ITALIC
            }

            editButton {
                fontSize = 1.5.em
                prefWidth = 2.em
                padding = box(0.1.em)
            }
        }
    }

    override fun getInitialFocus(): Node {
        return newPostButton
    }

    fun onWantPostEdit(post: PostAndDir) {
        PostEditor.showEditorFor(state, post.dir.id)
    }
}

data class PostAndDir(
    val dir: OnePost,
    val post: JournalPost
)

/**
 * TableCell implementation that manages a Node and knows how to update it.
 * This avoids some of the boilerplate of TableCell.
 */
abstract class NodeTableCell<T>: TableCell<T, Void>() {
    /** The node managed by this table cell. */
    abstract val node: Node
    /**
     * Update the node with the new data. The prior state may either be empty
     * or previous data, so manage state accordingly.
     */
    abstract fun update(item: T)

    override fun updateItem(item: Void?, empty: Boolean) {
        super.updateItem(item, empty)
        val rowItem = tableRow?.item
        graphic = if (empty || rowItem == null) {
            null
        } else {
            update(rowItem)
            node
        }
    }
}
