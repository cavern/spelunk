package org.timmc.spelunk.gui

import javafx.scene.Node
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Tooltip
import javafx.scene.layout.Pane
import javafx.util.StringConverter
import org.timmc.spelunk.model.Visibility
import tornadofx.toObservable

/**
 * Make a choice box for post visibility.
 */
fun makeVisibilityChoiceBox(): ChoiceBox<Visibility> {
    return ChoiceBox(Visibility.entries.toList().toObservable()).apply {
        converter = object : StringConverter<Visibility>() {
            // These two don't implement a round-trip; one deals in
            // enum names and the other in human-readable names.
            // Probably fine?
            override fun fromString(s: String): Visibility = Visibility.valueOf(s)
            override fun toString(v: Visibility): String = v.shortName
        }
        valueProperty().addListener { _, _, newValue ->
            tooltip = Tooltip("${newValue.shortName}: ${newValue.describeRecipients}")
        }
    }
}

/**
 * Like ListView, but less complicated and featureful.
 */
class ListingManager<I, C: Pane>(
    val container: C,
    val placeholder: Node,
    val rowFactory: (I) -> Node,
    initialItems: Iterable<I> = emptyList()
) {
    private var items: List<I> = emptyList()
    private var rows: List<Node> = emptyList()

    init {
        setItems(initialItems)
    }

    fun setItems(newItems: Iterable<I>) {
        val newItemList = newItems.toList()
        if (newItemList.isEmpty()) {
            items = emptyList()
            rows = emptyList()
            container.children.setAll(placeholder)
        } else {
            // TODO Only create new rows for new items
            val newRows = newItemList.map(rowFactory) // TODO error handling
            items = newItemList
            rows = newRows
            container.children.setAll(newRows)
        }
    }
}
