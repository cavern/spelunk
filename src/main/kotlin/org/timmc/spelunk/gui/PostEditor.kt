package org.timmc.spelunk.gui

import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.collections.ObservableList
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.ButtonBar
import javafx.scene.control.ButtonType
import javafx.scene.control.CheckBox
import javafx.scene.control.ChoiceBox
import javafx.scene.control.ContextMenu
import javafx.scene.control.Dialog
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.control.ScrollPane
import javafx.scene.control.Separator
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.control.TextInputDialog
import javafx.scene.control.Tooltip
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.TransferMode
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.scene.text.FontPosture
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import javafx.stage.FileChooser
import javafx.stage.Stage
import javafx.stage.WindowEvent
import javafx.util.Callback
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.State
import org.timmc.spelunk.Utils
import org.timmc.spelunk.gui.PostEditor.Companion.showEditorFor
import org.timmc.spelunk.model.JournalPost
import org.timmc.spelunk.model.OnePost
import org.timmc.spelunk.model.PostStatus
import org.timmc.spelunk.model.Visibility
import org.timmc.spelunk.os
import org.timmc.spelunk.wrap
import tornadofx.Stylesheet
import tornadofx.action
import tornadofx.addClass
import tornadofx.box
import tornadofx.button
import tornadofx.cssclass
import tornadofx.csspseudoclass
import tornadofx.disableWhen
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.insets
import tornadofx.item
import tornadofx.label
import tornadofx.listview
import tornadofx.multi
import tornadofx.observableListOf
import tornadofx.px
import tornadofx.removeClass
import tornadofx.textfield
import tornadofx.tooltip
import tornadofx.vgrow
import java.io.File
import java.nio.file.ClosedWatchServiceException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption
import java.nio.file.StandardWatchEventKinds
import java.nio.file.WatchKey
import java.nio.file.WatchService
import kotlin.collections.set
import kotlin.concurrent.read
import kotlin.concurrent.write

/**
 * Window for editing a post or creating a new draft. Use [showEditorFor] to
 * get or reuse an editor window for an existing post.
 *
 * Keeps a copy of the original post and a copy representing what would be
 * saved to disk; when they differ, the Save button is enabled. Changes come
 * from UI events and filesystem events, which are also responsible for updating
 * other UI elements.
 */
class PostEditor(
    val state: State,
    /** Post being edited, or null if a new draft. */
    var postDir: OnePost?
) {
    companion object {
        /** Map of post IDs to editors. This also serves as a lock. */
        val openEditors: MutableMap<String, PostEditor> = mutableMapOf()

        /**
         * Create a new editor window for this post, or find existing one, and
         * show it.
         */
        fun showEditorFor(state: State, postId: String): PostEditor {
            synchronized(openEditors) {
                val existing = openEditors[postId]
                return if (existing == null) {
                    val newEditor = PostEditor(state, state.repo.journal.posts.lookup(postId)).apply {
                        createAndShowEditor()
                    }
                    openEditors[postId] = newEditor
                    newEditor
                } else {
                    existing.apply {
                        stage.toFront()
                    }
                }
            }
        }

        /**
         * Create a new editor window for a new post with no ID or contents.
         */
        fun showNewDraftEditor(state: State): PostEditor {
            return PostEditor(state, null).apply { createAndShowEditor() }
        }
    }

    /*== App state ==*/

    var cfg = state.configRead()

    /*== Editor state ==*/

    /** Post at time editor was started (blank post if new draft) */
    var postSaved: JournalPost

    init {
        val postDir = postDir
        if (postDir == null) {
            postSaved = JournalPost.makeBlankPost(cfg)
        } else {
            state.postLocker(postDir.id).read {
                postSaved = postDir.post.readJson()
            }
        }
    }

    /**
     * State of the post being edited, starting out as either a copy
     * of the post being edited, or a blank object if this is
     * a new draft.
     */
    var postEditing: JournalPost = postSaved

    /** Feed for attachments view. */
    val attachmentsObs: ObservableList<Attachment> = observableListOf()

    /** Observable for whether post has a directory yet. */
    val postHasId = SimpleBooleanProperty(postDir != null)

    /*== UI refs ==*/

    lateinit var stage: Stage
    lateinit var subjectField: TextField
    lateinit var tagsContainer: FlowPane
    lateinit var tagsNoneLabel: Label
    lateinit var filterTypeSelector: ChoiceBox<Visibility>
    lateinit var bodyTextArea: TextArea
    lateinit var attachmentsContainer: ListView<Attachment>
    lateinit var saveButton: Button
    lateinit var publishStatusButton: Button
    lateinit var deleteButton: Button

    /*== Closeable resources ==*/

    /**
     * Filesystem watcher for post. Assumes posts are all on same filesystem as
     * repo root.
     */
    val postFilesWatcher: WatchService = state.repo.path.fileSystem.newWatchService()
    /** Thread to watch for file changes in the content-files directory. */
    val attachmentsWatcherThread: Thread = object : Thread() {
        override fun run() {
            while(true) {
                val key: WatchKey = try { postFilesWatcher.take() }
                    catch (e: InterruptedException) { return }
                    catch (e: ClosedWatchServiceException) { return }

                if (key.pollEvents().isNotEmpty()) {
                    // NB: There can be a large number of events from just one
                    // file copy.

                    // Do everything stateful on the UI thread I don't have to
                    // worry so much about synchronization...
                    // Note that postDir should never be null at this point.
                    Platform.runLater { postDir?.let { updateAttachmentsFromDisk(it) } }
                }
                // Reset to keep receiving events; return value indicates
                // whether the dir is still valid.
                if (!key.reset()) return
            }
        }
    }.apply {
        isDaemon = true
    }

    /*== Initialization ==*/

    fun createAndShowEditor(): Stage {
        stage = Stage()
        stage.icons.add(SpelunkApp.appIcon)
        stage.scene = makeScene()
        stage.setOnCloseRequest { closeRequestEvent -> onTryClose(closeRequestEvent) }
        stage.setOnHidden { onClose() }

        initSceneData()

        postDir?.let { startWatchingContentsDir(it) }

        stage.sizeToScene()
        stage.show()

        return stage
    }

    /**
     * Start watching the post's contents directory. This should
     * only be called at stage startup or when a draft is first written to a new
     * post directory.
     */
    fun startWatchingContentsDir(postDir: OnePost) {
        attachmentsWatcherThread.name = "Post-files watcher for ${postDir.path.fileName}"
        attachmentsWatcherThread.start()

        val contentsDir = postDir.attachments.path
        if (Files.isDirectory(contentsDir)) {
            contentsDir.register(postFilesWatcher,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE,
                StandardWatchEventKinds.ENTRY_MODIFY)
        }

        updateAttachmentsFromDisk(postDir) // initial load
    }

    fun makeScene(): Scene {
        // Subject field
        val subject = HBox().apply {
            alignment = Pos.CENTER_LEFT
            spacing = 5.0
            label("Subject:")
            subjectField = textfield {
                hgrow = Priority.ALWAYS
                textProperty().addListener { _, _, newValue -> onUpdateSubject(newValue) }
            }
        }
        // Tags entry and listing
        val tagsEntry = HBox().apply {
            alignment = Pos.CENTER_LEFT
            spacing = 5.0
            label("Tags:")
            textfield {
                hgrow = Priority.ALWAYS
                focusedProperty().addListener { _, _, newValue ->
                    // If I navigate away from the tags input area without
                    // selecting a tag, clear the text field
                    if (newValue == false)
                        this.clear()
                }
                action {
                    onAddTag(text)
                    text = ""
                }
            }
        }
        tagsContainer = FlowPane().apply {
            alignment = Pos.CENTER_LEFT
            hgap = 5.0
            vgap = 5.0
            tagsNoneLabel = label("no tags") {
                font = Font.font(
                    Font.getDefault().family,
                    FontPosture.ITALIC,
                    -1.0
                )
            }
        }
        // Visibility selector
        val visibility = HBox().apply {
            alignment = Pos.CENTER_LEFT
            spacing = 5.0
            label("Who can see this?")
            filterTypeSelector = makeVisibilityChoiceBox()
            filterTypeSelector.valueProperty().addListener { _, _, newValue ->
                onChangeVisibilityType(newValue)
            }
            children.add(filterTypeSelector)
        }
        // Body editing
        bodyTextArea = TextArea().apply {
            vgrow = Priority.ALWAYS
            textProperty().addListener { _, _, newValue ->
                onBodyTextEdit(newValue)
            }
            tabMeansTraversal()
            isWrapText = true
        }
        // Attachments
        val attachmentsListing = ScrollPane().apply {
            vgrow = Priority.SOMETIMES
            fitToWidthProperty().set(true)
            fitToHeightProperty().set(true)
            prefHeight = 100.0
            attachmentsContainer = listview(attachmentsObs) {
                placeholder = Label("Drop files here to attach")
                cellFactory = Callback { AttachmentListCell() }
                setOnDragOver { event ->
                    if (event.dragboard.hasFiles() && event.dragboard.files.all { it.isFile }) {
                        event.acceptTransferModes(TransferMode.COPY)
                    }
                }
                setOnDragEntered { addClass(Styles.dragOver) }
                setOnDragExited { removeClass(Styles.dragOver) }
                setOnDragDropped { event ->
                    val success = event.dragboard.files.orEmpty().fold(true) { success, file ->
                        try { success && onRequestAddFile(file) }
                        catch(e: Exception) { false }
                    }
                    event.isDropCompleted = success
                    event.consume()
                }
            }
        }
        val attachmentsControls = BorderPane().apply {
            left = button("Attach file...") {
                tooltip("Open a file chooser to attach one or more files to this post (copies to attachments directory)")
                action {
                    FileChooser().apply {
                        title = "Select files to attach"
                    }.showOpenMultipleDialog(stage)?.map { onRequestAddFile(it) }
                }
            }
            right = button("Show files") {
                tooltip("Open a file browser to see these files in context")
                disableWhen(postHasId.not())
                action {
                    postDir?.let { os.showFileManager(it.attachments.path) }
                }
            }
        }
        // Post status controls
        val postSaveAndStatus = BorderPane().apply {
            left = hbox {
                alignment = Pos.BOTTOM_LEFT
                spacing = 5.0
                saveButton = button("Save") {
                    font = Font.font(Font.getDefault().family,20.0)
                    action { onSave() }
                }
                publishStatusButton = button {
                    action { onTogglePublishStatus() }
                }
            }
            deleteButton = Button("Delete").apply {
                alignment = Pos.BOTTOM_LEFT
                isDisable = postDir == null
                action { onAskDelete() }
            }
            right = deleteButton
        }

        val root = VBox().apply {
            alignment = Pos.TOP_LEFT
            spacing = 5.0
            padding = insets(5.0)
            minWidth = 200.0
            prefWidth = 600.0

            children.setAll(
                subject,
                bodyTextArea,
                Separator(),
                tagsEntry, tagsContainer,
                visibility,
                Label("Attachments:"), attachmentsListing, attachmentsControls,
                Separator(),
                postSaveAndStatus
            )
        }
        val scene = Scene(root)
        scene.stylesheets.add(Styles().externalForm)
        return scene
    }

    /** UI element for a row in the attachment table. */
    inner class AttachmentListCell: ListCell<Attachment>() {
        var cb: CheckBox = CheckBox().apply {
            // Cover full width so context menu available all the way across
            maxWidth = Double.MAX_VALUE
            selectedProperty().addListener { _, _, selectedNow ->
                item?.let { onAttachmentToggle(it, selectedNow) }
            }
        }

        override fun updateItem(item: Attachment?, empty: Boolean) {
            super.updateItem(item, empty)

            if (empty || item == null) {
                graphic = null
                return
            }

            cb.text = if (item.status == AttachmentStatus.MISSING) {
                cb.addClass(Styles.attachmentMissing)
                "${item.filename} is missing from disk"
            } else {
                cb.removeClass(Styles.attachmentMissing)
                val sizeFmt = item.size?.let { Utils.formatDataSize(it) } ?: ""
                "${item.filename} $sizeFmt ${item.contentType}"
            }
            cb.selectedProperty().set(item.status != AttachmentStatus.EXTRANEOUS)

            cb.contextMenu = ContextMenu().apply {
                val filePath = postDir!!.attachments.path.resolve(item.filename)
                // Present/absent is the main determinant of the menu
                if (item.status.isPresent) {
                    item("Open") { action {
                        Runtime.getRuntime().exec(arrayOf("xdg-open", filePath.toString()))
                    }}
                    item("Rename") { action {
                        TextInputDialog(item.filename).apply {
                            title = "Rename attachment"
                            headerText = "What do you want to rename this attachment to?"
                        }.showAndWait().ifPresent { onAskAttachmentRename(item, it) }
                    }}
                    item("Delete") { action {
                        val deleteButton = ButtonType("Delete file", ButtonBar.ButtonData.YES)
                        val cancelButton = ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE)
                        Dialog<ButtonType>().apply {
                            title = "Delete attachment"
                            headerText = "Delete attachment?"
                            dialogPane.content = TextFlow(Text("""
                            This will delete the attached file. (If you attached it from elsewhere,
                            it won't delete the original, though.)
                            """.trimIndent().wrap()))
                            dialogPane.buttonTypes.setAll(deleteButton, cancelButton)
                        }.showAndWait().ifPresent { if (it == deleteButton) onAskAttachmentDelete(item) }
                    }}
                } else {
                    // Special case: AttachmentStatus.MISSING
                    if (item.status.isIncluded) {
                        item("Replace with...") { action {
                            FileChooser().apply {
                                title = "Choose a file to attach as ${item.filename}"
                                showOpenDialog(stage)?.let { onRequestAddFile(it, item.filename) }
                            }
                        }}
                    }
                }
            }

            graphic = cb
        }
    }

    class Styles: Stylesheet() {
        companion object {
            val dragOver by csspseudoclass()
            val attachmentMissing by cssclass()
        }

        init {
            dragOver {
                backgroundColor = multi(Color.BLUE, Color.WHITE)
                backgroundInsets = multi(box(0.px), box(3.px))
            }

            attachmentMissing {
                fontStyle = FontPosture.ITALIC
            }
        }
    }

    /*== Data lifecycle ==*/

    fun initSceneData() {
        subjectField.text = postEditing.subject
        updateTagsContainer()
        filterTypeSelector.value = postEditing.visibility
        bodyTextArea.text = postSaved.body
        updatePublishStatusButton()

        // This updates the state of things that want to know whether the post
        // has been edited from its original form.
        onEdit()
    }

    /** When I try to close the window, warn me if there are unsaved changes. */
    fun onTryClose(closeRequestEvent: WindowEvent) {
        val edited = postEditing != postSaved
        if (!edited) {
            return // allow to close
        }
        val save = ButtonType("Save changes", ButtonBar.ButtonData.YES)
        val discard = ButtonType("Discard changes", ButtonBar.ButtonData.NO)
        val cancel = ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE)
        val prompt = Dialog<ButtonType?>().apply {
            title = "Unsaved changes"
            headerText = "Save changes?"
            contentText = "You have unsaved changes on this post."
            dialogPane.buttonTypes.setAll(save, discard, cancel)
        }
        when (prompt.showAndWait().orElse(cancel)) {
            save -> onSave()
            cancel -> closeRequestEvent.consume() // prevent closing
            // otherwise just close (covers `discard` option)
        }
    }

    /** Clean up when window is closed. */
    fun onClose() {
        attachmentsWatcherThread.interrupt()
        postFilesWatcher.close()
        // Once it's hidden, don't keep a reference or reuse it.
        postDir?.id.let { openEditors.remove(it) }
    }

    /*== Builders ==*/

    fun makeTagDeleteButton(tag: String): Button {
        val icon = ImageView(Image(resourceStream("icon-delete.png")))
            .apply {
                fitHeight = 13.0
                fitWidth = 13.0
            }
        return Button(tag, icon).apply {
            tooltip { text = """Remove tag "$tag"""" }
            action { onDeleteTag(tag) }
        }
    }

    /*== View updaters ==*/

    fun updateTagsContainer() {
        val tags = postEditing.tags
        tagsContainer.children.removeIf { it != tagsNoneLabel }
        tagsContainer.children.addAll(tags.map { makeTagDeleteButton(it) })
        tagsNoneLabel.visibleProperty().set(tags.isEmpty())
        tagsNoneLabel.managedProperty().set(tags.isEmpty())
    }

    /** Update the attachments view's data feed from disk. */
    fun updateAttachmentsFromDisk(postDir: OnePost) {
        val contentDir = postDir.attachments

        val selectedFileNames = postEditing.attachments
            .map { it.filename }
            .toSet()
        val groundTruth = state.postLocker(postDir.id).read {
            Files.list(contentDir.path).filter {
                Files.isRegularFile(it)
            }.map { contentFile ->
                val fileName = contentFile.fileName.toString()
                val status = if (selectedFileNames.contains(fileName)) AttachmentStatus.SELECTED
                else AttachmentStatus.EXTRANEOUS
                Attachment(
                    status = status,
                    filename = fileName,
                    contentType = Files.probeContentType(contentFile),
                    size = Files.size(contentFile)
                )
            }.toList()
        }
        val missingFromDiskNames = selectedFileNames
            .minus(groundTruth.map {it.filename}.toSet())
            .map { Attachment(
                status = AttachmentStatus.MISSING,
                filename = it,
                size = null,
                contentType = null
            ) }
        // FIXME: This messes up ordering of attachments.
        // TODO: Add interface for reordering attachments, and adjust code to preserve the order.
        attachmentsObs.setAll((groundTruth + missingFromDiskNames).sortedBy { it.filename })
    }

    fun updatePublishStatusButton() {
        when (postEditing.status) {
            PostStatus.DRAFT -> {
                publishStatusButton.text = "...and publish"
                publishStatusButton.tooltip = Tooltip("Save changes and publish to your journal")
            }
            PostStatus.PUBLISHED -> {
                publishStatusButton.text = "...and retract as draft"
                publishStatusButton.tooltip = Tooltip("Save changes and unpublish from your journal")
            }
        }
    }

    /*== Event handlers ==*/

    /** Reflect state changes after an edit. */
    fun onEdit() {
        val edited = postEditing != postSaved
        saveButton.disableProperty().set(!edited)
        stage.title = listOfNotNull(
            if (edited) "*" else null,
            (postDir?.id ?: "[new]") + ":",
            subjectField.text.ifEmpty { "[no subject]" }
        ).joinToString(" ")
    }

    /** When subject is edited, update title. */
    fun onUpdateSubject(newValue: String) {
        postEditing = postEditing.copy(subject = newValue)
        onEdit()
    }

    fun onChangeVisibilityType(newValue: Visibility) {
        postEditing = postEditing.copy(visibility = newValue)
        onEdit()
    }

    fun onAddTag(tagValue: String) {
        postEditing = postEditing.copy(tags = postEditing.tags.plus(tagValue))
        updateTagsContainer()
        onEdit()
    }

    fun onDeleteTag(tagValue: String) {
        postEditing = postEditing.copy(tags = postEditing.tags.minus(tagValue))
        updateTagsContainer()
        onEdit()
    }

    fun onBodyTextEdit(newValue: String) {
        postEditing = postEditing.copy(body = newValue)
        onEdit()
    }

    /**
     * When an attachment is selected or deselected, update state and UI.
     */
    fun onAttachmentToggle(item: Attachment, selected: Boolean) {
        val withoutFile = postEditing.attachments.filter {
            it.filename != item.filename
        }
        val newAttachments = if (selected) {
            // Avoids duplicate entries
            withoutFile.plus(JournalPost.JournalPostAttachment(filename = item.filename))
        } else {
            withoutFile
        }
        postEditing = postEditing.copy(attachments = newAttachments)

        // Also update the UI's attachment feed, but might as well combine it
        // with refreshing data from disk.
        updateAttachmentsFromDisk(postDir!!)

        onEdit()
    }

    /**
     * When I ask to rename an attachment, validate the request, do it, and
     * update state and UI.
     */
    fun onAskAttachmentRename(toRename: Attachment, requestedFileName: String) {
        if (!requestedFileName.matches(Regex("[^/]+"))) return
        val attachmentsPath = postDir!!.attachments.path
        val newPath = attachmentsPath.resolve(requestedFileName)

        state.postLocker(postDir!!.id).write {
            if (Files.exists(newPath) || postEditing.attachments.any { it.filename == requestedFileName }) {
                return
            }

            Files.move(attachmentsPath.resolve(toRename.filename), newPath)
        }

        postEditing = postEditing.copy(attachments = postEditing.attachments.map {
            if (it.filename == toRename.filename)
                it.copy(filename = requestedFileName)
            else
                it
        })

        onEdit()
    }

    /**
     * When I ask to delete an attachment, do it and update state and UI.
     */
    fun onAskAttachmentDelete(toDelete: Attachment) {
        val filePath = postDir!!.attachments.path.resolve(toDelete.filename)

        state.postLocker(postDir!!.id).write {
            Files.delete(filePath) // throws on failure, shortcircuiting the following
        }

        postEditing = postEditing.copy(attachments = postEditing.attachments.filter {
            it.filename != toDelete.filename
        })

        onEdit()
    }

    /**
     * Find a non-conflicting filename for dropping srcFile into dropDir. Not
     * guaranteed to be non-conflicting (due to concurrency) and does not
     * protect against adversarial inputs.
     */
    fun pickDropFileName(srcFile: File, dropDir: Path): String {
        val existing = Files.list(dropDir).map {
            it.fileName.toString()
        }.toList().toSet()
        val srcName = srcFile.name
        if (existing.contains(srcName)) {
            for (ctr in 1..1000) {
                val candidateName = "$srcName-$ctr"
                if (!existing.contains(candidateName))
                    return candidateName
            }
            // Give up on sequential names and use a random string
            return "$srcName-${Crypto.hiddenId()}"
        } else {
            return srcName
        }
    }

    /**
     * When a file is dropped on the attachment area, add it to the model
     * and let the attachments observable get updated by the watcher.
     *
     * This can also force a new draft to be saved to disk to make sure there's a
     * directory for attachments.
     */
    fun onRequestAddFile(file: File, overrideDestName: String? = null): Boolean {
        if (!file.isFile)
            return false

        val postDir = postDir ?: onSave() // force a save
        val destDir = postDir.attachments.path

        // The write above can be done under a separate lock -- and in fact
        // must be, since we aren't guaranteed until after the onSave that
        // there is even a postDir to acquire a lock for.
        val destName = state.postLocker(postDir.id).write {
            val destName = overrideDestName ?: pickDropFileName(file, destDir)
            // By default, will not overwrite existing files
            Files.copy(file.toPath(), destDir.resolve(destName), StandardCopyOption.COPY_ATTRIBUTES)
            destName
        }

        // Filter out any entries with colliding filename, just in case
        val newContentFiles = postEditing.attachments.filter { it.filename != destName }
            .plus(JournalPost.JournalPostAttachment(filename = destName))
        postEditing = postEditing.copy(attachments = newContentFiles)
        // The file watcher should pick up the FS change and update
        // `attachmentsObs` later on the UI thread
        onEdit()
        return true
    }

    /**
     * Save post to existing or new post directory. Afterwards, [postDir] is
     * not null.
     */
    fun onSave(): OnePost {
        val existingPostDir = postDir
        val retPostDir = existingPostDir ?: state.repo.journal.newPost()
        postEditing = postEditing.copy(
            persistentId = state.postIdEncrypted(retPostDir.id.toLong()), // just changes first time
            updatedDate = System.currentTimeMillis() / 1000
        )

        state.postLocker(retPostDir.id).write {
            if (existingPostDir == null) {
                // Create post and do init appropriate for that
                retPostDir.initPost(postEditing)
                postDir = retPostDir
                postHasId.set(true)
                startWatchingContentsDir(retPostDir)
                openEditors[retPostDir.id] = this
            } else {
                existingPostDir.post.writeJson(postEditing)
            }
            postSaved = postEditing
        }
        deleteButton.isDisable = false

        onEdit()
        state.bus.postChange.broadcast(retPostDir.id)
        return retPostDir
    }

    /** When the delete button is pressed. */
    fun onAskDelete() {
        val postDir = postDir ?: return

        val delete = ButtonType("Delete post and attachments", ButtonBar.ButtonData.APPLY)
        val cancel = ButtonType("Cancel", ButtonBar.ButtonData.NO)
        val prompt = Dialog<ButtonType?>().apply {
            title = "Delete post"
            headerText = "Delete this post?"
            dialogPane.content = Text(
                "The entire post and all of its attachments will be deleted irrevocably."
            ).apply {
                wrappingWidth = 600.0
            }
            dialogPane.buttonTypes.setAll(delete, cancel)
        }
        if (prompt.showAndWait().orElse(cancel) == delete) {
            state.postLocker(postDir.id).write {
                postDir.deletionMarker.write("Post deleted ${System.currentTimeMillis()}\n")
                postDir.attachments.deleteRecursively()
                Files.deleteIfExists(postDir.post.path)
            }
            state.bus.postChange.broadcast(postDir.id)
            stage.close()
        }
    }

    /**
     * When publish status is changed, toggle status and save post.
     */
    fun onTogglePublishStatus() {
        postEditing = postEditing.copy(status = when (postEditing.status) {
            PostStatus.DRAFT -> PostStatus.PUBLISHED
            PostStatus.PUBLISHED -> PostStatus.DRAFT
        })
        updatePublishStatusButton()
        onEdit() // probably not necessary, since onSave will call it too
        // Saving is needed here for two reasons:
        // - It notifies the publisher that a post needs to be un/published
        // - Even if that weren't true, if we're going to make changes remotely,
        //   better make them locally first!
        onSave()
    }
}

/**
 * Represents an attachment (or lack thereof) for display.
 *
 * These are files that are *not* the main file, and are either in the content
 * directory or were previously selected but are now missing.
 */
data class Attachment(
    val status: AttachmentStatus,
    val filename: String,
    val size: Long?,
    val contentType: String?
)

enum class AttachmentStatus(
    val isPresent: Boolean,
    val isIncluded: Boolean
) {
    /** File is present and selected for inclusion. */
    SELECTED(isPresent = true, isIncluded = true),
    /** File is present but had not been selected for inclusion. */
    EXTRANEOUS(isPresent = true, isIncluded = false),
    /** File is missing even though it had been selected for inclusion. */
    MISSING(isPresent = false, isIncluded = true)
}
