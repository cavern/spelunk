package org.timmc.spelunk.gui

import javafx.application.Platform
import org.timmc.spelunk.Publishing
import org.timmc.spelunk.model.AddressBook
import org.timmc.spelunk.model.Config

/**
 * Event bus for the GUI.
 *
 * This is specific to the GUI because it runs callbacks on the UI thread.
 */
class Bus {
    /** Notified when a post is created, modified, or deleted; arg is postId. */
    val postChange = Topic1<String>()

    /** Notified when the publish status changes; arg is new status. */
    val publishStatusChange = Topic1<Publishing.PublishStatus>()

    /**
     * Notified when a new post shows up in a contact's journal, or an existing
     * one is edited. Note that posts that have been removed do not cause events.
     */
    val fetchedNewOrUpdatedPost = Topic0()

    /** Notified when the fetch status changes. */
    val fetchStatusChange = Topic0()

    /** Notified when address book (or any contact) updated. */
    val addressBookChanged = Topic1<AddressBook>()

    /** Notified when config is edited; arg is new config object. */
    val configChange = Topic1<Config>()
}

/** Generic topic, just needing a broadcast(...) function implemented. */
abstract class TopicN<L> {
    private val listenersLock = Object()
    /** Only update when [listenersLock] is held! */
    protected var listeners = listOf<L>()

    /**
     * Register to be notified when a message is broadcast on this topic.
     */
    fun addListener(toAdd: L) {
        synchronized(listenersLock) {
            listeners = listeners.plus(toAdd)
        }
    }
}

/** Topic where no data is sent to listeners. */
class Topic0: TopicN<() -> Unit>() {
    /** Asynchronously call all listeners on this topic. */
    fun broadcast() {
        listeners.forEach { Platform.runLater { it.invoke() } }
    }
}

/** Topic where listeners receive one value as a message. */
class Topic1<P: Any>: TopicN<(P) -> Unit>() {
    /** Asynchronously call all listeners on this topic with the message. */
    fun broadcast(message: P) {
        listeners.forEach { Platform.runLater { it.invoke(message) } }
    }
}
