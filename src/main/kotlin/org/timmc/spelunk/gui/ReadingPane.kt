package org.timmc.spelunk.gui

import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.image.Image
import javafx.scene.input.KeyCode
import javafx.scene.input.MouseButton
import javafx.scene.layout.BorderStrokeStyle
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.scene.text.FontPosture
import javafx.scene.text.FontWeight
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import org.timmc.spelunk.Fetching
import org.timmc.spelunk.State
import org.timmc.spelunk.memoize
import org.timmc.spelunk.model.cavern.CavVisibility
import org.timmc.spelunk.os
import tornadofx.Stylesheet
import tornadofx.addClass
import tornadofx.borderpane
import tornadofx.box
import tornadofx.c
import tornadofx.cssclass
import tornadofx.em
import tornadofx.flowpane
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.imageview
import tornadofx.insets
import tornadofx.multi
import tornadofx.px
import tornadofx.text
import tornadofx.textflow
import tornadofx.tooltip
import tornadofx.vbox
import java.nio.file.Files
import java.nio.file.Path
import java.time.Instant

class ReadingPane(val state: State): ModalView() {
    override val node: Node by lazy { initialize() }

    private lateinit var postsListing: ListingManager<Fetching.FetchedPost, VBox>

    private fun initialize(): Node {
        // This allows for collapsible margins and centering
        val root = HBox().apply {
            alignment = Pos.TOP_CENTER
            padding = insets(5)

            // The one child is the vertical stack of posts
            val postsContainer = vbox {
                alignment = Pos.TOP_LEFT
                spacing = 15.0
                maxWidth = 850.0
            }
            postsListing = ListingManager(postsContainer,
                Text("No posts"), ::maybeMakePostNode)
        }

        state.bus.fetchedNewOrUpdatedPost.addListener(::updateListing)
        updateListing()

        // TODO listen on Bus for fetch events, once those exist
        root.stylesheets.add(Styles().externalForm)
        return root
    }

    /** Try to make a node for a post, or make a failure message instead. */
    private fun maybeMakePostNode(fetchedPost: Fetching.FetchedPost): Node {
        return try {
            makePostNode(fetchedPost)
        } catch (t: Throwable) {
            TextFlow().apply {
                addClass(Styles.postLoadFailure)
                text("[Failed to load post ${fetchedPost.postId}: ${t.javaClass.simpleName} ${t.message}]")
            }
        }
    }

    val imageForVisibilityType = { vt: CavVisibility ->
        Image(resourceStream(when (vt) {
            CavVisibility.PUBLIC -> "icon-vt-public-20.png"
            CavVisibility.BROAD -> "icon-vt-broad-20.png"
            CavVisibility.KNOWN -> "icon-vt-known-20.png"
            CavVisibility.FILTERED -> "icon-vt-filtered-20.png"
        }))
    }.memoize()

    private fun makePostNode(fetchedPost: Fetching.FetchedPost): Node {
        val post = fetchedPost.post

        val subject = post.subject.takeUnless { it.isBlank() } ?: "[no subject]"
        val baseDateline = "on " + displayTime(post.publishDate)
        val fullDateline = if (post.updatedDate == post.publishDate) {
            baseDateline
        } else {
            baseDateline + " (updated ${displayTime(post.updatedDate)})"
        }

        return VBox().apply {
            addClass(Styles.post)
            alignment = Pos.TOP_LEFT
            minWidth = 300.0
            prefWidth = 800.0
            maxWidth = 800.0

            borderpane {
                addClass(Styles.postHeader)
                center = vbox {
                    textflow {
                        addClass(Styles.subjectLine)
                        text(subject)
                    }
                    hbox {
                        addClass(Styles.datelineAndVisibility)
                        alignment = Pos.CENTER_LEFT
                        spacing = 5.0
                        imageview(imageForVisibilityType(post.visibilityType)) {
                            addClass(Styles.visibilityIcon)
                            hgrow = Priority.NEVER
                            tooltip("Visibility: ${post.visibilityType.whoDescription}")
                        }
                        textflow {
                            hgrow = Priority.ALWAYS
                            text(fullDateline)
                        }
                    }
                    textflow {
                        addClass(Styles.byline)
                        val handle = fetchedPost.contact.localHandle?.takeIf { it.isNotBlank() }
                            ?: "[no name]"
                        text("Posted by $handle at ${fetchedPost.contact.fetchUri}")
                    }
                }
                left = hbox{
                    addClass(Styles.authorIconBox)
                    imageview(Image(resourceStream("icon-author-no-avatar-100.png")))
                }
            }
            textflow {
                addClass(Styles.body)
                text(post.body)
            }
            if (fetchedPost.attachments.isNotEmpty()) {
                flowpane {
                    children.setAll(fetchedPost.attachments.map {
                        makeAttachmentNode(it)
                    })
                }
            }
            textflow {
                addClass(Styles.tags)
                text(when {
                    post.tags.isEmpty() -> "No tags"
                    else -> "Tagged: " + post.tags.joinToString()
                })
            }
        }
    }

    val imageAttachmentTypeUnknown = Image(resourceStream("attachment-unknown.png"))
    val imageMimeTypes = setOf("image/png", "image/gif", "image/jpeg")

    private fun makeAttachmentNode(attach: Fetching.FetchedAttachment): Node {
        val img = if (Files.exists(attach.storage) && imageMimeTypes.contains(attach.meta.allegedMimeType)) {
            Image(Files.newInputStream(attach.storage))
        } else {
            imageAttachmentTypeUnknown
        }
        return VBox().apply {
            addClass(Styles.attachment)
            alignment = Pos.TOP_CENTER
            isFocusTraversable = true
            imageview(img) {
                addClass(Styles.attachmentPreview)
                fitHeight = 80.0
                fitWidth = 80.0
                isPreserveRatio = true
                isSmooth = true
            }
            textflow { text(attach.meta.clearFilename) }
            setOnMouseClicked {
                if (it.button == MouseButton.PRIMARY) {
                    when (it.clickCount) {
                        1 -> requestFocus() // left-click
                        2 -> showAttachment(attach.storage) // double-click
                    }
                }
            }
            setOnKeyPressed { if (it.code == KeyCode.ENTER) {
                showAttachment(attach.storage)
            } }
        }
    }

    private fun showAttachment(attachPath: Path) {
        os.showFileManager(attachPath.parent)
    }

    private class Styles: Stylesheet() {
        companion object {
            val post by cssclass()
            val postHeader by cssclass()
            val subjectLine by cssclass()
            val datelineAndVisibility by cssclass()
            val byline by cssclass()
            val visibilityIcon by cssclass()
            val authorIconBox by cssclass()
            val body by cssclass()
            val postLoadFailure by cssclass()
            val tags by cssclass()
            val attachment by cssclass()
            val attachmentPreview by cssclass()
        }

        init {
            post {
                borderWidth += box(1.px)
                borderColor += box(c("#aaa"))
                borderRadius += box(3.px)
                padding = box(0.3.em)
            }

            postHeader {
                borderWidth = multi(box(0.px, 0.px, 1.px, 0.px))
                borderColor = multi(box(c("#00000020")))
                borderStyle = multi(BorderStrokeStyle.DOTTED)
            }

            subjectLine {
                fontSize = 1.3.em
                padding = box(0.em, 0.em, 0.3.em, 0.em)
            }

            datelineAndVisibility {
                padding = box(0.em, 0.em, 0.3.em, 0.em)
            }

            byline {
                padding = box(0.em, 0.em, 0.3.em, 0.em)
            }

            authorIconBox {
                padding = box(0.em, 0.6.em, 0.6.em, 0.em)
            }

            body {
                padding = box(0.7.em, 0.em, 0.7.em, 0.em)
            }

            tags {
                borderWidth = multi(box(1.px, 0.px, 0.px, 0.px))
                borderColor = multi(box(c("#00000020")))
                borderStyle = multi(BorderStrokeStyle.DOTTED)

                fontStyle = FontPosture.ITALIC
            }

            attachment {
                borderWidth = multi(box(2.px))
                borderColor = multi(box(c("#aaa")))
                borderRadius = multi(box(3.px))

                maxWidth = 150.px
                padding = box(0.2.em)

                fill = Color.BLACK
                backgroundColor = multi(Color.WHITE)

                and(focused) {
                    fill = Color.BLACK
                    backgroundColor = multi(c("#def"))
                    borderColor = multi(box(c("#8af")))
                }
            }

            attachmentPreview {
                borderWidth = multi(box(2.px))
                borderColor = multi(box(c("#333")))
                borderRadius = multi(box(3.px))
            }

            postLoadFailure {
                fontSize = 1.5.em
                fontWeight = FontWeight.BOLD
                padding = box(1.em)
            }
        }
    }

    private val postComparator = Comparator
        .comparing<Fetching.FetchedPost, Instant>({ it.discoveryDate }, naturalOrder())
        .thenComparing({ it.post.publishDate }, naturalOrder())
        .reversed()!!

    private fun updateListing() {
        val posts = state.fetching.getAllPosts().sortedWith(postComparator)
        postsListing.setItems(posts)
    }
}
