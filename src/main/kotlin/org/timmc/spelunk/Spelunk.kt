package org.timmc.spelunk

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import org.timmc.spelunk.cmd.CreateCmd
import org.timmc.spelunk.cmd.FetchCmd
import org.timmc.spelunk.cmd.GuiCmd
import org.timmc.spelunk.cmd.InitCmd
import org.timmc.spelunk.cmd.ListCmd
import org.timmc.spelunk.cmd.MaintenanceCmd
import org.timmc.spelunk.cmd.PushCmd

class Spelunk : CliktCommand(
    name = "spelunk",
    help = """
  A Cavern app. Cavern is an underground social network.

  Launches the GUI by default; use `spelunk gui` to pass additional options
  in GUI mode. Other subcommands are the CLI interface.
  """.trimIndent(),
    invokeWithoutSubcommand = true
) {
     override fun run() {
        // If no subcommand was selected, launch the GUI
        if (currentContext.invokedSubcommand == null) {
            GuiCmd().parse(emptyList())
        }
    }
}

/**
 * This is a function because RepoCommandBase instances are stateful (they cache
 * the base dir).
 */
fun cli(): Spelunk {
    return Spelunk().subcommands(
        GuiCmd(),
        InitCmd(),
        ListCmd(),
        CreateCmd(),
        PushCmd(),
        FetchCmd(),
        MaintenanceCmd
    )
}

/**
 * "Backdoor" entry point for programmatic use; throws rather than calling
 * System.exit().
 */
fun mainInternal(vararg args: String) {
    cli().parse(listOf(*args))
}

/** Normal JVM entry point. */
fun main(args: Array<String>) {
    try {
        cli().main(args)
    } catch(t: Throwable) {
        Utils.die(t)
    }
}
