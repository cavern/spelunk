package org.timmc.spelunk.compat

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.TextColumnType
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.transactions.transaction
import org.timmc.spelunk.SQLite3
import org.timmc.spelunk.State
import org.timmc.spelunk.State.Companion.Upgrader
import org.timmc.spelunk.State.Companion.Version
import org.timmc.spelunk.State.Companion.latestVersion
import org.timmc.spelunk.Utils
import java.nio.file.Path
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.deleteExisting
import kotlin.io.path.deleteIfExists
import kotlin.io.path.deleteRecursively
import kotlin.io.path.exists
import kotlin.io.path.isDirectory
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.moveTo
import kotlin.io.path.name
import kotlin.io.path.readText
import kotlin.io.path.writeText

/**
 * The upgraders are kept extracted from State in their own file and package for
 * two reasons:
 *
 * - Make it easier to tell what dependencies the upgraders have, since the
 *   upgraders make it harder to accept breaking changes in dependencies and
 *   internal APIs; and
 * - Make it easier to navigate State.kt, which was becoming overrun with
 *   upgrader code.
 *
 * Upgraders have to be written carefully because by their nature they operate
 * in a world where the source code of the rest of the application is in
 * mismatch to the expected DB schema and filesystem layout.
 *
 * Guidelines for writing upgraders:
 *
 * - Use hardcoded paths. Don't use Repo's filesystem navigation (such as
 *   `state.repo.cache`) or anything that relies on it implicitly (such as
 *   `state.fetching`).
 * - Use SQL directly rather than the Exposed classes in State, Fetching, etc.
 *   If you want to use Exposed, copy Table definitions into the upgrader.
 * - Write your own data mapping code, since data classes can change as well.
 *
 * Try to keep the output of this command short:
 *
 * ```
 * ack 'state\.([A-Za-z0-9_\-.]+)' src/main/kotlin/org/timmc/spelunk/compat/ -ho | sort | uniq
 * ```
 */
object StateUpgrade {
    /**
     * All known upgraders for earlier iterations of the repository format.
     *
     * These should form a chain such that for any two adjacent items A and
     * B, `A.version < B.version` and `A.targetVersion == B.version`.
     * (The last upgrader's target should equal [latestVersion].)
     *
     * Each upgrader's target version must be higher than its base version,
     */
    val formatUpgraders: List<Upgrader<*>> = listOf(
        Upgrade_13_0_to_14_0,
        Upgrade_14_0_to_15_0,
        Upgrade_15_0_to_16_0,
        Upgrade_16_0_to_17_0,
    )
}

/** Encode string for SQL. */
fun sqlStr(data: String): String {
    return TextColumnType().nonNullValueToString(data)
}

/** Encode bytes for SQL (SQLite-specific). */
@Suppress("unused")
fun sqlBlob(data: ByteArray): String {
    return 'x' + sqlStr(Utils.hexEncode(data))
}

/**
 * Move [old] file or directory to [new], as long as [forReal] is true.
 * Otherwise, just throw if the operation would not be possible.
 */
fun moveIf(forReal: Boolean, old: Path, new: Path) {
    if (new.exists()) {
        throw IllegalStateException("Cannot rename $old to $new, since the latter already exists")
    }
    if (forReal) {
        old.moveTo(new, overwrite = false)
    }
}

/**
 * Many of the upgraders (and everything before state version 13) do not produce
 * a plan object. This provides a little support by allowing them to not declare
 * their plan type.
 */
abstract class UpgraderNoPlan(
    version: Version, targetVersion: Version
): Upgrader<Void?>(version, targetVersion) {
    abstract fun doPreflight(state:State)
    abstract fun doUpgrade(state:State)

    override fun preflightAndPlan(state: State): Void? {
        doPreflight(state)
        return null
    }

    override fun upgradeByPlan(state: State, plan: Void?) {
        doUpgrade(state)
    }
}

/**
 * Add all-zero hashes to already-downloaded attachment metadata and convert
 * keys to a compact hex representation rather than a nested object with an
 * array of numbers.
 *
 * A SodiumSecretStreamKey, for example, ended up serialized to JSON as
 * {"keyBytes": {"bytes": [12, 34, 56, ..., 78, 9]}} rather than just
 * "0C2238...4E09".
 */
@Suppress("ClassName")
object Upgrade_13_0_to_14_0 : Upgrader<List<Pair<Path, String>>>(
    Version(13, 0), Version(14, 0),
) {
    /** Old data class that we're getting rid of. */
    data class ByteArrayEq13(
        val bytes: ByteArray
    ) {
        override fun equals(other: Any?): Boolean {
            return other is ByteArrayEq13 && bytes.contentEquals(other.bytes)
        }

        override fun hashCode(): Int {
            return bytes.contentHashCode()
        }
    }

    data class SodiumSecretStreamKey13(
        val keyBytes: ByteArrayEq13,
    )

    data class CavPointer13(
        val hiddenId: String,
        val symmetricKey: SodiumSecretStreamKey13,
    )

    data class CavDataPointer14(
        val hiddenId: String,
        val symmetricKey: String,
        val objectHash: String?, // added, nullable field
    )

    data class CavContent13(
        val clearFilename: String,
        val disposition: String? = null,
        val allegedSize: Long,
        val allegedMimeType: String?,
        val pointer: CavPointer13,
    )

    data class CavContent14(
        val clearFilename: String,
        val disposition: String? = null,
        val allegedSize: Long,
        val allegedMimeType: String?,
        val pointer: CavDataPointer14,
    )

    data class CavPost13(
        val id: String,
        val type: String,
        val subject: String,
        val body: String,
        val bodyFormat: String,
        val tags: List<String> = emptyList(),
        val publishDate: Long,
        val updatedDate: Long,
        val visibilityType: String,
        val contentFiles: List<CavContent13>
    )

    data class CavPost14(
        val id: String,
        val type: String,
        val subject: String,
        val body: String,
        val bodyFormat: String,
        val tags: List<String> = emptyList(),
        val publishDate: Long,
        val updatedDate: Long,
        val visibilityType: String,
        val contentFiles: List<CavContent14>
    )

    val json: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    private val nullHash = "00".repeat(16)

    private fun convertPointer(pointer: CavPointer13): CavDataPointer14 {
        return CavDataPointer14(
            hiddenId = pointer.hiddenId,
            symmetricKey = Utils.hexEncode(pointer.symmetricKey.keyBytes.bytes),
            objectHash = nullHash,
        )
    }

    private fun convertAttachment(attachment: CavContent13): CavContent14 {
        return CavContent14(
            clearFilename = attachment.clearFilename,
            disposition = attachment.disposition,
            allegedSize = attachment.allegedSize,
            allegedMimeType = attachment.allegedMimeType,
            pointer = convertPointer(attachment.pointer),
        )
    }

    private fun convertPost(post: CavPost13): CavPost14 {
        return CavPost14(
            id = post.id, type = post.type, subject = post.subject,
            body = post.body, bodyFormat = post.bodyFormat, tags = post.tags,
            publishDate = post.publishDate, updatedDate = post.updatedDate,
            visibilityType = post.visibilityType,
            contentFiles = post.contentFiles.map(::convertAttachment),
        )
    }

    internal fun convertPostJson(input: String): String {
        val post = json.adapter(CavPost13::class.java).fromJson(input)!!
        return json.adapter(CavPost14::class.java).indent("  ")
            .toJson(convertPost(post))
    }

    override fun preflightAndPlan(state: State): List<Pair<Path, String>> {
        val repoPath = state.repo.path

        val journalsRoot = repoPath.resolve("cache/downloaded/")
        if (!journalsRoot.isDirectory())
            return emptyList()

        return journalsRoot.listDirectoryEntries("contact_*").flatMap { journal ->
            journal.listDirectoryEntries("post_*.json").map { postPath ->
                postPath to convertPostJson(postPath.readText())
            }
        }.toList()
    }

    override fun upgradeByPlan(state: State, plan: List<Pair<Path, String>>) {
        plan.forEach { (dest, contents) ->
            dest.deleteExisting()
            Utils.writeFileAtomic(dest, contents)
        }
    }
}

/**
 * DodgyFeistel has changed its implementation from SHA-256 to BLAKE2b. New
 * post IDs will need a higher prefix to prevent collisions, so we rotate the
 * key, forcing a new version prefix.
 *
 * This is a major version upgrade because rolling back to v14.0 would allow
 * collisions.
 */
@Suppress("ClassName")
object Upgrade_14_0_to_15_0: UpgraderNoPlan(
    Version(14, 0), Version(15, 0),
) {
    override fun doPreflight(state: State) { }

    override fun doUpgrade(state: State) {
        state.postIdShuffleKeyRotate()
    }
}

/**
 * Split Metadata into Versioning and Status tables in publishing DB and add
 * last-published spec version to status.
 *
 * Just delete the publishing DB; while it could be adjusted, the next updater
 * has a better reason to delete it.
 */
@Suppress("ClassName")
object Upgrade_15_0_to_16_0: UpgraderNoPlan(
    version = State.Companion.Version(15, 0),
    targetVersion = State.Companion.Version(16, 0),
) {
    override fun doPreflight(state: State) { }

    @OptIn(ExperimentalPathApi::class)
    override fun doUpgrade(state: State) {
        state.repo.path.resolve("cache/publishing.sqlite3").deleteIfExists()
        state.repo.path.resolve("cache/out/").deleteRecursively()
    }
}

/**
 * Run some database operations, only committing if [forReal] is true.
 */
fun <R> tryTransaction(db: Database, forReal: Boolean, block: (Transaction) -> R?): R? {
    return transaction(db) {
        val tx = this
        val result = block(tx)
        if (!forReal)
            tx.rollback()
        result
    }
}

/**
 * The Great Renaming (of journal parts).
 *
 * - In own journal's posts, rename "content" dir to "attach".
 * - In publishing journal directory structure, rename "content" to
 *   "attachments" and change files from `c_*.bin` to `a_*.bin`.
 * - In fetched posts, remove `disposition` field and rename "contentFiles" to
 *   "attachments".
 */
@Suppress("ClassName")
object Upgrade_16_0_to_17_0: UpgraderNoPlan(
    Version(16, 0), Version(17, 0),
) {
    data class CavPost16(
        val id: String,
        val type: String,
        val subject: String,
        val body: String,
        val bodyFormat: String,
        val tags: List<String> = emptyList(),
        val publishDate: Long,
        val updatedDate: Long,
        val visibilityType: String,
        val contentFiles: List<CavContent16>, // renamed
    )

    data class CavPost17(
        val id: String,
        val type: String,
        val subject: String,
        val body: String,
        val bodyFormat: String,
        val tags: List<String> = emptyList(),
        val publishDate: Long,
        val updatedDate: Long,
        val visibilityType: String,
        val attachments: List<CavAttachment17>,
    )

    data class CavContent16(
        val clearFilename: String,
        val disposition: String?, // to remove
        val allegedSize: Long,
        val allegedMimeType: String?,
        val pointer: Map<String, String>,
    )

    data class CavAttachment17(
        val clearFilename: String,
        val allegedSize: Long,
        val allegedMimeType: String?,
        val pointer: Map<String, String>,
    )

    private fun convertFetchedAttachment(attachment: CavContent16): CavAttachment17 {
        // drops disposition field
        return CavAttachment17(
            clearFilename = attachment.clearFilename,
            allegedSize = attachment.allegedSize,
            allegedMimeType = attachment.allegedMimeType,
            pointer = attachment.pointer,
        )
    }

    private fun convertFetchedPost(post: CavPost16): CavPost17 {
        return CavPost17(
            id = post.id, type = post.type, subject = post.subject,
            body = post.body, bodyFormat = post.bodyFormat, tags = post.tags,
            publishDate = post.publishDate, updatedDate = post.updatedDate,
            visibilityType = post.visibilityType,
            attachments = post.contentFiles.map(::convertFetchedAttachment), // rename
        )
    }

    val json: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    internal fun convertFetchedPostJson(input: String): String {
        val post = json.adapter(CavPost16::class.java).fromJson(input)!!
        return json.adapter(CavPost17::class.java).indent("  ")
            .toJson(convertFetchedPost(post))
    }

    val numeric = Regex("""^[0-9]+$""")

    @OptIn(ExperimentalPathApi::class)
    fun runUpgrade(state: State, forReal: Boolean) {
        val repoPath = state.repo.path

        // First, update DB columns.

        // State DB:
        //
        // - sig_public_key and sig_private_key_enc become non-nullable (delayed
        //   update to match earlier code change) and move to before box keys
        //   to match sig/box key ordering in code.
        // - self_index_hidden_id and self_index_symmetric_key renamed to use
        //   catalog instead of index.

        val stateDB = SQLite3.database(repoPath.resolve("settings.sqlite3"), true)
        tryTransaction(stateDB, forReal) { tx ->
            tx.exec(
                """
                CREATE TABLE self_keys_17_0 (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    sig_public_key BLOB NOT NULL,
                    sig_private_key_enc BLOB NOT NULL,
                    box_public_key BLOB NOT NULL,
                    box_private_key_enc BLOB NOT NULL,
                    is_active BOOLEAN NOT NULL,
                    created_ms BIGINT NOT NULL,
                    description TEXT NOT NULL,
                    self_catalog_hidden_id TEXT NOT NULL,
                    self_catalog_symmetric_key BLOB NOT NULL
                )
            """
            )
            tx.exec("""
                INSERT INTO self_keys_17_0
                   SELECT id, sig_public_key, sig_private_key_enc,
                     box_public_key, box_private_key_enc,
                     is_active, created_ms, description,
                     self_index_hidden_id, self_index_symmetric_key
                   FROM self_keys
            """)
            tx.exec(
                "DROP TABLE self_keys"
            )
            tx.execInBatch(listOf(
                "ALTER TABLE self_keys_17_0 RENAME TO self_keys"
            ))
        }

        // Fetching DB:
        //
        // - Rename `metadata` to `versioning`
        // - Rename `journal` table columns to use "catalog" instead of "index"

        // If the fetch DB didn't exist until now, it may have been freshly
        // recreated and doesn't need this migration.
        val fetchDB = SQLite3.database(repoPath.resolve("cache/download.sqlite3"), true)
        val versioningTableCount = transaction(fetchDB) {
            this.exec("""
                select count(*) as table_count from sqlite_master where type = "table" and name = "versioning"
            """) { rs ->
                rs.next()
                rs.getInt("table_count")
            }
        }

        if (versioningTableCount != 1) {
            tryTransaction(fetchDB, forReal) { tx ->
                tx.exec("DROP TABLE metadata")
                tx.exec("""
                CREATE TABLE versioning (
                    schema_version INT NOT NULL
                )
            """)
                tx.exec("""
                INSERT INTO versioning (schema_version) VALUES (6)
            """)

                tx.exec(
                    """
                CREATE TABLE journal_17_0 (
                    contact_id BIGINT NOT NULL PRIMARY KEY,
                    catalog_hidden_id TEXT NULL,
                    catalog_symmetric_key VARBINARY(32) NULL
                )
            """
                )
                tx.exec("""
                INSERT INTO journal_17_0
                   SELECT contact_id, index_hidden_id, index_symmetric_key
                   FROM journal
            """)
                tx.exec(
                    "DROP TABLE journal"
                )
                tx.execInBatch(listOf(
                    "ALTER TABLE journal_17_0 RENAME TO journal"
                ))
            }
        }

        // Next, rewrite individual data files

        // Fetched posts
        val fetchedRoot = repoPath.resolve("cache/downloaded/")
        if (fetchedRoot.isDirectory()) {
            fetchedRoot.listDirectoryEntries("contact_*").forEach { fetchedContact ->
                fetchedContact.listDirectoryEntries("post_*.json").map { fetchedPostPath ->
                    val newJson = convertFetchedPostJson(fetchedPostPath.readText())
                    if (forReal) {
                        fetchedPostPath.writeText(newJson)
                    }
                }
            }
        }

        // Finally, move files and directories, starting from leaves and working
        // towards the root.

        // Own post attachment dirs
        repoPath.resolve("journal").listDirectoryEntries()
            .filter { it.name.matches(numeric) }
            .forEach { postDir ->
                val oldAttach = postDir.resolve("content")
                if (oldAttach.exists()) {
                    moveIf(forReal, oldAttach, postDir.resolve("attach"))
                }
            }

        // Publishing cache is deleted; we could rename files, but it would be
        // more of a pain to adjust the msgpack files. Loss of publishing DB is
        // not a big deal.
        state.repo.path.resolve("cache/publishing.sqlite3").deleteIfExists()
        state.repo.path.resolve("cache/out/").deleteRecursively()
    }

    override fun doPreflight(state: State) {
        runUpgrade(state, false)
    }

    override fun doUpgrade(state: State) {
        runUpgrade(state, true)
    }
}

// Adding a new upgrader? Bump the version in State, and add upgrader to the
// list at the top of this file.
