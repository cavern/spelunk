package org.timmc.spelunk.compat

import org.timmc.spelunk.Utils
import java.nio.file.Files
import java.nio.file.Path

/**
 * One planned step in an upgrade.
 *
 * TODO: Replace with something that can actually do rollbacks of any modified
 *  files, and that keeps a machine-readable log of all actions so that a
 *  rescue can be performed if even the rollback fails.
 */
interface Step {
    /**
     * Check if this step should work, and throw if not. Can be called multiple
     * times, as it should have no side effects.
     */
    fun preflight() // FIXME This isn't actually called!

    /**
     * Actually run the step. Must only be called once.
     */
    fun execute()
}

class PreflightFailedE(message: String): RuntimeException(message)

/**
 * A plan to create a directory that does *not* already exist, in a parent dir
 * that *does* already exist.
 */
class MakeDir(
    val newDir: Path
): Step {
    override fun preflight() {
        if (Files.exists(newDir)) {
            throw PreflightFailedE("Planned to create directory '$newDir', but there's already a file or directory there.")
        }

        if (!Files.exists(newDir.parent)) {
            throw PreflightFailedE("Planned to create a directory under ${newDir.parent} but that parent directory does not exist.")
        }

        if (!Files.isWritable(newDir.parent)) {
            throw PreflightFailedE("Planned to create a directory under ${newDir.parent} but that parent directory is not writeable.")
        }
    }

    override fun execute() {
        Files.createDirectory(newDir)
    }
}

/**
 * A plan to move a file/directory from one path to another, creating any
 * necessary parent dirs for the destination if needed. Asserts that destination
 * path is not occupied.
 */
class MoveFile(
    val oldPath: Path,
    val newPath: Path,
): Step {
    override fun preflight() {
        if (!Files.isReadable(oldPath)) {
            throw PreflightFailedE("Planned to move '$oldPath' to '$newPath', but source file is not readable.")
        }
        if (!Files.isWritable(oldPath)) {
            throw PreflightFailedE("Planned to move '$oldPath' to '$newPath', but would not be able to delete/move source file .")
        }

        if (Files.exists(newPath)) {
            throw PreflightFailedE("Planned to move '$oldPath' to '$newPath', but destination file already exists.")
        }

        val newPathAncestor = Utils.findExistingAncestor(newPath)
        if (!Files.isWritable(newPathAncestor)) {
            throw PreflightFailedE("Planned to move '$oldPath' to '$newPath', but destination ancestor dir '$newPathAncestor' is not writeable.")
        }
    }

    override fun execute() {
        Files.createDirectories(newPath.parent)
        Files.move(oldPath, newPath)
    }
}
