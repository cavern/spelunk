package org.timmc.spelunk

import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.exists
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.update
import org.jetbrains.exposed.sql.transactions.transaction
import org.timmc.spelunk.model.AwsS3Config
import org.timmc.spelunk.model.Config
import org.timmc.spelunk.model.Contact
import org.timmc.spelunk.model.CatalogFinder
import org.timmc.spelunk.model.JournalPost
import org.timmc.spelunk.model.Keypair
import org.timmc.spelunk.model.LocalFilesConfig
import org.timmc.spelunk.model.OnePost
import org.timmc.spelunk.model.RsyncConfig
import org.timmc.spelunk.model.WebDavConfig
import org.timmc.spelunk.model.cavern.CavAttachment
import org.timmc.spelunk.model.cavern.CavDataPointer
import org.timmc.spelunk.model.cavern.CavIdCurrent
import org.timmc.spelunk.model.cavern.CavIdentity
import org.timmc.spelunk.model.cavern.CavCatalog
import org.timmc.spelunk.model.cavern.CavCatalogPointer
import org.timmc.spelunk.model.cavern.CavCatalogWrapper
import org.timmc.spelunk.model.cavern.CavPost
import org.timmc.spelunk.model.cavern.CiphertextDigest
import org.timmc.spelunk.model.cavern.signData
import org.timmc.spelunk.publish.APusher
import org.timmc.spelunk.publish.PushAwsS3
import org.timmc.spelunk.publish.PushLocalFiles
import org.timmc.spelunk.publish.PushRsync
import org.timmc.spelunk.publish.PushWebdav
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.time.Instant
import java.util.Base64
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import kotlin.concurrent.read

private val logger = KotlinLogging.logger {}

/**
 * Process and coordination for outbound sync of journal, aka publishing.
 *
 * May break in read-only mode, but should be safe otherwise (will attempt not
 * to write.)
 */
class Publishing(val state: State) {
    companion object {
        /**
         * Latest schema version understood for publishing DB.
         *
         * Changes to this without a corresponding Upgrader will cause this DB
         * to be wiped and reinitialized on the next run.
         */
        const val DB_SCHEMA_VERSION = 7
    }

    /** Versioning information, used for upgrade/recreate check. Single row. */
    object Versioning : Table(name = "versioning") {
        /**
         * When compared to [Companion.DB_SCHEMA_VERSION], tells whether an
         * upgrade/recreate is required.
         */
        val schemaVersion = integer("schema_version")
        // NOTE: Cannot add/remove/change fields without messing up the DB
        //  validity check, which accesses this table via Exposed API.
    }

    /**
     * Single-row table with status information.
     *
     * Any actual settings should be stored in the State DB.
     */
    object Status : Table(name = "status") {
        /**
         * Last timestamp of a fully successful publish (millis since epoch).
         * NULL means have never published (since last DB recreate, at least).
         */
        val lastSuccessfulPublish = long("last_success_ts_ms").nullable()

        /**
         * The Cavern spec version of the last successful publish. If this
         * doesn't match the app's current spec version, a republish is needed.
         */
        val lastSpecVersionPublished = text("last_spec_publish").nullable()
    }

    /**
     * Information about contacts.
     */
    object Contacts : Table(name = "contact") {
        /** Foreign key reference to Contact.id */
        val contactId = long("contact_id")
        /**
         * The hiddenId to use when publishing a catalog for this contact.
         *
         * Making it unguessable and not related to the key material means that
         * an attacker with access to all published files will have a hard time
         * determining who I'm talking to.
         *
         * Currently, each contact has their own catalog file and shared secret;
         * in order to fully implement Cavern ADR 3, we'll need to start
         * grouping catalogs (and secrets) by what recipients have access to.
         */
        val catalogHiddenId = text("contact_hidden_id")
        /**
         * The symmetric key to use when publishing a catalog for this contact.
         */
        val catalogSymmetricKey = binary("contact_symmetric_key", 32)

        override val primaryKey = PrimaryKey(contactId)
    }

    /** Represents a row in the Contacts table. */
    data class ContactPublishing(
        val contactId: Long,
        val catalogPointer: CavCatalogPointer
    )

    // These tables each represent both the local and remote copies of data,
    // internally segregated by the isLocalCopy field.

    /** Journal post hiddenContentIds, local and remote */
    object JournalPosts : Table("journal_post") {
        /** Internal ID, [OnePost.id] */
        val postId = text("post_id")
        val isLocalCopy = bool("is_local_copy")

        /**
         * Hash of the contents of the post and its attachments, including all
         * metadata. When this changes, [hiddenId] and [symmetricKey]
         * *must* also change (and those should not change if content hash does
         * not, since that would cause wasted bandwidth for sync.)
         */
        val contentHash = text("content_hash")
        /**
         * The hiddenID to use for the post. This must be changed only and
         * always at the same time as [symmetricKey].
         */
        val hiddenId = text("hidden_id")
        /**
         * Symmetric key for encrypting the post. Must be changed only and
         * always at the same time as [hiddenId].
         */
        val symmetricKey = binary("symmetric_key", 32)

        init {
            index(true, postId, isLocalCopy)
        }
    }

    /**
     * Journal attachment hiddenContentIds, local and remote.
     *
     * See [JournalPosts] for descriptions of fields where not provided.
     */
    object JournalAttachments : Table("journal_attachment") {
        val postId = text("post_id")
        /** Filename of attachment. */
        val filename = text("filename")
        val isLocalCopy = bool("is_local_copy")

        val contentHash = text("content_hash")
        val hiddenId = text("hidden_id")
        val symmetricKey = binary("symmetric_key", 32)

        init {
            index(true, postId, filename, isLocalCopy)
        }
    }

    private val repo = state.repo

    private val executor: Executor = Executors.newSingleThreadExecutor { runnable ->
        Thread(runnable).apply {
            name = "Publish worker"
            isDaemon = true
        }
    }

    /**
     * Has the initial scan started yet? This is used to prevent anything else
     * from happening before the initial scan.
     *
     * Should only be accessed from executor thread.
     */
    private var hasInitialScanKickedOff = false

    /**
     * What is the state of publishing?
     *
     * Should only be accessed from executor thread.
     */
    private var publishStatus = PublishStatus.STARTUP

    // Change to some kind of observable/property thing?
    private fun changePublishStatus(status: PublishStatus) {
        publishStatus = status
        state.bus.publishStatusChange.broadcast(status)
    }

    enum class PublishStatus {
        /** Program is still in startup and has not yet established local cache. */
        STARTUP,
        /** A publish sync is running. */
        RUNNING,
        /** Sync is not running, and the most recent one failed. */
        FINISHED_ERROR,
        /** Sync is not running, and the most recent one succeeded. */
        FINISHED_SUCCESS,
    }

    // ===================== //
    // Connection management //
    // ===================== //

    private var db = database()

    init {
        // If there's no valid DB, try destroying the old one and recreating
        // it, since it's a locally regeneratable cache.
        try {
            checkDBValid()
        } catch (t: Throwable) {
            recreateDB()
        }
    }

    /**
     * Get a connection to the publishDB in the repo. Opens read-only if
     * repo instance does not allow writes.
     */
    private fun database(): Database {
        repo.cache.ensureDir()
        return SQLite3.database(repo.cache.publishDB.path, repo.allowWrite)
    }

    /**
     * Run a transaction on the publishing database, returning the results.
     */
    private fun <R> tx(funk: Transaction.() -> R): R {
        return transaction(db, funk)
    }

    /**
     * Throw if DB is unusable, including not having current schema version.
     */
    private fun checkDBValid() {
        val version = tx {
            if (!Versioning.exists())
                throw RuntimeException("Publish DB not yet initialized.")
            // We assume that Versioning always has the same shape
            Versioning.selectAll().map { it[Versioning.schemaVersion] }.firstOrNull()
        }
        if (version != DB_SCHEMA_VERSION)
            throw RuntimeException("""
                Publish DB has wrong schema version;
                expected $DB_SCHEMA_VERSION but found $version
            """.trimIndent().wrap())
    }

    /**
     * Drop the DB if it exists and create a fresh one.
     */
    private fun recreateDB() {
        repo.cache.publishDB.requireWriteable("delete publish state cache")
        Files.deleteIfExists(repo.cache.publishDB.path)
        db = database()

        tx {
            SchemaUtils.create(
                Versioning, Status, Contacts, JournalPosts, JournalAttachments
            )

            // Make the single row for Status table
            Status.insert {
                it[lastSuccessfulPublish] = null
                it[lastSpecVersionPublished] = null
            }

            // Finally, fill out schema version. This must come last, since its
            // presence signals that init is complete.
            Versioning.insert { it[schemaVersion] = DB_SCHEMA_VERSION }
        }

        checkDBValid()
    }

    // ================== //
    // External interface //
    // ================== //

    /**
     * Read information about most recent publish.
     */
    fun lastPublish(): LastPublish {
        return tx {
            Status.selectOnly {
                LastPublish(
                    timestamp = it[Status.lastSuccessfulPublish]?.let { ms ->
                        Instant.ofEpochMilli(ms)
                    },
                    spec = it[Status.lastSpecVersionPublished],
                )
            }
        }
    }

    /**
     * Scan all posts for changes and try to publish any changes.
     */
    fun queueInitialSync() {
        executor.execute { task_initialScan() }
        executor.execute { task_pushChanges() }
    }

    /**
     * Same as [queueInitialSync] but run synchronously.
     */
    fun runInitialSync() {
        task_initialScan()
        task_pushChanges()
    }

    /**
     * Update the DB for one post and try to publish any changes.
     */
    fun queueScanOne(postId: String) {
        executor.execute { task_scanOne(postId) }
        executor.execute { task_pushChanges() }
    }

    // ================================ //
    // Local data state: Executor tasks //
    // ================================ //

    /** In executor: Scan all posts for changes and update the DB. */
    @Suppress("FunctionName")
    private fun task_initialScan() {
        hasInitialScanKickedOff = true

        val allPostDirs = repo.journal.posts.list()

        val posts = mutableListOf<PostCacheEntry>()
        val attachments = mutableListOf<AttachmentCacheEntry>()
        allPostDirs.map { post ->
            state.postLocker(post.id).read {
                val entries = try {
                    cacheEntriesForPost(post)
                } catch (t: Throwable) {
                    logger.warn("Failed to build publishing cache entries for post ${post.id}")
                    t.printStackTrace()
                    null
                }
                entries?.let { (postCache, attachCache) ->
                    posts.add(postCache)
                    attachments.addAll(attachCache)
                }
            }
        }

        tx {
            JournalPosts.deleteWhere { isLocalCopy eq true }
            JournalAttachments.deleteWhere { isLocalCopy eq true }
            posts.map { insertLocalPostEntry(it) }
            attachments.map { insertLocalAttachmentEntry(it) }
        }
    }

    /**
     * In executor: Update the DB with any changes to one post.
     *
     * Throws if initial scan hasn't run (or at least started).
     */
    @Suppress("FunctionName")
    private fun task_scanOne(postId: String) {
        check(hasInitialScanKickedOff) {
            "Refusing to update a single post's publish status before initial scan"
        }

        val postDir = repo.journal.posts.lookup(postId)

        state.postLocker(postId).read {
            // null pair if post is missing (e.g. deleted) or unpublishable
            val (postCache, attachCache) = cacheEntriesForPost(postDir)
                ?: (null to null)

            tx {
                // Delete old hiddenContentId either way (handles deleted case)
                JournalPosts.deleteWhere {
                    (JournalPosts.postId eq postDir.id)
                        .and(isLocalCopy eq true)
                }
                JournalAttachments.deleteWhere {
                    (JournalAttachments.postId eq postDir.id)
                        .and(isLocalCopy eq true)
                }
                // This, then, functionally completes either an update or an insert
                if (postCache != null) {
                    insertLocalPostEntry(postCache)
                }
                attachCache?.map { insertLocalAttachmentEntry(it) }
            }
        }
    }

    /** Check DB for existing local post cache entry, by post ID. */
    internal fun lookupPostCacheEntry(id: String): PostCacheEntry? {
        return tx { JournalPosts.select {
            (JournalPosts.postId eq id).and(JournalPosts.isLocalCopy eq true)
        }.map(PostCacheEntry.Companion::fromDB).firstOrNull() }
    }

    /** Check DB for existing local attachment cache entry, by post ID and filename. */
    private fun lookupAttachmentCacheEntry(postId: String, filename: String): AttachmentCacheEntry? {
        return tx { JournalAttachments.select {
            (JournalAttachments.postId eq postId)
                .and(JournalAttachments.filename eq filename)
                .and(JournalAttachments.isLocalCopy eq true)
        }.map(AttachmentCacheEntry.Companion::fromDB).firstOrNull() }
    }

    /**
     * Build cache entries for a post and its attachments, or null if
     * cannot publish this post. May throw if post is unreadable.
     */
    private fun cacheEntriesForPost(post: OnePost): Pair<PostCacheEntry, List<AttachmentCacheEntry>>? {
        val existing = lookupPostCacheEntry(post.id)
        val hash = contentHashForPost(post)
            ?: return null

        val postCache = if (existing != null && existing.contentHash == hash) {
            existing
        } else {
            PostCacheEntry(
                postId = post.id,
                contentHash = hash,
                hiddenId = Crypto.hiddenId(),
                symmetricKey = Crypto.symmetricStreamKey()
            )
        }

        val attachCaches = post.post.readJson().attachments
            .distinctBy { it.filename }
            // FIXME This currently hashes the attachments twice (once for the
            // post and once for each attachment)
            .mapNotNull { cacheEntryForAttachment(post, it)}

        return postCache to attachCaches
    }

    /**
     * Build cache entry for an attachment, or null if attachment is missing or
     * unreadable.
     */
    private fun cacheEntryForAttachment(post: OnePost, attachment: JournalPost.JournalPostAttachment): AttachmentCacheEntry? {
        val existing = lookupAttachmentCacheEntry(post.id, attachment.filename)
        val hash = contentHashForAttachment(post, attachment)
            ?: return null

        return if (existing != null && existing.contentHash == hash) {
            existing
        } else {
            AttachmentCacheEntry(
                postId = post.id,
                filename = attachment.filename,
                contentHash = hash,
                hiddenId = Crypto.hiddenId(),
                symmetricKey = Crypto.symmetricStreamKey()
            )
        }
    }

    private fun insertLocalPostEntry(post: PostCacheEntry) {
        tx {
            JournalPosts.insert {
                it[postId] = post.postId
                it[isLocalCopy] = true
                it[contentHash] = post.contentHash
                it[hiddenId] = post.hiddenId
                it[symmetricKey] = post.symmetricKey.sharedBytes
            }
        }
    }

    private fun insertLocalAttachmentEntry(attachment: AttachmentCacheEntry) {
        tx {
            JournalAttachments.insert {
                it[postId] = attachment.postId
                it[filename] = attachment.filename
                it[isLocalCopy] = true
                it[contentHash] = attachment.contentHash
                it[hiddenId] = attachment.hiddenId
                it[symmetricKey] = attachment.symmetricKey.sharedBytes
            }
        }
    }

    // ================================= //
    // Remote data state: Executor tasks //
    // ================================= //

    /**
     * In executor: Push out all changes
     */
    @Suppress("FunctionName")
    private fun task_pushChanges() {
        val publishConfig = state.configRead().publishing
        if (publishConfig == null) {
            changePublishStatus(PublishStatus.STARTUP) // FIXME why not FINISHED_ERROR?
            return
        } else {
            changePublishStatus(PublishStatus.RUNNING)
        }

        try {
            // We need to make sure we start publishing even before there are
            // posts; if we've never published before, it's at least important
            // to get our keys published, etc.
            val lastPublish = lastPublish()

            // TODO: Also sync if address book has changed (but just catalogs)
            // TODO: Also sync if own keyring has changed
            val whyNeedSync = Utils.explainAction(listOf(
                "No record of last publish" to {
                    lastPublish.timestamp == null
                },
                "No record of Cavern format used for previous publish" to {
                    lastPublish.spec == null
                },
                "Last Cavern format used for publishing is now outdated" to {
                    lastPublish.spec != Constants.CAVERN_SPEC
                },
                "Attachments have changed since last publish" to {
                    !areAttachmentsSynced()
                },
                "Posts have changed since last publish" to {
                    !arePostsSynced()
                },
            ))
            // For now, use old sync any time anything changes
            if(whyNeedSync != null) {
                logger.info("Publishing because: $whyNeedSync")
                cleanFullSync()
                markSyncClean()
            } else {
                logger.info("Skipping publish")
            }
            changePublishStatus(PublishStatus.FINISHED_SUCCESS)
        } catch (t: Throwable) {
            changePublishStatus(PublishStatus.FINISHED_ERROR)
            throw t
        }
        // TODO: Actually implement incremental sync. This will require pushers
        // to be able to push collections of files, not entire directories at
        // once. May also require ability to push a stream, not a file. (New
        // interface to allow overlap with old method?)
        //
        // Approximately:
        // - Check DB for local/remote hiddenContentId mismatches
        // - If no mismatch, set state to clean and return
        // - Upload new/changed things, incrementally updating DB as they're
        //   pushed, and failing entire task if there are any failures:
        //     - Upload attachments missing on remote
        //     - Upload posts missing on remote
        //     - Given list of posts either needing upload or delete, recreate
        //       catalogs and upload them. (How do we know which catalogs
        //       are affected? May need to store an audience list for each post
        //       in the local and remote caches. Note that address book and
        //       filter lists are an implicit dependency here.)
        // - Delete things missing on local side, first posts and then
        //   attachments
        // - Mark clean
    }

    // ================= //
    // Utility functions //
    // ================= //

    /**
     * Produce a deterministic content hash for the post, which will change if
     * the body, metadata, or attached files change.
     *
     * Returns null if not publishable for any reason.
     */
    private fun contentHashForPost(postDir: OnePost): String? {
        if (postDir.isDeleted()) return null
        // Not using readJson because we want the hash and the raw JSON to be
        // derived from the same snapshot of the raw bytes.
        val postJSON = postDir.post.read()
        val post = postDir.post.fromJson(postJSON)
        if (!post.isPublishable()) return null

        val attachHashes = post.attachments.distinctBy { it.filename }
            .mapNotNull { rawHashForAttachment(postDir, it) }
        return Utils.hexEncode(hashData(listOf(postJSON.toByteArray()) + attachHashes))
    }

    /**
     * Similar to [contentHashForPost] but for just one attachment; null if
     * file does not exist or is otherwise not readable.
     */
    private fun contentHashForAttachment(postDir: OnePost, attachment: JournalPost.JournalPostAttachment): String? {
        // FIXME do something with this error rather than simply suppressing it
        // Maybe a Bus method for errors?
        return suppressThrowAsNull { rawHashForAttachment(postDir, attachment) }?.let {
            Utils.hexEncode(it)
        }
    }

    /** Core of [contentHashForAttachment] -- everything but the hex encoding. */
    private fun rawHashForAttachment(postDir: OnePost, attachment: JournalPost.JournalPostAttachment): ByteArray? {
        val attachPath = postDir.attachments.path.resolve(attachment.filename)
        val contentsHash = if (Files.exists(attachPath)) {
            hashFileContents(attachPath)
        } else {
            return null
        }
        return hashData(listOf(
            attachment.filename.toByteArray(),
            contentsHash))
    }

    /**
     * Produce some kind of reasonably strong hash from a file's bytes.
     */
    private fun hashFileContents(path: Path): ByteArray {
        return hashData(Files.newInputStream(path).chunkIterator())
    }

    /**
     * Produce some kind of reasonably strong hash from bytes.
     */
    private fun hashData(data: Iterable<ByteArray>): ByteArray {
        val hasher = Blake2bHasher.unkeyed(32)
        data.map { hasher.update(it) }
        return Base64.getEncoder().encode(hasher.digest())
    }

    /**
     * Look up publishing details for a contact. (Intended for unit tests.)
     */
    internal fun lookupContactPublishing(contactId: Long): ContactPublishing? {
        return tx {
            Contacts.select { Contacts.contactId eq contactId }.map {
                ContactPublishing(
                    contactId = it[Contacts.contactId],
                    catalogPointer = CavCatalogPointer(
                        hiddenId = it[Contacts.catalogHiddenId],
                        symmetricKey = SodiumSecretStreamKey(it[Contacts.catalogSymmetricKey])
                    )
                )
            }.firstOrNull()
        }
    }

    /**
     * Get a contact-for-publishing object from an address book Contact,
     * creating it in the DB first if it doesn't already exist.
     */
    private fun getOrCreateContactPublishing(contact: Contact): ContactPublishing {
        return tx {
            val found = lookupContactPublishing(contact.id)
            if (found != null) {
                found
            } else {
                val ret = ContactPublishing(
                    contactId = contact.id,
                    catalogPointer = CavCatalogPointer.new()
                )
                Contacts.insert {
                    it[contactId] = ret.contactId
                    it[catalogHiddenId] = ret.catalogPointer.hiddenId
                    it[catalogSymmetricKey] = ret.catalogPointer.symmetricKey.sharedBytes
                }
                ret
            }
        }
    }

    /** One row in the [JournalAttachments] table, either local or remote. */
    private data class AttachmentCacheEntry(
        val postId: String,
        val filename: String,
        val contentHash: String,
        // TODO: Replace with pointer
        val hiddenId: String,
        val symmetricKey: SodiumSecretStreamKey,
    ) {
        companion object {
            fun fromDB(row: ResultRow): AttachmentCacheEntry {
                return AttachmentCacheEntry(
                    postId = row[JournalAttachments.postId],
                    filename = row[JournalAttachments.filename],
                    contentHash = row[JournalAttachments.contentHash],
                    hiddenId = row[JournalAttachments.hiddenId],
                    symmetricKey = SodiumSecretStreamKey(row[JournalAttachments.symmetricKey])
                )
            }
        }
    }

    /** One row in the [JournalPosts] table, either local or remote. */
    internal data class PostCacheEntry(
        val postId: String,
        val contentHash: String,
        // TODO: Replace with pointer
        val hiddenId: String,
        val symmetricKey: SodiumSecretStreamKey
    ) {
        companion object {
            fun fromDB(row: ResultRow): PostCacheEntry {
                return PostCacheEntry(
                    postId = row[JournalPosts.postId],
                    contentHash = row[JournalPosts.contentHash],
                    hiddenId = row[JournalPosts.hiddenId],
                    symmetricKey = SodiumSecretStreamKey(row[JournalPosts.symmetricKey])
                )
            }
        }
    }

    /** Discover whether the local and remote attachments are synced. */
    private fun areAttachmentsSynced(): Boolean {
        return tx {
            val local = JournalAttachments.select { JournalAttachments.isLocalCopy eq true }
                .map(AttachmentCacheEntry.Companion::fromDB)
            val remote = JournalAttachments.select { JournalAttachments.isLocalCopy eq false }
                .map(AttachmentCacheEntry.Companion::fromDB)
            local.toSet() == remote.toSet()
        }
    }

    /** Discover whether the local and remote posts are synced. */
    private fun arePostsSynced(): Boolean {
        return tx {
            val local = JournalPosts.select { JournalPosts.isLocalCopy eq true }
                .map(PostCacheEntry.Companion::fromDB)
            val remote = JournalPosts.select { JournalPosts.isLocalCopy eq false }
                .map(PostCacheEntry.Companion::fromDB)
            local.toSet() == remote.toSet()
        }
    }

    // XXX This copies the local cache state over the remote cache state to
    // indicate that everything is clean. This is only needed until incremental
    // sync is in place.
    private fun markSyncClean() {
        tx {
            JournalAttachments.deleteWhere { isLocalCopy eq false }
            JournalAttachments.batchInsert(JournalAttachments.selectAll()) { local ->
                this[JournalAttachments.postId] = local[JournalAttachments.postId]
                this[JournalAttachments.filename] = local[JournalAttachments.filename]
                this[JournalAttachments.isLocalCopy] = false
                this[JournalAttachments.contentHash] = local[JournalAttachments.contentHash]
                this[JournalAttachments.hiddenId] = local[JournalAttachments.hiddenId]
                this[JournalAttachments.symmetricKey] = local[JournalAttachments.symmetricKey]
            }

            JournalPosts.deleteWhere { isLocalCopy eq false }
            JournalPosts.batchInsert(JournalPosts.selectAll()) { local ->
                this[JournalPosts.postId] = local[JournalPosts.postId]
                this[JournalPosts.isLocalCopy] = false
                this[JournalPosts.contentHash] = local[JournalPosts.contentHash]
                this[JournalPosts.hiddenId] = local[JournalPosts.hiddenId]
                this[JournalPosts.symmetricKey] = local[JournalPosts.symmetricKey]
            }

            Status.update {
                it[lastSuccessfulPublish] = Instant.now().toEpochMilli()
                it[lastSpecVersionPublished] = Constants.CAVERN_SPEC
            }
        }
    }

    // ================================ //
    // Publishing & pre-cache full-sync //
    // ================================ //

    /**
     * Publish journal, assuming all files may need publishing.
     *
     * In the general case this is not an incremental publish, but some
     * publishing methods may be able to perform an incremental sync anyway.
     */
    private fun cleanFullSync() {
        val cfg = state.configRead()
        encryptToOutbox(cfg)
        val pusher = getConfiguredPusher()
        if (pusher == null) {
            logger.debug("No publishing mechanism configured.")
            return
        }

        try {
            logger.debug("Running: ${pusher.description()}")
            val secondsToPublish = timeSeconds { pusher.push() }
            logger.info("Published via ${pusher.description()} in ${String.format("%.3f", secondsToPublish)} seconds")
        } catch (e: Exception) {
            throw RuntimeException("Failed to completely publish: $e", e)
        }
    }

    fun getConfiguredPusher(): APusher<*>? {
        return when (val pubConfig = state.configRead().publishing) {
            null -> null
            is WebDavConfig -> PushWebdav(repo, pubConfig)
            is AwsS3Config -> PushAwsS3(repo, pubConfig)
            is RsyncConfig -> PushRsync(repo, pubConfig)
            is LocalFilesConfig -> PushLocalFiles(repo, pubConfig)
        }
    }

    /**
     * Completely update the outbox with an encrypted journal graph.
     *
     * Clears contents first, then recreates everything with new symmetric
     * keys and filenames.
     */
    private fun encryptToOutbox(cfg: Config) {
        repo.requireWriteable("write encrypted journal for publishing")
        val addressBook = state.addressBookRead()
        val keyring = state.keyringRead()

        val outbox = repo.cache.outbox
        try {
            outbox.deleteRecursively()
        } catch (e: Exception) {
            throw SpelunkExit("Could not clear outgoing encrypted files working directory")
        }
        outbox.ensureDir()
        outbox.cavernRoot.ensureDir()
        outbox.cavernRoot.catalogs.ensureDir()
        outbox.cavernRoot.posts.ensureDir()
        outbox.cavernRoot.attachments.ensureDir()

        val publishTime = Instant.now().epochSecond
        val defaultCatalog = CavCatalog(
            publishTimestamp = publishTime,
            posts = emptyList(),
        )

        // Catalogs for contacts, will build incrementally
        val catalogsByContact: MutableMap<Contact, CavCatalog> = mutableMapOf()
        // Catalog encrypted for myself
        var selfCatalog = defaultCatalog

        // Encrypt all posts that are publishable
        // FIXME Just use the DB, rather than recalculating hashes for all posts
        for ((post, postOffer) in repo.journal.posts.list().mapNotNull(::encryptPost)) {
            // Place offers into relevant catalogs
            // TODO: Short-circuit visibility expansion when PUBLIC
            val recipients = addressBook.visibilityToContacts(post.visibility)
            for (contact in recipients) {
                val catalog = catalogsByContact.getOrDefault(contact, defaultCatalog)
                catalogsByContact[contact] = catalog.copy(posts = catalog.posts.plusElement(postOffer))
            }
            // And a copy for myself
            selfCatalog = selfCatalog.copy(posts = selfCatalog.posts.plusElement(postOffer))
        }

        // Set up pairs of catalogs and the public keys to encrypt them to,
        // including an extra one encrypted to myself.
        //
        // Filter out contacts without a public key.
        val catalogsToEncrypt: List<Pair<Recipient, CavCatalog>> =
            catalogsByContact.mapNotNull { (contact, catalog) ->
                recipientFromContact(contact)?.let { it to catalog }
            } + listOf(recipientFromMe(keyring.current) to selfCatalog)

        // Encrypt each catalog for a recipient
        //
        // TODO: Group recipients by identical CavCatalog and give all recipients
        //  in a group the same pointer. (ADR 3)
        for ((recipient, catalog) in catalogsToEncrypt) {
            encryptCatalog(
                catalog = catalog, senderKeypair = keyring.current.boxKeys,
                recip = recipient,
            )
        }

        // Finally, build a catalog-finder directory based on contact hiddenIds
        val finderData = CatalogFinder.encrypt(
            recipients = catalogsToEncrypt.map(Pair<Recipient, *>::first),
            myKeys = keyring
        )

        outbox.cavernRoot.identity.writeBytes(
            CavIdentity(
                spec = Constants.CAVERN_SPEC,
                sigKey = keyring.current.sigKeys.publicKey,
                idSigned = signData(
                    CavIdCurrent(
                        boxKey = keyring.current.boxKeys.publicKey,
                        mainUrl = cfg.publishing?.journalUrl,
                    ),
                    CavIdCurrent.TOPIC_OWN_CURRENT_IDENTITY,
                    keyring.current.sigKeys.privateKey
                ),
                name = cfg.ownName,
                catalogFinder = finderData,
            ).toMsgPack()
        )

        outbox.indexHtml.write("""
            <!doctype html>
            <html>
              <head>
                <meta charset="utf-8">
                <title>This is a Cavern journal</title>
                ${Constants.INDEX_FILE_MARKER}
              </head>
              <body>
                Hi! You've reached my Cavern journal—but it can't be accessed via a web browser.
                You'll need to run a
                <a href="https://www.brainonfire.net/cavern/">Cavern</a>
                app to see my posts.
              </body>
            </html>
        """.trimIndent())
    }

    /**
     * Model of a publishing recipient. This is currently a single individual.
     */
    data class Recipient(
        val catalogPointer: CavCatalogPointer,
        val boxPublicKey: SodiumBoxPublicKey,
    )

    private fun recipientFromMe(myKeypair: Keypair): Recipient {
        return Recipient(
            catalogPointer = myKeypair.selfCatalogPointer,
            boxPublicKey = myKeypair.boxKeys.publicKey
        )
    }

    private fun recipientFromContact(contact: Contact): Recipient? {
        return contact.boxKey?.let { boxKey ->
            val forPub = getOrCreateContactPublishing(contact)
            Recipient(
                catalogPointer = forPub.catalogPointer,
                boxPublicKey = boxKey
            )
        }
    }

    /**
     * Encrypt a catalog to disk, for one recipient.
     */
    private fun encryptCatalog(
        catalog: CavCatalog, senderKeypair: SodiumBoxKeyPair, recip: Recipient,
    ) {
        val recipients = listOf(recip.boxPublicKey)
        val catalogWrapper = CavCatalogWrapper.wrapCatalog(catalog, senderKeypair, recipients)

        val catalogsDir = repo.cache.outbox.cavernRoot.catalogs
        val dataIn = catalogWrapper.toMsgPack().inputStream()
        Utils.withTempFile(catalogsDir.byHiddenId(recip.catalogPointer.hiddenId).path) {
            Crypto.symmetricEncryptStream(input = dataIn, output = it,
                key = recip.catalogPointer.symmetricKey
            )
        }
    }

    /**
     * Encrypt a post to disk, or null if not publishable.
     *
     * Return a pointer to the file.
     */
    private fun encryptPost(postDir: OnePost): Pair<JournalPost, CavCatalog.PostOffer>? {
        val id = postDir.name

        val (post, pEntry, attachments) = state.postLocker(id).read {
            if (postDir.isDeleted()) return null
            val post = postDir.post.readJson()
            if (!post.isPublishable())
                return null

            val (pEntry, aEntries) = cacheEntriesForPost(postDir)
                ?: return null

            val attachments = aEntries.distinctBy { it.filename }
                .map { encryptAttachment(postDir, it) }

            PostEncryptResults(post, pEntry, attachments)
        }

        val publishable = CavPost(
            id = post.persistentId,
            type = Constants.POST_TYPE_JOURNAL,
            subject = post.subject,
            body = post.body,
            bodyFormat = Constants.MIME_PLAINTEXT,
            tags = post.tags,
            publishDate = post.publishDate,
            updatedDate = post.updatedDate,
            visibilityType = post.visibility.type,
            attachments = attachments,
        )

        val hash = encryptData(publishable.toMsgPack(),
            repo.cache.outbox.cavernRoot.posts.byHiddenId(pEntry.hiddenId).path,
            pEntry.symmetricKey
        )

        return post to CavCatalog.PostOffer(
            post.persistentId,
            CavDataPointer(
                hiddenId = pEntry.hiddenId,
                symmetricKey = pEntry.symmetricKey,
                cipherHash = hash,
            )
        )
    }

    private data class PostEncryptResults(
        val post: JournalPost,
        val cacheEntry: PostCacheEntry,
        val attachments: List<CavAttachment>
    )

    /**
     * Helper for encrypting data to disk.
     *
     * Returns hash of encrypted file.
     */
    private fun encryptData(data: ByteArray, dest: Path, key: SodiumSecretStreamKey): CiphertextDigest {
        val dataIn = data.inputStream()
        return Utils.withTempFile(dest) { Crypto.symmetricEncryptStream(dataIn, it, key) }
    }

    /**
     * Encrypt an attachment, and return the metadata, which includes
     * a pointer to the file; TODO: null if cannot read file.
     */
    private fun encryptAttachment(postDir: OnePost, cacheEntry: AttachmentCacheEntry): CavAttachment {
        val attachFile = postDir.attachments.path.resolve(cacheEntry.filename)

        // TODO Check if there's a race condition we care about between
        // creating the file's content hash and actually uploading the file.
        val dataIn = Files.newInputStream(attachFile)

        val writingTo = repo.cache.outbox.cavernRoot.attachments.byHiddenId(cacheEntry.hiddenId).path
        val hash = Files.newOutputStream(writingTo).use {
            Crypto.symmetricEncryptStream(dataIn, it, cacheEntry.symmetricKey)
        }

        return CavAttachment(
            clearFilename = cacheEntry.filename,
            allegedSize = Files.readAttributes(attachFile, BasicFileAttributes::class.java).size(),
            allegedMimeType = Files.probeContentType(attachFile),
            pointer = CavDataPointer(
                hiddenId = cacheEntry.hiddenId,
                symmetricKey = cacheEntry.symmetricKey,
                cipherHash = hash,
            )
        )
    }
}

/**
 * Status of the most recent successful publish, if any.
 */
data class LastPublish(
    /** Timestamp of last publish, if any. */
    val timestamp: Instant?,
    /** The Cavern spec version used in the last publish, if any. */
    val spec: String?,
)

/**
 * Add slash to end of string if there isn't one already.
 */
fun String.slash() = this.ensureLastChar('/')
