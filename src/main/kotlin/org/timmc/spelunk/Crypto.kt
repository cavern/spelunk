package org.timmc.spelunk

import com.goterl.lazysodium.LazySodiumJava
import com.goterl.lazysodium.SodiumJava
import com.goterl.lazysodium.interfaces.Box
import com.goterl.lazysodium.interfaces.DiffieHellman
import com.goterl.lazysodium.interfaces.GenericHash
import com.goterl.lazysodium.interfaces.PwHash
import com.goterl.lazysodium.interfaces.Random
import com.goterl.lazysodium.interfaces.SecretBox
import com.goterl.lazysodium.interfaces.SecretStream
import com.goterl.lazysodium.interfaces.Sign
import com.goterl.lazysodium.utils.Key
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import com.sun.jna.NativeLong
import org.timmc.spelunk.model.cavern.CiphertextDigest
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.io.PushbackInputStream
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Crypto helper, mostly a wrapper on libsodium.
 */
class Crypto {
    companion object {

        /**
         * Don't initialize Sodium unless we need it.
         * (Incidentally, the "Lazy" in "LazySodium" is a red herring here;
         * the only lazy-loading is done by the lazy() delegate.)
         */
        internal val sodium: LazySodiumJava by lazy { LazySodiumJava(SodiumJava()) }

        /*======================*
         * Symmetric encryption *
         *======================*/

        /**
         * The cleartext chunk size used in stream encryption. If changed,
         * decryption will fail on previously encrypted data.
         */
        const val STREAM_CHUNK_BYTES = 4000

        /**
         * Produce a secret key suitable for use with symmetric encryption and
         * decryption.
         */
        fun symmetricStreamKey(): SodiumSecretStreamKey {
            return SodiumSecretStreamKey(
                (sodium as SecretStream.Lazy).cryptoSecretStreamKeygen().asBytes
            )
        }

        // TODO: Extract the hashing/verification of the ciphertext to some
        //  composable stream wrapper class.

        /**
         * Encrypt stream with given symmetric key.
         *
         * Does not close output stream.
         *
         * Returns hash of encrypted stream.
         */
        fun symmetricEncryptStream(
            input: InputStream, output: OutputStream, key: SodiumSecretStreamKey
        ): CiphertextDigest {
            val stream = (sodium as SecretStream.Native)
            val hasher = Blake2bHasher.unkeyed(CiphertextDigest.BYTES_LEN)

            // Initialize stream, write the header
            val state = SecretStream.State()
            val header = ByteArray(SecretStream.HEADERBYTES)
            stream.cryptoSecretStreamInitPush(state, header, key.sharedBytes)
            || throw ESymmetricEncryptHeaderWrite()
            output.write(header)
            output.flush()
            hasher.update(header)

            // Encrypt and write chunks of output
            val cipherChunk = ByteArray(STREAM_CHUNK_BYTES + SecretStream.ABYTES)
            val msgIter = StreamChunker(input, STREAM_CHUNK_BYTES)
            for (msgChunk in msgIter) {
                val tag = if (msgIter.hasNext())
                    SecretStream.TAG_MESSAGE
                else
                    SecretStream.TAG_FINAL
                val msgBytes = msgChunk.size

                val cipherChunkSize = msgBytes + SecretStream.ABYTES
                // Regression test: Prevent memory corruption in C lib when
                // chunking goes wrong
                assert(cipherChunk.size >= cipherChunkSize) {
                    throw ESymmetricEncryptChunkSize(
                        cipherChunkSize = cipherChunk.size,
                        messageChunkSize = msgBytes,
                    )
                }
                stream.cryptoSecretStreamPush(state, cipherChunk,
                    msgChunk, msgBytes.toLong(), tag)
                || throw ESymmetricEncryptChunkWrite()
                output.write(cipherChunk, 0, cipherChunkSize)
                output.flush()
                hasher.update(cipherChunk.sliceArray(0 until cipherChunkSize))
            }

            return CiphertextDigest(hasher.digest())
        }

        /**
         * Decrypt stream using given symmetric key.
         *
         * Throws if hash is provided and does not match digest of the ciphertext.
         *
         * Does not close output stream.
         */
        fun symmetricDecryptStream(
            input: InputStream, output: OutputStream,
            key: SodiumSecretStreamKey, expectedCipherHash: CiphertextDigest?,
        ) {
            val stream = (sodium as SecretStream.Native)
            val verifier = if (expectedCipherHash == null)
                null
            else
                Blake2bHasher.unkeyed(CiphertextDigest.BYTES_LEN)

            // Initialize stream, read the header
            val state = SecretStream.State()
            val header = ByteArray(SecretStream.HEADERBYTES)
            if (input.read(header) != header.size)
                throw ESymmetricDecryptHeaderShort()
            verifier?.update(header)

            stream.cryptoSecretStreamInitPull(state, header, key.sharedBytes)
            || throw ESymmetricDecryptInit() // unreachable error, though

            // Decrypt and write chunks of output
            val cipherIter = StreamChunker(input, STREAM_CHUNK_BYTES + SecretStream.ABYTES)
            val msgChunk = ByteArray(STREAM_CHUNK_BYTES) // may be overlong
            val tag = ByteArray(1)
            var chunksRead = 0
            for (cipherChunk in cipherIter) {
                verifier?.update(cipherChunk)

                val lastChunk = !cipherIter.hasNext()
                // cipherChunk array only as long as its data
                val msgBytes = cipherChunk.size - SecretStream.ABYTES

                stream.cryptoSecretStreamPull(state, msgChunk, tag,
                    cipherChunk, cipherChunk.size.toLong())
                || throw ESymmetricDecryptChunk(chunkIndex = chunksRead, chunkSize = cipherChunk.size)

                output.write(msgChunk, 0, msgBytes)
                output.flush()
                chunksRead++

                if (tag[0] == SecretStream.TAG_FINAL) {
                    if (lastChunk) {
                        break
                    } else {
                        throw ESymmetricDecryptStreamExcess()
                    }
                }
            }

            if (tag[0] != SecretStream.TAG_FINAL) {
                throw ESymmetricDecryptStreamShort()
            }

            if (expectedCipherHash != null && !expectedCipherHash.matches(verifier!!.digest())) {
                throw ECiphertextIntegrity()
            }
        }

        /**
         * A convenience function to use [symmetricEncryptStream] with byte arrays.
         */
        fun symmetricEncrypt(data: ByteArray, key: SodiumSecretStreamKey): Pair<ByteArray, CiphertextDigest> {
            val out = ByteArrayOutputStream()
            val hash = symmetricEncryptStream(data.inputStream(), out, key)
            return out.toByteArray() to hash
        }

        /**
         * A convenience function to use [symmetricDecryptStream] with byte arrays.
         */
        fun symmetricDecrypt(
            encryptedData: ByteArray,
            key: SodiumSecretStreamKey, expectedCipherHash: CiphertextDigest?,
        ): ByteArray {
            val out = ByteArrayOutputStream()
            symmetricDecryptStream(encryptedData.inputStream(), out, key, expectedCipherHash)
            return out.toByteArray()
        }

        /**
         * A convenience function to use [symmetricDecryptStream] with a
         * streaming input but an in-memory output.
         */
        fun symmetricDecrypt(
            encryptedData: InputStream,
            key: SodiumSecretStreamKey, expectedCipherHash: CiphertextDigest?,
        ): ByteArray {
            val out = ByteArrayOutputStream()
            symmetricDecryptStream(encryptedData, out, key, expectedCipherHash)
            return out.toByteArray()
        }

        /*=======================*
         * Asymmetric encryption *
         *=======================*/

        /**
         * Create a keypair suitable for use with asymmetric encryption.
         */
        fun createBoxKeys(): SodiumBoxKeyPair {
            val pair = (sodium as Box.Lazy).cryptoBoxKeypair()
            return SodiumBoxKeyPair(
                publicKey = SodiumBoxPublicKey(pair.publicKey.asBytes),
                privateKey = SodiumBoxPrivateKey(pair.secretKey.asBytes),
            )
        }

        /**
         * Encrypt a small byte array in memory, producing a slightly larger byte-array
         * encrypted to the public key `recipPubKey` and signed by the private
         * key `senderPrivKey`.
         *
         * This uses <em>deniable authentication</em>; the recipient can
         * authenticate the message, but can't prove the sender's identity
         * to anyone else.
         *
         * The output takes the form of nonce + MAC header + ciphertext.
         */
        fun asymmetricEncrypt(message: ByteArray,
                              recipPubKey: SodiumBoxPublicKey,
                              senderPrivKey: SodiumBoxPrivateKey
        ): ByteArray {
            val box = sodium as Box.Native

            val nonce: ByteArray = (sodium as Random).nonce(Box.NONCEBYTES)
            val cipherText = ByteArray(Box.MACBYTES + message.size)

            box.cryptoBoxEasy(cipherText, message, message.size.toLong(), nonce,
                recipPubKey.sharedBytes, senderPrivKey.sharedBytes)
            || throw EAsymmetricEncrypt()

            return nonce + cipherText
        }

        /**
         * Decrypt a small ciphertext produced by asymmetricEncrypt, returning
         * the message as a byte array (slightly smaller than ciphertext).
         * Tries decrypting with every private key in [recipPrivKeys] and
         * verifies with sender's public key [senderPubKey].
         *
         * Expects input to take the form of nonce + MAC header + ciphertext
         */
        fun asymmetricDecrypt(encAndNonce: ByteArray,
                              recipPrivKeys: List<SodiumBoxPrivateKey>,
                              senderPubKey: SodiumBoxPublicKey
        ): ByteArray {
            val box = sodium as Box.Native

            if (recipPrivKeys.isEmpty())
                throw EAsymmetricDecryptNoKeysProvided()

            for (recipPrivKey in recipPrivKeys) {
                // I don't know if these get mutated, so slice them fresh each iteration
                val nonce = encAndNonce.sliceArray( 0 until Box.NONCEBYTES)
                val cipherText = encAndNonce.sliceArray(Box.NONCEBYTES until encAndNonce.size)

                val message = ByteArray(cipherText.size - Box.MACBYTES)
                val success = box.cryptoBoxOpenEasy(message,
                    cipherText, cipherText.size.toLong(), nonce,
                    senderPubKey.sharedBytes, recipPrivKey.sharedBytes)
                if (success) {
                    return message
                }
            }
            throw EAsymmetricDecryptNoMatchingKey()
        }

        /*===============*
         * Key agreement *
         *===============*/

        /**
         * From one person's private key and another person's public key, derive
         * a base shared secret. This 32 byte output is not safe to use as a
         * shared secret directly; it should be hashed with both public keys
         * as well as a domain separation constant before use.
         */
        fun x25519SharedSecret(
            onePrivateKey: SodiumBoxPrivateKey, otherPublicKey: SodiumBoxPublicKey
        ): ByteArray {
            return (sodium as DiffieHellman.Lazy).cryptoScalarMult(
                onePrivateKey.asSodiumKey(), otherPublicKey.asSodiumKey(),
            ).asBytes
        }

        /*=========================*
         * Various kinds of random *
         *=========================*/

        /**
         * Produce a lowercase alphanumeric string suitable for use in
         * making unguessable filenames (assuming an online attacker).
         */
        fun hiddenId(): String {
            // If an attacker guessed 10,000 filenames per second for 50 years,
            // with 1,000,000 targets, we could expect them to find one file if
            // there were about 2^64 possible names to guess from.
            //
            // This works out to 13 chars after Base32 encoding (no padding).
            return Utils.base32Encode((sodium as Random).randomBytesBuf(8))
        }

        /**
         * Return random bytes suitable for use as a shared secret, including
         * for use as a PBKDF input.
         */
        fun sharedSecret(): ByteArray {
            // 128 bits of entropy is sufficient for almost any purpose
            return (sodium as Random).randomBytesBuf(16)
        }

        /*======================================*
         * Key derivation and password handling *
         *======================================*/

        /**
         * Ops limit for the Argon hash used in encrypting the secret key. This
         * cannot be changed without invalidating existing ciphertexts.
         *
         * Chosen to be equal to the "sensitive" ops limit recommendation.
         */
        internal const val MASTER_DERIVATION_OPS_LIMIT: Long = 4

        /**
         * Mem limit for the Argon hash used in encrypting the secret key. This
         * cannot be changed without invalidating existing ciphertexts.
         *
         * Chosen to be equal to the "moderate" memory limit recommendation. 256
         * MB is a considerable amount of memory and is more of a hard limit
         * than CPU time is.
         */
        internal const val MASTER_DERIVATION_MEM_LIMIT: Long = 256 * 1024 * 1024

        /**
         * Expand the master password into an intermediate symmetric key, which
         * is then used to encrypt the user's secret key.
         *
         * Parameters have been chosen so that this will take about one second
         * on an old laptop; this should be an acceptable wait during
         * application start.
         */
        private fun masterPasswordToIntermediateKey(password: ByteArray, salt: ByteArray): Key {
            val bytesNeededForKey = SecretStream.KEYBYTES
            val keyBytes = ByteArray(bytesNeededForKey)
            // Using Native interface because the Lazy interface only accepts a
            // string for a password, not bytes.
            (sodium as PwHash.Native).cryptoPwHash(
                keyBytes, // output byte array
                keyBytes.size,
                password, // input byte array
                password.size,
                salt,
                MASTER_DERIVATION_OPS_LIMIT,
                NativeLong(MASTER_DERIVATION_MEM_LIMIT),
                // Use Argon2id v1.3
                PwHash.Alg.PWHASH_ALG_ARGON2ID13
            ) || throw EDeriveKeyFromPassword()
            return Key.fromBytes(keyBytes)
        }

        /**
         * Encrypts data using Argon2id 1.3 PBKDF algorithm. This should
         * generally be used to encrypt a Data-Encrypting Key, which is in turn
         * used to encrypt actual user data.
         *
         * A symmetric key is derived from the password + a random salt. The
         * given data is encrypted with the derived key using
         * [SecretBox.Lazy.cryptoSecretBoxEasy].
         *
         * The returned byte array is the concatenation of the Argon derivation
         * salt, the secret box nonce, and the secret box cipher.
         *
         * If a deterministic encryption is required for testing, the Argon salt
         * and box nonce can be passed in, but this should *only* be used for
         * testing or in special circumstances where a special randomness
         * source is required. These should always be fresh random bytes.
         */
        fun encryptWithPassword(plainData: ByteArray, password: ByteArray,
                                forceArgonSalt: ByteArray? = null,
                                forceBoxNonce: ByteArray? = null
        ): ByteArray {
            val salt = forceArgonSalt ?: (sodium as Random).randomBytesBuf(PwHash.ARGON2ID_SALTBYTES)
            val derivedKey = masterPasswordToIntermediateKey(password, salt)

            val boxNonce = forceBoxNonce ?: sodium.nonce(SecretBox.NONCEBYTES)
            val cipherHex = (sodium as SecretBox.Lazy).cryptoSecretBoxEasy(
                Utils.hexEncode(plainData),
                boxNonce,
                derivedKey)

            return salt + boxNonce + Utils.hexDecode(cipherHex)
        }

        /**
         * Decrypts by reversing [encryptWithPassword].
         */
        fun decryptWithPassword(cryptData: ByteArray, password: ByteArray): ByteArray {
            val (salt, box) = cryptData.divideAt(PwHash.ARGON2ID_SALTBYTES)
            val (boxNonce, cipherBytes) = box.divideAt(SecretBox.NONCEBYTES)
            val derivedKey = masterPasswordToIntermediateKey(password, salt)

            val plainHex = (sodium as SecretBox.Lazy).cryptoSecretBoxOpenEasy(
                Utils.hexEncode(cipherBytes),
                boxNonce,
                derivedKey)

            return Utils.hexDecode(plainHex)
        }

        /*=========*
         * Signing *
         *=========*/

        /**
         * Create a keypair for signing data with Ed25519.
         */
        fun createSigningKeys(): SodiumSigningKeyPair {
            val pair = (sodium as Sign.Lazy).cryptoSignKeypair()
            return SodiumSigningKeyPair(
                publicKey = SodiumSigningPublicKey(pair.publicKey.asBytes),
                privateKey = SodiumSigningPrivateKey(pair.secretKey.asBytes),
            )
        }

        /**
         * Sign some data in Ed25519 detached mode.
         */
        fun signData(data: ByteArray, key: SodiumSigningPrivateKey): Ed25519Signature {
            val signature = ByteArray(Sign.BYTES)

            (sodium as Sign.Native).cryptoSignDetached(
                signature, data, data.size.toLong(), key.sharedBytes
            ) || throw ESignData()

            return Ed25519Signature(signature)
        }

        /**
         * Verify an Ed25519 detached signature. Throws on failure.
         */
        fun verifySignature(
            signature: Ed25519Signature,
            data: ByteArray,
            verificationKey: SodiumSigningPublicKey
        ) {
            (sodium as Sign.Native).cryptoSignVerifyDetached(
                signature.sharedBytes,
                data, data.size,
                verificationKey.sharedBytes
            ) || throw EVerifySignature()
        }

        /*================*
         * Authentication *
         *================*/

        /**
         * Create a MAC over [data] using the given secret [key]. The MAC output
         * is [outSize] bytes long.
         *
         * This uses the keyed BLAKE2b hash function.
         */
        fun macCreate(data: ByteArray, key: ByteArray, outSize: Int): ByteArray {
            return Blake2bHasher.keyed(key, outSize).digest(data)
        }

        /**
         * Verify that the output of [macCreate] matches a provided MAC value.
         */
        fun macVerify(expected: ByteArray, actual: ByteArray): Boolean {
            if (expected.size != actual.size) {
                throw EAuthenticationCodeMatchSize(expected = expected.size, actual = actual.size)
            }
            return sodium.sodium.sodium_memcmp(expected, actual, expected.size) == 0
        }
    }
}

/**
 * Create a BLAKE2b hash digest of the given size using incremental
 * updates, and an optional key.
 *
 * - Output length must be between [BLAKE_HASH_OUT_MIN] and
 *   [BLAKE_HASH_OUT_MAX], inclusive.
 * - The key should be at least 16 bytes, and cannot be larger than 64
 *   bytes. 32 is usually ideal.
 *
 * If you need an unkeyed hash, call [unkeyed] rather than passing an empty
 * array to [keyed].
 */
class Blake2bHasher private constructor(val key: ByteArray?, val outSize: Int) {
    companion object {
        const val BLAKE_HASH_OUT_MIN = 1
        const val BLAKE_HASH_OUT_MAX = 64

        /** Begin a keyed BLAKE2b digest. Key cannot be empty. */
        fun keyed(key: ByteArray, outSize: Int): Blake2bHasher {
            return Blake2bHasher(key, outSize)
        }

        /** Begin an unkeyed BLAKE2b digest. */
        fun unkeyed(outSize: Int): Blake2bHasher {
            return Blake2bHasher(null, outSize)
        }
    }

    private val hashing = (Crypto.sodium as GenericHash.Native)
    private val state = ByteArray(hashing.cryptoGenericHashStateBytes())

    private val lock = Object()
    private val finalized = AtomicBoolean(false)

    init {
        if (key == null) {
            hashing.cryptoGenericHashInit(state, outSize)
        } else {
            if (key.size > 64) {
                throw IllegalArgumentException("BLAKE2b key cannot be longer than 64 bytes")
            }
            if (key.isEmpty()) {
                throw IllegalArgumentException(
                    "If a BLAKE2b key is provided it cannot be empty " +
                    "(use the unkeyed interface instead)"
                )
            }
            hashing.cryptoGenericHashInit(state, key, key.size, outSize)
        }

        if (outSize < BLAKE_HASH_OUT_MIN) {
            throw IllegalArgumentException("BLAKE2b output size must be at least $BLAKE_HASH_OUT_MIN")
        }
        if (outSize > BLAKE_HASH_OUT_MAX) {
            throw IllegalArgumentException("BLAKE2b output size must be at most $BLAKE_HASH_OUT_MAX")
        }
    }

    /** Pour more bytes into the digest. */
    fun update(addBytes: ByteArray): Blake2bHasher {
        synchronized(lock) {
            if (finalized.get())
                throw IllegalStateException("Blake2bHasher.update called after finalization")
            hashing.cryptoGenericHashUpdate(state, addBytes, addBytes.size.toLong())
        }
        return this
    }

    /** Finish the digest. */
    fun digest(): ByteArray {
        val digest = ByteArray(outSize)
        synchronized(lock) {
            if (finalized.getAndSet(true))
                throw IllegalStateException("Blake2bHasher.digest called twice")
            hashing.cryptoGenericHashFinal(state, digest, digest.size)
        }
        return digest
    }

    /** Combined interface that calls [update] and then [digest]. */
    fun digest(addBytes: ByteArray): ByteArray {
        return update(addBytes).digest()
    }
}

/**
 * Wrapper for some kind of key in lazy-sodium.
 *
 * lazy-sodium provides a Key class that is a thin wrapper over a byte array,
 * but that doesn't distinguish the different types of keys
 * or validate the incoming byte array sizes. This could
 * potentially lead to the wrong kind of key being used in some API call. These
 * subclasses of SodiumKey make up for this, as well as having data-class
 * properties.
 */
abstract class SodiumKey(size: Int, bytes: ByteArray): BytesSized(size, bytes) {
    /**
     * Convert to Key type for lazy-sodium.
     */
    fun asSodiumKey(): Key {
        return Key.fromBytes(sharedBytes)
    }
}

/**
 * Shared-secret key for libsodium "secret stream" operations.
 */
class SodiumSecretStreamKey(bytes: ByteArray): SodiumKey(SecretStream.KEYBYTES, bytes) {
    companion object {
        val hexJsonAdapter = object {
            @ToJson fun toJson(data: SodiumSecretStreamKey) = data.asHex()
            @FromJson fun fromJson(hex: String) = SodiumSecretStreamKey(Utils.hexDecode(hex))
        }
    }
}

/**
 * Public key for libsodium "crypto box" operations -- asymmetric authenticated
 * cryptography using X25519 key exchange, XSalsa20 stream cipher, and Poly1305
 * MAC authentication.
 */
class SodiumBoxPublicKey(bytes: ByteArray): SodiumKey(Box.PUBLICKEYBYTES, bytes)

/**
 * Private key for libsodium "crypto box" operations.
 */
class SodiumBoxPrivateKey(bytes: ByteArray): SodiumKey(Box.SECRETKEYBYTES, bytes)

/**
 * Keypair for libsodium "crypto box" operations.
 */
data class SodiumBoxKeyPair(
    val publicKey: SodiumBoxPublicKey,
    val privateKey: SodiumBoxPrivateKey,
)

class SodiumSigningPublicKey(bytes: ByteArray): SodiumKey(Sign.PUBLICKEYBYTES, bytes)

class SodiumSigningPrivateKey(bytes: ByteArray): SodiumKey(Sign.SECRETKEYBYTES, bytes)

data class SodiumSigningKeyPair(
    val publicKey: SodiumSigningPublicKey,
    val privateKey: SodiumSigningPrivateKey,
)

/**
 * Detached signature for Ed25519 signing and verification.
 */
class Ed25519Signature(bytes: ByteArray): BytesSized(Sign.BYTES, bytes);


/**
 * Thrown when there's an encryption or decryption failure (although sometimes
 * there's a SodiumException instead -- TODO fix that.)
 */
abstract class ECryptography(message: String): Exception(message)
abstract class EEncrypt(message: String): ECryptography(message)
abstract class EDecrypt(message: String): ECryptography(message)

/** Various failures to encrypt using symmetric stream cipher. */
abstract class ESymmetricEncrypt(message: String): EEncrypt(message)
class ESymmetricEncryptHeaderWrite: ESymmetricEncrypt("Failed to write header when encrypting")
class ESymmetricEncryptChunkSize(
    cipherChunkSize: Int, messageChunkSize: Int
): ESymmetricEncrypt("Cipher array $cipherChunkSize long, message array $messageChunkSize long")
class ESymmetricEncryptChunkWrite: ESymmetricEncrypt("Failed to write chunk when encrypting")

/** Various failures to encrypt using symmetric stream cipher. */
abstract class ESymmetricDecrypt(message: String): EDecrypt(message)
class ESymmetricDecryptHeaderShort:
    ESymmetricDecrypt("Could not read entire header for symmetric stream decrypt")
class ESymmetricDecryptInit: ESymmetricDecrypt("Failed to init stream when decrypting")
class ESymmetricDecryptChunk(
    val chunkIndex: Int, val chunkSize: Int,
): ESymmetricDecrypt("Failed to decrypt from stream on chunk $chunkIndex ($chunkSize bytes)")
class ESymmetricDecryptStreamExcess:
    ESymmetricDecrypt("Reached final tag when decrypting but stream still has data")
class ESymmetricDecryptStreamShort:
    ESymmetricEncrypt("Final decrypted chunk in stream was not a TAG_FINAL.")

class EAsymmetricEncrypt: EEncrypt("Failed to perform asymmetric encryption")
abstract class EAsymmetricDecrypt(message: String): EDecrypt(message)
class EAsymmetricDecryptNoMatchingKey:
    EAsymmetricDecrypt("Failed to perform asymmetric decryption; no private key worked")
class EAsymmetricDecryptNoKeysProvided:
    EAsymmetricDecrypt("Failed to perform asymmetric decryption; no private keys provided")

class ECiphertextIntegrity: ECryptography("Object for decryption did not match integrity hash.")

class EDeriveKeyFromPassword: ECryptography("""
    Could not hash master password to intermediate key. This may
    be due to insufficient memory, or perhaps your CPU does not
    support Argon2's instruction set.""".trimIndent().wrap()
)

class ESignData: ECryptography("Failed to sign data")
class EVerifySignature: ECryptography("Failed to verify signed data")

class EAuthenticationCodeCreate: ECryptography("Could not create message authentication code")
class EAuthenticationCodeMatchSize(
    expected: Int, actual: Int,
): ECryptography("Could not verify message authentication code; expected $expected bytes but received $actual bytes.")

/**
 * Return next byte in stream without advancing the pointer (not in a
 * thread-safe manner, though). Returns -1 if end of stream.
 */
fun PushbackInputStream.peek(): Int {
    val next = read()
    if (next >= 0)
        unread(next)
    return next
}

/**
 * Chunk an input stream into equal size byte arrays, presenting an Iterator
 * interface. Not thread-safe in the slightest.
 */
class StreamChunker(
    /**
     * Stream to read from. Nothing else should be reading from this stream,
     * especially not concurrently.
     */
    inputStream: InputStream,
    /**
     * Number of bytes to read each time
     */
    val chunkSize: Int
) : Iterator<ByteArray> {
    /**
     * Use a pushback stream so we can peek ahead one byte.
     */
    private val pushbackInputStream = PushbackInputStream(inputStream)

    /**
     * Return true if there is at least one more chunk to read (possibly not
     * a full chunk.)
     */
    override fun hasNext(): Boolean {
        return pushbackInputStream.peek() >= 0
    }

    /**
     * Read a chunk from the stream. Call hasNext() first to ensure this will
     * work; throws if end-of-stream already reached.
     *
     * The returned array is of length chunkSize or smaller.
     */
    override fun next(): ByteArray {
        return Utils.inputStreamReadNBytes(pushbackInputStream, chunkSize)
    }
}

/**
 * A balanced Feistel cipher operating on at most 8 bytes in the form of Longs.
 *
 * This probably isn't suitable for any application with high confidentiality,
 * integrity, or performance requirements.
 */
class DodgyFeistel(
    /**
     * The cipher key, expressed as bytes. This must be high-entropy.
     */
    key: ByteArray,
    /**
     * Number of bits to operate on, an even number in [2, 64]. Encryption and
     * decryption will be valid within the range of `[0, 2^totalBits)` (or
     * rather the two's complement interpretation.)
     */
    val totalBits: Int
) {
    init {
        if (totalBits < 2) {
            throw IllegalArgumentException("totalBits must be at least 2")
        }
        if (totalBits > 64) {
            throw IllegalArgumentException("totalBits must be at most 64")
        }
        if (totalBits % 2 != 0) {
            throw IllegalArgumentException("totalBits must be even")
        }
    }

    /** Bits per half. */
    internal val sideBits: Int = totalBits / 2

    /** Key for each round. */
    internal val roundKeys: List<ByteArray> = keyToRoundKeys(key)

    /**
     * Mask of [sideBits] lower bits set to 1.
     */
    internal val mask = bottomNSet(sideBits)

    /**
     * Smallest number of bytes which can contain [sideBits].
     */
    internal val sideBytesCeil: Int = divCeil(sideBits, 8)

    internal fun applyRoundKey(side: Long, roundKey: ByteArray): Long {
        val hash = Blake2bHasher.unkeyed(32)
            .update(longToBytes(side, sideBytesCeil))
            .update(roundKey)
            .digest()
        return bytesToLong(hash) and mask
    }

    internal fun runRound(halves: Pair<Long, Long>, roundKey: ByteArray): Pair<Long, Long> {
        val (left, right) = halves
        return right to (left xor applyRoundKey(right, roundKey))
    }

    internal fun splitHalves(input: Long): Pair<Long, Long> {
        val left = (input shr sideBits) and mask
        val right = input and mask
        return left to right
    }

    internal fun combineHalves(halves: Pair<Long, Long>): Long {
        val (left, right) = halves
        return (left shl sideBits) or right
    }

    internal fun crypt(input: Long, orderedRoundKeys: List<ByteArray>): Long {
        val (left, right) = orderedRoundKeys.fold(splitHalves(input), ::runRound)
        // Swap left and right again to undo the switch in the last round
        return combineHalves(right to left)
    }

    fun encrypt(input: Long): Long {
        return crypt(input, roundKeys)
    }

    fun decrypt(input: Long): Long {
        return crypt(input, roundKeys.reversed())
    }

    companion object {
        /**
         * Yield an integer with the bottom n bits set.
         */
        internal fun bottomNSet(n: Int): Long {
            var m = 0L
            for (i in 0 until n) {
                m = (m shl 1) + 1
            }
            return m
        }

        private val lowByteMask = bottomNSet(8)

        fun bytesToLong(bytes: ByteArray): Long {
            return bytes.fold(0L) { acc: Long, byte: Byte ->
                (acc shl 8) or (byte.toLong() and lowByteMask)
            }
        }

        fun longToBytes(data: Long, nBytes: Int = 8): ByteArray {
            val arr = ByteArray(nBytes)
            var remain = data
            for (i in (nBytes - 1) downTo 0) {
                arr[i] = remain.toByte()
                remain = remain ushr 8
            }
            return arr
        }

        /**
         * Size of each round key in bytes.
         */
        const val BYTES_PER_ROUND_KEY = 8

        /**
         * Number of Feistel rounds.
         */
        const val ROUNDS = 10

        internal fun keyToRoundKeys(key: ByteArray): List<ByteArray> {
            val bytesPerHash = 64 // BLAKE2b-512

            // Produce enough bytes for all the round keys:
            // BLAKE2b-512(0 || key) || BLAKE2b-512(1 || key) || ...
            val hashesNeeded = divCeil(BYTES_PER_ROUND_KEY * ROUNDS, bytesPerHash)
            val keySource = (0L..hashesNeeded).flatMap { i ->
                Blake2bHasher.unkeyed(bytesPerHash)
                    .update(longToBytes(i))
                    .update(key)
                    .digest().toList()
            }

            return keySource.chunked(BYTES_PER_ROUND_KEY) {
                it.toByteArray()
            }.take(ROUNDS)
        }

        /**
         * Integer division, but rounding up instead of down. Only takes
         * positive arguments.
         */
        internal fun divCeil(dividend: Int, divisor: Int): Int {
            assert(dividend > 0)
            assert(divisor > 0)
            return dividend / divisor + if (dividend % divisor == 0) 0 else 1
        }
    }
}
