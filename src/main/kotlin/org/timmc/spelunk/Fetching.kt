package org.timmc.spelunk

import com.goterl.lazysodium.exceptions.SodiumException
import mu.KotlinLogging
import org.apache.http.HttpHost
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.config.RegistryBuilder
import org.apache.http.conn.DnsResolver
import org.apache.http.conn.socket.ConnectionSocketFactory
import org.apache.http.conn.socket.PlainConnectionSocketFactory
import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.apache.http.protocol.HttpContext
import org.apache.http.ssl.SSLContexts
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.exists
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.msgpack.value.ValueFactory
import org.timmc.johnny.HostedUri
import org.timmc.johnny.TextPath
import org.timmc.johnny.Urls
import org.timmc.spelunk.model.CavAttachmentsDir
import org.timmc.spelunk.model.CavCatalogsDir
import org.timmc.spelunk.model.CavPostsDir
import org.timmc.spelunk.model.Config
import org.timmc.spelunk.model.Contact
import org.timmc.spelunk.model.ContactPatch
import org.timmc.spelunk.model.DecryptedJournal
import org.timmc.spelunk.model.EncryptedJournal
import org.timmc.spelunk.model.CatalogFinder
import org.timmc.spelunk.model.Keyring
import org.timmc.spelunk.model.cavern.CavAttachment
import org.timmc.spelunk.model.cavern.CavDataPointer
import org.timmc.spelunk.model.cavern.CavIdentity
import org.timmc.spelunk.model.cavern.CavCatalog
import org.timmc.spelunk.model.cavern.CavCatalogPointer
import org.timmc.spelunk.model.cavern.CavCatalogWrapper
import org.timmc.spelunk.model.cavern.CavPost
import org.timmc.spelunk.model.cavern.EAuthenticatedDataNoMatchingMacs
import java.io.InputStream
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.Socket
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.text.Normalizer
import java.time.Instant
import java.util.concurrent.ExecutorService
import java.util.concurrent.Future
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.RejectedExecutionException
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.locks.ReentrantReadWriteLock
import java.util.regex.Pattern
import javax.net.ssl.SSLContext
import kotlin.concurrent.read
import kotlin.concurrent.write

private val logger = KotlinLogging.logger {}

/**
 * Process and coordination for fetching contacts' journals.
 */
class Fetching(val state: State) {
    companion object {
        /** Latest schema version understood for fetch DB. */
        const val DB_SCHEMA_VERSION = 6

        /** Versioning information, used for upgrade/recreate check. Single row. */
        object Versioning : Table(name = "versioning") {
            /**
             * When compared to [Companion.DB_SCHEMA_VERSION], tells whether an
             * upgrade/recreate is required.
             */
            val schemaVersion = integer("schema_version")
            // NOTE: Cannot add/remove/change fields without messing up the DB
            //  validity check, which accesses this table via Exposed API.
        }

        /**
         * Journals table caches information about contacts' journals.
         */
        object Journals : Table(name = "journal") {
            /** The Contact.id of the contact whose journal this represents. */
            val contactId = long("contact_id")
            /** The hiddenId of the catalog file the contact published for me last. */
            val myCatalogHiddenId = text("catalog_hidden_id").nullable()
            /** The symmetric key of the catalog file the contact published for me last. */
            val myCatalogSymmetricKey = binary("catalog_symmetric_key", 32).nullable()
            // TODO Store and use ETag from identity and catalog files

            override val primaryKey = PrimaryKey(contactId)
        }

        /**
         * Posts table caches post pointers from journals.
         *
         * Since posts are always downloaded, the presence of a post in
         * this table indicates it has been downloaded.
         */
        object Posts : Table(name = "posts") {
            /** The Contact.id of the journal this post is from. */
            val contactId = long("contact_id")
            /**
             * The logical ID for this post, as reported by the journal.
             *
             * If a post is edited, it keeps its [postId] but the [hiddenId]
             * changes.
             */
            val postId = text("post_id")
            /** The hiddenID used for fetching this post. */
            val hiddenId = text("hidden_id")
            /**
             * The timestamp (milliseconds since Unix epoch) when this post was
             * first discovered by my app in the contact's catalog.
             */
            val discoveryDate = long("discovery_date")

            override val primaryKey = PrimaryKey(contactId, postId)
        }

        /**
         * Attachments table caches attachment pointers.
         *
         * Attachments, unlike posts, are *not* necessarily always downloaded,
         * so this includes information on how to download them.
         */
        object Attachments : Table(name = "attachments") {
            /** The Contact.id of the journal this post is from. */
            val contactId = long("contact_id")
            /**
             * The hiddenID used for fetching this attachment.
             *
             * If an attachment is edited, it appears as a new attachment
             * object with no connection to the previous record. Think of it
             * as opaque content addressing.
             */
            val hiddenId = text("hidden_id")
            /**
             * Filename under which this attachment should be stored on disk.
             * This is a munged version of the name that should be generically
             * safe for writing to my filesystem, whether Linux, Mac, or
             * Windows. It is only set the first time the attachment is
             * encountered; if multiple posts refer to this object with
             * different filenames, the first one prevails.
             */
            val storageFilename = text("storage_filename")

            override val primaryKey = PrimaryKey(contactId, hiddenId)
        }
    }

    private val repo = state.repo

    /**
     * A set of contact IDs currently being fetched. Access must be
     * synchronized on this object's lock.
     */
    private val contactFetchMutex = mutableSetOf<Long>()
    private val contactThreadIds = AtomicLong(0)
    /**
     * Executor for fetching contact journals, executing concurrently; tasks
     * must not be added unless the caller has successfully added the contact's
     * ID to the [contactFetchMutex], lest there be concurrent fetches on a
     * journal.
     */
    private val contactExecutor: ExecutorService = ThreadPoolExecutor(
        20, 20,
        60L, TimeUnit.SECONDS,
        LinkedBlockingQueue()
    ) { runnable ->
        Thread(runnable).apply {
            name = "Fetch worker for contacts #${contactThreadIds.getAndIncrement()}"
            isDaemon = true
        }
    }.apply {
        rejectedExecutionHandler = ThreadPoolExecutor.CallerRunsPolicy()
    }

    private val objectThreadIds = AtomicLong(0)
    /**
     * Executor for fetching journal objects, executing concurrently in a
     * sizeable pool. Caller is expected to ensure that any unwanted concurrency
     * is prevented, e.g. by deduplicating object fetches before placing them on
     * this queue.
     */
    private val objectFetchExecutor: ExecutorService = ThreadPoolExecutor(
        200, 200,
        60L, TimeUnit.SECONDS,
        LinkedBlockingQueue()
        ) { runnable -> Thread(runnable).apply {
            name = "Fetch worker for objects #${objectThreadIds.getAndIncrement()}"
            isDaemon = true
        }}.apply {
            rejectedExecutionHandler = ThreadPoolExecutor.CallerRunsPolicy()
        }

    // ===================== //
    // Connection management //
    // ===================== //

    private var db = database()

    /** Asynchronous copy of the main Config. */
    private var configCache = state.configRead()

    init {
        // If there's no valid DB, try destroying the old one and recreating
        // it, since it's a locally regeneratable cache.
        try {
            checkDBValid()
        } catch (t: Throwable) {
            recreateDB()
        }

        state.bus.configChange.addListener { newConfig ->
            configCache = newConfig
        }
    }

    /**
     * Get a connection to the fetchDB in the repo. Opens read-only if
     * repo instance does not allow writes.
     */
    private fun database(): Database {
        repo.cache.ensureDir()
        return SQLite3.database(repo.cache.fetchDB.path, repo.allowWrite)
    }

    /**
     * Run a transaction on the fetch database, returning the results.
     */
    private fun <R> tx(funk: Transaction.() -> R): R {
        return transaction(db, funk)
    }

    /**
     * Throw if DB is unusable, including not having current schema version.
     */
    private fun checkDBValid() {
        val version = tx {
            if (!Versioning.exists())
                throw RuntimeException("Fetch DB not yet initialized.")
            Versioning.selectAll().map { it[Versioning.schemaVersion] }.firstOrNull()
        }
        if (version != DB_SCHEMA_VERSION)
            throw RuntimeException("""
                Fetch DB has wrong schema version;
                expected $DB_SCHEMA_VERSION but found $version
            """.trimIndent().wrap())
    }

    /**
     * Drop the DB if it exists and create a fresh one.
     */
    private fun recreateDB() {
        repo.cache.fetchDB.requireWriteable("initialize fetch DB")
        Files.deleteIfExists(repo.cache.fetchDB.path)
        db = database()

        tx {
            SchemaUtils.create(
                Versioning, Journals, Posts, Attachments
            )

            // Finally, fill out version record. This must come last, since its
            // presence signals that init is complete.
            Versioning.insert { it[schemaVersion] = DB_SCHEMA_VERSION }
        }

        checkDBValid()
    }

    // ================== //
    // External interface //
    // ================== //

    /**
     * Fetch journals for all contacts, asynchronously and in parallel, whether
     * they've been updated recently or not.
     */
    fun queueFetchAll(): List<EnqueueResults> {
        return state.addressBookRead().contacts
            .filter { it.fetchUri != null }.sortedBy { it.separation }
            .map { queueFetchOne(it) }
    }

    /**
     * Fetch all journals, but wait until all have been fetched (successfully
     * or not.) Throw if any fetches failed.
     */
    fun blockingFetchAll() {
        queueFetchAll().forEach {
            when (it) {
                is EnqueueResults.Enqueued -> it.future.get()
                is EnqueueResults.AlreadyEnqueued -> { }
                is EnqueueResults.FailedToEnqueue -> throw it.thrown
            }
        }
    }

    /**
     * Fetch journal for one contact, asynchronously.
     *
     * Returns Future of task if it was enqueued, or null if already in queue or
     * failed to add for some reason.
     */
    fun queueFetchOne(contact: Contact): EnqueueResults {
        val canQueue = synchronized(contactFetchMutex) {
            contactFetchMutex.add(contact.id) // true if added
        }
        if (!canQueue)
            return EnqueueResults.AlreadyEnqueued

        return try {
            val future = contactExecutor.submit {
                // TODO extract this block as a private function
                try {
                    state.bus.fetchStatusChange.broadcast()
                    val secondsToFetch = timeSeconds { fetchOne(contact) }
                    logger.info { "Fetched journal at <${contact.fetchUri}> in ${String.format("%.3f", secondsToFetch)} seconds" }
                } finally {
                    synchronized(contactFetchMutex) {
                        contactFetchMutex.remove(contact.id)
                    }
                    state.bus.fetchStatusChange.broadcast()
                }
            }
            EnqueueResults.Enqueued(future)
        } catch(e: RejectedExecutionException) {
            synchronized(contactFetchMutex) {
                contactFetchMutex.remove(contact.id)
            }
            EnqueueResults.FailedToEnqueue(e)
        }
    }

    sealed class EnqueueResults {
        class Enqueued(val future: Future<*>): EnqueueResults()
        data object AlreadyEnqueued: EnqueueResults()
        class FailedToEnqueue(val thrown: Throwable): EnqueueResults()
    }

    /** Get all posts, in some order. */
    fun getAllPosts(): List<FetchedPost> {
        return tx {
            val addressBook = state.addressBookRead()
            val contactsById = addressBook.contacts.associateBy { it.id }
            Posts.selectAll().mapNotNull {
                val entry = PostsEntry.fromDB(it)
                val contact = contactsById[entry.contactId]
                    ?: return@mapNotNull null
                getOnePost(entry, contact)
            }
        }
    }

    private fun getOnePost(
        postCache: PostsEntry,
        contact: Contact
    ): FetchedPost? {
        val inbox = repo.cache.inboxes.byContactId(postCache.contactId)
        val postPath = inbox.postFile(postCache.hiddenId)
        postLocker(postCache.postId).read {
            if (!Files.exists(postPath.path)) {
                logger.warn { "Missing post file at ${postPath.path}" }
                return null
            }
            val post = try {
                postPath.readJson()
            } catch (t: Throwable) {
                logger.warn(t) { "Unexpected error reading post file at ${postPath.path}" }
                return null
            }
            if (post.type != Constants.POST_TYPE_JOURNAL) {
                logger.warn("Unrecognized post type '${post.type}' at  at ${postPath.path}")
                return null
            }

            val attachments = post.attachments.mapNotNull { attachMeta ->
                val storageName = findAttachmentStorageFilename(contact.id, attachMeta.pointer.hiddenId)
                    ?: run {
                        logger.warn { """
                            Expected to find storage filename for attachment
                             ${attachMeta.pointer.hiddenId} in journal for contact ${contact.id}
                            """.trimIndent().wrap()
                        }
                        return@mapNotNull null
                    }
                val storagePath = inbox.attachmentEnclosure(attachMeta.pointer.hiddenId, storageName)
                FetchedAttachment(attachMeta, storagePath)
            }

            return FetchedPost(contact = contact, postId = postCache.postId,
                discoveryDate = postCache.discoveryDate,
                post = post,
                attachments = attachments)
        }
    }

    data class FetchedPost(
        val contact: Contact,
        val postId: String,
        val discoveryDate: Instant,
        val post: CavPost,
        val attachments: List<FetchedAttachment>
    )

    data class FetchedAttachment(
        val meta: CavAttachment,
        /** Location on disk where the attachment is stored (if it was fetched.) */
        val storage: Path
    )

    data class PostsEntry(
        val contactId: Long,
        val postId: String,
        val hiddenId: String,
        val discoveryDate: Instant
    ) {
        companion object {
            fun fromDB(row: ResultRow) = PostsEntry(
                contactId = row[Posts.contactId],
                postId = row[Posts.postId],
                hiddenId = row[Posts.hiddenId],
                discoveryDate = Instant.ofEpochMilli(row[Posts.discoveryDate])
            )
        }
    }

    /** Read/write lock cache for posts. Managed by [postLocker]. */
    private val postLockerCache = mutableMapOf<String, ReentrantReadWriteLock>()

    /**
     * Get the read/write lock for a post by ID.
     *
     * Even though posts are never announced in the DB until they're written to
     * disk, they can also be updated in place, so they need a read/write lock.
     *
     * Attachments, on the other hand, are immutable. They do not need locks.
     *
     * (This is different from [State.postLocker] because Cavern's post/attachment
     * model is many-many whereas Spelunk's repo constrains it to
     * one-many, and because Spelunk's repo is not content-addressed.)
     */
    fun postLocker(postId: String): ReentrantReadWriteLock {
        return synchronized(postLockerCache) {
            postLockerCache.getOrPut(postId) { ReentrantReadWriteLock(true) }
        }
    }

    data class FetchStatus(
        /** Journals currently being fetched, or queued to be fetched. */
        val journalsInFlight: Int
    )

    /**
     * Get the current fetch status. May not be entirely consistent.
     */
    fun getStatus(): FetchStatus {
        synchronized(contactFetchMutex) {
            return FetchStatus(journalsInFlight = contactFetchMutex.size)
        }
    }

    // ========= //
    // Internals //
    // ========= //

    fun getCachedContactsCatalogPointerForMe(contact: Contact): CavCatalogPointer? {
        return tx {
            Journals.select { Journals.contactId eq contact.id }
                .map {
                    val hid = it[Journals.myCatalogHiddenId]
                    val key = it[Journals.myCatalogSymmetricKey]
                    if (hid != null && key != null) {
                        CavCatalogPointer(
                            hiddenId = hid,
                            symmetricKey = SodiumSecretStreamKey(key)
                        )
                    } else {
                        null
                    }
                }
                .firstOrNull()
        }
    }

    fun setCachedContactsCatalogPointerForMe(contact: Contact, catalogPointer: CavCatalogPointer) {
        tx {
            Journals.upsert(Journals.contactId) {
                it[contactId] = contact.id
                it[myCatalogHiddenId] = catalogPointer.hiddenId
                it[myCatalogSymmetricKey] = catalogPointer.symmetricKey.sharedBytes
            }
        }
    }

    /**
     * Synchronously fetch one contact, possibly throwing. Must be called under
     * lock.
     *
     * We might not know the contact's public key yet, and we also might not
     * know the hiddenID under which the contact is publishing a catalog for us,
     * or whether they even are. This information will be fetched or updated
     * either if it is missing or if the catalog file can't be found under a
     * currently known hiddenID.
     *
     * If the catalog file can't be found, and the identity file is subsequently
     * refreshed, a second attempt will be made to find it.
     */
    private fun fetchOne(contact: Contact) {
        val fetcher = contact.fetchUri?.let { interpretFetchUri(it, configCache) }
            ?: throw RuntimeException("Skipping contact ${contact.id}; don't know how to fetch journal: <${contact.fetchUri}>")
        logger.info { "Fetching journal for contact ${contact.id} at <${contact.fetchUri}>" }

        val keyring = state.keyringRead()

        // Start with the stored data; might be updated later in method
        var catalogPointer = getCachedContactsCatalogPointerForMe(contact)
        var contactPublicKey = contact.boxKey

        // Closure to perform the catalog fetch.
        // Called either one or two times, depending on whether it throws a
        // retriable exception.
        val maybeFetchCatalog = {
            // Can't close over these and also do smart-cast from nullable.
                pointer: CavCatalogPointer?, pubkey: SodiumBoxPublicKey? ->

            if (pointer == null) throw EFetchCatalogUnknownHiddenId()
            if (pubkey == null) throw EFetchCatalogUnknownSigningKey()

            fetchCatalog(
                fetcher = fetcher,
                catalogPointer = pointer,
                contactPublicKey = pubkey,
                keyring = keyring,
            )
        }

        // Try to fetch and decrypt the catalog
        val catalog = try {
            try {
                maybeFetchCatalog(catalogPointer, contactPublicKey)
            } catch (t: Throwable) {
                // One opportunity to retry, if it might be solved by
                // re-fetching the identity file.
                if (t is EFetchCatalogMissingPrereq
                    || t is EObjectDoesNotExist
                    || t is ECryptography
                    || t is SodiumException
                    || t is EAuthenticatedDataNoMatchingMacs
                ) {
                    // Refresh the identity and try again.
                    logger.info("Updating identity and retrying catalog fetch for contact ${contact.id} after error: $t")
                    val (newPointer, newBoxKey) = updateIdentity(fetcher, contact, keyring)
                    catalogPointer = newPointer
                    contactPublicKey = newBoxKey
                    maybeFetchCatalog(catalogPointer, contactPublicKey)
                } else {
                    throw t
                }
            }
        } catch (t: Throwable) {
            val idDesc = catalogPointer?.hiddenId?.let {
                "hidden ID $it"
            } ?: "unknown ID"
            val causeMessage = if (t is EFetch) {
                t.message
            } else {
                "$t"
            }
            val message = "Unable to fetch journal ${contact.fetchUri} (contact ${contact.id}, $idDesc): $causeMessage"
            throw EFetchJournal(message, t)
        }

        // Compare the fresh catalog with the cached catalog. Are there new posts,
        // or posts with changed pointers? Collect those, since we'll want to
        // download them. (To keep things concurrency-friendly, don't update
        // the DB with those new pointers until the files are on disk.)
        val postsNeedingFetch = catalog.posts.distinctBy { it.id }.filter { post ->
            val existingPointer = tx {
                Posts.select {
                    (Posts.contactId eq contact.id).and(Posts.postId eq post.id)
                }.map { it[Posts.hiddenId] }.firstOrNull()
            }
            existingPointer != post.pointer.hiddenId
        }

        // Time to start dropping stuff to disk
        val inbox = repo.cache.inboxes.byContactId(contact.id)
        inbox.ensureDir()

        // For each post that has been added or edited, download it and record
        // the post in the DB. These parts are done asynchronously.
        val postsDiscoveredTsMs = System.currentTimeMillis() // all get same timestamp
        val attachmentHiddenIdsSeen = mutableSetOf<String>()
        val postFutures = postsNeedingFetch.map { postOffer ->
            objectFetchExecutor.submit {
                logger.info { "Trying to fetch post ${postOffer.id} for contact ${contact.id}" }
                val post = try {
                    postLocker(postOffer.id).write {
                        // 1. Fetch the post
                        val post = fetcher.fetchPost(postOffer.pointer) {
                            val enc = it.readBytes()
                            logger.debug {
                                "Post $postOffer data ${enc.size} bytes with hash ${postOffer.pointer.cipherHash}"
                            }
                            val dec = Crypto.symmetricDecrypt(
                                enc, postOffer.pointer.symmetricKey, postOffer.pointer.cipherHash,
                            )
                            CavPost.fromMsgpack(dec)
                        }
                        // 2. Save it to disk
                        inbox.postFile(postOffer.pointer.hiddenId).writeJson(post)
                        post
                    }
                } catch (t: Throwable) {
                    throw EFetchPost(
                        stableId = postOffer.id, hiddenId = postOffer.pointer.hiddenId,
                        cause = t,
                    )
                }
                // 3. Update DB with new or changed pointer
                tx { Posts.upsert(Posts.contactId, Posts.postId) { row ->
                    row[contactId] = contact.id
                    row[postId] = postOffer.id
                    row[hiddenId] = postOffer.pointer.hiddenId
                    row[discoveryDate] = postsDiscoveredTsMs
                } }
                // 4. Record attachments in DB
                val attachmentFutures = post.attachments.mapNotNull { cmeta ->
                    // Make sure it's in the DB first, and get the storageFilename
                    val storageFilename = dbRecordAttachment(contact.id, cmeta)

                    // For now, fetch all files.
                    // TODO Only download small files, such as emoji?
                    // Don't allow multiple post executions to both fetch
                    // the same attachment, let alone concurrently. Add to
                    // set to signal intent to download.
                    val added = synchronized(attachmentHiddenIdsSeen) {
                        attachmentHiddenIdsSeen.add(cmeta.pointer.hiddenId)
                    }
                    if (added) {
                        objectFetchExecutor.submit {
                            ensureAttachmentDownloaded(inbox, fetcher, cmeta, storageFilename)
                        }
                    } else {
                        null // someone else is already downloading this
                    }
                }

                // Wait for all attachments to finish before allowing post fetch
                // to dequeue
                attachmentFutures.map { it.get() }
                state.bus.fetchedNewOrUpdatedPost.broadcast()
            }
        }

        // Wait for all posts to finish before allowing the journal fetch to
        // dequeue
        postFutures.map { it.get() }
    }

    /**
     * Assert that this binary data is of the currently understood spec version.
     *
     * Given bytes that are expected to unpack as a MessagePack map with a top
     * level "spec" key, read the spec value as a string. If it is not an
     * understood version, throw an [EFetchSpec].
     */
    internal fun assertSpec(raw: ByteArray, sourceHint: String) {
        val foundSpec = try {
            val topMap = MsgUnpackable.unpackValue(raw).asMapValue()
            topMap.map()[ValueFactory.newString("spec")]!!
                .asStringValue().asString()
        } catch (t: Throwable) {
            throw EFetchSpecUnavailable(sourceHint, t)
        }
        if (foundSpec != Constants.CAVERN_SPEC) {
            if (Constants.UNSUPPORTED_CAVERN_SPECS.contains(foundSpec)) {
                throw EFetchSpecUnsupported(foundSpec, sourceHint)
            } else {
                throw EFetchSpecUnknown(foundSpec, sourceHint)
            }
        }
    }

    /**
     * Fetch the contact's identity file, updating the stored contact (will then
     * be guaranteed to have a non-null public key) and returning the catalog
     * pointer and public box key they're currently offering.
     *
     * Throws if contact isn't publishing a catalog for us.
     */
    private fun updateIdentity(
        fetcher: Fetcher, storedContact: Contact, keyring: Keyring,
    ): Pair<CavCatalogPointer, SodiumBoxPublicKey> {
        logger.debug("Reading identity info for ${storedContact.id} (${storedContact.fetchUri})")
        val identity = fetcher.fetchIdentity {
            val bytes = it.readBytes()
            assertSpec(bytes, "identity")
            CavIdentity.fromMsgpack(bytes)
        }

        val contactPubKey = identity.readPublicKey()

        // Update our knowledge of the Contact's keys. This must happen before
        // we try getting the catalog pointer, since that may not exist.
        // TODO: Also update our knowledge of their preferred name, etc.
        val patch = ContactPatch(
            sigKey = MaybePatch.Present(contactPubKey.sigPublicKey),
            boxKey = MaybePatch.Present(contactPubKey.boxPublicKey),
            handleReceived = MaybePatch.Present(identity.name),
        ).minifyAgainst(storedContact)
        if (patch.sigKey is MaybePatch.Present && storedContact.sigKey != null) {
            // TODO Only accept changed sig key if there's a valid forward signature chain we trust
            // TODO Add to audit log and possibly notify user
            logger.warn { """
                Contact's sig key changed. ID = ${storedContact.id} at <${storedContact.fetchUri}>
                changed their signing key from ${storedContact.sigKey.asHex()}
                to ${contactPubKey.sigPublicKey.asHex()}.
            """.trimIndent().wrap() }
        }
        if (patch.boxKey is MaybePatch.Present && storedContact.boxKey != null) {
            // TODO Add to audit log and possibly notify user
            logger.warn { """
                Contact's enc key changed. ID = ${storedContact.id} at <${storedContact.fetchUri}>
                changed their encryption key from ${storedContact.boxKey.asHex()}
                to ${contactPubKey.boxPublicKey.asHex()}.
            """.trimIndent().wrap() }
        }
        if (patch.hasChanges()) {
            state.addressBookPatchContact(storedContact.id, patch)
            patch.applyTo(storedContact)
        }

        // Update the catalog_finder record while we're at it. Might not be
        // necessary to do this decryption, but this should be an infrequent
        // call and it simplifies the retry logic in `fetchOne`.
        val catalogPointer = CatalogFinder.decrypt(
            identity.catalogFinder,
            contactPubKey.boxPublicKey,
            keyring,
        )
            ?: throw EFetchNothingInCatalogFinder()
        setCachedContactsCatalogPointerForMe(storedContact, catalogPointer)
        return catalogPointer to contactPubKey.boxPublicKey
    }

    /**
     * Fetch my catalog from a contact's journal.
     */
    private fun fetchCatalog(
        fetcher: Fetcher, catalogPointer: CavCatalogPointer,
        contactPublicKey: SodiumBoxPublicKey, keyring: Keyring,
    ): CavCatalog {
        return fetcher.fetchCatalog(catalogPointer) { stream ->
            val dec = Crypto.symmetricDecrypt(stream, catalogPointer.symmetricKey, null)
            assertSpec(dec, "catalog")
            val wrapper = CavCatalogWrapper.fromMsgpack(dec)
            wrapper.unwrapCatalog(
                senderKey = contactPublicKey, readerKeyPairs = keyring.allKeysReverseChrono().map { it.boxKeys },
            )
        }
    }

    /**
     * Record attachment in the database, if it's not already there. Either way,
     * return the storage filename.
     */
    fun dbRecordAttachment(contactId: Long, attach: CavAttachment): String {
        val attachmentHiddenId = attach.pointer.hiddenId
        return tx {
            val existing = findAttachmentStorageFilename(contactId, attachmentHiddenId)
            if (existing == null) {
                val mangled = storageFilename(attach.clearFilename)
                Attachments.insert { row ->
                    row[Attachments.contactId] = contactId
                    row[hiddenId] = attachmentHiddenId
                    row[storageFilename] = mangled
                }
                mangled
            } else {
                existing
            }
        }
    }

    /**
     * If an attachment is already in the DB, find the storage name that was
     * chosen for it. (No guarantee that it has been downloaded.)
     */
    private fun findAttachmentStorageFilename(contactId: Long, attachmentHiddenId: String): String? {
        return tx {
            Attachments.select {
                (Attachments.contactId eq contactId)
                    .and(Attachments.hiddenId eq attachmentHiddenId)
            }.map { it[Attachments.storageFilename] }.firstOrNull()
        }
    }

    /**
     * Either download the offered attachment or make sure it is already in
     * place.
     */
    fun ensureAttachmentDownloaded(
        inbox: DecryptedJournal, fetcher: Fetcher,
        attach: CavAttachment, storageFilename: String
    ) {
        val destPath = inbox.attachmentEnclosure(attach.pointer.hiddenId, storageFilename)
        if (Files.exists(destPath)) return

        // TODO Prevent absurdly large files from being written (cut off stream
        // after advertised number of bytes, too?)
        try {
            fetcher.fetchAttachment(attach.pointer) { downloadInputStream ->
                Files.createDirectories(destPath.parent) // create enclosure dir
                Utils.withTempFile(destPath) { tmpOutputStream ->
                    Crypto.symmetricDecryptStream(downloadInputStream, tmpOutputStream,
                        attach.pointer.symmetricKey, attach.pointer.cipherHash)
                }
            }
        } catch (t: Throwable) {
            throw RuntimeException("""
                Error while downloading or decrypting attachment with ID
                ${attach.pointer.hiddenId} to inbox ${inbox.name}
            """.trimIndent().wrap(), t)
        }
    }
}


// ====== //
// Errors //
// ====== //

/** General failure to fetch something from a journal. */
abstract class EFetch(message: String, cause: Throwable? = null): Exception(message, cause)

abstract class EFetchSpec(message: String, cause: Throwable? = null):
    EFetch(message, cause)

class EFetchSpecUnavailable(componentHint: String, cause: Throwable):
    EFetchSpec("Unable to determine spec version when reading $componentHint", cause)

abstract class EFetchSpecBadValue(
    val foundSpec: String, val componentHint: String, badType: String,
): EFetchSpec("Encountered $badType spec version in $componentHint: $foundSpec")

class EFetchSpecUnknown(foundSpec: String, componentHint: String):
    EFetchSpecBadValue(foundSpec, componentHint, "unknown")

class EFetchSpecUnsupported(foundSpec: String, componentHint: String):
    EFetchSpecBadValue(foundSpec, componentHint, "unsupported")

class EFetchNothingInCatalogFinder: EFetch("Contact isn't publishing anything for me")

// Internal exception used to signal need to refetch identity doc
abstract class EFetchCatalogMissingPrereq(message: String): EFetch(message)
class EFetchCatalogUnknownHiddenId:
    EFetchCatalogMissingPrereq("Contact's hiddenID for us is not yet known")
class EFetchCatalogUnknownSigningKey:
    EFetchCatalogMissingPrereq("Contact's signing key is not yet known")

class EFetchPost(
    val stableId: String, val hiddenId: String, override val cause: Throwable
): EFetch(
    "Could not fetch post $stableId (offered with hidden ID $hiddenId)", cause
)

/** Exception signifying an object we tried to fetch does not exist. */
class EObjectDoesNotExist(val path: String):
    EFetch("Journal component does not exist: <$path>")

/**
 * Signifies that a journal fetch failed.
 *
 * Used to add information to underlying error.
 */
class EFetchJournal(message: String, override val cause: Throwable?): EFetch(message, cause)


// ================= //
// Filename cleaning //
// ================= //

private const val anums = """\p{Alnum}"""
private val unsafeFilenameCharacterRuns = Pattern.compile("""[^$anums\-._]+""",
    Pattern.UNICODE_CHARACTER_CLASS)
private val nonAlphanumericBorders = Pattern.compile("""^[^$anums]+|[^$anums]+$""",
    Pattern.UNICODE_CHARACTER_CLASS)
private val nonAlpanumericRuns = Pattern.compile("""([^$anums])\1+""",
    Pattern.UNICODE_CHARACTER_CLASS)

/**
 * Come up with a safe filename that can be used to save an attachment
 * to disk, given the suggested filename. Try to preserve as much of the
 * given file name as possible.
 */
fun storageFilename(suggestedFileName: String): String {
    // Reduce chances of stripping diacritics, e.g. when encountering "n" +
    // combining tilde rather than "ñ". This won't help for all scripts, though.
    var s = Normalizer.normalize(suggestedFileName, Normalizer.Form.NFC)
    // Elide anything outside an allowlist of characters -- alphanumerics, as
    // construed by Unicode, plus a few others.
    s = unsafeFilenameCharacterRuns.matcher(s).replaceAll("_")
    // Must start and end with alphanumerics to avoid `--flags` and other
    // special meanings
    s = nonAlphanumericBorders.matcher(s).replaceAll("")
    // No long non-alphanumeric sequences that might be confusing in a narrow
    // UI. Replace any sequence of 1+ of a character with just one of that char.
    s = Utils.replaceAll(nonAlpanumericRuns, s) { it.group(1) }
    // Check if there are any OS-specific restrictions
    s = os.cleanFileName(s)
    // Make sure there's *something* to work with
    if (s.isEmpty()) {
        s = "file_${Crypto.hiddenId()}.bin"
    }
    return s
}

// ===================== //
// Fetch implementations //
// ===================== //

/**
 * Given a journal URL, figure out what the base URL is.
 *
 * This removes any of the following suffixes from the path:
 *
 * - `*.html`
 * - `*.htm`
 * - `_cavern/` or `_cavern`
 * - `_cavern/identity.mpk`
 *
 * and ensures the result has a trailing slash.
 */
fun inferJournalBaseUrl(given: HostedUri): HostedUri {
    val reserialized = given.path.format().lowercase()
    val ndrop = when {
        reserialized.endsWith(".htm") -> 1
        reserialized.endsWith(".html") -> 1
        reserialized.endsWith("/_cavern") -> 1
        reserialized.endsWith("/_cavern/") -> 1
        reserialized.endsWith("/_cavern/identity.mpk") -> 2
        else -> 0
    }
    val path = TextPath.EMPTY.addSegments(given.path.segments.dropLast(ndrop))
    return given.withPath(path.withTrailingSlash(true))
}

/**
 * Parse the URI and get an appropriate Fetcher, or null if URL is invalid
 * or unsupported.
 */
// TODO Return string explaining error
fun interpretFetchUri(uri: String, config: Config): Fetcher? {
    val parsed = runCatching { Urls.parse(uri) }.getOrNull() ?: return null
    return when (parsed.scheme) {
        "http", "https" -> {
            // I can't support locations with baked-in username/password until
            // there's a clear mechanism for distinguishing between locations
            // for public consumption vs. private access, which is most
            // important in the upcoming gossip protocol.
            //
            // And fragment is unused on the server side, so strip it out.
            val unsupportedParts = listOf(parsed.userInfoRaw, parsed.fragmentRaw)
            if (unsupportedParts.all { it == null }) {
                val useProxy = if (isOnionSite(parsed)) {
                    // Quietly ignore if fails to parse
                    config.torSocksProxy?.let { suppressThrowAsNull { parseSocketAddress(it) } }
                } else {
                    null
                }
                val base = inferJournalBaseUrl(parsed).withUserPass(null).withFragment(null)
                HttpFetcher(base, useProxy)
            } else {
                logger.debug { "Could not use fetcher URL, since it has userinfo or fragment: <$uri>" }
                null
            }
        }
        "file" -> {
            // Only support path in file URIs.
            val unsupportedParts = listOf(parsed.userInfoRaw, parsed.portRaw,
                parsed.queryRaw, parsed.fragmentRaw)
            // Accept either file:/foo/bar or file:///foo/bar as local file
            val isLocal = parsed.hostRaw == ""
            if (isLocal && unsupportedParts.all { it == null }) {
                val base = inferJournalBaseUrl(parsed)
                FileFetcher(base)
            } else {
                logger.debug { "Could not use fetcher URL, since it has more than just path: <$uri>" }
                null
            }
        }
        else -> null
    }
}

/**
 * Parse a socket address in textual form, containing a host (hostname, IPv4, or
 * IPv6 address) and a port, delimited by a colon.
 *
 * For IPv6 addresses, the address must be delimited by square brackets, as
 * recommended in <http://tools.ietf.org/html/rfc5952>.
 */
fun parseSocketAddress(addr: String): InetSocketAddress {
    val delimPos = addr.lastIndexOf(':') // IPv6 host can contain colon
    if (delimPos < 0) throw IllegalArgumentException("Socket address missing : delimiter")

    val host = addr.substring(0, delimPos)
    val portStr = addr.substring(delimPos + 1)
    if (host.isEmpty()) throw IllegalArgumentException("Socket address missing host")
    if (portStr.isEmpty()) throw IllegalArgumentException("Socket address missing port")
    if (!portStr.all { it in '0'..'9' }) throw IllegalArgumentException("Socket address port is non-numeric")

    val portNum = portStr.toInt()
    if (portNum !in 1..65535) throw IllegalArgumentException("Socket address port out of range [1, 65535]")

    // Special check to enforce [IPv6]:port format. Might relax this later,
    // since InetSocketAddress will accept either, but there's a small risk of
    // someone passing a bare IPv6 and the last segment being interpreted as a
    // port. (This would have to involve an address with a `::`.)
    if (host.contains(':') && !(host.startsWith('[') && host.endsWith(']')))
        throw IllegalArgumentException("Socket address must enclose IPv6 address in square brackets.")

    return InetSocketAddress(host, portNum)
}

/**
 * Detect Tor onion server URLs based on domain name.
 */
fun isOnionSite(parsed: HostedUri): Boolean {
    return Regex("""\.onion\.?$""", RegexOption.IGNORE_CASE).containsMatchIn(parsed.hostRaw)
}

/**
 * Generic notion of fetching all the parts of a journal.
 *
 * Methods make some kind of request, and pass an input stream to their given
 * `reader` argument, which is expected (but not required) to consume the
 * stream. Once the reader returns, the stream is closed and the request is
 * ended, and the reader's return value is returned.
 *
 * Each implementation is responsible for tacking on the _cavern/ subdirectory.
 */
abstract class Fetcher(val base: HostedUri) {
    abstract fun <R> fetchIdentity(reader: (InputStream) -> R): R

    abstract fun <R> fetchCatalog(pointer: CavCatalogPointer, reader: (InputStream) -> R): R

    abstract fun <R> fetchPost(pointer: CavDataPointer, reader: (InputStream) -> R): R

    abstract fun <R> fetchAttachment(pointer: CavDataPointer, reader: (InputStream) -> R): R
}

/**
 * Fetcher where all the parts of the journal are fetched in basically the same
 * way, and have the same "shape" as the standard HTTP journal layout.
 *
 * Each implementation is responsible for tacking on the _cavern/ subdirectory.
 */
abstract class StandardShapeFetcher(base: HostedUri): Fetcher(base) {
    /**
     * Fetch object from `base/_cavern/path/into`.
     *
     * Implementation must take responsibility for adding `_cavern` to the path
     * sequence.
     */
    abstract fun <R> fetchObject(
        /** Same semantics as all Fetcher readers. */
        reader: (InputStream) -> R,
        /** Path in journal from root directory, as additional segments. */
        vararg pathInto: String,
        /**
         * Whether to request cache-busting, if relevant for the protocol.
         * Should be used on any objects that aren't content-addressed.
         */
        cacheBust: Boolean
    ): R

    override fun <R> fetchIdentity(reader: (InputStream) -> R): R {
        return fetchObject(reader, EncryptedJournal.IDENTITY_FILE_NAME,
            // Changes infrequently. TODO: Don't use cachebusting, but recheck
            // with cachebusting if catalog file decryption fails due to signature
            // mismatch or perspective file is missing?
            cacheBust = true)
    }

    override fun <R> fetchCatalog(pointer: CavCatalogPointer, reader: (InputStream) -> R): R {
        return fetchObject(reader, EncryptedJournal.CATALOGS_DIR_NAME,
            CavCatalogsDir.hiddenIdToFileName(pointer.hiddenId),
            // Always, always want latest, and the cache entry is only valid
            // for me, anyhow.
            cacheBust = true)
    }

    override fun <R> fetchPost(pointer: CavDataPointer, reader: (InputStream) -> R): R {
        return fetchObject(reader, EncryptedJournal.POSTS_DIR_NAME,
            CavPostsDir.hiddenIdToFileName(pointer.hiddenId),
            // The content at a CavDataPointer should never change
            cacheBust = false)
    }

    override fun <R> fetchAttachment(pointer: CavDataPointer, reader: (InputStream) -> R): R {
        return fetchObject(reader, EncryptedJournal.ATTACHMENTS_DIR_NAME,
            CavAttachmentsDir.hiddenIdToFileName(pointer.hiddenId),
            // The content at a CavDataPointer should never change
            cacheBust = false)
    }
}

/**
 * Read journal parts from disk.
 */
class FileFetcher(base: HostedUri): StandardShapeFetcher(base) {
    companion object {
        fun buildPath(base: Path, more: Iterable<String>): Path {
            return more.fold(base) { acc, s -> acc.resolve(s) }
        }

        fun uriPathToFilePath(path: TextPath): Path {
            return buildPath(Paths.get("/"), path.withTrailingSlash(false).segments)
        }
    }

    val basePath = uriPathToFilePath(base.path)

    override fun <R> fetchObject(
        reader: (InputStream) -> R, vararg pathInto: String, cacheBust: Boolean
    ): R {
        val fullPath = buildPath(basePath, listOf(Constants.CAVERN_DIR) + pathInto.asList())
        if (!Files.exists(fullPath))
            throw EObjectDoesNotExist(fullPath.toString())
        return reader(Files.newInputStream(fullPath))
    }
}

/**
 * Read journal parts from an HTTP/HTTPS server.
 */
class HttpFetcher(base: HostedUri, val socksProxy: InetSocketAddress?): StandardShapeFetcher(base) {
    /**
     * Fetch an object from a journal.
     */
    override fun <R> fetchObject(
        reader: (InputStream) -> R,
        vararg pathInto: String,
        cacheBust: Boolean
    ): R {
        val url = base.mapPath { it.addSegments(listOf(Constants.CAVERN_DIR, *pathInto)) }
        logger.debug { "Fetching <${url.format()}>" }
        val request = HttpGet(url.format())
        if (cacheBust) {
            request.addHeader("Cache-Control", "no-cache")
        }
        return doCall(request).use { response ->
            when (val status = response.statusLine.statusCode) {
                200 -> response.entity.content.use(reader)
                // Amazon S3 gives a 403 rather than a 404 for a missing object
                // when the requester doesn't have ListBucket permissions. Go
                // ahead and throw 401 in here as well; there are likely to be
                // similar situations with other servers.
                401, 403, 404, 410 -> throw EObjectDoesNotExist(url.format())
                else -> throw RuntimeException("Unexpected HTTP status code $status when fetching journal component at <${url.format()}>")
            }
        }
    }

    fun doCall(request: HttpGet): CloseableHttpResponse {
        return if (socksProxy == null) {
            defaultClient.execute(request)
        } else {
            socksClient.execute(request, makeSocksContext(socksProxy))
        }
    }

    companion object {
        /** Configure common options for all client types. */
        fun clientBuilder(): HttpClientBuilder {
            return HttpClients.custom().apply {
                // TODO Retry handling
                // TODO Redirect handling
                setUserAgent("Spelunk (Cavern client)")
            }
        }

        /** HTTP client for non-proxied calls. */
        val defaultClient = clientBuilder().build()!!

        //===================//
        // SOCKS connections //
        //===================//

        // Most of the stuff in here is just so we can replace the remoteAddress
        // in the connection with an unresolved address when making a SOCKS
        // client. This allows DNS resolution to happen on the other side of the
        // proxy, which is important for things like Tor.

        private const val SOCKS_ADDRESS_ATTRIBUTE = "socks.address"

        fun createSocksSocket(context: HttpContext): Socket {
            val socksaddr = context.getAttribute(SOCKS_ADDRESS_ATTRIBUTE) as InetSocketAddress
            return Socket(Proxy(Proxy.Type.SOCKS, socksaddr))
        }

        /**
         * For a resolved address that was derived from a hostname and port,
         * produce a new address that is "unresolved" (uses the hostname
         * directly again) and retains the same port.
         */
        fun deresolve(remoteAddress: InetSocketAddress, host: HttpHost): InetSocketAddress {
            return InetSocketAddress.createUnresolved(host.hostName, remoteAddress.port)
        }

        /**
         * Plain connections that defer name resolution of the remote address.
         */
        object SocksConnectionSocketFactory: PlainConnectionSocketFactory() {
            override fun createSocket(context: HttpContext): Socket = createSocksSocket(context)

            override fun connectSocket(connectTimeout: Int, socket: Socket?, host: HttpHost, remoteAddress: InetSocketAddress, localAddress: InetSocketAddress?, context: HttpContext): Socket {
                return super.connectSocket(connectTimeout, socket, host, deresolve(remoteAddress, host), localAddress, context)
            }
        }

        /**
         * TLS connections that defer name resolution of the remote address.
         */
        class SocksSSLConnectionSocketFactory(sslContext: SSLContext): SSLConnectionSocketFactory(sslContext) {
            override fun createSocket(context: HttpContext): Socket = createSocksSocket(context)

            override fun connectSocket(connectTimeout: Int, socket: Socket?, host: HttpHost, remoteAddress: InetSocketAddress, localAddress: InetSocketAddress?, context: HttpContext): Socket {
                return super.connectSocket(connectTimeout, socket, host, deresolve(remoteAddress, host), localAddress, context)
            }
        }

        private val socksRegistry = RegistryBuilder.create<ConnectionSocketFactory>()
            .register("http", SocksConnectionSocketFactory)
            .register("https", SocksSSLConnectionSocketFactory(SSLContexts.createSystemDefault()))
            .build()

        private val socksConnectionManager = PoolingHttpClientConnectionManager(
            socksRegistry,
            // Return a fake DNS record which we will later ignore in [deresolve].
            // This way we can use hostnames that won't resolve on this side.
            DnsResolver { arrayOf(InetAddress.getByAddress(byteArrayOf(0, 0, 0, 0))) }
        )

        /** HTTP client for SOCKS calls, requiring use of [makeSocksContext]. */
        val socksClient = clientBuilder()
            .setConnectionManager(socksConnectionManager)
            .build()!!

        /** Make a context object for SOCKS requests. */
        fun makeSocksContext(socksProxy: InetSocketAddress): HttpClientContext {
            return HttpClientContext.create().apply {
                setAttribute(SOCKS_ADDRESS_ATTRIBUTE, socksProxy)
            }
        }

        // === //
    }
}
