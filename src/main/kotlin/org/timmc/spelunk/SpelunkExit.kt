package org.timmc.spelunk

class SpelunkExit(override val message: String): RuntimeException(message) {
    companion object {
        fun from(t: Throwable): SpelunkExit {
            return SpelunkExit("${t.javaClass.name}: ${t.message}")
        }
    }
}
