package org.timmc.spelunk.cmd

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands

val MaintenanceCmd = object : CliktCommand(
    name = "maint",
    help = "Organizes various maintenance subcommands"
) {
    override fun run() {
        // no-op
    }
}.subcommands(RotatePostShuffleKeyCmd())


class RotatePostShuffleKeyCmd : RepoCommandBase(
    name = "rotate-post-id-secret",
    help = """
        Regenerate the secret that protects post ID metadata.
        
        Post IDs are protected in a way that prevents readers from knowing how
        many posts you have written and whether there are any which they cannot
        see. This relies on a secret stored in a config file. If the secret is
        exposed, this protection is removed for anyone who possesses the secret.

        Most people will not feel the need for this protection, and it is
        unlikely for this secret to be exposed. However, if this secret has
        somehow been compromised, this command can be run to generate a new
        secret.

        Regenerating the secret ensures that the IDs of any new posts are
        protected. However, all readers will be able to see that a rotation has
        happened.
        """
) {
    override fun runInRepo() {
        state.postIdShuffleKeyRotate()
    }
}
