package org.timmc.spelunk.cmd

import org.timmc.spelunk.model.JournalPost
import kotlin.concurrent.write

class CreateCmd : RepoCommandBase(
    name = "create",
    help = "Make a new post"
) {
    override fun runInRepo() {
        val cfg = state.configRead()

        val post = JournalPost.makeBlankPost(cfg)
        val postDir = state.repo.journal.newPost()
        state.postLocker(postDir.id).write {
            postDir.initPost(post)
        }

        println("Post file: ${postDir.post.path}")
        println("When you're done, change its status to 'publish' and use the push tool")
    }
}
