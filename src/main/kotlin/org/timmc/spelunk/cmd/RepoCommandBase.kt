package org.timmc.spelunk.cmd

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.check
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.help
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.path
import org.timmc.spelunk.SpelunkExit
import org.timmc.spelunk.State
import org.timmc.spelunk.State.Companion.RepoUsability
import org.timmc.spelunk.model.ProfilesFinder
import org.timmc.spelunk.wrap
import java.nio.file.Files
import java.nio.file.Path

/**
 * Base class for commands that need to run in the context of a repository.
 */
abstract class RepoCommandBase(name: String, help: String): CliktCommand(name = name, help = help) {

    private val requestedRepoDir: Path? by option("--dir")
        .help("Path to an alternative Spelunk directory")
        .path(canBeFile = false, mustExist = true, mustBeReadable = true)
        .check { Files.isDirectory(it) }

    private val allowUpgrade: Boolean by option("--upgrade")
        .help("Upgrade repo if it uses an older format")
        .flag("--no-upgrade", default = false)


    /**
     * Base directory of Spelunk repository.
     */
    val baseDir: Path by lazy {
        requestedRepoDir
            ?: ProfilesFinder().readProfiles()?.getDefaultRepoPath()
            ?: throw SpelunkExit("No Spelunk repository configured")
    }

    /**
     * The Spelunk repository state, for use by subclasses.
     */
    lateinit var state: State

    /**
     * Perform repository existence/init checks and call [runInRepo] if OK
     */
    override fun run() {
        val (lock, initialState) = State.getWriteableWithLock(baseDir)
        lock.use {
            val baseAbsPath = baseDir.toAbsolutePath()
            val settingsAbsPath = initialState.repo.settings.path.toAbsolutePath()

            val usability = try {
                initialState.checkUsability()
            } catch (e: Exception) {
                throw SpelunkExit("Couldn't parse settings file in repository ($settingsAbsPath): ${e.message}")
            }

            // Perform repository checks, and maybe upgrade it.
            val stateToUse = when (usability) {
                is RepoUsability.Uninitialized -> {
                    throw SpelunkExit("This Spelunk repository has not been initialized: $baseAbsPath")
                }
                is RepoUsability.Unknown -> {
                    throw SpelunkExit("Spelunk repository is of unknown version ${usability.version}: $baseAbsPath")
                }
                is RepoUsability.UpgradeRequired -> {
                    if (allowUpgrade) {
                        // Produces new state value to use
                        State.repositoryFullUpgrade(initialState)
                    } else {
                        throw SpelunkExit("""
                            Spelunk repository at $baseAbsPath needs to be upgraded
                            from version ${usability.upgrader.version}; pass the
                            `--upgrade` flag to allow this.
                        """.trimIndent().wrap())
                    }
                }
                is RepoUsability.Usable -> {
                    initialState // good to go
                }
            }

            this.state = stateToUse
            runInRepo()
        }
    }

    /**
     * "Main method" for subclasses, to be run iff the repository looks valid.
     */
    abstract fun runInRepo()
}
