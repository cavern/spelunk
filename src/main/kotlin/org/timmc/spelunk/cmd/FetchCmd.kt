package org.timmc.spelunk.cmd

import org.timmc.spelunk.EFetch
import java.util.concurrent.ExecutionException

class FetchCmd: RepoCommandBase(
    name = "fetch",
    help = "Fetch new posts from contacts' journals"
) {
    override fun runInRepo() {
        try {
            state.fetching.blockingFetchAll()
        } catch(e: ExecutionException) {
            if (e.cause is EFetch) {
                System.err.println(e.cause!!.message)
            } else {
                e.cause!!.printStackTrace()
            }
        }
    }
}

