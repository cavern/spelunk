package org.timmc.spelunk.cmd

import com.github.ajalt.clikt.parameters.options.help
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.choice
import org.timmc.spelunk.ellipsize
import org.timmc.spelunk.model.JournalPost
import org.timmc.spelunk.model.OnePost
import org.timmc.spelunk.model.PostStatus
import kotlin.concurrent.read

class ListCmd : RepoCommandBase(
    name = "list",
    help = "List postsDir"
) {
    val statusFilter: PostStatus? by option("--status")
        .help("Only list posts with this status")
        .choice(*PostStatus.entries.map { it.name to it }.toTypedArray())

    override fun runInRepo() {
        val postsOrdered = state.repo.journal.posts.list().sortedBy { it.id.toInt() }
        for (postDir in postsOrdered) {
            try {
                state.postLocker(postDir.id).read {
                    if (postDir.isDeleted()) return@read
                    val postMeta = postDir.post.readJson()
                    // Filter on status, maybe
                    if (statusFilter == null || postMeta.status == statusFilter)
                        println(printableLine(postDir, postMeta, 79))
                }
            } catch (e: Exception) {
                System.err.println("Malformed post ${postDir.path}: $e")
            }
        }
    }

    companion object {
        fun printableLine(postDir: OnePost, postMeta: JournalPost, byteBudget: Int): String {
            val id = postDir.id
            val drafterisk = if (postMeta.status != PostStatus.PUBLISHED) "*" else " "
            return "$drafterisk$id: ${previewRagged(postMeta)}".ellipsize(byteBudget)
        }

        /**
         * Create a preview line of unbounded length from a post, in the
         * form `subject: body text`.
         */
        fun previewRagged(postMeta: JournalPost): String {
            val subjectPart = if (postMeta.subject.isBlank())
                "[no subject]"
            else
                squishText(postMeta.subject)

            val bodyStartRaw: String = postMeta.body.take(4000)
            val bodyPart = squishText(bodyStartRaw)

            return "$subjectPart: $bodyPart"
        }

        val nonPrinting = Regex("""[^\p{Graph}]+""")
        /** Turn all whitespace into single spaces, and trim from ends. */
        fun squishText(text: String): String {
            return nonPrinting.replace(text, " ").trim()
        }
    }
}
