package org.timmc.spelunk.cmd

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.check
import com.github.ajalt.clikt.parameters.options.help
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.path
import javafx.application.Application
import org.timmc.spelunk.Utils
import org.timmc.spelunk.gui.SpelunkApp
import java.nio.file.Files
import java.nio.file.Path

class GuiCmd: CliktCommand(
    name = "gui",
    help = "Launch the graphical interface with additional options"
) {
    private val requestedRepoDir: Path? by option("--dir")
        .help("Start in this Spelunk directory rather than the default")
        .path(canBeFile = false)
        .check { Files.isDirectory(it) || Files.isDirectory(it.parent) }


    override fun run() {
        // This is stupid, but it allows passing of arguments to a JavaFX app;
        // JavaFX wants to control the startup process and expects the app to
        // parse any CLI arguments, rather than having something external do
        // the parsing and *then* decide to launch the GUI app.
        val options = GuiOptions(requestedRepoDir = requestedRepoDir?.toString())
        val optionsJson = Utils.json.adapter(GuiOptions::class.java).toJson(options)
        Application.launch(SpelunkApp::class.java, optionsJson)
    }
}

data class GuiOptions(
    val requestedRepoDir: String?
)
