package org.timmc.spelunk.cmd

class PushCmd: RepoCommandBase(
    name = "push",
    help = "Push updated posts out so people can see them"
) {
    override fun runInRepo() {
        state.publishing.runInitialSync()
    }
}

