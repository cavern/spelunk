package org.timmc.spelunk

import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.attribute.PosixFilePermission
import java.nio.file.attribute.PosixFilePermissions


/**
 * Abstraction layer for OS-specific configuration and operations.
 */
interface OS {
    /**
     * Determine the current JVM's process ID, or null if cannot.
     *
     * Shim for `ProcessHandle.current().pid()` which exists in JDK 9+.
     */
    fun processId(): String

    /**
     * Return true if the process with the given process ID exists.
     *
     * Shim for `ProcessHandle.of(pid).map { it.isAlive }.orElse(false)`
     * which is possible in JDK 9+.
     */
    fun processExists(pid: String): Boolean

    /**
     * Directory where Spelunk can store its pointer to the documents
     * directory.
     */
    fun indirectionConfigDir(): Path

    /**
     * Guess a location where I will likely be happy to have my first journal
     * located. This *should* be a location that does not already exist, or at
     * least is an empty directory, but is not guaranteed to. May return
     * different results for different users, even on the same OS.
     */
    fun guessDesirableJournalLocation(): Path

    /**
     * Given a filename that should be generically safe, further clean it up
     * so that it's safe for this OS in particular as well. May return empty
     * string, in which case a generated name will be used.
     *
     * Any transformations on this name should not work against the stripping
     * done in [storageFilename].
     */
    // For Windows, watch out for device names such as LPT and COM:
    // https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file
    // Also, I think check for magic UUIDs?
    fun cleanFileName(generallySafe: String): String

    /**
     * Create a file, making sure it is only readable (and writeable) by owner.
     *
     * Throws if file already exists.
     */
    fun makeOwnerOnlyFile(f: Path)

    /**
     * As UI guidance, give an example of an absolute path.
     */
    fun exampleAbsolutePath(): String

    /**
     * Open a file manager to show a directory.
     */
    fun showFileManager(dir: Path)

    //== Keyboard shortcuts ==//

    /** Key press indicates desire to switch to next tab? */
    fun keypressIsTabSwitchForward(event: KeyEvent): Boolean

    /** Key press indicates desire to switch to previous tab? */
    fun keypressIsTabSwitchBackward(event: KeyEvent): Boolean
}

/**
 * Linux, with Debian in mind. May or may not apply to MacOS, BSDs, or other
 * UNIX-like systems.
 */
object Linux: OS {
    override fun processId(): String {
        // I found that
        //
        // System.getProperty("sun.java.launcher.pid")
        //
        // didn't work on my Debian 10 Linux machine, and
        //
        // ManagementFactory.getRuntimeMXBean().name.split("@", limit=2)[0]
        //
        // was just too horrible and brittle to try (undocumented and subject to
        // change) even though supposedly it is "cross-platform" and did give
        // the same result as canonicalizing /proc/self.
        //
        // Reading from /proc/self should work in non-bizarre situations.
        return File("/proc/self").canonicalFile.name
    }

    override fun processExists(pid: String): Boolean {
        if (!pid.matches(Regex("^[0-9]+$"))) {
            return false
        }
        return File("/proc/$pid").isDirectory
    }

    override fun indirectionConfigDir(): Path {
        // A nod to the XDG Base Directory Specification
        // https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
        // even though main data files are handled differently.
        // TODO: Conform to XDG Base Dir spec for cache, at least
        val xdgConfigHome = emptyToNull(System.getenv("XDG_CONFIG_HOME"))
            ?: (System.getenv("HOME") + "/.config")
        return Paths.get(xdgConfigHome, "spelunk")
    }

    override fun guessDesirableJournalLocation(): Path {
        // Find a subdirectory of $HOME that seems like a place where I might
        // store my journal, or default to $HOME itself
        val likelyBasesHome = Regex("docs|documents|personal", RegexOption.IGNORE_CASE)
        val home = Paths.get(System.getenv("HOME"))
        val mostLikelyBase = Files.list(home).filter { candidate ->
            Files.isDirectory(candidate)
            && Files.isWritable(candidate)
            && likelyBasesHome.matches(candidate.fileName.toString())
        }.toList().firstOrNull() ?: home
        // Suggest making a new directory inside it
        return mostLikelyBase.resolve("spelunk")
    }

    override fun cleanFileName(generallySafe: String): String {
        // Just clean up any really long filenames.
        return if (generallySafe.length > 250) {
            generallySafe.take(100) + "_TRUNCATED_" + generallySafe.takeLast(100)
        } else {
            generallySafe
        }
    }

    override fun makeOwnerOnlyFile(f: Path) {
        Files.createFile(f,
            PosixFilePermissions.asFileAttribute(
                setOf(PosixFilePermission.OWNER_READ, PosixFilePermission.OWNER_WRITE)))
    }

    override fun exampleAbsolutePath(): String {
        return "/home/alice/docs"
    }

    override fun showFileManager(dir: Path) {
        // As of 2019-11-28, the java.awt.Desktop API's browseFileDirectory is
        // unimplemented for both Windows 10 and Linux:
        // https://bugs.openjdk.java.net/browse/JDK-8233994
        Runtime.getRuntime().exec(arrayOf("xdg-open", dir.toString()))
    }

    fun isTabSwitch(event: KeyEvent): Boolean {
        return event.code == KeyCode.TAB && event.isControlDown
            && !event.isMetaDown && !event.isAltDown
    }

    override fun keypressIsTabSwitchForward(event: KeyEvent): Boolean {
        return isTabSwitch(event) && !event.isShiftDown
    }

    override fun keypressIsTabSwitchBackward(event: KeyEvent): Boolean {
        return isTabSwitch(event) && event.isShiftDown
    }
}

/**
 * Return input if not empty or null, else return null.
 */
fun emptyToNull(value: String?): String? {
    return if (value == "") null else value
}

/**
 * Determine what OS I'm running Spelunk on.
 */
fun getOS(): OS {
    // Hardcoded for now; later might be listed in a classpath resource.
    return Linux
}

/**
 * The OS I'm running Spelunk on.
 */
val os by lazy { getOS() }
