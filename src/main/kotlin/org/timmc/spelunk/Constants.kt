package org.timmc.spelunk

object Constants {
    /**
     * Spec string used for published journal's identity file and catalog files.
     */
    const val CAVERN_SPEC = "cavern0.3"

    /**
     * Spec strings for older versions of Cavern, no longer readable in this
     * version of Spelunk.
     */
    val UNSUPPORTED_CAVERN_SPECS = listOf("cavern0", "cavern0.1", "cavern0.2")

    /**
     * Name of Cavern directory inside the publish root.
     */
    const val CAVERN_DIR = "_cavern"

    /**
     * Post type indicating a journal entry.
     */
    const val POST_TYPE_JOURNAL = "journal"

    /**
     * MIME type for plain text.
     */
    const val MIME_PLAINTEXT = "text/plain"

    /**
     * Marker indicating that an index.html file in the publish base was
     * placed there by a Cavern app, and is therefore eligible for
     * replacement.
     */
    const val INDEX_FILE_MARKER = "<!-- cavern-app-managed -->"
}
