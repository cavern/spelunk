package org.timmc.spelunk.publish

import org.timmc.spelunk.model.LocalFilesConfig
import org.timmc.spelunk.model.Repo
import java.io.IOException
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import kotlin.io.path.deleteExisting
import kotlin.io.path.exists
import kotlin.io.path.inputStream
import kotlin.io.path.isRegularFile
import kotlin.io.path.notExists
import kotlin.io.path.outputStream

/**
 * Publisher implementation for a local filesystem destination.
 */
class PushLocalFiles(
    override val repo: Repo,
    cfg: LocalFilesConfig
): APusher<Unit>() {
    private val rootDir = Paths.get(cfg.rootDir)

    override fun description(): String {
        return "Local directory $rootDir"
    }

    override fun ensureDirectory(rel: Path) {
        Files.createDirectories(rootDir.resolve(rel))
    }

    override fun uploadDir(rel: Path) {
        val from = repo.cache.outbox.path.resolve(rel)
        val to = rootDir.resolve(rel)

        Files.createDirectories(to)
        for (fromFile in Files.list(from)) {
            if (!Files.isRegularFile(fromFile)) {
                throw IOException("Cannot copy unexpected file type at <$fromFile>")
            }
            Files.copy(
                fromFile, to.resolve(fromFile.fileName),
                StandardCopyOption.REPLACE_EXISTING, LinkOption.NOFOLLOW_LINKS
            )
        }
    }

    override fun cleanDir(rel: Path, uploaded: Unit) {
        val from = repo.cache.outbox.path.resolve(rel)
        val to = rootDir.resolve(rel)

        for (toFile in Files.list(to)) {
            if (!Files.exists(from.resolve(toFile.fileName), LinkOption.NOFOLLOW_LINKS)) {
                Files.deleteIfExists(toFile)
            }
        }
    }

    override fun pushOneFile(rel: Path) {
        Files.copy(
            repo.cache.outbox.path.resolve(rel),
            rootDir.resolve(rel),
            StandardCopyOption.REPLACE_EXISTING, LinkOption.NOFOLLOW_LINKS
        )
    }

    override fun <R> readRemoteFile(rel: Path, reader: (InputStream?) -> R): R {
        val p = rootDir.resolve(rel)
        return if (p.exists()) {
            p.inputStream()
        } else {
            null
        }.use(reader)
    }

    override fun writeRemoteFile(rel: Path, bytes: InputStream) {
        rootDir.resolve(rel).outputStream().use {
            bytes.copyTo(it)
        }
    }

    override fun deleteRemoteFile(rel: Path) {
        val p = rootDir.resolve(rel)
        if (p.notExists()) {
            return
        } else if (p.isRegularFile()) {
            p.deleteExisting()
        } else {
            throw IOException("Cannot delete dir/link/other")
        }
    }
}
