package org.timmc.spelunk.publish

import mu.KotlinLogging
import org.timmc.spelunk.model.Repo
import org.timmc.spelunk.model.RsyncConfig
import org.timmc.spelunk.slash
import java.io.IOException
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.exists
import kotlin.io.path.inputStream
import kotlin.io.path.outputStream

private val logger = KotlinLogging.logger {}

/**
 * Publisher implementation for an rsync destination.
 */
class PushRsync(
    override val repo: Repo,
    val cfg: RsyncConfig
): APusher<Unit>() {
    override fun description(): String {
        return "rsync ${cfg.user}@${cfg.server}:${cfg.remoteDir}"
    }

    override fun uploadDir(rel: Path) {
        rsyncRelative(rel, isDir=true, recursive=true, doDeletes=false)
    }

    override fun cleanDir(rel: Path, uploaded: Unit) {
        rsyncRelative(rel, isDir=true, recursive=true, doDeletes=true)
    }

    override fun pushOneFile(rel: Path) {
        rsyncRelative(rel, isDir=false, recursive=true, doDeletes=true)
    }

    override fun ensureDirectory(rel: Path) {
        rsyncRelative(rel, isDir=true, recursive=false, doDeletes=false)
    }

    /**
     * Perform an rsync between outbox and `remoteDir/`.
     */
    fun rsyncRelative(rel: Path, isDir: Boolean, recursive: Boolean, doDeletes: Boolean) {
        val srcAbs = repo.cache.outbox.path.resolve(rel).toString()

        val mainArgs = listOf("--archive", "--mkpath")
        // Nota bene: --archive must come before --no-recursive
        val noRecurseArgs = if (recursive) emptyList() else listOf("--no-recursive")
        val delArgs = if (doDeletes) listOf("--delete-after") else emptyList()

        val exitCode = rsyncGeneric(
            if (isDir) srcAbs.slash() else srcAbs,
            onServer(rel),
            mainArgs + noRecurseArgs + delArgs
        )
        if (exitCode != 0)
            throw IOException("Failed to push via rsync: Exit code $exitCode")
    }

    private fun onServer(destRel: Path): String {
        val userPortion = cfg.user?.let { "$it@" } ?: ""
        val serverPortion = userPortion + cfg.server
        return "$serverPortion:${cfg.remoteDir.slash()}$destRel"
    }

    private fun rsyncGeneric(src: String, dest: String, options: List<String>): Int {
        val coreArgs = listOf(
            // Suppress usual rsync noise, unless there's an error
            "--quiet",
            // Critical if a remote file path happens to contain a space
            "--protect-args",
        )
        val sshArgs = cfg.rsh?.let { listOf("--rsh", it) } ?: emptyList()

        val cmd = listOf("rsync") + coreArgs + options + sshArgs +
            listOf(src, dest)

        logger.info("RSync: ${cmd.joinToString(" ")}")
        val proc = ProcessBuilder(cmd).inheritIO().start()
        proc.waitFor()
        return proc.exitValue()
    }

    override fun <R> readRemoteFile(rel: Path, reader: (InputStream?) -> R): R {
        val peek = repo.cache.outbox.peek
        Files.deleteIfExists(peek.path)

        val exitCode = rsyncGeneric(
            onServer(rel),
            peek.path.toString(),
            emptyList(),
        )

        val ret = if (peek.path.exists()) {
            if (exitCode == 23) {
                // 23: "Partial transfer due to error"
                // If the file is there but we got an error, it's probably partially written -- fail
                throw IOException("Failed to fully read file via rsync (exit=$exitCode): $rel")
            } else {
                peek.path.inputStream()
            }
        } else {
            if (exitCode == 23) {
                // 23: "Partial transfer due to error"
                // This appears to be what we get if the remote file isn't there
                null
            } else {
                // Some other error -- fail
                throw IOException("Failed to read file via rsync (exit=$exitCode): $rel")
            }
        }.use(reader)

        Files.deleteIfExists(peek.path)
        return ret
    }

    override fun writeRemoteFile(rel: Path, bytes: InputStream) {
        val peek = repo.cache.outbox.peek
        Files.deleteIfExists(peek.path)

        peek.path.outputStream().use {
            bytes.copyTo(it)
        }

        val exitCode = rsyncGeneric(
            peek.path.toString(),
            onServer(rel),
            emptyList(),
        )
        Files.deleteIfExists(peek.path)

        if (exitCode != 0) {
            throw IOException("Failed to write file via rsync: $rel")
        }
    }

    override fun deleteRemoteFile(rel: Path) {
        val peek = repo.cache.outbox.peek
        Files.deleteIfExists(peek.path)

        // This is ridiculous, but it works...
        // Copy from remote to local, but ask rsync to delete the remote file
        // once this is done.
        val exitCode = rsyncGeneric(
            onServer(rel),
            peek.path.toString(),
            listOf("--remove-source-files"),
        )
        Files.deleteIfExists(peek.path) // didn't actually want it

        when (exitCode) {
            23 -> return // "partial transfer" -- probably didn't exist
            0 -> return // deleted remote
            else -> throw IOException("rsync failed to delete remote file, exit=$exitCode")
        }
    }
}
