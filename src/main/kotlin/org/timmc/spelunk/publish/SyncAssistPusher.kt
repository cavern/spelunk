package org.timmc.spelunk.publish

import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors

/**
 * Wrapper for pushers that cannot natively sync. Requires a few additional
 * methods, for deleting files, creating directories, and listing
 * directories.
 */
abstract class SyncAssistPusher: APusher<Set<String>>() {
    // Returns the list of files that were uploaded (without dir path)
    override fun uploadDir(rel: Path): Set<String> {
        val srcDir = repo.cache.outbox.path.resolve(rel)
        ensureDirectory(rel)
        val uploaded = Files.list(srcDir).map { f ->
            pushOneFile(rel.resolve(f.fileName))
            f.fileName.toString()
        }.collect(Collectors.toList())
        return uploaded.toSet()
    }

    // Delete anything in the relative `folderName` on the server that is not
    // in the `uploaded` list (which names the child files, without path.)
    override fun cleanDir(rel: Path, uploaded: Set<String>) {
        for (child in listDir(rel)) {
            if (!uploaded.contains(child)) {
                deleteRemoteFile(rel.resolve(child))
            }
        }
    }

    /**
     * List contents of directory relative to publish root.
     */
    abstract fun listDir(rel: Path): List<String>
}
