package org.timmc.spelunk.publish

import mu.KotlinLogging
import org.timmc.spelunk.model.AwsS3Config
import org.timmc.spelunk.model.Repo
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest
import software.amazon.awssdk.services.s3.model.GetObjectRequest
import software.amazon.awssdk.services.s3.model.ListObjectsRequest
import software.amazon.awssdk.services.s3.model.NoSuchKeyException
import software.amazon.awssdk.services.s3.model.PutObjectRequest
import java.io.InputStream
import java.nio.file.Path

private val logger = KotlinLogging.logger {}

/**
 * Publisher implementation for an Amazon S3 destination.
 */
class PushAwsS3(
    override val repo: Repo,
    val cfg: AwsS3Config
): SyncAssistPusher() {
    companion object {
        /**
         * Produce the S3 key prefix representing the publish root under the configured
         * prefix, but with no starting slash, and ending with a slash if
         * non-empty.
         */
        internal fun makeFilesKeyPrefix(cfg: AwsS3Config): String {
            return (cfg.keyPrefix.trimEnd('/') + '/')
                .trimStart('/')
        }
    }

    val client = S3Client.builder()
        .credentialsProvider(
            StaticCredentialsProvider.create(AwsBasicCredentials.create(cfg.iamKeyId, cfg.iamKeySecret))
        )
        .region(Region.of(cfg.region))
        .build()!!

    val filesKeyPrefix = makeFilesKeyPrefix(cfg)

    override fun description(): String {
        return "AWS S3 ${cfg.bucket}/${cfg.keyPrefix}"
    }

    override fun pushOneFile(rel: Path) {
        val fromFile = repo.cache.outbox.path.resolve(rel)
        val toKey = filesKeyPrefix + rel
        logger.info("S3 Put: ${cfg.bucket}/$toKey")
        client.putObject(
            PutObjectRequest.builder().bucket(cfg.bucket).key(toKey).build(),
            fromFile
        )
    }

    override fun deleteRemoteFile(rel: Path) {
        val delKey = filesKeyPrefix + rel
        logger.info("S3 Delete: ${cfg.bucket}/$delKey")
        client.deleteObject(
            DeleteObjectRequest.builder().bucket(cfg.bucket).key(delKey).build()
        )
    }

    override fun ensureDirectory(rel: Path) {
        // no-op -- directories aren't reified in S3
    }

    override fun listDir(rel: Path): List<String> {
        val listPrefix = "$filesKeyPrefix$rel/"
        val ret = mutableListOf<String>()
        var marker = listPrefix // start listing here -- alphabetical order
        while (true) {
            logger.info("S3 List: ${cfg.bucket} from '$marker'")
            val req = ListObjectsRequest.builder()
                .bucket(cfg.bucket)
                .prefix(listPrefix)
                .marker(marker)
                // Roll up anything under deeper "directories" and don't
                // return them (will be listed as "common prefixes").
                // S3 really fights against the directory tree model.
                .delimiter("/")
                .build()
            val resp = client.listObjects(req)

            ret += resp.contents().map {
                it.key().removePrefix(listPrefix)
            }

            if (resp.isTruncated) {
                marker = resp.nextMarker()
            } else {
                break
            }
        }
        logger.debug("S3 listing results: {}", ret)
        return ret.toList()
    }

    override fun <R> readRemoteFile(rel: Path, reader: (InputStream?) -> R): R {
        val readKey = filesKeyPrefix + rel
        return try {
            logger.info("S3 Get: ${cfg.bucket}/$readKey")
            client.getObjectAsBytes(
                GetObjectRequest.builder().bucket(cfg.bucket).key(readKey).build()
            ).asInputStream()
        } catch (e: NoSuchKeyException) {
            null
        }.use(reader)
    }

    override fun writeRemoteFile(rel: Path, bytes: InputStream) {
        val writeKey = filesKeyPrefix + rel
        logger.info("S3 Put: ${cfg.bucket}/$writeKey")
        client.putObject(
            PutObjectRequest.builder().bucket(cfg.bucket).key(writeKey).build(),
            // MIME type is required, though I'd rather not have to provide it here
            RequestBody.fromContentProvider({ bytes }, "application/octet-stream")
        )
    }
}
