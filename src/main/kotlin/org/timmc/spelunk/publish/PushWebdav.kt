package org.timmc.spelunk.publish

import org.timmc.spelunk.WebDav
import org.timmc.spelunk.model.Repo
import org.timmc.spelunk.model.WebDavConfig
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Path

/**
 * Publisher implementation for a WebDAV destination.
 */
class PushWebdav(
    override val repo: Repo,
    cfg: WebDavConfig,
    val webdav: WebDav = WebDav.make(cfg)
): SyncAssistPusher() {
    override fun description(): String {
        return "WebDAV ${webdav.server}${webdav.basePath}"
    }

    override fun ensureDirectory(rel: Path) {
        webdav.ensureDirectory(rel.toString())
    }

    override fun listDir(rel: Path): List<String> {
        return webdav.listFilesInDir(rel.toString()).map { it.name }
    }

    override fun pushOneFile(rel: Path) {
        Files.newInputStream(repo.cache.outbox.path.resolve(rel)).use {
            writeRemoteFile(rel, it)
        }
    }

    override fun deleteRemoteFile(rel: Path) {
        webdav.delete(rel.toString())
    }

    override fun <R> readRemoteFile(rel: Path, reader: (InputStream?) -> R): R {
        return webdav.readFile(rel.toString()).use(reader)
    }

    override fun writeRemoteFile(rel: Path, bytes: InputStream) {
        webdav.put(rel.toString(), bytes)
    }
}
