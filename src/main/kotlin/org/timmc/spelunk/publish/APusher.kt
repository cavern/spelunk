package org.timmc.spelunk.publish

import mu.KotlinLogging
import org.timmc.spelunk.Constants
import org.timmc.spelunk.Node
import org.timmc.spelunk.model.Repo
import java.io.InputStream
import java.nio.file.Path
import kotlin.io.path.relativeTo

private val logger = KotlinLogging.logger {}

/**
 * Partial implementation of publishing, with methods for implementations to
 * override.
 */
abstract class APusher<T> {
    abstract val repo: Repo

    /** Human-readable description of this publish method (how and to where) */
    abstract fun description(): String

    /**
     * Make the publish location look just like the outbox (ignoring any
     * unrecognized paths in the publish location's root).
     */
    fun push() {
        val out = repo.cache.outbox

        fun rel(outboxNode: Node): Path {
            return outboxNode.path.relativeTo(out.path)
        }

        ensureDirectory(rel(out.cavernRoot))

        // Upload attachments first, then posts, then catalogs. Afterwards,
        // perform deletions in reverse order. This provides the most consistent
        // view to a concurrent reader.

        val attachmentsUploaded = uploadDir(rel(out.cavernRoot.attachments))
        val postsUploaded = uploadDir(rel(out.cavernRoot.posts))
        val catalogsUploaded = uploadDir(rel(out.cavernRoot.catalogs))

        pushOneFile(rel(out.cavernRoot.identity))

        cleanDir(rel(out.cavernRoot.catalogs), catalogsUploaded)
        cleanDir(rel(out.cavernRoot.posts), postsUploaded)
        cleanDir(rel(out.cavernRoot.attachments), attachmentsUploaded)

        // Try to write the "this is a Cavern journal" page if possible, but
        // don't fail if it doesn't work; just best-effort here. Log a warning
        // at most.
        val shouldUploadIndexFile = try {
            val existing = readRemoteFile(rel(out.indexHtml)) {
                it?.readBytes()?.decodeToString()
            }

            if (existing == null) {
                logger.info("Writing new index.html file (did not exist)")
                true
            } else if (existing.contains(Constants.INDEX_FILE_MARKER)) {
                logger.info("Overwriting index.html file, since it is marked as overwriteable")
                true
            } else {
                logger.info("Leaving index.html file alone, as it is not marked as being overwriteable by Cavern apps")
                false
            }
        } catch (e: Exception) {
            logger.info("Warning: Could not read remote index.html to check if it should be updated")
            false
        }

        if (shouldUploadIndexFile) {
            try {
                pushOneFile(rel(out.indexHtml))
            } catch (e: Exception) {
                logger.info("Warning: Could not index.html file alone")
            }
        }
    }

    /**
     * Create a directory (possibly including parents) if it's not already there.
     */
    abstract fun ensureDirectory(rel: Path)

    /**
     * Upload the contents of an outbox directory to the matching directory
     * under the publish root on the server.
     *
     * Returns information that can be used for cleaning the directory
     * in the second pass, and will be passed to `cleanDir`.
     */
    abstract fun uploadDir(rel: Path): T

    /**
     * Delete anything in the relative `folderName` under the publish root on the server that
     * needs to be deleted after everything new or changed has been uploaded
     * (`uploaded` is the return value of `uploadDir`).
     */
    abstract fun cleanDir(rel: Path, uploaded: T)

    /**
     * Write a single file relative to the outbox.
     *
     * Assumes parent directories are already present.
     */
    abstract fun pushOneFile(rel: Path)

    /**
     * Read the specified remote relative path into [reader] and return whatever
     * reader returns.
     *
     * If the remote file does not exist, reader is called with null.
     */
    abstract fun <R> readRemoteFile(rel: Path, reader: (InputStream?) -> R): R

    /**
     * Write remote file, assuming parent directory already in place.
     */
    abstract fun writeRemoteFile(rel: Path, bytes: InputStream)

    /**
     * Delete remote file, if it exists.
     */
    abstract fun deleteRemoteFile(rel: Path)
}
