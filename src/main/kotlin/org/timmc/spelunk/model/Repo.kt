package org.timmc.spelunk.model

import org.timmc.spelunk.Constants
import org.timmc.spelunk.Directory
import org.timmc.spelunk.Leaf
import org.timmc.spelunk.ModelLeaf
import org.timmc.spelunk.Node
import org.timmc.spelunk.dir
import org.timmc.spelunk.leaf
import org.timmc.spelunk.leaves
import org.timmc.spelunk.model.cavern.CavPost
import org.timmc.spelunk.modelLeaves
import org.timmc.spelunk.os
import org.timmc.spelunk.subdirs
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

/**
 * Profiles config file, a.k.a. "where is my repo?"
 */
data class Profiles(
    /**
     * Which profile to use as the default, as an index into the list.
     */
    val defaultIndex: Int,
    /**
     * List of profiles.
     */
    val profiles: List<Profile>
) {
    /**
     * One profile.
     */
    data class Profile(
        /** Absolute path to repo directory. */
        val path: String
    )

    /**
     * Find the path to the default repo, or null if none are configured.
     *
     * Takes profile indicated by defaultIndex, or first if defaultIndex is
     * invalid.
     */
    fun getDefaultRepoPath(): Path? {
        val profile = profiles.getOrNull(defaultIndex) ?: profiles.firstOrNull()
        return profile?.let { Paths.get(it.path) }
    }

    /** Return true iff there is a default profile. */
    fun hasDefault(): Boolean {
        return profiles.getOrNull(defaultIndex) != null
    }

}

class ProfilesFinder(
    val altConfigDir: Path? = null
) {
    /**
     * Path to profiles config file.
     */
    fun configPath(): Path {
        val dir = altConfigDir ?: os.indirectionConfigDir()
        return dir.resolve("profiles.json")
    }

    /**
     * Read the profiles configuration, or null if does not exist.
     */
    fun readProfiles(): Profiles? {
        val path = configPath()
        if (!Files.exists(path)) {
            return null
        }
        return ModelLeaf(path, false, Profiles::class.java).readJson()
    }

    /**
     * Write or overwrite the profiles configuration.
     */
    fun writeProfiles(profiles: Profiles) {
        val path = configPath()
        Files.createDirectories(path.parent)
        return ModelLeaf(path, true, Profiles::class.java).writeJson(profiles)
    }

    /**
     * Add a new profile to the list. Will become default only if there
     * are no existing defaults.
     */
    fun addProfile(path: Path) {
        val newProfile = Profiles.Profile(
            path = path.toAbsolutePath().toString()
        )
        val oldProfiles = readProfiles()
        val newProfiles = if (oldProfiles == null) {
            Profiles(defaultIndex = 0, profiles = listOf(newProfile))
        } else {
            val newProfileList = oldProfiles.profiles.plus(newProfile)
            oldProfiles.copy(
                defaultIndex = if (oldProfiles.hasDefault())
                    oldProfiles.defaultIndex
                else
                    newProfileList.lastIndex,
                profiles = newProfileList
            )
        }
        writeProfiles(newProfiles)
    }
}

/**
 * A Spelunk repository's layout on disk, including both my own docs
 * and generated files.
 */
class Repo(
    /** The root path of the repository. */
    path: Path,
    /** Whether or not to permit writes or  filesystem modifications in the repository. */
    allowWrite: Boolean
) : Directory(path, allowWrite) {

    companion object {
        fun lockfileLocation(repoBase: Path): Path {
            return repoBase.resolve(".lock")
        }
    }

    /**
     * Database containing publishing configuration, keyring, address book, etc.
     */
    val settings by leaf("settings.sqlite3")
    /**
     * Binary data used as a master password when the user has not chosen one.
     *
     * This provides no additional security against a local adversary with
     * filesystem access, as by necessity it is stored in the clear on disk.
     *
     * However, it is in a separate file from the database where the encrypted
     * keys are stored, in order to reduce the damage if the settings DB is
     * shared or leaked. Additionally, the choice of binary data *slightly*
     * reduces the chances of opening and pasting it by accident or the data
     * being obvious in a disk image.
     */
    val privateKeyPassbytes by leaf("private-key-pass")
    val journal by dir("journal", JournalDir::class.java)
    val cache by dir("cache", CacheDir::class.java)

    /** The collection of nodes that must be present for a valid repo. */
    fun requiredParts(): List<Node> {
        return listOf(settings)
    }

    /** The collection of all nodes that a repo knows about. */
    fun understoodParts(): List<Node> {
        // TODO Kind of fragile to hardcode these; can I use reflection instead?
        return requiredParts() + listOf(
            privateKeyPassbytes, journal, cache
        )
    }

    /**
     * Is the directory at least partially initialized? (Not blank from
     * Spelunk's perspective.)
     */
    fun isInitPartial(): Boolean {
        return understoodParts().any { Files.exists(it.path) }
    }

    /**
     * Is the repo fully initialized? (Has all expected files.)
     */
    fun isInitComplete(): Boolean {
        return requiredParts().all { Files.exists(it.path) }
    }
}

class JournalDir(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    val posts by subdirs(Regex("[0-9]+"), OnePost::class.java)

    /** Create a new post directory with the next ID. */
    fun newPost(): OnePost {
        requireWriteable("create new post")

        return synchronized(postIdLock) {
            val existingIds = posts.list().map {
                it.name.toLong()
            }
            val maxId: Long? = existingIds.maxOrNull()
            val newId = if (maxId == null) 0 else maxId + 1

            val postDir = posts.lookup("$newId")
            try {
                Files.createDirectory(postDir.path)
            } catch (e: FileAlreadyExistsException) {
                throw RuntimeException("Could not create a new post without collision because of concurrent access")
            }

            postDir.attachments.ensureDir()
            postDir
        }
    }

    companion object {
        /** Used to ensure that next-post-ID generation is collision-free. */
        private val postIdLock = Object()
    }
}

/**
 * Each post has a JSON document and an attachments directory.
 */
class OnePost(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    val post by leaf("post.json", JournalPost::class.java)
    /**
     * If this file is present, pretend this post directory doesn't exist.
     * This is a "tombstone" that allows the post's ID to be reserved even
     * after deletion.
     */
    val deletionMarker by leaf("DELETED")
    val attachments by dir("attach")

    /**
     * The sequential internal ID of this post.
     */
    val id: String = name

    init {
        id.toLong() // post IDs must be numeric
    }

    /**
     * Returns true if post has been deleted and should not be looked at further.
     * Should be called under a reader lock.
     */
    fun isDeleted(): Boolean = Files.exists(deletionMarker.path)

    /**
     * Initialize with the given post data.
     * Should be called under a writer lock.
     */
    fun initPost(data: JournalPost) {
        post.writeJson(data)
    }
}

/**
 * Cache directory. Contents may be deleted when Spelunk is quiescent
 * at the expense of having to perform additional computation and network
 * operations in the future, and with the risk of losing posts and files
 * that have since been deleted from contacts' journals (or that are from
 * journals that are no longer published or accessible.)
 */
class CacheDir(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    /**
     * Outbox: Files prepped for publishing to server.
     */
    val outbox by dir("out", OutboxDir::class.java)
    /**
     * Inboxes, one per contact.
     */
    val inboxes by dir("downloaded", InboxesDir::class.java)
    /**
     * Database recording the publish status of my journal, for outbound sync.
     */
    val publishDB by leaf("publishing.sqlite3")
    /**
     * Database recording the fetch status of my contacts' journals.
     */
    val fetchDB by leaf("download.sqlite3")
}

/**
 * The cache directory containing all publishable files.
 */
class OutboxDir(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    val cavernRoot by dir(Constants.CAVERN_DIR, EncryptedJournal::class.java)

    /** Used for temporarily downloading an arbitrary file. */
    val peek by leaf("tmp-peek")

    val indexHtml by leaf("index.html")
}


/**
 * Directory representing an encrypted, published journal (either my own in
 * preparation for publishing, or one I have retrieved from a contact.)
 *
 * TODO: Clarify purpose -- is this actually used for both, or just publishing?
 */
class EncryptedJournal(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    val identity by leaf(IDENTITY_FILE_NAME)
    val catalogs by dir(CATALOGS_DIR_NAME, CavCatalogsDir::class.java)
    val posts by dir(POSTS_DIR_NAME, CavPostsDir::class.java)
    val attachments by dir(ATTACHMENTS_DIR_NAME, CavAttachmentsDir::class.java)

    companion object {
        const val IDENTITY_FILE_NAME = "identity.mpk"
        const val CATALOGS_DIR_NAME = "catalogs"
        const val POSTS_DIR_NAME = "posts"
        const val ATTACHMENTS_DIR_NAME = "attachments"

        val allFileNames = setOf(IDENTITY_FILE_NAME,
            CATALOGS_DIR_NAME, POSTS_DIR_NAME, ATTACHMENTS_DIR_NAME)
    }
}

class CavCatalogsDir(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    private val listing by leaves(fileNameMatcher, "catalog data")

    fun byHiddenId(hiddenId: String): Leaf {
        return listing.lookup(hiddenIdToFileName(hiddenId))
    }

    companion object {
        val fileNameMatcher = Regex("""c_[0-9a-z]+\.bin""")
        fun hiddenIdToFileName(hiddenId: String) = "c_$hiddenId.bin"
    }
}

class CavPostsDir(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    private val listing by leaves(fileNameMatcher, "post data")

    fun byHiddenId(hiddenId: String): Leaf {
        return listing.lookup(hiddenIdToFileName(hiddenId))
    }

    companion object {
        val fileNameMatcher = Regex("""p_[0-9a-z]+\.bin""")
        fun hiddenIdToFileName(hiddenId: String) = "p_$hiddenId.bin"
    }
}

class CavAttachmentsDir(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    private val listing by leaves(fileNameMatcher, "attachment data")

    fun byHiddenId(hiddenId: String): Leaf {
        return listing.lookup(hiddenIdToFileName(hiddenId))
    }

    companion object {
        val fileNameMatcher = Regex("""a_[0-9a-z]+\.bin""")
        fun hiddenIdToFileName(hiddenId: String) = "a_$hiddenId.bin"
    }
}

/**
 * The cache directory containing all contact inboxes.
 *
 * Inboxes are named by contacts' internal sequential IDs.
 */
class InboxesDir(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    internal val listing by subdirs(inboxNameMatcher, DecryptedJournal::class.java)

    fun byContactId(id: Long): DecryptedJournal {
        return listing.lookup(inboxName(id))
    }

    companion object {
        val inboxNameMatcher = Regex("""contact_[0-9]+""")
        fun inboxName(contactId: Long) = "contact_$contactId"
    }
}

/**
 * The decrypted journal of a contact.
 */
class DecryptedJournal(path: Path, allowWrite: Boolean): Directory(path, allowWrite) {
    private val posts by modelLeaves(postFileNameMatcher, CavPost::class.java)
    private val attachmentEnclosures by subdirs(enclosureDirNameMatcher, ContactAttachmentEnclosure::class.java)

    fun postFile(hiddenId: String): ModelLeaf<CavPost> {
        return posts.lookup(postFileName(hiddenId))
    }

    /**
     * Retrieve an attachment file's enclosure Path by a combination of its hiddenId and the storage
     * file name I picked for it based on its advertised name.
     */
    fun attachmentEnclosure(hiddenId: String, storageFileName: String): Path {
        return attachmentEnclosures.lookup(enclosureDirName(hiddenId)).path.resolve(storageFileName)
    }

    companion object {
        val postFileNameMatcher = Regex("""post_[0-9a-z]+.json""")
        val enclosureDirNameMatcher = Regex("""enclosure_[0-9a-z]+""")

        fun postFileName(hiddenId: String) = "post_$hiddenId.json"
        fun enclosureDirName(hiddenId: String) = "enclosure_$hiddenId"
    }
}

/**
 * Each decrypted attachment is held in its very own directory so that it can
 * retain as close as possible a filename to what was advertised in the post
 * without worrying about collisions.
 */
class ContactAttachmentEnclosure(path: Path, allowWrite: Boolean): Directory(path, allowWrite)
