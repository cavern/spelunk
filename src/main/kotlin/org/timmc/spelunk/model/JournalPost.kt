package org.timmc.spelunk.model

import mu.KotlinLogging
import org.timmc.spelunk.DodgyFeistel
import org.timmc.spelunk.model.cavern.CavVisibility
import org.timmc.spelunk.wrap
import java.math.BigInteger
import java.util.Base64
import java.util.concurrent.atomic.AtomicBoolean

private val logger = KotlinLogging.logger {}

/**
 * Representation of a single post.
 */
data class JournalPost(
    /**
     * Permanent ID string for this post, unique within the journal.
     *
     * If empty, this post is not publishable.
     *
     * - Originally this was set to a v4 UUID
     * - After ADR 3, this is set to `N:XXXX`, where N is the post shuffle key
     *   version in decimal and XXXX is the encrypted post ID (Feistel-encrypted
     *   with the post shuffle key, then base64-encoded)
     */
    val persistentId: String,
    /**
     * Subject line of post. Should be single line; all whitespace runs
     * (including but not limited to newlines) may be replaced by single
     * spaces when displayed, at implementation's discretion.
     */
    val subject: String,
    val body: String,
    val tags: List<String> = emptyList(),
    val status: PostStatus,
    /**
     * Timestamp of first publish as seconds since Unix epoch, or last modified
     * date if not yet published.
     */
    val publishDate: Long,
    /** Timestamp of last modification as seconds since Unix epoch. */
    val updatedDate: Long,
    /**
     * Indicates the type of filtering in place for this post.
     */
    val visibility: Visibility,
    /**
     * Attachments to the post.
     *
     * It is possible that there could be multiple entries here with the same
     * filename, although this *shouldn't* ever happen.
     */
    val attachments: List<JournalPostAttachment>
) {
    fun isPublishable(): Boolean {
        if (status != PostStatus.PUBLISHED) {
            return false
        }

        if (persistentId.isEmpty()) {
            if (warnPublishEmptyId.compareAndSet(false, true)) {
                logger.warn("Journal entry found with empty ID but marked publishable (one-time warning)")
            }
            return false
        }

        return true
    }

    /**
     * One of the files attached to a post.
     *
     * If there's a file in the directory but it isn't explicitly listed here,
     * it must not be included during publishing.
     */
    data class JournalPostAttachment(
        /**
         * Filename within the post's attach directory.
         */
        val filename: String
    )

    companion object {
        private val warnPublishEmptyId = AtomicBoolean(false)

        /** Make a new empty post with default values. */
        fun makeBlankPost(cfg: Config): JournalPost {
            return makeBlankPost(cfg.defaultPostVisibility)
        }

        fun makeBlankPost(defaultPostVisibility: Visibility): JournalPost {
            val tsSec = System.currentTimeMillis() / 1000
            return JournalPost(
                persistentId = "", // empty will be caught by isPublishable
                subject = "",
                body = "",
                status = PostStatus.DRAFT,
                publishDate = tsSec,
                updatedDate = tsSec,
                visibility = defaultPostVisibility,
                attachments = emptyList()
            )
        }

        /**
         * Convert an internal, sequential post ID to an external ID that does
         * not reveal number of posts or the presence of gaps.
         *
         * @param [version] Prefixed to the encrypted value as namespacing to
         *     allow for changes to the key or the encryption implementation.
         */
        internal fun encryptPostId(postId: Long, key: ByteArray, version: Int): String {
            // Base64 takes input in blocks of 3 bytes, so we want to use up the
            // entire space of 3-byte numbers before moving on to the 6-byte
            // numbers and so on. This gives the least information to an
            // attacker while still allowing some degree of optimization.
            // Realistically, no one is ever going to use more than 6 bytes, and
            // essentially no one will use more than 3, but let's do the math
            // anyhow. (In fact, 9 bytes won't work, as it overflows the
            // Long-based math in the Feistel implementation.)
            val blockSize = 3
            val bitsInBlock = blockSize * 8
            val base = BigInteger.TWO.pow(bitsInBlock)

            // How many blocks do we need? One at minimum, and another for
            // every power of base.
            var blocks = 1
            var remain = BigInteger.valueOf(postId)
            while (remain >= base) {
                remain /= base
                blocks++
            }

            val cipher = DodgyFeistel(
                key = key,
                totalBits = blocks * bitsInBlock
            )
            val encryptedId = cipher.encrypt(postId)
            val encryptedAsBytes = DodgyFeistel.longToBytes(
                data = encryptedId,
                nBytes = blocks * blockSize
            )
            val encryptedAsBase64 = Base64.getEncoder().encodeToString(encryptedAsBytes)
            return "$version:$encryptedAsBase64"
        }
    }
}

/**
 * The visibility level of a post or other piece of information.
 */
enum class Visibility(
    val shortName: String,
    val describeRecipients: String,
    val type: CavVisibility
) {
    JUST_ME("Just me",
        "For my eyes only",
        CavVisibility.FILTERED),
    QUAINTS("Quaints",
        "All of my friends and other quaints (acquaintances)",
        CavVisibility.KNOWN),
    QOAQ1("Quaints of quaints",
        """
        All of my quaints, and all of *their* friends and acquaintances as well
        (one degree of separation)
        """.trimIndent().wrap(),
        CavVisibility.BROAD),
    QOAQ2("Quaints of quaints of quaints",
        """
        All of my quaints, quaints of quaints, and even people 2 degrees of
        separation from me (some of our respective acquaintances know each other)
        """.trimIndent().wrap(),
        CavVisibility.BROAD),
    PUBLIC("Everyone",
        "Completely public; possibly a large number of people with no obvious social ties",
        CavVisibility.PUBLIC),

    // This enum is used in a DB; any changes may require a schema version bump
}

enum class PostStatus {
    // A draft is only visible locally, regardless of access filters
    DRAFT,
    // A published post can be seen by others, according to access filters
    // (once it has been synced out)
    PUBLISHED;
}
