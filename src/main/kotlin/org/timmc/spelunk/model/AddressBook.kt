package org.timmc.spelunk.model

import org.timmc.spelunk.DataPatch
import org.timmc.spelunk.MaybePatch
import org.timmc.spelunk.SodiumBoxPublicKey
import org.timmc.spelunk.SodiumSigningPublicKey

/**
 * List of contacts, both direct and otherwise.
 */
data class AddressBook(
    /** All contacts. */
    val contacts: List<Contact> = emptyList()
) {
    /** Given a visibility filter, return the set of all covered contacts. */
    fun visibilityToContacts(visibility: Visibility): Set<Contact> {
        return when (visibility) {
            // TODO: PUBLIC is really more than just all known contacts;
            // should this be handled differently?
            Visibility.PUBLIC -> contacts.toSet()
            Visibility.QOAQ2 -> contacts.filter { it.separation <= 2 }.toSet()
            Visibility.QOAQ1 -> contacts.filter { it.separation <= 1 }.toSet()
            Visibility.QUAINTS -> contacts.filter { it.separation == 0 }.toSet()
            Visibility.JUST_ME -> emptySet()
        }
    }
}

/**
 * One contact in the address book.
 *
 * This conflates several things:
 *
 * - A person
 * - A journal that I can fetch
 * - A recipient to whom I can encrypt things
 */
data class Contact(
    /**
     * A local, permanent, unique ID for this contact, since handle, keys, and
     * locations may all change over time.
     *
     * This is used as a foreign key reference in other tables.
     *
     * Use [Contact.PLACEHOLDER_ID] here if object hasn't been stored in the DB.
     */
    val id: Long,
    /**
     * Fetch URI trusted to represent this contact. Might be null, in which
     * case I've probably just added this person so they can read my journal,
     * not so I can read theirs.
     */
    val fetchUri: String?,
    /**
     * Contact's signature verification key -- their cryptographic identity.
     * Null if first fetch has not yet occurred (and has not been entered
     * manually).
     */
    val sigKey: SodiumSigningPublicKey?,
    /**
     * Public key this contact has advertised for Box operations. Null if first
     * fetch has not yet occurred.
     */
    val boxKey: SodiumBoxPublicKey?,
    /**
     * Degree of separation from me. `0`` is a direct contact/quaint; `1` is
     * a quaint-of-a-quaint. Likely will not store values higher than `2`.
     */
    val separation: Int,
    /** The handle this contact calls themselves by. */
    val handleReceived: String?,
    /** My own name for this contact, if specified. */
    val handleOverride: String?

    // TODO: Audit log for past changes to keys and locations, including
    // provenance and actor. "key $id added on $DATE to match known location
    // $loc; action taken automatically based on key gossip found on $Joe's
    // journal at $location and signed with $key" -- but structured.
) {
    /** How to display this contact's handle locally (take override into account.) */
    val localHandle: String? = handleOverride ?: handleReceived

    companion object {
        /**
         * Fake value for [id] to be used in objects which haven't been stored
         * in the DB.
         */
        const val PLACEHOLDER_ID: Long = -9999
    }
}

data class ContactPatch (
    val fetchUri: MaybePatch<String> = MaybePatch.Absent(),
    val sigKey: MaybePatch<SodiumSigningPublicKey?> = MaybePatch.Absent(),
    val boxKey: MaybePatch<SodiumBoxPublicKey?> = MaybePatch.Absent(),
    val separation: MaybePatch<Int> = MaybePatch.Absent(),
    val handleReceived: MaybePatch<String?> = MaybePatch.Absent(),
    val handleOverride: MaybePatch<String?> = MaybePatch.Absent()
) : DataPatch
