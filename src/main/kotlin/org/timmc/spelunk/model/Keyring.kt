package org.timmc.spelunk.model

import org.timmc.spelunk.SodiumBoxKeyPair
import org.timmc.spelunk.SodiumSigningKeyPair
import org.timmc.spelunk.model.cavern.CavCatalogPointer

/**
 * Keyring of my own cryptographic identity.
 */
data class Keyring(
    /**
     * My current keypair. This is the one other people should use when encrypting
     * files for me, and the one I'll use for signing.
     */
    val current: Keypair,
    /**
     * Other keys: Rotated, revoked, whatever. Not to be used for signing,
     * but can be used to decrypt others' journals.
     */
    val inactiveKeys: List<Keypair>
) {
    /**
     * Return current keypair followed by older keys, with the latter in
     * descending order by creation timestamp.
     */
    fun allKeysReverseChrono(): List<Keypair> {
        return listOf(current) +
                inactiveKeys.sortedByDescending { it.createdTsMs }
    }
}

/**
 * One of my own cryptographic identities, complete with private keys and
 * metadata.
 * TODO: Rename "Keyset" or "Key" or something -- it's now a pair of pairs
 */
data class Keypair(
    /**
     * Pointer for the catalog file I publish
     * for myself so I can read my own published journal.
     */
    val selfCatalogPointer: CavCatalogPointer,
    /** Libsodium public/private keypair for signature operations. */
    val sigKeys: SodiumSigningKeyPair,
    /** Libsodium public/private keypair for cryptobox operations. */
    val boxKeys: SodiumBoxKeyPair,
    /**
     * Whether this key is considered active. (May or may not actually connote
     * the current key, although only one key *should* be active at a time.)
     */
    val isActive: Boolean,
    /** Timestamp when key was created, in millis since Unix epoch. */
    val createdTsMs: Long,
    /** Description of key (free text) */
    val description: String
)
