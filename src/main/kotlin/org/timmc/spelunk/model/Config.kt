package org.timmc.spelunk.model

import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import org.timmc.spelunk.DataPatch
import org.timmc.spelunk.MaybePatch

/**
 * Root Spelunk configuration file.
 */
data class Config(
    /**
     * My own name.
     */
    val ownName: String?,
    /**
     * Default visibility level when drafting a post.
     * @see [JournalPost.visibility]
     */
    val defaultPostVisibility: Visibility,

    /**
     * Configuration for publishing, or null if not configured.
     */
    val publishing: PublishLocationConfig?,

    /**
     * SOCKS proxy address for Tor connections, as host:port.
     *
     * TODO: Determine format for IPv6 host -- square-bracketed?
     */
    val torSocksProxy: String?,
)

/** Changes that can be made to a Config. */
data class ConfigPatch(
    val ownName: MaybePatch<String?> = MaybePatch.Absent(),
    val defaultPostVisibility: MaybePatch<Visibility> = MaybePatch.Absent(),
    val publishing: MaybePatch<PublishLocationConfig?> = MaybePatch.Absent(),
    val torSocksProxy: MaybePatch<String?> = MaybePatch.Absent(),
) : DataPatch

/**
 * Configuration for writing to a location.
 */
sealed class PublishLocationConfig(
    /**
     * The type field is used to determine which publish method to use, but
     * also for which config model to parse it as.
     */
    val type: String,

    /**
     * The URL of the published journal that will be advertised to all readers
     * and associated with your public key. This is the canonical,
     * probably-public URL, which may differ from URLs that are used by the
     * author's app when updating the journal.
     *
     * If not set, journal may not be publishable.
     * (TODO: Define in protocol how a journal without a canonical location is represented.)
     */
    open val journalUrl: String?,
) {
    companion object {
        val moshiAdapter = PolymorphicJsonAdapterFactory.of(PublishLocationConfig::class.java, "type")
            .withSubtype(WebDavConfig::class.java, "webdav")
            .withSubtype(AwsS3Config::class.java, "aws-s3")
            .withSubtype(RsyncConfig::class.java, "rsync")
            .withSubtype(LocalFilesConfig::class.java, "file")!!
    }
}

/**
 * Configuration for talking to a WebDAV server.
 */
data class WebDavConfig(
    override val journalUrl: String?,
    /**
     * Server URL. Scheme, hostname, optional port, optional path.
     */
    val url: String,
    /**
     * Username for authenticating to the server
     */
    val username: String,
    /**
     * Password for authenticating to the server
     */
    val password: String,
): PublishLocationConfig("webdav", journalUrl)

/**
 * Configuration for writing to Amazon S3.
 */
data class AwsS3Config(
    override val journalUrl: String?,
    /**
     * Name of the bucket.
     */
    val bucket: String,
    /**
     * Prefix under which journal is published. This is like the directory path
     * under which the files will live, although S3 natively manages objects
     * with opaque keys, not as files in a filesystem.
     *
     * If this does not end with a slash, a slash will be appended before use.
     * An empty value indicates the root of the bucket will be used.
     */
    val keyPrefix: String,
    /**
     * AWS region of the bucket, e.g. "us-east-1".
     */
    val region: String,
    /**
     * An AWS IAM access key ID with the ability to Get, Put, List, and Delete
     * objects under the [keyPrefix] in the [bucket].
     */
    val iamKeyId: String,
    /**
     * The AWS IAM secret access key that goes with the [iamKeyId].
     */
    val iamKeySecret: String
): PublishLocationConfig("aws-s3", journalUrl)

/**
 * Configuration for fetching/publishing via rsync.
 */
data class RsyncConfig(
    override val journalUrl: String?,
    /**
     * Server to rsync to (just the hostname or domain name).
     */
    val server: String,
    /**
     * Absolute path on remote server to sync with.
     */
    val remoteDir: String,
    /**
     * Username for SSH'ing into remote server, if different from local username.
     */
    val user: String? = null,
    /**
     * Command string for remote shell. If you need a nonstandard SSH port,
     * for example, this would be "ssh -p 8022"
     */
    val rsh: String? = null
): PublishLocationConfig("rsync", journalUrl)

/**
 * Configuration for publishing to a local directory.
 */
data class LocalFilesConfig(
    override val journalUrl: String?,
    val rootDir: String
): PublishLocationConfig("file", journalUrl) {
    // TODO Add validate method that fails if path not absolute
}
