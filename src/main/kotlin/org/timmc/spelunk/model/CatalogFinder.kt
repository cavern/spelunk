package org.timmc.spelunk.model

import org.timmc.spelunk.BytesEq
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.Publishing
import org.timmc.spelunk.SodiumBoxPublicKey
import org.timmc.spelunk.model.cavern.CavCatalogPointer

/**
 * A CatalogFinder document helps new quaints learn the secret locations of the
 * catalog files I upload for them.
 *
 * Each line is encrypted to one recipient's public key. When decrypted, it
 * reveals a pointer to a catalog. Their catalog file will be (more or less
 * permanently) located at that pointer, although it can change.
 *
 * This scheme ensures that an attacker cannot readily learn who I am talking
 * to even if they can see all the files on my server. It's bulky and slow, but
 * it usually only has to be done once per quaint, the first time I read from
 * their server.
 */
class CatalogFinder {

    // TODO: Each line should decrypt to a *list* of pointers, since my address
    // book might contain multiple records with the same public key. This could
    // happen when someone maintains two journals and uses the same public key
    // for both, or if for some reason I just happen to have multiple records
    // for this person.
    //
    // The recipient should fetch all of the catalogs and merge them.
    //
    // Alternatively, I merge the catalogs for them and use some kind of merged ID
    // to advertise it to them?
    //
    // ...more realistically, don't allow there to be multiple quaints with the
    // same public key, and change quaints to allow multiple locations?

    companion object {
        /**
         * Given a list of recipients, return encrypted CatalogFinder list.
         */
        fun encrypt(
            recipients: List<Publishing.Recipient>, myKeys: Keyring
        ): List<CatalogFinderEntry> {
            val myBoxPrivateKey = myKeys.current.boxKeys.privateKey
            return recipients.map { recip ->
                CatalogFinderEntry(Crypto.asymmetricEncrypt(
                    message = recip.catalogPointer.toMsgPack(),
                    recipPubKey = recip.boxPublicKey,
                    senderPrivKey = myBoxPrivateKey
                ))
            }
        }

        /**
         * Given a CatalogFinder list, try decrypting all the lines
         * until I can find a hiddenId. Return the first one I find, or
         * null if none found.
         */
        fun decrypt(
            entries: List<CatalogFinderEntry>,
            senderPubKey: SodiumBoxPublicKey, selfKeyring: Keyring,
        ): CavCatalogPointer? {
            val myKeysDesc = selfKeyring.allKeysReverseChrono()
            // Try lines × my private keys
            for (entry in entries) {
                try {
                    val decrypted = Crypto.asymmetricDecrypt(
                        encAndNonce = entry.sharedBytes,
                        recipPrivKeys = myKeysDesc.map { it.boxKeys.privateKey },
                        senderPubKey = senderPubKey
                    )
                    return CavCatalogPointer.fromMsgpack(decrypted)
                } catch (e: Exception) {
                    // just try next combination
                }
            }
            return null
        }
    }
}

/**
 * A byte array representing an encrypted catalog_finder entry.
 */
class CatalogFinderEntry(bytes: ByteArray): BytesEq(bytes)
