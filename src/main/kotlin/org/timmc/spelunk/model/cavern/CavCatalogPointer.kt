package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.SodiumSecretStreamKey
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.MapContext
import org.timmc.spelunk.MsgPackable
import org.timmc.spelunk.MsgUnpackable

/**
 * Pointer to a published catalog file, indicating how to find it and how to read it.
 *
 * This pointer type is only used for catalogs, which are signed rather than
 * hashed. (Unlike [CavDataPointer], it lacks a hash field.)
 */
data class CavCatalogPointer(
    /**
     * Target catalog's hidden ID (used in constructing path).
     *
     * This should be relatively stable over time, even as the catalog file it
     * points to is updated. (This is different than from data pointers.)
     */
    val hiddenId: String,
    /**
     * A symmetric key for decrypting the target file.
     */
    val symmetricKey: SodiumSecretStreamKey,
): MsgPackable {
    override fun serialize(): Any {
        return mapOf(
            "hid" to hiddenId,
            "key" to symmetricKey.sharedBytes,
        )
    }

    companion object: MsgUnpackable<CavCatalogPointer> {
        override fun deserialize(ctx: MapContext): CavCatalogPointer {
            return CavCatalogPointer(
                hiddenId = ctx.getAs<String>("hid"),
                symmetricKey = SodiumSecretStreamKey(ctx.getAs<ByteArray>("key")),
            )
        }

        /**
         * Make a fresh, random catalog pointer.
         */
        fun new(): CavCatalogPointer {
            return CavCatalogPointer(
                hiddenId = Crypto.hiddenId(),
                symmetricKey = Crypto.symmetricStreamKey(),
            )
        }
    }
}
