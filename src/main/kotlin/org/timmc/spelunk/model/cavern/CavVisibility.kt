package org.timmc.spelunk.model.cavern

/**
 * External description of the visibility of a post or comment.
 */
enum class CavVisibility(val code: String, val whoDescription: String) {
    PUBLIC(code = "public",
        whoDescription = "Everyone in the world"),
    BROAD(code = "broad",
        whoDescription = "The author's larger social circles"),
    KNOWN(code = "known",
        whoDescription = "The author's friends and other quaints"),
    FILTERED(code = "filtered",
        whoDescription = "Some of the author's quaints, possibly very few"),
}
