package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.SodiumBoxPublicKey
import org.timmc.spelunk.SodiumSigningPublicKey

/**
 * A Cavern user's composite public key -- the several pieces of their
 * cryptographic identity.
 */
data class CavernPublicKey(
    /** Key for verifying user's signature -- primary identity key. */
    val sigPublicKey: SodiumSigningPublicKey,
    /** Key to which messages may be encrypted for this user. */
    val boxPublicKey: SodiumBoxPublicKey,
)
