package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.Constants
import org.timmc.spelunk.MapContext
import org.timmc.spelunk.MsgPackable
import org.timmc.spelunk.MsgUnpackable
import org.timmc.spelunk.SodiumBoxKeyPair
import org.timmc.spelunk.SodiumBoxPublicKey

/**
 * The contents of a catalog file, an outer layer that just specifies the spec
 * version in effect and the authenticated data.
 */
data class CavCatalogWrapper(
    /** Spec version, same as [CavIdentity.spec]. */
    val spec: String,
    /**
     * Authenticated wrapper for actual catalog data.
     *
     * Use [unwrapCatalog] to access this.
     */
    val catalog: CavMultiMac<CavCatalog>,
): MsgPackable {
    /**
     * Unpack the catalog data after verifying the authentication tags using the
     * given keys.
     */
    fun unwrapCatalog(
        senderKey: SodiumBoxPublicKey, readerKeyPairs: List<SodiumBoxKeyPair>,
    ): CavCatalog {
        // Return value includes the verifying public key, but we don't have a
        // use for it yet beyond testing. However, it will later become useful
        // in determining when other users have picked up a changed public key.
        return catalog.authenticate(
            senderKey = senderKey, readerKeyPairs = readerKeyPairs,
            topic = TOPIC_CATALOG_INTEGRITY, unpacker = CavCatalog::fromMsgpack,
        ).first
    }

    override fun serialize(): Any {
        return mapOf(
            "spec" to spec,
            "catalog" to catalog.serialize(),
        )
    }

    companion object: MsgUnpackable<CavCatalogWrapper> {
        override fun deserialize(ctx: MapContext): CavCatalogWrapper {
            return CavCatalogWrapper(
                spec = ctx.getAs<String>("spec"),
                catalog = CavMultiMac.unpackableFor(CavCatalog::class.java)
                    .deserialize(ctx.getMap("catalog")),
            )
        }

        /**
         * Create an authenticated [CavCatalogWrapper] around the specified catalog
         * using the given keys.
         */
        fun wrapCatalog(
            catalog: CavCatalog,
            senderKeys: SodiumBoxKeyPair,
            readerKeys: List<SodiumBoxPublicKey>,
        ): CavCatalogWrapper {
            return CavCatalogWrapper(
                spec = Constants.CAVERN_SPEC,
                catalog = CavMultiMac.makeAuthenticatedData(
                    type = CavCatalog::class.java,
                    payload = catalog.toMsgPack(),
                    senderKeyPair = senderKeys,
                    readerKeys = readerKeys,
                    topic = TOPIC_CATALOG_INTEGRITY,
                )
            )
        }

        /**
         * Domain separation and purpose value to use in
         * [CavMultiMac.authenticate] when computing [catalog].
         *
         * This value is private because it should not be used directly;
         * instead, callers should use [wrapCatalog] and [unwrapCatalog].
         */
        private const val TOPIC_CATALOG_INTEGRITY = "cavern-catalog-integrity"
    }
}

/**
 * A catalog file presented to an app, representing a complete listing of
 * all posts available to particular identity.
 */
data class CavCatalog(
    /**
     * When this catalog was published, in seconds since UNIX epoch.
     */
    val publishTimestamp: Long,
    /**
     * A list of posts that can be retrieved, along with sufficient information
     * to fetch and decrypt them (and to decide whether to do so.)
     */
    val posts: List<PostOffer>,
): MsgPackable {
    override fun serialize(): Any {
        return mapOf(
            "publish_ts" to publishTimestamp,
            "posts" to posts.map { it.serialize() }
        )
    }

    companion object: MsgUnpackable<CavCatalog> {
        override fun deserialize(ctx: MapContext): CavCatalog {
            return CavCatalog(
                publishTimestamp = ctx.getAs<Long>("publish_ts"),
                posts = ctx.getList("posts").items().map {
                    PostOffer.deserialize(it.asMap())
                },
            )
        }
    }

    /**
     * Part of a catalog file, this tells how to fetch a post.
     */
    data class PostOffer(
        /** The stable ID of the post */
        val id: String,
        /** Pointer to a post file */
        val pointer: CavDataPointer
    ): MsgPackable {
        override fun serialize(): Any {
            return mapOf(
                "id" to id,
                "ptr" to pointer.serialize()
            )
        }
        companion object: MsgUnpackable<PostOffer> {
            override fun deserialize(ctx: MapContext): PostOffer {
                return PostOffer(
                    id = ctx.getAs<String>("id"),
                    pointer = CavDataPointer.deserialize(ctx.getMap("ptr")),
                )
            }
        }
    }
}
