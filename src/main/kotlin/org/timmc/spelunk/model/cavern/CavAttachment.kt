package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.MapContext
import org.timmc.spelunk.MsgPackable
import org.timmc.spelunk.MsgUnpackable

/**
 * An attachment's metadata, including how to find the actual file.
 *
 * Attachments are not necessarily in a many-to-one relationship with
 * posts; emoji images, for instance, may be attached to multiple posts.
 * Apps should consider a storage strategy that accounts for this (such
 * as the one used in publishing.)
 */
data class CavAttachment(
    /**
     * Filename requested for use in user interface. This is a base file
     * name, with no directory structure.
     *
     * May not be safe to use directly on disk (special characters such
     * as slashes and leading dots, non-unique names, empty, overlong names,
     * special names with meaning to the operating system) or when displaying
     * (right-to-left override characters, excessive whitespace runs that
     * hide the extension in the truncation, other spoofing issues).
     * Therefore, the app may need to maintain a stateful mapping of
     * attachment names to actual filenames on disk. The actual filenames
     * should bear a close resemblance to the displayed names so that they
     * are meaningful when opened in an external editor or viewer.
     *
     * Other files in the post may reference this file by this filename.
     */
    // TODO rename
    val clearFilename: String,
    /**
     * Size of file in bytes, advisory only. I might wish to avoid
     * downloading large files by default.
     */
    val allegedSize: Long,
    /**
     * MIME type of file, advisory only. I might
     * choose whether to download an attached file partly on the basis
     * of the reported file type.
     */
    val allegedMimeType: String?,
    /**
     * Pointer to attachment data.
     */
    val pointer: CavDataPointer,
): MsgPackable {
    override fun serialize(): Any {
        return mapOf(
            "clearFilename" to clearFilename,
            "allegedSize" to allegedSize,
            "allegedMimeType" to allegedMimeType,
            "pointer" to pointer.serialize(),
        ).filterValues { it != null }
    }

    companion object: MsgUnpackable<CavAttachment> {
        override fun deserialize(ctx: MapContext): CavAttachment {
            return CavAttachment(
                clearFilename = ctx.getAs<String>("clearFilename"),
                allegedSize = ctx.getAs<Long>("allegedSize"),
                allegedMimeType = ctx.getOptional("allegedMimeType")?.cast<String>(),
                pointer = CavDataPointer.deserialize(ctx.getMap("pointer")),
            )
        }
    }
}
