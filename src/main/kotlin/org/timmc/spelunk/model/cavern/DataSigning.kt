/**
 * Utilities for signing and verifying data in MsgPack format.
 *
 * The wrapper structure is specific to the Cavern protocol, which is why this
 * is not just part of the Crypto class.
 */
package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.BytesEq
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.Ed25519Signature
import org.timmc.spelunk.MapContext
import org.timmc.spelunk.MsgPackable
import org.timmc.spelunk.MsgUnpackable
import org.timmc.spelunk.SodiumSigningPrivateKey
import org.timmc.spelunk.SodiumSigningPublicKey
import org.timmc.spelunk.friendlyClassName

/**
 * Sign the payload according to an identity and a signing topic.
 * The result may be unpacked with [CavSignedData.verify].
 *
 * Using different topics for different types of messages prevents messages
 * intended for one use from being substituted in for another message.
 * The topic is implicit in context (the protocol, and the field the signed data
 * was presented in).
 */
fun <T: MsgPackable> signData(
    payload: T, topic: String, signingKey: SodiumSigningPrivateKey
): CavSignedData<T> {
    val payloadBytes = payload.toMsgPack()
    val signature = Crypto.signData(signableBytes(payloadBytes, topic), signingKey)
    return CavSignedData(signature, BytesEq(payloadBytes))
}

/**
 * Prepare a payload for signing or verification by combining it with a signing
 * topic.
 */
private fun signableBytes(payloadBytes: ByteArray, topic: String): ByteArray {
    return topic.encodeToByteArray() + byteArrayOf(0x00) + payloadBytes
}

/**
 * Signed Cavern data that has not yet been verified.
 *
 * Verifying the data with [verify] will also parse and unpack it.
 */
class CavSignedData<T: MsgPackable>(
    private val signature: Ed25519Signature,
    /** Private so that it can only be accessed via verification. */
    private val unverifiedPayload: BytesEq,
): MsgPackable {
    /**
     * Verify the signature and unpack the data.
     *
     * Fails if:
     *
     * - Signature not valid or not made with key matching the given public key
     * - Topic string in signed wrapper does not match the given one
     *
     * [payloadReader]: Mapper from bytes to [T] type.
     */
    fun verify(
        expectedTopic: String,
        verificationKey: SodiumSigningPublicKey,
        payloadReader: (ByteArray) -> T,
    ): T {
        val signable = signableBytes(unverifiedPayload.sharedBytes, expectedTopic)
        Crypto.verifySignature(signature, signable, verificationKey)
        val verifiedPayload = unverifiedPayload.sharedBytes // verified, now
        return payloadReader(verifiedPayload)
    }

    override fun serialize(): Any {
        return mapOf(
            "sig" to signature.sharedBytes,
            "pay" to unverifiedPayload.sharedBytes,
        )
    }

    // Because we keep the payload inaccessible until it is verified, we can't
    // make this a data class -- so we need to implement this trio by hand.

    override fun equals(other: Any?): Boolean {
        if (other !is CavSignedData<*>) return false;

        return (signature == other.signature) and (unverifiedPayload == other.unverifiedPayload)
    }

    override fun hashCode(): Int {
        return arrayOf(signature, unverifiedPayload).hashCode()
    }

    override fun toString(): String {
        return "${friendlyClassName(javaClass)}{sig=${signature.asHex()}}"
    }

    companion object {
        /** Build a CavSignedData unpacker for a given payload type. */
        fun <T: MsgPackable> unpackableFor(): MsgUnpackable<CavSignedData<T>> {
            return object : MsgUnpackable<CavSignedData<T>> {
                override fun deserialize(ctx: MapContext): CavSignedData<T> {
                    return CavSignedData(
                        signature = Ed25519Signature(ctx.getAs<ByteArray>("sig")),
                        unverifiedPayload = BytesEq(ctx.getAs<ByteArray>("pay")),
                    )
                }
            }
        }
    }
}
