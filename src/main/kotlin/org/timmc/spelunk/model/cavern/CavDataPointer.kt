package org.timmc.spelunk.model.cavern

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.timmc.spelunk.BytesSized
import org.timmc.spelunk.MapContext
import org.timmc.spelunk.SodiumSecretStreamKey
import org.timmc.spelunk.MsgPackable
import org.timmc.spelunk.MsgUnpackable
import org.timmc.spelunk.Utils

/**
 * Pointer to a published file, indicating how to find it and how to read it.
 *
 * This pointer is used for posts and comments, but not for catalogs.
 *
 * Data objects are hashed for integrity, rather than signed, and have a hidden
 * ID that changes if the object's content changes.
 */
data class CavDataPointer(
    /**
     * Target object's hidden ID (used in constructing path). If the hidden ID
     * has not changed, then the object can be assumed to have not changed.
     * This value should be unguessable to an attacker and not derived in any
     * way from the content or metadata of the object.
     */
    val hiddenId: String,
    /**
     * A symmetric key for decrypting the target file.
     *
     * TODO: use null bytes when data has already been downloaded.
     */
    val symmetricKey: SodiumSecretStreamKey,
    /**
     * Hash of the ciphertext.
     *
     * TODO: Use null bytes when data has already been downloaded.
     */
    val cipherHash: CiphertextDigest,
): MsgPackable {
    override fun serialize(): Any {
        return mapOf(
            "hid" to hiddenId,
            "key" to symmetricKey.sharedBytes,
            "int" to cipherHash.sharedBytes,
        )
    }

    companion object: MsgUnpackable<CavDataPointer> {
        override fun deserialize(ctx: MapContext): CavDataPointer {
            return CavDataPointer(
                hiddenId = ctx.getAs<String>("hid"),
                symmetricKey = SodiumSecretStreamKey(ctx.getAs<ByteArray>("key")),
                cipherHash = CiphertextDigest(ctx.getAs<ByteArray>("int")),
            )
        }
    }
}

/**
 * The BLAKE2b 16-byte hash of an object's ciphertext, used in pointer
 * integrity.
 */
class CiphertextDigest(bytes: ByteArray): BytesSized(BYTES_LEN, bytes) {
    companion object {
        const val BYTES_LEN = 16

        val hexJsonAdapter = object {
            @ToJson fun toJson(data: CiphertextDigest) = data.asHex()
            @FromJson fun fromJson(hex: String) = CiphertextDigest(Utils.hexDecode(hex))
        }
    }
}
