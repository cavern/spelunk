package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.MapContext
import org.timmc.spelunk.MsgPackable
import org.timmc.spelunk.MsgUnpackable
import org.timmc.spelunk.SodiumBoxPublicKey
import org.timmc.spelunk.SodiumSigningPublicKey
import org.timmc.spelunk.model.CatalogFinderEntry

/**
 * The common starting place for all apps accessing a published journal.
 *
 * This represents the journal author's identity within Cavern, as well as
 * assigning external (private) identities to quaints.
 */
data class CavIdentity(
    /**
     * Spec in use by the published journal.
     * See [org.timmc.spelunk.Constants.CAVERN_SPEC].
     */
    val spec: String,
    /**
     * The author's main public key -- used only for signing. This is the
     * linchpin of the author's identity.
     */
    val sigKey: SodiumSigningPublicKey,
    /**
     * Additional identity components, under signature.
     * See [readPublicKey].
     */
    val idSigned: CavSignedData<CavIdCurrent>,
    /**
     * The name this person wants to be known by.
     */
    val name: String? = null,
    /**
     * Lookup list for recipient to find their catalog file.
     *
     * Each entry is a pointer encrypted to one recipient's public key. When
     * decrypted, it reveals how to find and read a catalog file.
     */
    val catalogFinder: List<CatalogFinderEntry>,
): MsgPackable {
    /**
     * Read the public keys of this identity, verifying that the enc key is
     * signed by the sig key.
     */
    fun readPublicKey(): CavernPublicKey {
        val idPayload = idSigned.verify(
            CavIdCurrent.TOPIC_OWN_CURRENT_IDENTITY,
            sigKey,
            CavIdCurrent.Companion::fromMsgpack
        )

        return CavernPublicKey(
            sigPublicKey = sigKey,
            boxPublicKey = idPayload.boxKey,
        )
    }

    override fun serialize(): Any {
        return mapOf(
            "spec" to spec,
            "sig_key" to sigKey.sharedBytes,
            "id_signed" to idSigned.serialize(),
            "name" to name,
            "catalog_finder" to catalogFinder.map(CatalogFinderEntry::sharedBytes),
        ).filterValues { it != null }
    }

    companion object: MsgUnpackable<CavIdentity> {
        override fun deserialize(ctx: MapContext): CavIdentity {
            return CavIdentity(
                spec = ctx.getAs<String>("spec"),
                sigKey = SodiumSigningPublicKey(ctx.getAs<ByteArray>("sig_key")),
                idSigned = CavSignedData.unpackableFor<CavIdCurrent>().deserialize(ctx.getMap("id_signed")),
                name = ctx.getOptional("name")?.cast<String>(),
                catalogFinder = ctx.getList("catalog_finder").items().map {
                    CatalogFinderEntry(it.cast<ByteArray>())
                },
            )
        }
    }
}

/**
 * Author's current cryptographic and network identity.
 */
data class CavIdCurrent(
    /** Author's public key for encryption. */
    val boxKey: SodiumBoxPublicKey,
    /** Author's primary journal location. */
    val mainUrl: String?,
): MsgPackable {
    override fun serialize(): Any {
        return mapOf<String, Any?>(
            "enc_key" to boxKey.sharedBytes,
            "main_url" to mainUrl,
        ).filterValues { it != null }
    }

    companion object: MsgUnpackable<CavIdCurrent> {
        override fun deserialize(ctx: MapContext): CavIdCurrent {
            return CavIdCurrent(
                boxKey = SodiumBoxPublicKey(ctx.getAs<ByteArray>("enc_key")),
                mainUrl = ctx.getOptional("main_url")?.cast<String>(),
            )
        }

        /**
         * Topic to use when the sig key is asserting that the [CavIdCurrent]
         * payload is the current identity information (as of the timestamp).
         */
        const val TOPIC_OWN_CURRENT_IDENTITY = "cavern-own-current-identity"
    }
}
