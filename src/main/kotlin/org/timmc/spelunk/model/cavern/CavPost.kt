package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.MapContext
import org.timmc.spelunk.MsgPackable
import org.timmc.spelunk.MsgUnpackable

/**
 * A Cavern post, including pointers to any attachments.
 */
data class CavPost(
    /**
     * The stable ID of the post. This is preserved even if the post is edited,
     * migrated to another journal, or published to a different location.
     * It is unique within the journal. May not be filesystem-safe.
     */
    val id: String,
    /**
     * The type of post, e.g. "journal". Apps must ignore posts with an
     * unrecognized type value, as this is an extension mechanism.
     */
    val type: String,

    // In future iterations of the protocol, or extensions by post type, many of
    // the following might be optional for some types of posts. But all posts
    // will have an ID and a type.

    /**
     * Subject line of post.
     */
    val subject: String,
    /**
     * Body of post. This is plain text or text with markup.
     *
     * Interpretation depends on [bodyFormat], but if type is unrecognized then
     * body should be displayed as if it is `text/plain`.
     */
    val body: String,
    /**
     * MIME type of body.
     */
    val bodyFormat: String,
    /**
     * Optional list of free-text tags attached to the post. Order should be
     * retained.
     */
    val tags: List<String> = emptyList(),
    /**
     * Date when this post was published, as seconds since Unix epoch. This may
     * change, e.g. if a post is retracted and then re-published much later.
     * Negative values (dates before epoch) are permitted, as are dates in the
     * future.
     */
    val publishDate: Long,
    /**
     * Date when this post was last updated, as seconds since Unix epoch.
     * Indicates the time of most recent edit. As with publishDate, may be
     * negative or far-future.
     */
    val updatedDate: Long,
    /**
     * An indicator of how broadly this post is being shared. This can be used
     * to help me get a sense of how many people might see my comments on other
     * people's posts, and possibly how sensitive the author considers the
     * topic to be (although some people may only ever post to quaints.)
     */
    val visibilityType: CavVisibility,
    /** Files attached to this post */
    val attachments: List<CavAttachment>,
): MsgPackable {
    override fun serialize(): Any {
        return mapOf(
            "id" to id,
            "type" to type,
            "subject" to subject,
            "body" to body,
            "bodyFormat" to bodyFormat,
            "tags" to tags,
            "publishDate" to publishDate,
            "updatedDate" to updatedDate,
            "visibilityType" to visibilityType.code,
            "attachments" to attachments.map { it.serialize() }
        )
    }

    companion object: MsgUnpackable<CavPost> {
        override fun deserialize(ctx: MapContext): CavPost {
            return CavPost(
                id = ctx.getAs<String>("id"),
                type = ctx.getAs<String>("type"),
                subject = ctx.getAs<String>("subject"),
                body = ctx.getAs<String>("body"),
                bodyFormat = ctx.getAs<String>("bodyFormat"),
                tags = ctx.getOptional("tags")?.let { node ->
                    node.asList().items().map { it.cast<String>() }
                } ?: emptyList(),
                publishDate = ctx.getAs<Long>("publishDate"),
                updatedDate = ctx.getAs<Long>("updatedDate"),
                visibilityType = run {
                    val code = ctx.getAs<String>("visibilityType")
                    // TODO: Should this really throw if the code is not recognized?
                    CavVisibility.entries.first { it.code == code }
                },
                attachments = ctx.getList("attachments").items().map {
                    CavAttachment.deserialize(it.asMap())
                },
            )
        }
    }
}
