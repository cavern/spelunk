package org.timmc.spelunk.model.cavern

import org.timmc.spelunk.Blake2bHasher
import org.timmc.spelunk.BytesSized
import org.timmc.spelunk.Crypto
import org.timmc.spelunk.MapContext
import org.timmc.spelunk.MsgPackable
import org.timmc.spelunk.MsgUnpackable
import org.timmc.spelunk.SodiumBoxKeyPair
import org.timmc.spelunk.SodiumBoxPublicKey
import org.timmc.spelunk.friendlyClassName
import java.io.ByteArrayOutputStream
import java.util.Arrays

/**
 * Deniable authentication for Cavern data via pairwise MACs.
 *
 * One MAC is computed over the payload for each reader, using a shared
 * secret only the author and reader know. This secret is derived from their
 * X25519 keys. Readers check if any MAC matches the one they compute.
 */
class CavMultiMac<T>(
    /** Just used to make error messages more informative. */
    val type: Class<T>,
    /** Data that can be unpacked to the payload type. */
    val payload: ByteArray,
    /**
     * Multi-MAC data structure. This is a concatenation of MACs,
     * one per reader.
     */
    val macs: ByteArray,
): MsgPackable {
    /** Check if computed MAC is present in the macs array. */
    internal fun macPresent(computedMac: CavMac): Boolean {
        if (macs.size % MAC_LENGTH != 0)
            throw EAuthenticatedDataMacsUneven(
                type = type, allMacsSize = macs.size,
            )
        val findBytes = computedMac.sharedBytes
        for (offset in IntProgression.fromClosedRange(0, macs.size - 1, MAC_LENGTH)) {
            val isFound = Arrays.equals(
                findBytes, 0, MAC_LENGTH,
                macs, offset, offset + MAC_LENGTH,
            )
            if (isFound) {
                return true
            }
        }
        return false
    }

    /**
     * Try to authenticate the data as being sent by [senderKey] to one or more
     * of the [readerKeyPairs]. The [topic] value is specific to some data
     * structure and its intended use.
     *
     * Calls [unpacker] on the payload if authentication succeeds; otherwise,
     * throws [EAuthenticatedData].
     *
     * On success, returns pair of the unpacked payload and the public key that
     * succeeded in verifying a MAC. (This could be used to aid key rotation.)
     */
    fun authenticate(
        senderKey: SodiumBoxPublicKey, readerKeyPairs: List<SodiumBoxKeyPair>,
        topic: String, unpacker: (ByteArray) -> T,
    ): Pair<T, SodiumBoxPublicKey> {
        if (readerKeyPairs.isEmpty())
            throw AssertionError(
                "No reader keys supplied to CavMultiMac when trying to " +
                        "unpack a ${friendlyClassName(type)} payload."
            )

        val payloadDigest = PayloadDigest.of(payload)

        for (readerKeyPair in readerKeyPairs) {
            val sharedSecret = sharedSecretForReader(senderKey, readerKeyPair, topic)
            val computedMac = computeMac(payloadDigest, sharedSecret)

            if (macPresent(computedMac)) {
                return unpacker(payload) to readerKeyPair.publicKey
            }
        }

        throw EAuthenticatedDataNoMatchingMacs(type)
    }

    override fun serialize(): Any {
        return mapOf(
            "data" to payload,
            "macs" to macs,
        )
    }

    companion object {
        /** Create an object that can unpack a CavMultiMac from msgpack. */
        fun <T> unpackableFor(payloadClass: Class<T>): MsgUnpackable<CavMultiMac<T>> {
            return object : MsgUnpackable<CavMultiMac<T>> {
                override fun deserialize(ctx: MapContext): CavMultiMac<T> {
                    return CavMultiMac(
                        type = payloadClass,
                        payload = ctx.getAs<ByteArray>("data"),
                        macs = ctx.getAs<ByteArray>("macs"),
                    )
                }
            }
        }

        /**
         * MACs are truncated to this many bytes. (Set by Cavern spec.)
         */
        internal const val MAC_LENGTH = 16

        /**
         * Create the sender-reader shared secret using what the sender knows.
         *
         * [topic] is used for domain separation by payload type and the
         * purpose of the authentication.
         */
        internal fun sharedSecretForSender(
            senderKeyPair: SodiumBoxKeyPair, readerKey: SodiumBoxPublicKey,
            topic: String,
        ): IntegritySecret {
            // Point multiplication gives the base shared secret
            val base = Crypto.x25519SharedSecret(senderKeyPair.privateKey, readerKey)
            val digest = Blake2bHasher.unkeyed(IntegritySecret.BYTES_LEN)
                .update(base)
                // Explicitly include public keys, as done in libsodium
                .update(senderKeyPair.publicKey.sharedBytes).update(readerKey.sharedBytes)
                // Include topic for domain separation
                .update(topic.encodeToByteArray())
                .digest()
            return IntegritySecret(digest)
        }

        /**
         * Create the sender-reader shared secret using what the reader knows.
         *
         * This is the reader's counterpart to [sharedSecretForSender].
         */
        internal fun sharedSecretForReader(
            senderKey: SodiumBoxPublicKey, readerKeyPair: SodiumBoxKeyPair,
            topic: String,
        ): IntegritySecret {
            val base = Crypto.x25519SharedSecret(readerKeyPair.privateKey, senderKey)
            val digest = Blake2bHasher.unkeyed(IntegritySecret.BYTES_LEN)
                .update(base)
                .update(senderKey.sharedBytes).update(readerKeyPair.publicKey.sharedBytes)
                .update(topic.encodeToByteArray())
                .digest()
            return IntegritySecret(digest)
        }

        /**
         * Compute a MAC over the payload given the shared secret created by
         * [sharedSecretForSender] or [sharedSecretForReader].
         */
        internal fun computeMac(
            payloadDigest: PayloadDigest, sharedSecret: IntegritySecret,
        ): CavMac {
            return CavMac(Crypto.macCreate(
                data = payloadDigest.sharedBytes, key = sharedSecret.sharedBytes, outSize = MAC_LENGTH,
            ))
        }

        /**
         * Create a [CavMultiMac].
         */
        fun <T> makeAuthenticatedData(
            type: Class<T>, payload: ByteArray, senderKeyPair: SodiumBoxKeyPair,
            readerKeys: List<SodiumBoxPublicKey>, topic: String,
        ): CavMultiMac<T> {
            val payloadDigest = PayloadDigest.of(payload)

            val macs = ByteArrayOutputStream()
            for (readerKey in readerKeys) {
                val sharedSecret = sharedSecretForSender(senderKeyPair, readerKey, topic)
                val mac = computeMac(payloadDigest, sharedSecret)
                macs.writeBytes(mac.sharedBytes)
            }

            return CavMultiMac(
                type = type, payload = payload, macs = macs.toByteArray(),
            )
        }
    }
}

/** Shared secret between author and reader, used to compute MACs. */
internal class IntegritySecret(bytes: ByteArray): BytesSized(BYTES_LEN, bytes) {
    companion object {
        const val BYTES_LEN = 16
    }
}

/**
 * Blake2b digest of a payload to be authenticated.
 */
class PayloadDigest(bytes: ByteArray): BytesSized(BYTES_LEN, bytes) {
    companion object {
        const val BYTES_LEN = 16

        fun of(payload: ByteArray): PayloadDigest {
            return PayloadDigest(
                Blake2bHasher.unkeyed(BYTES_LEN).digest(payload)
            )
        }
    }
}

/**
 * A MAC of the appropriate length for [CavMultiMac].
 */
class CavMac(bytes: ByteArray): BytesSized(CavMultiMac.MAC_LENGTH, bytes) {
    /**
     * Safe equality check between two MACs.
     *
     * Timing independence doesn't matter so much for a desktop app using an
     * asynchronous protocol, but why not.
     */
    fun matches(otherMac: CavMac): Boolean {
        return Crypto.macVerify(sharedBytes, otherMac.sharedBytes)
    }
}

/**
 * Some kind of error when dealing with authenticated data.
 */
abstract class EAuthenticatedData(
    type: Class<*>, submessage: String
): Exception("Could not authenticate ${friendlyClassName(type)} data; $submessage")

class EAuthenticatedDataMacsUneven(
    val type: Class<*>, val allMacsSize: Int,
): EAuthenticatedData(
    type,
    "MACs section was $allMacsSize bytes long, which is not a multiple of " +
            "MAC length (${CavMultiMac.MAC_LENGTH})."
)

/**
 * None of the MACs matched. This might be retriable if the
 * sender's key has recently changed.
 */
class EAuthenticatedDataNoMatchingMacs(
    val type: Class<*>,
): EAuthenticatedData(
    type,
    "none of the MACs matched the ones we computed."
)
