package org.timmc.spelunk

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.timmc.spelunk.compat.StateUpgrade
import org.timmc.spelunk.gui.Bus
import org.timmc.spelunk.model.AddressBook
import org.timmc.spelunk.model.Config
import org.timmc.spelunk.model.ConfigPatch
import org.timmc.spelunk.model.Contact
import org.timmc.spelunk.model.ContactPatch
import org.timmc.spelunk.model.JournalPost
import org.timmc.spelunk.model.Keypair
import org.timmc.spelunk.model.Keyring
import org.timmc.spelunk.model.PublishLocationConfig
import org.timmc.spelunk.model.Repo
import org.timmc.spelunk.model.Visibility
import org.timmc.spelunk.model.cavern.CavCatalogPointer
import java.nio.file.Path
import java.util.concurrent.locks.ReentrantReadWriteLock

/**
 * Top-level bundle of stateful singletons and models with coordinated updates.
 *
 * This unfortunately conflates access to the main DB with top-level state
 * management.
 */
class State private constructor (
    /** The root path of the repository. */
    val root: Path,
    /**
     * Whether or not to permit writes or filesystem modifications in the
     * repository.
     */
    val allowWrite: Boolean
) {
    companion object {
        // =============== //
        // Factory methods //
        // =============== //

        /**
         * Create State object, returning it with a lockfile; it's your
         * responsibility to make *sure* that [LockfileResource.close] is called
         * on the lockfile resource before the program exits. It is recommended
         * to do this via:
         *
         * ```
         * val (lock, state) = State.getWriteableWithLock(rootPath)
         * lock.use { ...state... }
         * ```
         *
         * May throw [LockfileExistsErr].
         */
        fun getWriteableWithLock(root: Path): Pair<LockfileResource, State> {
            // Create lockfile before state object, just in case the latter does
            // any kind of initialization.
            val lock = Lockfile.createLockFile(Repo.lockfileLocation(root))
            try {
                val state = State(root, true)
                return lock to state
            } catch (t: Throwable) {
                lock.close()
                throw t
            }
        }

        /**
         * Get a State instance for this path, and promise not to try writing to
         * it; this is not exclusive access, so the repo may be changed out from
         * under the holder by some other Spelunk process that has acquired a
         * lock.
         */
        fun getReadOnly(root: Path): State {
            return State(root, false)
        }


        // ================= //
        // Repository format //
        // ================= //

        /** Scenarios for whether a repo is usable by a given version of Spelunk. */
        sealed class RepoUsability {
            /** Not an initialized repo, can't use at all. */
            data object Uninitialized : RepoUsability()

            /** The version pair is not understood; possibly a future version. */
            class Unknown(val version: Version) : RepoUsability()

            /** This is an old version, and can't be used at all unless the repo is upgraded. */
            class UpgradeRequired(val upgrader: Upgrader<*>) : RepoUsability()

            /** This is the current version. */
            data object Usable : RepoUsability()
        }

        /**
         * Major/minor version pair.
         */
        data class Version(val major: Long, val minor: Long) : Comparable<Version> {
            override fun toString(): String = "$major.$minor"

            override fun compareTo(other: Version): Int {
                val majorDiff = this.major.compareTo(other.major)
                return if (majorDiff == 0) {
                    this.minor.compareTo(other.minor)
                } else {
                    majorDiff
                }
            }
        }

        /**
         * The most recent version of the repository format -- the one that this
         * instance of Spelunk knows how to use directly.
         *
         * This needs to be coordinated with [StateUpgrade.formatUpgraders].
         */
        val latestVersion = Version(17, 0)

        /**
         * Run through all upgrades to bring repository to a format the current
         * codebase can fully understand, or die trying.
         *
         * The return value should be used by the calling code in place of the
         * passed state object.
         */
        fun repositoryFullUpgrade(originalState: State): State {
            val seenVersions = mutableSetOf<Version>()
            // Each upgrade returns a new State
            var currentState = originalState
            // Loop through upgrades until we reach Current (or something breaks)
            while (true) {
                when (val usability = currentState.checkUsability()) {
                    is RepoUsability.Uninitialized -> {
                        throw SpelunkExit("Cannot run upgrades on uninitialized repository")
                    }
                    is RepoUsability.Unknown -> {
                        throw SpelunkExit("Cannot run upgrades on repository of unknown format version: ${usability.version}")
                    }
                    is RepoUsability.UpgradeRequired -> {
                        // Unit tests should prevent this from ever happening,
                        // but while(true) makes me nervous.
                        if (!seenVersions.add(usability.upgrader.version)) {
                            throw SpelunkExit("Loop detected when running upgrades: $seenVersions + ${usability.upgrader.version}")
                        }
                        currentState = usability.upgrader.checkedUpgrade(currentState)
                    }
                    is RepoUsability.Usable -> {
                        return currentState
                    }
                }
            }
        }

        /**
         * Knows how to upgrade an earlier repo format of version [version] to
         * [targetVersion] when [checkedUpgrade] is called.
         *
         * P is an optional plan object of some sort, carrying information that
         * has been validated in the preflight phase into the upgrade phase for
         * execution.
         */
        abstract class Upgrader<P: Any?>(val version: Version, val targetVersion: Version) {
            /**
             * Upgrade the repository by one step. The state instance passed in
             * should not be used afterwards, but the return value can be used
             * instead.
             *
             * The old state instance must be writeable, having acquired a lock.
             */
            fun checkedUpgrade(state: State): State {
                // Ensure we're cleared for writing (i.e. have a lock)
                state.repo.requireWriteable("Upgrade repository format")
                // Double-check to ensure correct upgrader is being used
                val preRepoVersion = state.repositoryGetVersion()
                if (preRepoVersion != version) {
                    throw SpelunkExit("Upgrader for $version called on $preRepoVersion repo")
                }

                // This begins the mutex for upgraders, which will be ended
                // either by the preflight failing or the upgrade succeeding. If
                // the upgrade fails, the mutex will remain locked pending
                // manual intervention.
                state.repositoryBeginUpgrade()

                val plan = try {
                    preflightAndPlan(state)
                } catch (t: Throwable) {
                    state.repositoryCancelUpgrade()
                    throw t
                }

                upgradeByPlan(state, plan)

                // Atomically sets the new version and clears the upgrade flag
                // so that diagnosis and manual repair is not impeded in the
                // very unlikely circumstance that the program dies in between
                // the two.
                state.repositoryFinishUpgrade(targetVersion)

                // The idea here is that the old state value *might* have some
                // sort of cached data or references that are out of date for
                // the new format, and creating a new instance could avoid that.
                // (On the other hand, it's possible that the old state value
                // could leave some processes hanging around that would pose a
                // concurrency risk. It's not actually clear which is the more
                // plausible, pressing, or dangerous risk.)
                return State(state.root, state.allowWrite)
            }

            /**
             * Preflight checks for upgrader; throws if any preconditions
             * necessary for upgrade do not hold true. Must not actually make
             * changes to anything in repo.
             *
             * The following are already ensured by the [checkedUpgrade] function and
             * do not require special preflighting:
             *
             * - Lock acquired on repo
             * - Correct starting version
             * - [TRepository.isUpgrading] flag is false
             *
             * Can return a list of steps that the upgrader will execute.
             */
            protected abstract fun preflightAndPlan(state: State): P

            /**
             * Implementation of the actual upgrade process.
             *
             * May be informed by a plan produced by [preflightAndPlan].
             *
             * The following are already handled by [checkedUpgrade] function and
             * should not be done in this function:
             *
             * - Setting new version
             * - Checking and setting [TRepository.isUpgrading] flag
             */
            protected abstract fun upgradeByPlan(state: State, plan: P)
        }
    }

    val repo = Repo(root, allowWrite)

    // ================= //
    // Settings database //
    // ================= //

    /**
     * Information about repository itself. Single row.
     *
     * The repo major and minor version here also serve as the database's own
     * schema version.
     */
    object TRepository : Table("repository") {
        /**
         * Major version of Spelunk config & repo format, representing
         * backwards-incompatible changes. If this does not match
         * [latestVersion] then the DB and repo should not be used, except by
         * upgraders.
         */
        val versionMajor = long("version_major")
        /**
         * Minor version of Spelunk config & repo format, representing changes
         * that are theoretically backwards-compatible, at least for a read-only
         * State. (This is not yet implemented.)
         *
         * TODO: If [versionMajor] matches but [versionMinor] is greater than
         * Spelunk's own minor version, permit unknown fields in model files but
         * run in read-only mode (lest modifications to repo strip off
         * unrecognized fields).
         */
        val versionMinor = long("version_minor")
        /**
         * Flag to indicate if repository is in the middle of an upgrade. If
         * this is true on startup, Spelunk should die with an error message,
         * and not attempt to perform any upgrades.
         */
        val isUpgrading = bool("is_upgrading")

        // NOTE: Cannot add/remove/change fields without messing up the DB
        //  validity check for *older* versions of Spelunk, since they access
        //  this table via the Exposed API.
    }

    /** Config table, backing [Config]. Single row. */
    object TConfig : Table("config") {
        /** See [Config.defaultPostVisibility] */
        val defaultPostVisibility = text("default_post_visibility").default(Visibility.QOAQ1.name)
        /**
         * [PublishLocationConfig] document as encrypted JSON, or null if not
         * configured. Encryption is with DEK. May contain sensitive
         * information such as passwords or access keys.
         */
        val publishingEnc = blob("publishing_enc").nullable()
        /** See [Config.ownName] */
        val ownName = text("own_name").nullable()
        /**
         * Key used internally to generate shuffled post IDs. Not exposed in UI.
         * Decrypts with DEK to symmetric key bytes. Just a high entropy byte
         * array used to key a PRF.
         *
         * When this is changed, [postShuffleKeyVersion] must be incremented.
         */
        val postShuffleKeyEnc = blob("post_shuffle_key_enc").nullable()
        /**
         * Version number of [postShuffleKeyEnc].
         */
        val postShuffleKeyVersion = integer("post_shuffle_key_version").default(-1)
        /**
         * The Data-Encryption Key, or DEK, which is used to encrypt all other
         * encrypted information, and is itself encrypted by a key derived from
         * the master password.
         *
         * When decrypted, this is a libsodium secretstream key: [SodiumSecretStreamKey]
         */
        val dataKeyEnc = blob("data_key_enc")
        /** See [Config.torSocksProxy] */
        val torSocksProxy = text("tor_socks_proxy").nullable()
    }

    /** Address book, backing [Contact]. */
    object TContacts : Table("contact") {
        /** Autoincrementing ID; use negative values for placeholder. */
        val id = long("id").autoIncrement()
        val fetchUri = text("fetch_uri").nullable()
        val boxKey = blob("box_public_key").nullable()
        val separation = integer("separation")
        val handleReceived = text("handle_received").nullable()
        val handleOverride = text("handle_override").nullable()
        val sigKey = blob("sig_public_key").nullable()

        override val primaryKey = PrimaryKey(id)
    }

    /** Keyring for own keys, backing [Keyring] */
    object TKeyring : Table("self_keys") {
        /** Just an internal ID for the table -- not even read or written. */
        val id = long("id").autoIncrement()
        val sigPublicKey = blob("sig_public_key")
        val sigPrivateKeyEncrypted = blob("sig_private_key_enc")
        val boxPublicKey = blob("box_public_key")
        val boxPrivateKeyEncrypted = blob("box_private_key_enc")
        val isActive = bool("is_active")
        val createdTsMs = long("created_ms")
        val description = text("description")
        val selfCatalogHiddenId = text("self_catalog_hidden_id")
        val selfCatalogSymmetricKey = blob("self_catalog_symmetric_key")

        override val primaryKey = PrimaryKey(id)
    }

    // --------------------- //
    // Connection management //
    // --------------------- //

    private val db by lazy { database() }

    /**
     * Get a connection to the main DB in the repo. Opens read-only if
     * repo instance does not allow writes.
     */
    private fun database(): Database {
        return SQLite3.database(repo.settings.path, allowWrite)
    }

    /**
     * Run a transaction on the main database, returning the results.
     */
    private fun <R> tx(funk: Transaction.() -> R): R {
        return transaction(db, funk)
    }

    /**
     * Create initial Settings DB.
     */
    fun createDB(initialKeypair: Keypair) {
        repo.settings.requireWriteable("initialize state DB")

        // Write this first, since 1) the DB should be written last to signal
        // completion, and 2) keyringAddNewCurrent will want to read it from
        // disk.
        repo.privateKeyPassbytes.writeBytes(Crypto.sharedSecret())

        // This will be the Data-Encrypting Key -- encrypted with the master
        // password in the DB, and used to decrypt all other secrets.
        val dataKeyEncBytes = encryptWithMasterPassword(
            Crypto.symmetricStreamKey().sharedBytes
        )

        // Contains private keys and publishing passwords
        os.makeOwnerOnlyFile(repo.settings.path)

        tx {
            SchemaUtils.create(TRepository, TConfig, TContacts, TKeyring)

            // Set up the singleton row in the config table. Mostly default
            // values.
            TConfig.insert {
                it[dataKeyEnc] = dataKeyEncBytes.asBlob()
            }
            // Set up initial value
            postIdShuffleKeyRotate()

            keyringAddNewCurrent(initialKeypair)

            // Finally, fill out repo version. This should come last, since
            // its presence signals that init is complete. (Yes, it's in a
            // transaction. Belt and suspenders, though.)
            TRepository.insert {
                it[versionMajor] = latestVersion.major
                it[versionMinor] = latestVersion.minor
                it[isUpgrading] = false
            }
        }
    }

    // ================= //
    // Repository format //
    // ================= //

    /**
     * If there's a readable settings DB, return the major/minor version, else
     * throw.
     */
    private fun repositoryGetVersion(): Version {
        return tx {
            val rows = TRepository.selectAll().map {
                Version(it[TRepository.versionMajor], it[TRepository.versionMinor])
            }
            if (rows.size != 1)
                throw RuntimeException("""
                    Settings DB has wrong number of rows in repository table;
                    expected exactly 1 but found ${rows.size}
                """.trimIndent().wrap())
            rows.first()
        }
    }

    /**
     * Check whether this repository is in a format we understand, or whether it
     * is even a fully formed repository.
     */
    fun checkUsability(): RepoUsability {
        if (!repo.isInitComplete()) {
            return RepoUsability.Uninitialized
        }

        val repoVersion = repositoryGetVersion()
        if (latestVersion == repoVersion) {
            return RepoUsability.Usable
        }

        val upgrader = StateUpgrade.formatUpgraders.firstOrNull { it.version == repoVersion }
        return if (upgrader != null) {
            RepoUsability.UpgradeRequired(upgrader)
        } else {
            RepoUsability.Unknown(repoVersion)
        }
    }

    /**
     * Return true iff the upgrade flag is set to true in the state DB.
     */
    private fun repositoryIsUpgrading(): Boolean {
        return tx {
            TRepository.selectAll().map {
                it[TRepository.isUpgrading]
            }.first()
        }
    }

    /**
     * Set upgrade flag to true, or throw if it is already true.
     */
    private fun repositoryBeginUpgrade() {
        tx {
            if (repositoryIsUpgrading()) {
                throw SpelunkExit("Cannot start upgrade; upgrade already in progress (or previously failed)")
            } else {
                TRepository.update { it[isUpgrading] = true }
            }
        }
    }

    /**
     * Set upgrade flag to false, or throw if it is already false.
     */
    private fun repositoryCancelUpgrade() {
        tx {
            if (!repositoryIsUpgrading()) {
                throw SpelunkExit("Cannot mark upgrade cancelled; upgrade was not marked as started")
            } else {
                TRepository.update {
                    it[isUpgrading] = false
                }
            }
        }
    }

    /**
     * Atomically set new version and set upgrade flag to false. Throws if
     * upgrade flag was not set to true.
     */
    private fun repositoryFinishUpgrade(newVersion: Version) {
        tx {
            if (!repositoryIsUpgrading()) {
                throw SpelunkExit("Cannot mark upgrade finished; upgrade was not marked as started")
            } else {
                TRepository.update {
                    it[isUpgrading] = false
                    it[versionMajor] = newVersion.major
                    it[versionMinor] = newVersion.minor
                }
            }
        }
    }

    // =============== //
    // Config settings //
    // =============== //

    fun configRead(): Config {
        return tx {
            TConfig.selectOnly {
                val pubConfig = it[TConfig.publishingEnc]?.let { jsonEnc ->
                    val json = decrypt(jsonEnc.bytes).decodeToString()
                    Utils.json.adapter(PublishLocationConfig::class.java).fromJson(json)
                }

                Config(
                    ownName = it[TConfig.ownName],
                    defaultPostVisibility = Visibility.valueOf(it[TConfig.defaultPostVisibility]),
                    publishing = pubConfig,
                    torSocksProxy = it[TConfig.torSocksProxy],
                )
            }
        }
    }

    /**
     * Patch Config table with given changes, returning post-state.
     */
    fun configPatch(changes: ConfigPatch): Config {
        return tx {
            if (changes.hasChanges()) { // can't have empty update
                TConfig.update { r ->
                    changes.defaultPostVisibility.doIfPresent {
                        r[defaultPostVisibility] = it.name
                    }
                    changes.publishing.doIfPresent {
                        val jsonEnc: ByteArray? = it?.let {
                            val json = Utils.json.adapter(PublishLocationConfig::class.java).toJson(it)
                            encrypt(json.toByteArray())
                        }
                        r[publishingEnc] = jsonEnc?.asBlob()
                    }
                    changes.ownName.doIfPresent {
                        r[ownName] = it
                    }
                    changes.torSocksProxy.doIfPresent {
                        r[torSocksProxy] = it
                    }
                }
            }

            configRead()
        }.also { bus.configChange.broadcast(it) }
    }

    /**
     * Encrypt a post ID for public consumption.
     */
    fun postIdEncrypted(postId: Long): String {
        val (keyEnc, version) = tx {
            TConfig.selectOnly {
                it[TConfig.postShuffleKeyEnc]!!.bytes to it[TConfig.postShuffleKeyVersion]
            }
        }
        val key = decrypt(keyEnc)
        return JournalPost.encryptPostId(postId, key, version)
    }

    /**
     * Rotate post ID shuffle key.
     *
     * This is also used for initialization or if the encryption implementation
     * changes.
     */
    fun postIdShuffleKeyRotate() {
        return tx {
            val oldVer = TConfig.selectOnly(TConfig.postShuffleKeyVersion)

            TConfig.update {
                it[postShuffleKeyEnc] = encrypt(Crypto.sharedSecret()).asBlob()
                it[postShuffleKeyVersion] = oldVer + 1
            }
        }
    }

    // ============ //
    // Address Book //
    // ============ //

    /**
     * Read address book from DB.
     */
    fun addressBookRead(): AddressBook {
        return tx {
            AddressBook(TContacts.selectAll().map { row ->
                Contact(
                    id = row[TContacts.id],
                    fetchUri = row[TContacts.fetchUri],
                    sigKey = row[TContacts.sigKey]?.let { SodiumSigningPublicKey(it.bytes) },
                    boxKey = row[TContacts.boxKey]?.let { SodiumBoxPublicKey(it.bytes) },
                    separation = row[TContacts.separation],
                    handleReceived = row[TContacts.handleReceived],
                    handleOverride = row[TContacts.handleOverride]
                )
            })
        }
    }

    /**
     * Add a contact to the DB and return the resulting address book.
     *
     * The provided Contact should be discarded, as it does not have a valid ID.
     */
    fun addressBookAdd(provisionalContact: Contact): AddressBook {
        return tx {
            TContacts.insert {
                // it[id] is autoincrement
                it[fetchUri] = provisionalContact.fetchUri
                it[boxKey] = provisionalContact.boxKey?.sharedBytes?.asBlob()
                it[separation] = provisionalContact.separation
                it[handleReceived] = provisionalContact.handleReceived
                it[handleOverride] = provisionalContact.handleOverride
                it[sigKey] = provisionalContact.sigKey?.sharedBytes?.asBlob()
            }

            addressBookRead()
        }.also { bus.addressBookChanged.broadcast(it) }
    }

    /**
     * Modify a contact in the database.
     */
    fun addressBookPatchContact(existingContactId: Long, patch: ContactPatch): AddressBook {
        return tx {
            if (patch.hasChanges()) { // can't have empty update
                TContacts.update({ TContacts.id eq existingContactId }) { row ->
                    patch.fetchUri.doIfPresent { row[fetchUri] = it }
                    patch.sigKey.doIfPresent { row[sigKey] = it?.sharedBytes?.asBlob() }
                    patch.boxKey.doIfPresent { row[boxKey] = it?.sharedBytes?.asBlob() }
                    patch.separation.doIfPresent { row[separation] = it }
                    patch.handleReceived.doIfPresent { row[handleReceived] = it }
                    patch.handleOverride.doIfPresent { row[handleOverride] = it }
                }
            }

            addressBookRead()
        }.also { bus.addressBookChanged.broadcast(it) }
    }

    /**
     * Delete a contact from the database, by ID.
     */
    fun addressBookRemoveContact(toDelete: Contact): AddressBook {
        return tx {
            TContacts.deleteWhere { id eq toDelete.id }

            addressBookRead()
        }.also { bus.addressBookChanged.broadcast(it) }
    }

    // ======= //
    // Secrets //
    // ======= //

    /**
     * Get the master password, as bytes.
     */
    private fun secretsGetMasterPassword(): ByteArray {
        // For now, use hardcoded master password on disk; later, provide
        // pathway for user to set a master password. (This should be
        // accompanied by a key rotation.)
        return repo.privateKeyPassbytes.readBytes()
    }

    /**
     * Encrypt some bytes with the master password and return encrypted bytes.
     *
     * Should just be used for storing DEK.
     */
    internal fun encryptWithMasterPassword(data: ByteArray): ByteArray {
        return Crypto.encryptWithPassword(
            plainData = data,
            password = secretsGetMasterPassword()
        )
    }

    /**
     * Decrypt with the master password and return data bytes.
     *
     * Should just be used for retrieving DEK.
     */
    internal fun decryptWithMasterPassword(encrypted: ByteArray): ByteArray {
        return Crypto.decryptWithPassword(
            cryptData = encrypted,
            password = secretsGetMasterPassword()
        )
    }

    /**
     * Data Encryption Key, or DEK, decrypted on first use. This assumes the
     * DEK never changes at runtime, which at this time is true (and should
     * generally be true.)
     */
    private val dataKey: SodiumSecretStreamKey by lazy {
        val enc = tx { TConfig.selectOnly(TConfig.dataKeyEnc).bytes }
        SodiumSecretStreamKey(decryptWithMasterPassword(enc))
    }

    /**
     * Encrypt some bytes with the DEK.
     */
    internal fun encrypt(data: ByteArray): ByteArray {
        return Crypto.symmetricEncrypt(data, dataKey).first
    }

    /**
     * Decrypt with the DEK and return data bytes.
     */
    internal fun decrypt(encrypted: ByteArray): ByteArray {
        return Crypto.symmetricDecrypt(encrypted, dataKey, null)
    }

    // ======= //
    // Keyring //
    // ======= //

    /**
     * Read my own keyring from the DB. Throws if cannot identify current key.
     */
    fun keyringRead(): Keyring {
        val keypairs = tx {
            TKeyring.selectAll().map { row ->
                Keypair(
                    sigKeys = SodiumSigningKeyPair(
                        SodiumSigningPublicKey(row[TKeyring.sigPublicKey].bytes),
                        SodiumSigningPrivateKey(decrypt(row[TKeyring.sigPrivateKeyEncrypted].bytes))
                    ),
                    boxKeys = SodiumBoxKeyPair(
                        SodiumBoxPublicKey(row[TKeyring.boxPublicKey].bytes),
                        SodiumBoxPrivateKey(decrypt(row[TKeyring.boxPrivateKeyEncrypted].bytes))
                    ),
                    isActive = row[TKeyring.isActive],
                    createdTsMs = row[TKeyring.createdTsMs],
                    description = row[TKeyring.description],
                    selfCatalogPointer = CavCatalogPointer(
                        hiddenId = row[TKeyring.selfCatalogHiddenId],
                        symmetricKey = SodiumSecretStreamKey(row[TKeyring.selfCatalogSymmetricKey].bytes)
                    ),
                )
            }
        }
        val byActive = keypairs.groupBy { it.isActive }
        val active = (byActive[true] ?: emptyList())
            .sortedByDescending { it.createdTsMs }
        if (active.isEmpty()) {
            // TODO Auto-repair? Would be dodgy for a read operation.
            throw RuntimeException("No active keys in keyring")
        }
        // TODO Warn on more than one active key?
        return Keyring(
            current = active.first(),
            inactiveKeys = active.subList(1, active.size)
                .plus(byActive[false] ?: emptyList())
                .sortedBy { it.createdTsMs }
        )
    }

    /**
     * Add this new key to the database, setting it to be the only active key.
     * ([Keypair.isActive] on the input is ignored.)
     *
     * Returns the new keyring; the key given as input should be discarded and
     * read back out of the keyring.
     */
    fun keyringAddNewCurrent(newCurrent: Keypair): Keyring {
        return tx {
            TKeyring.update { it[isActive] = false }
            TKeyring.insert {
                it[sigPublicKey] = newCurrent.sigKeys.publicKey.sharedBytes.asBlob()
                it[sigPrivateKeyEncrypted] = encrypt(newCurrent.sigKeys.privateKey.sharedBytes).asBlob()
                it[boxPublicKey] = newCurrent.boxKeys.publicKey.sharedBytes.asBlob()
                it[boxPrivateKeyEncrypted] = encrypt(newCurrent.boxKeys.privateKey.sharedBytes).asBlob()
                it[isActive] = true
                it[createdTsMs] = newCurrent.createdTsMs
                it[description] = newCurrent.description
                it[selfCatalogHiddenId] = newCurrent.selfCatalogPointer.hiddenId
                it[selfCatalogSymmetricKey] = newCurrent.selfCatalogPointer.symmetricKey.sharedBytes.asBlob()
            }

            keyringRead()
        }
    }

    // ===== //
    // Posts //
    // ===== //

    /** Read/write lock cache for posts. Managed by [postLocker]. */
    private val postLockerCache = mutableMapOf<String, ReentrantReadWriteLock>()

    /**
     * Get the read/write lock for a post by ID.
     */
    fun postLocker(postId: String): ReentrantReadWriteLock {
        return synchronized(postLockerCache) {
            postLockerCache.getOrPut(postId) { ReentrantReadWriteLock(true) }
        }
    }

    // ======= //
    // Workers //
    // ======= //

    val bus by lazy { Bus() }

    /** Get or instantiate the Publishing worker. */
    val publishing by lazy { Publishing(this) }

    /** Get or instantiate the Fetching worker. */
    val fetching by lazy { Fetching(this) }
}

/**
 * Return the single row value for this column in a singleton table.
 */
fun <R> Table.selectOnly(column: Column<R>): R {
    return selectOnly { it[column] }
}

/**
 * Return value from the single row in a singleton table.
 */
fun <R> Table.selectOnly(block: (ResultRow) -> R): R {
    return selectAll().limit(1).map(block).first()
}

/**
 * Wrap a ByteArray in an ExposedBlob.
 *
 * This is just to support null-short-circuiting call chains by converting a
 * constructor into a method call.
 */
fun ByteArray.asBlob(): ExposedBlob {
    return ExposedBlob(this)
}
